"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var index_1 = require("app-veski/index");
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(index_1.VeskiModule)
    .catch(function (err) {
    console.log("Bootstrap error");
    console.log(err);
});
//# sourceMappingURL=main-jit.js.map