"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_1 = require("@angular/core");
var index_1 = require("app-veski/index");
var environment_1 = require("./environments/environment");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(index_1.VeskiModule)
    .catch(function (err) {
    console.log("Bootstrap error");
    console.log(err);
});
//# sourceMappingURL=main.js.map