var signupForm;

function signupSuccess(response) {
    window.location.replace('/signup-success');
}

function signup(event) {
    event.preventDefault();
    signupForm.submit();
    
    if (signupForm.hasErrors()) {
        return false;
    }

    $.ajax({
        type: "POST",
        url: "auth/signup",
        data: JSON.stringify(signupForm.getValues()),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: signupSuccess,
        error: function(errMsg) {
            var response = errMsg.responseJSON;
            var data = response.data;
            for (var key in data) {
                if (!data.hasOwnProperty(key)) {
                    continue;
                }

                signupForm.serverError(key, data[key].message);
            }
            signupForm.render();
        }
    });

    return false;
}

$(document).ready(function() {
    signupForm = new Form(
        ['name', 'surname', 'email', 'password', 'agreement'],
        {
            name: ['required', 'maxLength:128'],
            surname: ['required', 'maxLength:128'],
            email: ['required', 'maxLength:128', 'email'],
            password: ['required', 'minLength:6', 'maxLength:40'],
            agreement: ['checked']
        });
});
