var rresetPsswordForm;

function resetPasswordSuccess(response) {
    window.location.replace('/reset-password-success');
}

function resetPassword(event) {
    event.preventDefault();
    resetPsswordForm.submit();
    
    if (resetPsswordForm.hasErrors()) {
        return false;
    }

    $.ajax({
        type: "POST",
        url: "user/reset_password",
        data: JSON.stringify(resetPsswordForm.getValues()),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: resetPasswordSuccess,
        error: function(errMsg) {
            var response = errMsg.responseJSON;
            var data = response.data;
            for (var key in data) {
                if (!data.hasOwnProperty(key)) {
                    continue;
                }

                resetPsswordForm.serverError(key, data[key].message);
            }
            resetPsswordForm.render();
        }
    });

    return false;
}

$(document).ready(function() {
    resetPsswordForm = new Form(
        ['email'],
        {
            email: ['required', 'email'],
        });
});
