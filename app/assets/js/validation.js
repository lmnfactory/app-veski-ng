var Validation = function() {
    this.rules = {};
}

Validation.prototype.addRule = function(key, rule) {
    this.rules[key] = rule;
};

Validation.prototype.applyRule = function(data, value, ruleName, args) {
    if (!this.rules[ruleName]) {
        console.log("Rule does not exists '" + ruleName + "'");
        return false;
    }

    if (!this.rules[ruleName](data, value, args)) {
        return ruleName;
    }
    return false;
};

Validation.prototype.validate = function(data, rules) {
    var errors = false;
    for (var key in data) {
        if (!data.hasOwnProperty(key) || !rules[key]) {
            continue;
        }
        var value = data[key];
        var applyRules = rules[key];

        for (var i = 0; i < applyRules.length; i++) {
            var r = applyRules[i];
            var split = r.split(":");
            var ruleNameTmp = split.splice(0,1);

            var error = this.applyRule(data, value, ruleNameTmp[0], split);
            if (error !== false) {
                if (errors === false) {
                    errors = {};
                }
                errors[key] = error;
                break;
            }
        }
    }

    return errors;
};