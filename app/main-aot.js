"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var app_veski_module_ngfactory_1 = require("./aot/module/app-veski/src/app-veski.module.ngfactory");
platform_browser_1.platformBrowser().bootstrapModuleFactory(app_veski_module_ngfactory_1.VeskiModuleNgFactory)
    .catch(function (err) {
    console.log("Bootstrap factory error");
    console.log(err);
});
//# sourceMappingURL=main-aot.js.map