import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CoreModule } from 'core/index';

import { WebsocketService } from './service/websocket.service';

@NgModule({
    imports: [ CommonModule, CoreModule ],
    declarations: [],
    providers: [ WebsocketService ],
    exports: []
})
export class WebsocketModule {

}
