import { Injectable } from '@angular/core';

import { EventHandler } from 'core/index';

@Injectable()
export class WebsocketService {

    private eventHandler: EventHandler;
    private port: number;
    private serverAddr: string;
    private secure: boolean;
    private socket: WebSocket;
    private message: any;
    private sendQueue: any[];

    constructor(eventHandler:  EventHandler) {
        this.eventHandler = eventHandler;
        this.port = 5510;
        this.serverAddr = "";
        this.secure = false;
        this.message = "";
        this.sendQueue = [];
        this.socket = null;
    }

    private getUrl(): string {
        let prefix: string = "ws";
        if (this.secure) {
            prefix = "wss";
        }
        return prefix + "://" + this.serverAddr + ":" + this.port;
    }

    private handleOpen(): void {
        for (let i: number = 0; i < this.sendQueue.length; i++) {
            this.send(this.sendQueue[i]);
        }
    }

    config(serverAddr: string, port: number = 5510, secure: boolean = false): void {
        this.serverAddr = serverAddr;
        this.port = port;
        if (secure === true) {
            this.secure = secure;
        } else {
            this.secure = false;
        }
    }

    getMessage(): any {
        return this.message;
    }

    connect(): void {
        if (this.socket != null) {
            this.socket.close();
        }

        this.socket = new WebSocket(this.getUrl());
        this.socket.onmessage = this.handleMessage.bind(this);
        this.socket.onerror = this.handleError.bind(this);
        this.socket.onopen = this.handleOpen.bind(this);
    }

    send(message: any): void {
        if (this.socket == null || this.socket == undefined) {
            console.log("No socket connection with server.");
            return ;
        } else if (this.socket.readyState === 0) {
            this.sendQueue.push(message);
        } else if (this.socket.readyState === 1){
            this.socket.send(message);
        }
    }

    handleMessage(message: any): void {
        this.message = message;
        this.eventHandler.fire('websocket.message', message);
    }

    handleError(error: any): void {
        console.log("WEBSOCKET ERR:");
        console.log(error);
    }
}
