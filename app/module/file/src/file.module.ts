import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule, ModelService } from 'core/index';

import { FileFactory } from './object/file-factory.object';

@NgModule({
    imports: [ CommonModule, CoreModule ],
    exports: [],
    declarations: [],
    providers: [],
})
export class FileModule {

    constructor(modelService: ModelService) {
        modelService.addFactory('file', new FileFactory(modelService));
    }
}
