import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { EventHandler } from 'core/index';
import { fadeHiddenAnimation } from 'animation/index';
import { Autocomplete, AutocompleteService } from 'form-ui/index';

import { NotificationService } from '../service/notification.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-mobile-notification-list',
    templateUrl: 'mobile-notification-list.component.html',
    styleUrls: ['mobile-notification-list.component.css'],
    animations: [ fadeHiddenAnimation ]
})
export class MobileNotificationListComponent implements OnInit, OnDestroy {

    @Input('lmnShow')
    public show: boolean;
    @Output('lmnClose')
    private closeEmitter: EventEmitter<any>;
    public autocomplete: Autocomplete;
    public notifications: any[];
    private newNotifications: number;
    private eventHandler: EventHandler;
    private notificationService: NotificationService;
    private events: any[];

    constructor(autocompleteService: AutocompleteService, eventHandler: EventHandler, notificationService: NotificationService) {
        this.notificationService = notificationService;
        this.eventHandler = eventHandler;
        this.closeEmitter = new EventEmitter();
        this.events = [];
        this.notifications = [];
        this.newNotifications = 0;
        this.autocomplete = autocompleteService.create([]);
        this.autocomplete.setSelected(-1);
        this.autocomplete.show();
    }

    private onLoad(notifications: any[]): void {
        this.notifications = notifications;
        this.newNotifications = 0;
        for (let i: number = 0; i < this.notifications.length; i++) {
            if (this.notifications[i].get('new')) {
                this.newNotifications++;
            }
        }
    }

    ngOnInit() {
        this.onLoad(this.notificationService.all());
        this.events.push(
            this.eventHandler.on('notification.load', this.onLoad.bind(this))
        );
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    hide(): void {
        this.show = false;
        this.closeEmitter.emit({});
    }

    onSelect(item: any): void {
        if (item == null) {
            this.notificationService.seenAll();
        } else {
            this.notificationService.seen({id: item.get('id')});
            item.execute();
            this.hide();
        }
    }
}
