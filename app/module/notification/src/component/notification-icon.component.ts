import { Component, OnInit, OnDestroy } from '@angular/core';

import { EventHandler } from 'core/index';
import { OnSignin, AuthenticationService } from 'account/index';
import { Autocomplete, AutocompleteService } from 'form-ui/index';

import { NotificationService } from '../service/notification.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-notification-icon',
    templateUrl: 'notification-icon.component.html',
    styleUrls: ['notification-icon.component.css']
})

export class NotificationIconComponent extends OnSignin implements OnInit {

    private events: any[];
    private newNotifications: number;
    public notifications: any[];
    private visible: boolean;
    public autocomplete: Autocomplete;
    private notificationService: NotificationService;

    constructor(autocompleteService: AutocompleteService, eventHandler: EventHandler, notificationService: NotificationService, authService: AuthenticationService) {
        super(eventHandler, authService);
        this.notificationService = notificationService;
        this.events = [];
        this.notifications = [];
        this.newNotifications = 0;
        this.autocomplete = autocompleteService.create([]);
        this.autocomplete.setSelected(-1);
        this.autocomplete.hide();
    }

    private onLoad(notifications: any[]): void {
        this.notifications = notifications;
        this.newNotifications = 0;
        for (let i: number = 0; i < this.notifications.length; i++) {
            if (this.notifications[i].get('new')) {
                this.newNotifications++;
            }
        }
    }

    ngOnInit() {
        super.ngOnInit();
        this.onLoad(this.notificationService.all());
        this.events.push(
            this.eventHandler.on('notification.load', this.onLoad.bind(this))
        );
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    onSignin(): void {
        this.notificationService.request({});
    }

    onSelect(item: any): void {
        if (item == null) {
            this.notificationService.seenAll();
        } else {
            this.notificationService.seen({id: item.get('id')});
            item.execute();
        }
    }

    getNewCount(): number {
        return this.newNotifications;
    }

    toggle(event: any): void {
        event.stopPropagation();
        this.autocomplete.toggle();
        if (this.autocomplete.isVisible()) {
            this.notificationService.clearNew();
        }
    }
}