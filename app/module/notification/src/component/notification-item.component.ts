import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-notification-item',
    templateUrl: 'notification-item.component.html',
    styleUrls: ['notification-item.component.css']
})

export class NotificationItemComponent implements OnInit {

    @Input('lmnItem')
    public item: any;
    @Input('lmnSelected')
    public selected: boolean;
    
    constructor() {
        
    }

    ngOnInit(): void {}

    ngOnDestroy(): void {}
}