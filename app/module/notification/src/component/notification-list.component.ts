import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { fadeHiddenAnimation } from 'animation/index';
import { Autocomplete } from 'form-ui/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-notification-list',
    templateUrl: 'notification-list.component.html',
    styleUrls: ['notification-list.component.css'],
    animations: [ fadeHiddenAnimation ],
    host: {
        '(document:click)': 'onOutsideClick($event)',
    },
})

export class NotificationListComponent implements OnInit {


    @Input('lmnAutocomplete')
    public autocomplete: Autocomplete;
    @Output('lmnClose')
    private closeEmitter: EventEmitter<any>;
    @Output('lmnSelect')
    private selectEmitter: EventEmitter<any>;
    @Input('lmnItems')
    public notifications: any[];
    

    constructor() {
        this.closeEmitter = new EventEmitter();
        this.selectEmitter = new EventEmitter();
    }

    ngOnInit(): void {}

    ngOnDestroy(): void {}

    ngOnChange(): void {
        this.autocomplete.setItems(this.notifications);
    }

    onOutsideClick(event: any): void {
        if (!this.autocomplete.isVisible()) {
            return;
        }
        event.stopPropagation();
        this.closeEmitter.emit(event);
    }

    onSelect(item: any, event: any): void {
        event.stopPropagation();
        this.selectEmitter.emit(item);
    }
}