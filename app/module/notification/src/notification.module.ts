import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";

import { CoreModule, ModelService } from 'core/index';
import { WebsocketModule, WebsocketService } from 'websocket/index';

import { NotificationFactory } from './object/notification-factory.object';
import { NotificationService } from './service/notification.service';
import { NotificationRouterService } from './service/notification-router.service';
import { NotificationController } from './object/notification-controller.object';
import { NotificationIconComponent } from './component/notification-icon.component';
import { NotificationListComponent } from './component/notification-list.component';
import { MobileNotificationListComponent } from './component/mobile-notification-list.component';
import { NotificationItemComponent } from './component/notification-item.component';

@NgModule({
    imports: [ CommonModule, CoreModule, FlexLayoutModule, WebsocketModule ],
    declarations: [ NotificationIconComponent, NotificationListComponent, NotificationItemComponent, MobileNotificationListComponent ],
    providers: [ NotificationService, NotificationRouterService, NotificationController, NotificationFactory ],
    exports: [ NotificationIconComponent, MobileNotificationListComponent ]
})

export class NotificationModule {

    constructor(modelService: ModelService, notificationFactory: NotificationFactory) {
        modelService.addFactory('notification', notificationFactory);
    }
}
