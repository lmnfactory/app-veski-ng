import { Injectable } from '@angular/core';

import { EventHandler } from 'core/index';
import { AuthenticationService } from 'account/index';
import { WebsocketService } from 'websocket/index';

@Injectable()
export class NotificationRouterService {

    private websocketService: WebsocketService;
    private authenticationService: AuthenticationService;
    private eventHandler: EventHandler;

    private routes: Map<string, any>;
    private defaultRoute: any;

    constructor(websocketService: WebsocketService, authenticationService: AuthenticationService, eventHandler: EventHandler) {
        this.websocketService = websocketService;
        this.eventHandler = eventHandler;
        this.authenticationService = authenticationService;
        this.routes = new Map();
        this.defaultRoute = null;
        this.eventHandler.on('websocket.message', this.onMessage.bind(this));
        this.eventHandler.on('auth.signin', this.onSignin.bind(this));
        this.eventHandler.on('connection.online', this.reconnect.bind(this));
    }

    public reconnect(): void {
        this.websocketService.connect();
        this.onSignin();
    }

    route(path: string, handler: any): void {
        this.routes.set(path, handler);
    }

    default(handler: any): void {
        this.defaultRoute = handler;
    }

    getHandler(path: string): any {
        if (this.routes.has(path)) {
            return this.routes.get(path);
        }

        if (this.defaultRoute != null) {
            return this.defaultRoute;
        }
        return null;
    }

    onSignin(data: any = {}): void {
        let jwt: string = this.authenticationService.getJwtToken();
        this.websocketService.send(jwt);
    }

    onMessage(message: MessageEvent): void {
        let jsonMessage = JSON.parse(message.data);
        let handler: any = this.getHandler(jsonMessage.route);
        if (handler == undefined || handler == null) {
            return ;
        }
        handler(jsonMessage.body);
    }
}
