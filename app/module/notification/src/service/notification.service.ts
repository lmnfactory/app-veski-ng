import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { Model, ModelService, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';

@Injectable()
export class NotificationService {

    private notifications: Model[];
    private modelService: ModelService;
    private eventHandler: EventHandler;
    private http: LmnHttp;

    constructor(modelService: ModelService, eventHandler: EventHandler, http: LmnHttp) {
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.http = http;
        this.notifications = [];
    }

    private notifyLoad(): void {
        this.eventHandler.fire('notification.load', this.notifications);
    }

    create(data: any): Model {
        return this.modelService.create('notification', data);
    }

    all(): Model[] {
        return this.notifications;
    }

    add(notification: Model): void {
        this.notifications.splice(0, 0, notification);
        this.notifyLoad();
    }

    request(data: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/notification/list', data);

        observable.subscribe((data: any) => {
            this.notifications = [];
            let list = data.data;
            for (let i: number = 0; i < list.length; i++) {
                let model: Model = this.create(list[i]);
                this.notifications.push(model);
            }
            this.notifyLoad();
        });

        return observable;
    }

    clearNew(): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/notification/clear_new', {});

        observable.subscribe((data: any) => {
            for (let i: number = 0; i < this.notifications.length ; i++) {
                this.notifications[i].set('new', false);
            }
            this.notifyLoad();
        });

        return observable;
    }

    seenAll(): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/notification/seen_all', {});

        observable.subscribe((data: any) => {
            for (let i: number = 0; i < this.notifications.length ; i++) {
                this.notifications[i].set('seen', true);
            }
            //this.notifyLoad();
        });

        return observable;
    }

    seen(requstData: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/notification/seen', requstData);

        observable.subscribe((data: any) => {
            for (let i: number = 0; i < this.notifications.length ; i++) {
                if (this.notifications[i].get('id') != requstData.id) {
                    continue;
                }
                this.notifications[i].set('seen', true);
            }
            //this.notifyLoad();
        });

        return observable;
    }
}