import { Injectable } from '@angular/core';

import { Model } from 'core/index';
import { BrowserNotificationService, BrowserNotificationBuilder } from 'browser-notification/index';

import { NotificationService } from '../service/notification.service';

@Injectable()
export class NotificationController {

    private notificationService: NotificationService;
    private browserNotificationService: BrowserNotificationService;

    constructor(browserNotificationService: BrowserNotificationService, notificationService: NotificationService) {
        this.browserNotificationService = browserNotificationService;
        this.notificationService = notificationService;
    }

    onNotification(notificationBody: any): void {
        let model: Model = this.notificationService.create(notificationBody);
        let notificationBuilder: BrowserNotificationBuilder = this.browserNotificationService.build()
            .title(model.get('title'))
            .body(model.get('text'));
        if (model.get('exec')) {
            notificationBuilder.onExec(model.get('exec'));
        }
        notificationBuilder.show();
        
        this.notificationService.add(model);
    }
}
