import { ModelService, BasicModel, Model } from 'core/index';

export class NotificationModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    execute(): void {
        this.get('exec').execute();
    }

    export(): any {
        return {};
    }
}
