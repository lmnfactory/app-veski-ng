import { Injectable } from '@angular/core';

import { ModelService, ModelFactory, Model } from 'core/index';

import { NotificationModel } from './notification.model';

@Injectable()
export class NotificationFactory implements ModelFactory {

    private modelService: ModelService;
    private afterCreateCallbacks: any;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
        this.afterCreateCallbacks = {};
    }

    private hasAfterCreate(source: string): boolean {
        return (this.afterCreateCallbacks[source]);
    }

    afterCreate(key: string, model: Model): void {
        if (!this.hasAfterCreate(key)) {
            return;
        }

        let callbacks: any[] = this.afterCreateCallbacks[key];
        for (let i: number = 0; i < callbacks.length; i++) {
            callbacks[i](model);
        }
    }

    addAfterCreate(source: string, callback: any): void {
        if (!this.afterCreateCallbacks[source]) {
            this.afterCreateCallbacks[source] = [];
        }
        this.afterCreateCallbacks[source].push(callback);
    }

    create(data: any): Model {
        let model: Model = new NotificationModel(this.modelService);
        model.fill(data);

        this.afterCreate(model.get('source'), model);

        return model;
    }
}
