import { NgModule, LOCALE_ID }      from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FlexLayoutModule } from "@angular/flex-layout";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2FileDropModule } from 'ng2-file-drop';

import { environment } from '../../../environments/environment';

import { CoreModule, CacheService, InitLoadingService, Model } from 'core/index';
import { MenuModule } from 'menu/index';
import { AccountModule, SignoutComponent, AuthenticationService, JWTRequestInterceptor, JWTResponseInterceptor, UserService } from 'account/index';
import { SubjectModule, SubjectViewComponent, SubjectRouterService, SubjectService } from 'subject/index';
import { LayoutModule, AppLayoutComponent, BodySideLayoutComponent } from 'layout/index';
import { HttpCommunicationModule, HttpInterceptor } from 'http-communication/index';
import { NotificationModule, NotificationRouterService, NotificationController } from 'notification/index';
import { WebsocketModule, WebsocketService } from 'websocket/index';
import { FacultyModule, FacultyCacheService } from 'faculty/index';
import { UniversityModule } from 'university/index';
import { DegreeModule } from 'degree/index';
import { ThreadModule, ThreadNotificationController, ViewStandaloneComponent, ThreadService } from 'thread/index';
import { ColorModule } from 'lmn-color/index';
import { CalendarModule } from 'calendar/index';
import { SearchModule, SearchService } from 'search/index';
import { TagModule } from 'tag/index';
import { FormUIModule } from 'form-ui/index';
import { HeaderModule } from 'header/index';
import { TimelineModule } from 'timeline/index';

import { VeskiMainComponent }  from './component/veski-main.component';
import { IndexComponent }  from './component/index.component';
import { TagViewComponent }  from './component/tag-view.component';
import { SettingsComponent }  from './component/settings.component';
import { HeaderFixedComponent }  from './component/header-fixed.component';
import { MobileNavComponent }  from './component/mobile-nav.component';
import { FooterComponent }  from './component/footer.component';

import { VeskiProvider }  from './service/veski-provider.service';

const appRoutes: Routes = [
    {
        path: 'tag/:slug',
        component: TagViewComponent
    },
    {
        path: 'thread/:threadPid',
        component: SubjectViewComponent
    },
    {
        path: 'thread/:threadPid/e/:entryId',
        component: SubjectViewComponent
    },
    {
        path: 'subject/:publicId',
        component: SubjectViewComponent
    },
    {
        path: 'subject/:publicId/:tab',
        component: SubjectViewComponent
    },
    {
        path: 'subject/:publicId/:tab/:eventId',
        component: SubjectViewComponent
    },
    {
        path: 'signout',
        component: SignoutComponent
    },
    {
        path: '',
        component: IndexComponent
    },
];

@NgModule({
  imports:      [ BrowserModule, CommonModule, FormsModule, RouterModule.forRoot(appRoutes), NgbModule.forRoot(), ThreadModule, HttpModule, Ng2FileDropModule, FormUIModule, CoreModule, MenuModule, FlexLayoutModule, AccountModule, CalendarModule, SubjectModule, LayoutModule, HttpCommunicationModule, WebsocketModule, NotificationModule, DegreeModule, UniversityModule, FacultyModule, ColorModule, SearchModule, TagModule, HeaderModule, TimelineModule ],
  declarations: [ VeskiMainComponent, IndexComponent, TagViewComponent, SettingsComponent, HeaderFixedComponent, MobileNavComponent, FooterComponent ],
  bootstrap:    [ VeskiMainComponent ],
  providers:    [ VeskiProvider, { provide: LOCALE_ID, useValue: "sk-SK" }, { provide: APP_BASE_HREF, useValue: '/apka'} ]
})
export class VeskiModule {
    constructor(httpInterceptor: HttpInterceptor, authService: AuthenticationService, websocketService: WebsocketService, notificationRouterService: NotificationRouterService, cacheService: CacheService, facultyService: FacultyCacheService, threadNotificationController: ThreadNotificationController, initLoadingService: InitLoadingService, searchService: SearchService, notificationController: NotificationController, subjectRouterService: SubjectRouterService, subjectService: SubjectService, threadService: ThreadService, userService: UserService) {
        //initLoadingService.start('signin');
        cacheService.refresh();
        authService.autosignin();
        websocketService.config(environment.websocket.ip, environment.websocket.port, environment.websocket.secure);

        let jwtRequestInterceptor: JWTRequestInterceptor = new JWTRequestInterceptor(authService);
        httpInterceptor.addRequest(jwtRequestInterceptor);

        let jwtResponseInterceptor: JWTResponseInterceptor = new JWTResponseInterceptor(authService);
        httpInterceptor.addResponse(jwtResponseInterceptor);

        notificationRouterService.route('thread.vote', threadNotificationController.onVote.bind(threadNotificationController));
        notificationRouterService.route('thread.detail', threadNotificationController.onDetail.bind(threadNotificationController));
        notificationRouterService.route('thread.create', threadNotificationController.onCreate.bind(threadNotificationController));
        notificationRouterService.route('notification', notificationController.onNotification.bind(notificationController));

        websocketService.connect();

        searchService.mapModel('tag', 'tag');

        subjectRouterService.route((service: SubjectRouterService) => {
            let params: any = service.getParams();
            if (params.id) {
                subjectService.requestPrototype({subjectId: params.id})
                    .subscribe((subject: Model) => {
                        service.setPublicId(subject.get('subjectprototype.public_id'));
                    })
            }
        });

        subjectRouterService.route((service: SubjectRouterService) => {
            let params: any = service.getParams();
            if (params.threadPid) {
                threadService.requestSubject({public_id: params.threadPid})
                    .subscribe((subject: Model) => {
                        service.setPublicId(subject.get('subjectprototype.public_id'));
                    })
            }
        });
    }
}
