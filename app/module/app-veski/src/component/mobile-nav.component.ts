import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { EventHandler } from 'core/index';
import { NotificationService } from 'notification/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-mobile-nav',
    templateUrl: 'mobile-nav.component.html',
    styleUrls: ['mobile-nav.component.css']
})

export class MobileNavComponent implements OnInit {

    @Output('lmnSelect')
    public selectEmitter: EventEmitter<any>;
    @Input('lmnSelected')
    public selected: string;

    private eventHandler: EventHandler;
    private notificationService: NotificationService;
    private newNotifications: number;
    private events: any[];

    constructor(eventHandler: EventHandler, notificationService: NotificationService) {
        this.eventHandler = eventHandler;
        this.notificationService = notificationService;
        this.selectEmitter = new EventEmitter();
        this.selected = "";
        this.events = [];
        this.newNotifications = 0;
    }

    private onLoad(notifications: any[]): void {
        this.newNotifications = 0;
        for (let i: number = 0; i < notifications.length; i++) {
            if (notifications[i].get('new')) {
                this.newNotifications++;
            }
        }
    }

    ngOnInit(): void {
        this.events.push(
            this.eventHandler.on('notification.load', this.onLoad.bind(this))
        );
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    getNewCount(): number {
        return this.newNotifications;
    }

    select(nav: string): void {
        if (nav == this.selected) {
            this.selected = "";
        } else {
            this.selected = nav;
        }
        if (this.selected == "notification") {
            this.clearNotifications();
        }
        this.selectEmitter.emit(this.selected);
    }

    clearNotifications(): void {
        this.notificationService.clearNew();
    }
}