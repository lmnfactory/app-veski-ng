import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { EventHandler } from 'core/index';
import { HeaderService } from 'header/index';
import { ColorpaletteService } from 'lmn-color/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-header-fixed',
    templateUrl: 'header-fixed.component.html',
    styleUrls: ['header-fixed.component.css']    
})

export class HeaderFixedComponent implements OnInit, OnDestroy {
    
    private headerService: HeaderService;
    private eventHandler: EventHandler;
    private colorpaletteService: ColorpaletteService;
    public title: string;
    public tabs: any[];
    private events: any[];

    constructor(headerService: HeaderService, eventHandler: EventHandler, colorpaletteService: ColorpaletteService) {
        this.headerService = headerService;
        this.eventHandler = eventHandler;
        this.colorpaletteService = colorpaletteService;
        this.title = "";
        this.tabs = [];
        this.events = [];
    }

    private onChange(key: string): void {
        this.title = this.headerService.get('title');
        this.tabs = this.headerService.get('tabs');
        if (!this.tabs) {
            this.tabs = [];
        }
    }

    ngOnInit(): void {
        this.events.push(
            this.eventHandler.on('header.change', this.onChange.bind(this))
        );
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    onTabClick(tab: any): void {
        for (let i: number = 0; i < this.tabs.length; i++) {
            this.tabs[i].deselect();
        }
        if (!tab) {
            return;
        }
        tab.execute();
        tab.select();
    }

    getSubjectColor(): string {
        if (!this.headerService.get('subjectId')) {
            return "";
        }

        return this.colorpaletteService.getSubjectPaletteClass(this.headerService.get('subjectId'));
    }
}