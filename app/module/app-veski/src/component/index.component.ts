import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Model, EventHandler, EventToken, TabExec, InfiniteScrollService, LmnInfiniteScroll } from 'core/index';
import { AuthenticationService, OnSignin, UserService } from 'account/index';
import { MenuService } from 'menu/index';
import { fadeIfAnimation } from 'animation/index';
import { SubjectUserService } from 'subject/index';
import { HeaderService } from 'header/index';
import { ThreadAllService } from 'thread/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-index',
    templateUrl: 'index.component.html',
    styleUrls: ['index.component.css'],
    animations: [ fadeIfAnimation ]
})
export class IndexComponent extends OnSignin implements OnInit, OnDestroy {

    private menuService: MenuService;
    private route: ActivatedRoute;
    private headerService: HeaderService;
    private threadService: ThreadAllService;
    private infiniteScroll: LmnInfiniteScroll;
    private title: Title;
    private userService: UserService;
    @ViewChild('tabset')
    private tabset: any;

    private routeSubscription: any;
    private threadRefresh: EventToken;
    private subjectUserService: SubjectUserService;

    private events: any[];
    public subjects: any[];
    public allThreads: any[];    
    private message: string;
    private messages: string[];
    private rawMessage: string;

    constructor(menuService: MenuService, route: ActivatedRoute, eventHandler: EventHandler, subjectUserService: SubjectUserService, headerService: HeaderService, threadService: ThreadAllService, authService: AuthenticationService, infiniteScrollService: InfiniteScrollService, title: Title, userService: UserService) {
        super(eventHandler, authService);
        this.menuService = menuService;
        this.headerService = headerService;
        this.threadService = threadService;
        this.title = title;
        this.userService = userService;
        this.infiniteScroll = infiniteScrollService.create();
        this.route = route;
        this.events = [];
        this.subjects = [];
        this.allThreads = [];
        this.subjectUserService = subjectUserService;

        this.messages = [
            'Ahoj %NAME%, čo bolo dnes v škole? :)',
            'Vitaj %NAME%, ako to ide?',
            'Pekný deň, %NAME%.',
            '%NAME%, vitaj späť!',
            'Ach %NAME%, zas tá škola, čo? :/',
            'Čo nové, %NAME%?',
        ];
        this.message = "";
        this.rawMessage = "";
    }

    private onLoadThreads(): void {
        this.allThreads = this.threadService.all();
        this.infiniteScroll.resolveCount(this.allThreads.length);
    }

    private init(): void {
        this.threadService.request();
    }

    private loadMoreThreads(resolve: any): void {
        this.threadService.requestMore({
            until: this.allThreads[this.allThreads.length - 1].get('created_at')
        });
    }

    private prepareMessage(): void {
        let user: Model = this.userService.get();
        this.message = "Ahoj";
        if (user != null) {
            this.message = this.rawMessage.replace("%NAME%", user.get('name'));
        }
    }

    private pickMessage(): void {
        let randIndex: number = Math.floor(Math.random() * this.messages.length);
        this.rawMessage = this.messages[randIndex];
        this.prepareMessage();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.menuService.activate(null);
        this.subjects = this.subjectUserService.all();
        this.allThreads = this.threadService.all();

        let e = this.eventHandler.on('loading.end', (event: any) => {
            this.subjectUserService.request({});
        });
        this.events.push(e);

        e = this.eventHandler.on('subjectuser.load', (event: any) => {
            this.subjects = this.subjectUserService.all();
        });
        this.events.push(e);
        this.events.push(
            this.eventHandler.on('threadall.load', this.onLoadThreads.bind(this))
        );
        this.events.push(
            this.eventHandler.on('user.load', this.prepareMessage.bind(this))
        )

        this.title.setTitle("Veski");
        this.headerService.set('title', "Veski");
        this.headerService.set('subjectId', 0);
        this.headerService.set('tabs', [
            this.headerService.createTab('PREHĽAD', new TabExec(this.tabset, 'tab-main-1'), true),
            this.headerService.createTab('PREDMETY', new TabExec(this.tabset, 'tab-main-2')),
            this.headerService.createTab('PROFIL', new TabExec(this.tabset, 'tab-main-3')),
        ]);
        this.pickMessage();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        let i: number = 0;
        for(i = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    onSignin(): void {
        this.init();
    }

    onLoadMore(): void {
        this.infiniteScroll.load(this.loadMoreThreads.bind(this));
    }

    getWelcomeMessage(): string {
        return this.message;
    }
}
