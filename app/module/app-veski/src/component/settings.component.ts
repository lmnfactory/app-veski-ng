import { Component, OnInit } from '@angular/core';

import { BrowserNotificationService } from 'browser-notification/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-settings',
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.css']
})

export class SettingsComponent implements OnInit {

    private browserNotificationService: BrowserNotificationService;
    public enableBrowserNotification: boolean;

    constructor(browserNotificationService: BrowserNotificationService) {
        this.browserNotificationService = browserNotificationService;
    }

    ngOnInit() {
        this.enableBrowserNotification = this.browserNotificationService.hasPermission();
    }

    enable(): void {
        if (this.browserNotificationService.hasPermission()) {
            return;
        }

        let self: SettingsComponent = this;
        this.browserNotificationService.requestPermission()
            .then((permission: string) => {
                if (permission == "granted") {
                    self.enableBrowserNotification = true;
                }
            });
    }
}