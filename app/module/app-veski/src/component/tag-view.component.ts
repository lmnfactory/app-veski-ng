import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Model, Observer, EventHandler, EventToken, EmptyExec, InfiniteScrollService, LmnInfiniteScroll, Garbage, GarbageService } from 'core/index';
import { AuthenticationService, OnSignin } from 'account/index';
import { MenuService } from 'menu/index';
import { ThreadService } from 'thread/index';
import { fadeIfAnimation } from 'animation/index';
import { HeaderService } from 'header/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-tag-view',
    templateUrl: 'tag-view.component.html',
    styleUrls: ['tag-view.component.css'],
    animations: [ fadeIfAnimation ]
})

export class TagViewComponent extends OnSignin implements OnInit, OnDestroy {

    private route: ActivatedRoute;
    private threadService: ThreadService;
    private headerService: HeaderService;
    private infiniteScroll: LmnInfiniteScroll;
    private title: Title;
    private menuService: MenuService;

    private garbage: Garbage;
    public slug: string;
    public threads: Model[];
    private tag: any;

    constructor(menuService: MenuService, threadService: ThreadService, route: ActivatedRoute, eventHandler: EventHandler, authService: AuthenticationService, headerService: HeaderService, infiniteScrollService: InfiniteScrollService, title: Title, garbageService: GarbageService) {
        super(eventHandler, authService);
        this.threadService = threadService;
        this.headerService = headerService;
        this.route = route;
        this.title = title;
        this.menuService = menuService;
        this.garbage = garbageService.create();
        this.infiniteScroll = infiniteScrollService.create();
        this.slug = "";
        this.threads = [];
    }

    private load(): void {
        if (!this.slug || this.slug == "") {
            return;
        }
        this.garbage.subscription(
            this.threadService.requestForTag({slug: this.slug})
                .subscribe((threads: Model[]) => {
                    this.threads = threads;
                })
        );
    }

    private loadMoreThreads(resolve: any): void {
        this.garbage.subscription(
            this.threadService.requestForTag({
                slug: this.slug,
                until: this.threads[this.threads.length - 1].get('created_at')
            }).subscribe((threads: Model[]) => {
                for (let i: number = 0 ; i < threads.length; i++) {
                    this.threads.push(threads[i]);
                }
                resolve(threads.length > 0);
            })
        );
    }

    ngOnInit(): void {
        super.ngOnInit();
        
        this.menuService.activate(null);
        this.headerService.set('title', "#");
        this.headerService.set('subjectId', 0);
        this.headerService.set('tabs', [
            this.headerService.createTab('PRÍSPEVKY', new EmptyExec(), true),
        ]);

        this.garbage.subscription(
            this.route.params.subscribe((params: any) => {
                this.slug = params.slug;
                this.headerService.set('title', "#" + this.slug);
                this.title.setTitle("#" + this.slug)
                if (this.isSignIn()) {
                    this.load();
                }
            })
        );
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.garbage.destroy();
    }

    onSignin(): void {
        this.load();
    }

    onLoadMore(): void {
        this.infiniteScroll.load(this.loadMoreThreads.bind(this));
    }
}