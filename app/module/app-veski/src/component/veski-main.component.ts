declare let ga: Function;

import { Component, ElementRef, OnInit, OnDestroy, HostListener, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import {Router, NavigationEnd} from '@angular/router';

import { environment } from '../../../../environments/environment';

import { InitLoadingService, EventHandler, Garbage, GarbageService, Model } from 'core/index';
import { AuthenticationService, OnSignin } from 'account/index';
import { SubjectUserService } from 'subject/index';
import { FileDropService } from 'form-ui/index';
import { MenuService } from 'menu/index';
import { fadeHiddenAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'veski-main',
    templateUrl: 'veski-main.component.html',
    styleUrls: ['veski-main.component.css'],
    animations: [ fadeHiddenAnimation ]

})
export class VeskiMainComponent extends OnSignin implements OnInit, OnDestroy {

    private subjectUserService: SubjectUserService;
    public loading: InitLoadingService;
    private fileDropService: FileDropService;
    private menuService: MenuService;
    private renderer: Renderer2;
    private router: Router
    private events: any[];
    public hasMenu: boolean;
    private fileOverDelay: any;
    private dragCounter: number;
    private headerVisible: boolean;
    public mobileNav: string;
    private document: any;
    private garbage: Garbage;

    constructor(authService: AuthenticationService, eventHandler:EventHandler, subjectUserService: SubjectUserService, loadingService: InitLoadingService, fileDropService: FileDropService, menuService: MenuService, @Inject(DOCUMENT) document: any, renderer: Renderer2, router: Router, garbageService: GarbageService) {
        super(eventHandler, authService);
        this.subjectUserService = subjectUserService;
        this.loading = loadingService;
        this.fileDropService = fileDropService;
        this.menuService = menuService;
        this.renderer = renderer;
        this.router = router;
        this.document = document;
        this.garbage = garbageService.create();
        this.events = [];
        this.hasMenu = false;
        this.dragCounter = 0;
        this.headerVisible = false;
        this.mobileNav = "";

        this.router.events.subscribe(this.onRouteChange.bind(this));
    }

    private onRouteChange(event: any): void {
        if (environment.production) {
            if (event instanceof NavigationEnd) {
                ga('set', 'page', event.urlAfterRedirects);
                ga('send', 'pageview');
            }
        }

        if (event instanceof NavigationEnd) {
            this.setMobileNav('');
        }
    }

    hasNav(): boolean {
        return this.document.body.offsetWidth < 1279;
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.authService.isSignin()) {
            this.onSignin();
        }
        this.garbage.event(
            this.eventHandler.on('action.calendarevent.create', () => {
                if (this.hasNav()) {
                    this.setMobileNav('calendar');
                }
            })
        );
        this.garbage.event(
            this.eventHandler.on('action.calendarevent.edit', (editEvent: Model) => {
                if (this.hasNav()) {
                    this.setMobileNav('calendar');
                }
            })
        );
    }

    ngOnDestroy(): void {
        super.ngOnDestroy()
        this.garbage.destroy();
    }

    onSignin(): void {
        this.garbage.subscription(
            this.subjectUserService.request({})
                .subscribe((data: any) => {
                    this.hasMenu = true;
                })
        );
    }

    onFileOut(event: any): void {
        event.preventDefault();
        event.stopPropagation();
        this.dragCounter--;
        if (this.dragCounter == 0) {
            this.fileDropService.onFileOver(false);
        }
    }
    
    onFileOver(event: any): void {
        event.preventDefault();
        event.stopPropagation();
        if (this.dragCounter == 0) {
            this.fileDropService.onFileOver(true);
        }
        this.dragCounter++;
    }

    onFileDrop(event: any): void {
        event.preventDefault();
        event.stopPropagation();
        this.dragCounter = 0;
        this.fileDropService.onFileOver(false);
    }

    @HostListener('window:scroll', ['$event'])
    onScroll(event: any): void {        
        let header = this.document.body.querySelector('header');
        if (!header) {
            return;
        }
        let headerHeight: number = header.offsetTop + header.offsetHeight;
        this.headerVisible = window.scrollY > headerHeight;
    }

    showHeader(): boolean {
        return this.headerVisible;
    }

    setMobileNav(nav: string): void {
        this.mobileNav = nav;
        if (this.mobileNav != "") {
            this.renderer.addClass(this.document.body, 'lmn-modal-open');
        } else {
            this.renderer.removeClass(this.document.body, 'lmn-modal-open');
        }
    }

    isNavVisible(name: string): boolean {
        return this.mobileNav == name;
    }
}
