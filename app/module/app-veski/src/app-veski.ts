// COMPONENTS
export { VeskiMainComponent } from './component/veski-main.component';
export { IndexComponent }  from './component/index.component';
export { SettingsComponent }  from './component/settings.component';

// DIRECTIVES

// PIPES

// SERVICES

export { VeskiModule } from './app-veski.module';
