import { Component, OnInit, OnDestroy } from '@angular/core';

import { LmnDate, EventHandler, EventToken } from 'core/index';
import { OnSignin, AuthenticationService } from 'account/index';
import { CalendarService } from 'calendar/index';

import { TimelineService } from '../service/timeline.service';
import { HourGrid } from '../object/hour-grid.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-timeline',
    templateUrl: 'timeline.component.html',
    styleUrls: ['timeline.component.css']
})

export class TimelineComponent extends OnSignin implements OnInit, OnDestroy {
    
    private timelineService: TimelineService;
    private calendarService: CalendarService;
    public marks: number[];
    public eventLanes: HourGrid[];
    public markSize: number;
    private events: any[];
    private leftOffset: string;
    public tick: EventToken;

    constructor(timelineService: TimelineService, eventHandler: EventHandler, authService: AuthenticationService, calendarService: CalendarService) {
        super(eventHandler, authService);
        this.calendarService = calendarService;
        this.timelineService = timelineService;
        this.eventHandler = eventHandler;
        this.marks = [];
        for (let i: number = 1; i < 31; i++) {
            this.marks.push(i);
        }
        this.events = [];
        this.eventLanes = [new HourGrid(), new HourGrid()];
        this.markSize = 32;
        this.tick = new EventToken();
        this.updateTimeline();        
        let date: Date = new Date();
    }

    private init(): void {
        let until: LmnDate = new LmnDate(new Date());
        until.addWeek(1);
        this.timelineService.request({since: new Date(), until: until.date()});
    }

    private updateMarksOffset(): void {
        let date: Date = new Date();
        let minutes: number = date.getMinutes();
        this.leftOffset = (this.markSize * (0.5 - (minutes / 60))) + "px";
    }

    private updateEventsPosition(): void {
        this.tick.fire();
    }

    private addToLane(date: Date, event: any): void {
        for (let i: number = 0; i < this.eventLanes.length; i++) {
            if (!this.eventLanes[i].hasSpace(date, event)) {
                continue;
            }

            this.eventLanes[i].add(date, event);
            break;
        }
    }

    private onLoad(): void {
        for (let i: number = 0; i < this.eventLanes.length; i++) {
            this.eventLanes[i].reset();           
        }

        let days: any = this.timelineService.getDays();
        let events: any[] = this.timelineService.getEvents();
        let eventsById: any = {};
        for (let i: number = 0; i < events.length; i++) {
            eventsById[events[i].get('id')] = events[i];
        }

        for (var key in days) {
            if (!days.hasOwnProperty(key)) {
                continue;
            }
            let day: any = days[key];
            let dayEvents: any[] = day.get('events');
            for (let i: number = 0; i < dayEvents.length; i++) {
                let eventId: number = dayEvents[i].event_id
                this.addToLane(new Date(dayEvents[i].eventdate), eventsById[eventId]);
            }
        }
    }

    ngOnInit() {
        super.ngOnInit();
        this.events.push(
            this.eventHandler.on('timeline.load', this.onLoad.bind(this))
        );
        this.events.push(
            this.eventHandler.on('calendarevent.create', this.onSignin.bind(this))
        );

        setInterval(() => {
            this.updateTimeline();
        }, 60 * 1000);        
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    onSignin(): void {
        this.init();
    }

    updateTimeline(): void {
        this.updateMarksOffset();
        this.updateEventsPosition();
    }

    getLeftOffset(): string {
        return this.leftOffset;
    }
}