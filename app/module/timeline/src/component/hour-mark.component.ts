import { Component, OnInit, Input } from '@angular/core';

import { LmnDate } from 'core/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-hour-mark',
    templateUrl: 'hour-mark.component.html',
    styleUrls: ['timeline.component.css', 'hour-mark.component.css']
})

export class HourMarkComponent implements OnInit {

    @Input('lmnIndex')
    private index: number;
    @Input('lmnWidth')
    private width: number;

    private _show: number[];

    constructor() {
        this._show = [ 0, 3, 6, 9, 12, 15, 18, 21 ];
    }

    ngOnInit() { }

    getWidth(): string {
        return this.width + "px";
    }

    shouldShowNumber(): boolean {
        let hour: number = this.getNumber();
        return (hour % 3) == 0;
    }

    getNumber(): number {
        let date: LmnDate = new LmnDate(new Date());
        date.addHour(this.index);
        return date.date().getHours();
    }

    isNewDay(): boolean {
        return (this.getNumber() == 0);
    }
}