import { Component, OnInit, Input } from '@angular/core';

import { EventToken } from 'core/index';
import { CalendarService } from 'calendar/index';

import { TimelineEvent } from '../object/timeline-event.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-timeline-event',
    templateUrl: 'timeline-event.component.html',
    styleUrls: ['timeline-event.component.css']
})

export class TimelineEventComponent implements OnInit {

    @Input('lmnEvent')
    private event: any;
    @Input('lmnTimelineEvent')
    private timelineEvent: TimelineEvent;
    @Input('lmnUnitWidth')
    private unitWidth: number;
    @Input('lmnTick')
    private tick: EventToken;
    private calendarService: CalendarService;
    private offset: number;

    constructor(calendarService: CalendarService) {
        this.calendarService = calendarService;
        this.offset = 0;
    }

    private onUpdate(): void {
        let now: Date = new Date();
        let start: Date = this.timelineEvent.getSince();
        this.offset = ((start.getTime() - now.getTime()) / 1000 / 3600 * this.unitWidth);
    }

    ngOnInit() {
        this.onUpdate();
        this.tick.subscribe(this.onUpdate.bind(this));
    }

    getWidth(): string {
        return (this.timelineEvent.getDuration() / 3600 * this.unitWidth) + "px"
    }

    getOffset(): string {
        return this.offset + "px";
    }

    getColorClass(): string {
        return this.calendarService.getColorClass(this.event.get('id'));
    }
}