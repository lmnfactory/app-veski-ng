import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ModelService, Model, RouteExec, AbstractObservable, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { ColorpaletteService } from 'lmn-color/index';
import { UserCalendarService } from 'calendarevent/index';

@Injectable()
export class TimelineService {

    private http: LmnHttp;
    private modelService: ModelService;
    private eventHandler: EventHandler;
    private userCalendarService: UserCalendarService;
    private colorpaletteService: ColorpaletteService;
    private calendardays: any;
    private calendarevents: any[];

    constructor(http: LmnHttp, modelService: ModelService, eventHandler: EventHandler, userCalendarService: UserCalendarService, colorpaletteService: ColorpaletteService) {
        this.http = http;
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.userCalendarService = userCalendarService;
        this.colorpaletteService = colorpaletteService;
    }

    private notifyAll(): void {
        this.eventHandler.fire('timeline.load', this.getDays());
    }

    getDays(): any {
        return this.calendardays;
    }

    getEvents(): any[] {
        return this.calendarevents;
    }

    getEvent(id: number): Model {
        let i: number = 0;
        for (i = 0; i < this.calendarevents.length; i++) {
            if (id == this.calendarevents[i].get('id')) {
                return this.calendarevents[i];
            }
        }

        return null;
    }

    request(data: any): Observable<Response>{
        let observer: Observable<Response> = this.http.post('api/sharedcalendar/user_calendar', data);
            
        observer.subscribe((data: any) => {
            let value: any = this.userCalendarService.requestDigest(data);
            this.calendardays = value.days;
            this.calendarevents = value.events;
            this.notifyAll();
        });

        return observer;
    }
}