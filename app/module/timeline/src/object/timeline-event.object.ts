import { LmnDate } from 'core/index';

export class TimelineEvent {

    private since: Date;
    private until: Date;
    private duration: number;

    constructor(start: Date, duration: number) {
        this.duration = duration;
        this.since = start;
        let end = new LmnDate(new Date(start));
        end.addSeconds(duration)
        this.until = end.date();
    }

    getSince(): Date {
        return this.since;
    }

    getUntil(): Date {
        return this.until;
    }

    getDuration(): number {
        return this.duration;
    }

    hasOverlap(timelineEvent: TimelineEvent): boolean {
        return (this.since < timelineEvent.getUntil() && this.until > timelineEvent.getSince());
    }
}