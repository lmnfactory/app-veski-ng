import { TimelineEvent } from './timeline-event.object';

export class HourGrid {

    private blocks: any;
    private blocksize: number;
    private events: any[];

    constructor() {
        this.blocksize = 3600;
        this.reset();
    }

    reset(): void {
        this.events = [];
        this.blocks = {};
    }

    private getBlockId(timestamp: number) {
        return Math.floor(timestamp / (this.blocksize * 1000));
    }

    private getRelevantBlocks(timelineEvent: TimelineEvent): any[] {
        let blocks: any[] = [];
        let duration: number = timelineEvent.getDuration();

        let date: Date = new Date(timelineEvent.getSince());
        let prev: number = this.getBlockId(date.getTime())
        blocks.push(prev)
        let diff: number;
        let blockId: number;
        while (duration > 0) {
            if (duration < this.blocksize) {
                diff = duration;
            } else {
                diff = this.blocksize;
            }

            date.setTime(date.getTime() + (diff * 1000));
            blockId = this.getBlockId(date.getTime());
            if (blockId != prev) {
                blocks.push(blockId);
                prev = blockId;
            }
            duration -= diff;
        }
        return blocks;
    }

    private isBlockOverlap(blockId: number, timelineEvent: TimelineEvent): boolean {
        if (!this.blocks[blockId]) {
            return false;
        }
        let block: TimelineEvent[] = this.blocks[blockId];

        for (let i: number = 0; i < block.length; i++) {
            if (block[i].hasOverlap(timelineEvent)) {
                return true;
            }
        }

        return false;
    }

    private addToBlock(blockId: number, timelineEvent: TimelineEvent): void {
        if (!this.blocks[blockId]) {
            this.blocks[blockId] = [];
        }
        this.blocks[blockId].push(timelineEvent);
    }

    private createTimelineEvent(eventstart: Date, event: any): TimelineEvent {
        let duration: number = event.get('duration');
        if (event.get('allday')) {
            duration = 24 * 60 * 60;
            eventstart.setHours(0);
            eventstart.setMinutes(0);
            eventstart.setSeconds(0);
        }
        return new TimelineEvent(eventstart, duration);
    }

    hasSpace(eventstart: Date, event: any): boolean {
        let timelineEvent: TimelineEvent = this.createTimelineEvent(eventstart, event);

        let blocks: any[] = this.getRelevantBlocks(timelineEvent);
        for (let i: number = 0; i < blocks.length; i++) {
            if (this.isBlockOverlap(blocks[i], timelineEvent)) {
                return false;
            }
        }

        return true;
    }

    add(eventstart: Date, event: any): void {
        let timelineEvent: TimelineEvent = this.createTimelineEvent(eventstart, event);
        this.events.push({
            event: event,
            timelineEvent: timelineEvent
        });

        let blocks: any[] = this.getRelevantBlocks(timelineEvent);
        for (let i: number = 0; i < blocks.length; i++) {
            this.addToBlock(blocks[i], timelineEvent);
        }
    }

    getEvents(): any[] {
        return this.events;
    }
}