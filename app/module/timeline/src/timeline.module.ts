import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { CoreModule } from 'core/index';
import { CalendareventModule } from 'calendarevent/index';
import { CalendarModule } from 'calendar/index';

import { TimelineComponent } from './component/timeline.component';
import { TimelineEventComponent } from './component/timeline-event.component';
import { HourMarkComponent } from './component/hour-mark.component';

import { TimelineService } from './service/timeline.service';

@NgModule({
    imports: [ CommonModule, FlexLayoutModule, FormsModule, CoreModule, CalendareventModule, CalendarModule ],
    exports: [ TimelineComponent ],
    declarations: [ TimelineComponent, HourMarkComponent, TimelineEventComponent ],
    providers: [ TimelineService ],
})
export class TimelineModule { }
