export { TimelineComponent } from './component/timeline.component';

export { TimelineService } from './service/timeline.service';

export { TimelineModule } from './timeline.module';