import { Component, OnInit, OnChanges, Input, ViewChild } from '@angular/core';

import { fadeHiddenAnimation } from 'animation/index';
import { AutocompleteService } from 'form-ui/index';

import { SearchService } from '../service/search.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-search',
    templateUrl: 'search.component.html',
    styleUrls: ['search.component.css'],
    animations: [ fadeHiddenAnimation ]
})

export class SearchComponent implements OnInit {

    @Input('lmnAutofocus')
    public autofocus: boolean;
    @Input('lmnSearch')
    public searchValue: string;
    @ViewChild('searchInput')
    private autofocusElement: any;
    @Input('lmnCompact')
    public compactForm: boolean
    private focusValue: boolean;
    private visible: boolean;
    public items: any[];
    public selected: number;

    private searchService: SearchService;
    private autocompleteService: AutocompleteService;

    constructor(searchService: SearchService, autocompleteService: AutocompleteService) { 
        this.searchService = searchService;
        this.autocompleteService = autocompleteService;
        this.setItems([]);
        this.selected = 0;
        this.visible = false;
    }

    private setItems(items: any[]): void {
        this.items = items;
    }

    ngOnInit(): void { }

    ngOnChanges(): void {
        if (this.autofocus) {
            this.autofocusElement.nativeElement.focus();
        }
    }

    focus(): void {
        this.focusValue = true;
        this.visible = true;
    }

    blur(): void {
        this.focusValue = false;
        this.visible = false;
    }

    hasFocus(): boolean {
        return this.focusValue;
    }

    hasSearchValue(): boolean {
        return (this.searchValue != undefined && this.searchValue != "");
    }

    clearSearchValue(): void {
        this.searchValue = "";
    }

    isAutocompleteHidden(): boolean {
        return !this.visible;
    }

    showLabel(item: any, index: number): boolean {
        if (index == 0) {
            return true;
        }

        return item.get('group') != this.items[index - 1].get('group');
    }

    set(searchValue: any): void {
        this.searchValue = searchValue;

        this.autocompleteService.timeout(() => {
            this.visible = true;
            this.search();
        });
    }

    setSelected(selected: number): void {
        this.selected = selected;
        this.visible = true;
    }

    onSelect(searchModel: any): void {
        if (this.isAutocompleteHidden()) {
            return;
        }
        this.searchValue = searchModel.get('main');
        searchModel.get('exec').execute();
        this.visible = false;
    }

    onClose(event: any): void {
        this.visible = false;
    }

    search(): void {
        if (this.searchValue == "") {
            this.setItems([]);
            this.selected = 0;
            this.visible = false;
        }
        else {
            this.searchService.request({search: this.searchValue})
                .subscribe((data: any) => {
                    this.selected = 0;
                    let items: any[] = [];

                    for (let i: number = 0; i < data.data.length; i++) {
                        items.push(this.searchService.model(data.data[i]));
                    }

                    this.setItems(items);
                });
        }
    }
}