import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-search-group',
    templateUrl: 'search-group.component.html',
    styleUrls: ['search-group.component.css']
})

export class SearchGroupComponent implements OnInit {

    @Input('lmnName')
    private groupName: string;
    @Input('lmnLabel')
    public groupLabel: string;

    constructor() { }

    ngOnInit() { }
}