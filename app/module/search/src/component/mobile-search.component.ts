import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { fadeHiddenAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-mobile-search',
    templateUrl: 'mobile-search.component.html',
    styleUrls: ['mobile-search.component.css'],
    animations: [ fadeHiddenAnimation ]
})
export class MobileSearchComponent implements OnInit, OnDestroy {

    @Input('lmnShow')
    public show: boolean;
    @Output('lmnClose')
    private closeEmitter: EventEmitter<any>;

    constructor() {
        this.closeEmitter = new EventEmitter();
    }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {
        
    }

    hide(): void {
        this.show = false;
        this.closeEmitter.emit({});
    }
}
