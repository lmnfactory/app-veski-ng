import { ModelService, BasicModel } from 'core/index';

export class SearchModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    execute(): void {
        this.get('exec').execute();
    }

    _setGroup(data: any): any {
        return data;
    }

    _setRelevance(data: any): any {
        return data;
    }

    _setGrouplabel(data: any): any {
        return data;
    }

    export(): any {
        return {};
    }
}
