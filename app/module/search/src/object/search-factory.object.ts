import { ModelService, ModelFactory, Model } from 'core/index';

import { SearchModel } from './search.model';

export class SearchFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new SearchModel(this.modelService);
        model.fill(data);
        return model;
    }
}
