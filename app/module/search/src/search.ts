export { SearchComponent } from './component/search.component';
export { MobileSearchComponent } from './component/mobile-search.component';
export { SearchItemComponent } from './component/search-item.component';
export { SearchService } from './service/search.service';

export { SearchModule } from './search.module';