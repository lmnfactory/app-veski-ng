import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { CoreModule, ModelService } from 'core/index';
import { FormUIModule } from 'form-ui/index';

import { SearchComponent } from './component/search.component';
import { MobileSearchComponent } from './component/mobile-search.component';
import { SearchItemComponent } from './component/search-item.component';
import { SearchGroupComponent } from './component/search-group.component';
import { SearchService } from './service/search.service';
import { SearchFactory } from './object/search-factory.object';

@NgModule({
    imports: [ CommonModule, FormsModule, FlexLayoutModule, FormUIModule ],
    exports: [ SearchComponent, SearchItemComponent, MobileSearchComponent ],
    declarations: [ SearchComponent, SearchItemComponent, SearchGroupComponent, MobileSearchComponent ],
    providers: [ SearchService ],
})
export class SearchModule {

    constructor(modelService: ModelService) {
        modelService.addFactory('search', new SearchFactory(modelService));
    }
}
