import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { LmnHttp } from 'http-communication/index';

import { ModelService, Model } from 'core/index';

@Injectable()
export class SearchService {

    private http: LmnHttp;
    private modelService: ModelService;
    private modelMapping: any;

    constructor(http: LmnHttp, modelService: ModelService) {
        this.http = http;
        this.modelService = modelService;
        this.modelMapping = {};
    }

    mapModel(key: string, modelKey: string): void {
        this.modelMapping[key] = modelKey;
    }

    request(data: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('/api/search', data);
           
        observable.subscribe((data: any) => {});
        return observable
    }

    model(data: any): Model {
        if (!data.group) {
            console.log('Group value not found in search item');
            return null;
        }
        let group: string = data.group;

        if (!this.modelMapping[group]) {
            console.log("Group '" + group + "' not mapped in SearchService");
            return null;
        }

        let model: Model = this.modelService.create(this.modelMapping[group], data.data);
        let searchModel: Model = model.get('searchModel');
        searchModel.set('relevance', data.relevance);
        searchModel.set('group', data.group);

        return searchModel;
    }
}