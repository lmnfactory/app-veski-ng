import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Model, RouteRefreshExec } from 'core/index';

@Injectable()
export class NotificationExecHandlerService {

    private router: Router;

    constructor(router: Router) {
        this.router = router;
    }

    onCalendareventCreate(notification: Model): void {
        notification.set('exec', new RouteRefreshExec(['/subject', notification.get('options.subjectPid'), 'events', notification.get('source_id')], this.router, () => {
                // load calendarevents for subejct
            })
        );
    }
}