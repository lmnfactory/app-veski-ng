import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ModelService, AbstractObservable, Model, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { StateMachine, StateService } from 'state/index';
import { ColorpaletteService } from 'lmn-color/index';
import { UserCalendarService } from 'calendarevent/index';

import { CalendareventModel } from 'calendarevent/index';

@Injectable()
export class CalendarService extends AbstractObservable {

    private http: LmnHttp;
    private modelService: ModelService;
    private colorpaletteService: ColorpaletteService;
    private userCalendarService: UserCalendarService;
    private eventHandler: EventHandler;
    private calendarevents: Model[];

    constructor(http: LmnHttp, modelService: ModelService, colorpaletteService: ColorpaletteService, userCalendarService: UserCalendarService, eventHandler: EventHandler) {
        super();
        this.http = http;
        this.modelService = modelService;
        this.colorpaletteService = colorpaletteService;
        this.userCalendarService = userCalendarService;
        this.eventHandler = eventHandler;
        this.calendarevents = [];

        this.eventHandler.on('calendareventuser.load', this.onLoad.bind(this));
    }

    private onLoad(): void {
        this.calendarevents = this.userCalendarService.allEvents();
    }

    getDateKey(date: Date): string {
        let year: string = date.getFullYear() + "";
        let month: string = (date.getMonth() + 1) + "";
        let day: string = date.getDate() + "";

        if (month.length < 2) {
            month = "0" + month;
        }
        if (day.length < 2) {
            day = "0" + day;
        }

        return year + "_" + month + "_" + day;
    }

    all(): Model[] {
        return this.calendarevents;
    }

    add(calendarevent: Model): void {
        let id: string = calendarevent.get('id');
        this.calendarevents[id] = calendarevent;
        this.notifyAll();
    }

    get(id: number): Model {
        let i: number = 0;
        if (this.calendarevents[id]) {
            return this.calendarevents[id];
        }

        return null;
    }

    create(calendarevent: Model): Observable<Response> {
        let observer = this.http.post('api/calendar/create', calendarevent.export());

        observer.subscribe((data: any) => {
            let model: Model = this.modelService.create('calendarevent', data.data);
            this.add(model);
            this.eventHandler.fire('calendarevent.create', model);
        }, (err: any) => {return err;});

        return observer;
    }

    update(calendarevent: Model): Observable<Response> {
        let observer = this.http.post('api/calendar/update', calendarevent.export());

        observer.subscribe((data: any) => {
            let model: Model = this.modelService.create('calendarevent', data.data);
            this.refresh(model);
            this.eventHandler.fire('calendarevent.update', model);
        }, (err: any) => {return err;});

        return observer;
    }

    refresh(calendarevent: Model): void {
        let change: boolean = false;
        let id: string = calendarevent.get('id');
        if (this.calendarevents[id]) {
            this.calendarevents[id] = calendarevent;
            change = true;
        }

        if (change) {
            this.notifyAll();
        }
        else {
            this.add(calendarevent);
        }
    }

    request(data: any): void {
        this.userCalendarService.request(data);
    }

    hasPriority(eventId: number): boolean {
        let eventModel: Model = this.get(eventId);
        if (!eventModel) {
            return false;
        }

        return (eventModel.get('priority') == 5);
    }

    hasPriorityByDate(day: Date): boolean {
        let dayEvents: any[] = this.userCalendarService.all();
        let key: string = this.getDateKey(day);
        if (!dayEvents[key]) {
            return false;
        }

        for (let event of dayEvents[key].get('events')) {
            if (!this.hasPriority(event.event_id)) {
                continue;
            }

            return true;
        }

        return false;
    }

    getColorClass(eventId: number): string {
        let event: Model = this.get(eventId);
        if (event == null || event.get('calendareventsubject') == null) {
            return this.colorpaletteService.getSubjectPaletteClass(0);
        }

        return this.colorpaletteService.getSubjectPaletteClass(event.get('calendareventsubject.subject_id'));
    }
}
