import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { Model } from 'core/index';
import { SubjectUserService } from 'subject/index';
import { CalendareventModel } from 'calendarevent/index';

import { CalendarService } from '../service/calendar.service';
import { Calendar } from '../object/calendar.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-calendar-week-event',
    templateUrl: 'calendar-week-event.component.html',
    styleUrls: ['calendar.component.css', 'calendar-week-event.component.css']
})
export class CalendarWeekEventComponent implements OnInit, OnDestroy {

    @Input('lmnEvent')
    private event: any;
    @Output('lmnClick')
    private clickEmitter: EventEmitter<any>;

    private calendarService: CalendarService;
    private subjectUserService: SubjectUserService;
    private router: Router;
    public eventModel: any;

    constructor(calendarService: CalendarService, subjectUserService: SubjectUserService, router: Router) {
        this.calendarService = calendarService;
        this.subjectUserService = subjectUserService;
        this.router = router;
        this.clickEmitter = new EventEmitter<any>();
    }

    ngOnInit(): void {
        this.eventModel = this.calendarService.get(this.event.event_id);
    }

    ngOnDestroy(): void {

    }

    onClick(event: any): void {
        this.clickEmitter.emit(event);
    }

    hasPriority(): boolean {
        return this.calendarService.hasPriority(this.eventModel.get('id'));
    }

    getTitle(): string {
        return this.eventModel.get('name');
    }

    getTime(): Date {
        let date: Date = new Date(this.eventModel.get('eventstart'));
        return date;
    }

    isSelected(): boolean {
        return false;
    }

    getColorClass(): string {
        return this.calendarService.getColorClass(this.event.event_id);
    }

    goToEvent(): void {
        if (!this.eventModel.get('calendareventsubject.subject_id')) {
            return;
        }
        
        let subject: Model = this.subjectUserService.get(this.eventModel.get('calendareventsubject.subject_id'));
        if (subject == null) {
            return;
        }
        
        this.router.navigate(['subject', subject.get('subjectprototype.public_id'), 'events', this.eventModel.get('id')]);
    }
}
