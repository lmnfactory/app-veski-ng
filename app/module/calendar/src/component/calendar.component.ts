import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { DatePipe } from '@angular/common';

import { EventToken, Model, Observer, EventHandler, LmnDate, Garbage, GarbageService } from 'core/index';
import { AuthenticationService, OnSignin } from 'account/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';
import { SubjectUserService } from 'subject/index';
import { UserCalendarService } from 'calendarevent/index';

import { CalendarService } from '../service/calendar.service';
import { CalendarView } from '../object/calendar-view.interface';
import { MonthView } from '../object/month-view.object';
import { WeekView } from '../object/week-view.object';
import { Calendar } from '../object/calendar.object';


@Component({
    'moduleId': module.id,
    'selector': 'lmn-calendar',
    'templateUrl': 'calendar.component.html',
    styleUrls: ['calendar.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ],
    providers: [DatePipe]
})
export class CalendarComponent extends OnSignin implements OnInit, OnDestroy, Observer {

    private subjectPid: string;

    private calendarevents: Model[];
    @Input('lmnView')
    public view: string;
    private calendar: Calendar;
    private calendarViews: any;
    private activeView: CalendarView;
    private calendarService: CalendarService;
    private userCalendarService: UserCalendarService;
    private datePipe: DatePipe;
    private showValue: string;
    private garbage: Garbage;
    public editEvent: Model;

    constructor(calendarService: CalendarService, userCalendarService: UserCalendarService, eventHandler: EventHandler, authService: AuthenticationService, datePipe: DatePipe, garbageService: GarbageService) {
        super(eventHandler, authService);
        this.showValue = 'calendar';
        this.calendarService = calendarService;
        this.userCalendarService = userCalendarService;
        this.datePipe = datePipe;
        this.garbage = garbageService.create();
        this.calendar = new Calendar(new Date());
        this.calendarViews = {
            month: new MonthView(this.calendar, calendarService),
            week: new WeekView(this.calendar, calendarService)
        };
        this.selectView('month');
    }

    private refresh(): void {
        this.getView().activate();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.garbage.event(
            this.eventHandler.on('calendareventuser.change', this.refresh.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('calendareventuser.load', this.update.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('subjectuser.load', this.refresh.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('action.calendarevent.create', () => {this.openCreate()})
        )
        this.garbage.event(
            this.eventHandler.on('action.calendarevent.edit', (editEvent: Model) => {this.openCreate(editEvent)})
        )
        this.calendarevents = this.userCalendarService.all();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.garbage.destroy();
    }

    onSignin(): void {
        this.activateView('month');
    }

    getCalendar(): Calendar {
        return this.calendar;
    }

    onCreate(event: any): void {
        this.show('calendar');
        this.activeView.activate();
    }

    update(): void {
        this.calendarevents = this.userCalendarService.all();
        this.calendar.setEvents(this.calendarevents);
    }

    selectView(view: string): void {
        this.view = view;
        if (this.calendarViews[view]) {
            this.activeView = this.calendarViews[view];
        }
    }

    activateView(view: string): void {
        this.selectView(view);
        if (this.calendarViews[view]) {
            this.activeView.activate();
        }
    }

    selectDay(event: any): void {
        this.calendar.setSelected(event);
        this.activeView.select(event);
    }

    isViewSelected(key: string): boolean {
        return (this.view == key);
    }

    getTitle(): string {
        return this.activeView.getTitle();
    }

    rows(): any[] {
        return [0, 1, 2, 3, 4, 5];
    }

    columns(): any[] {
        return [0, 1, 2, 3, 4, 5, 6];
    }

    getView(): CalendarView {
        return this.activeView;
    }

    getMonthView(): CalendarView {
        return this.calendarViews.month;
    }

    getWeekView(): CalendarView {
        return this.calendarViews.week;
    }

    getUpcomingEvents(): any[] {
        let eventKey: string = this.calendarService.getDateKey(this.calendar.getSelected());
        let events: any = this.calendar.getEvents();
        if (!events[eventKey]) {
            return [];
        }
        return events[eventKey].get('events');
    }

    hasDayPriority(index: number): boolean {
        let cellDate: Date = this.activeView.getCellDate(index);
        return this.calendarService.hasPriorityByDate(cellDate);
    }

    getEvent(eventId: number): any {
        return this.calendarService.get(eventId);
    }

    getEvents(index: number): any {
        let date: Date = this.activeView.getCellDate(index);
        let eventKey: string = this.calendarService.getDateKey(date);
        let events: any = this.calendar.getEvents();
        if ((events[eventKey]) && (events[eventKey].get('events').length > 0)) {
            return events[eventKey].get('events');
        }
        return [];
    }

    shouldShow(key: string): boolean {
        return (key == this.showValue);
    }

    show(key: string): void {
        this.showValue = key;
    }

    isCurrentDay(dayIndex: number): boolean {
        let today: LmnDate = new LmnDate(new Date());
        return (this.activeView && this.activeView.hasCurrent() && today.getDay() == dayIndex);
    }

    getUpcommingEventTitle(): string {
        let lmnDate: LmnDate = new LmnDate(this.calendar.getSelected());
        if (lmnDate.isToday()) {
            return "Dnes";
        } else if (lmnDate.isTomorrow()) {
            return "Zajtra";
        }
        return this.datePipe.transform(lmnDate.date(), 'dd MMMM y');
    }

    openCreate(editEvent: Model = null): void {
        this.editEvent = editEvent;
        this.show('event-form');
    }
}
