import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import * as moment from 'moment/moment';

import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';
import { ModelService, Model, ValidationService, EventHandler, Garbage, GarbageService } from 'core/index';
import { FormBoxService, FormBoxEmitter } from 'form-ui/index';
import { SubjectUserService } from 'subject/index';
import { ColorpaletteService } from 'lmn-color/index';

import { CalendarService } from '../service/calendar.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-event-create-form',
    templateUrl: 'event-create-form.component.html',
    styleUrls: ['event-create-form.component.css', 'calendar.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})
export class EventCreateFormComponent implements OnInit, OnDestroy, OnChanges {

    @Output('lmnClose')
    private closeEmit: EventEmitter<any>;
    @Output('lmnSubmit')
    private submitEmit: EventEmitter<any>;

    private formBuilder: FormBuilder;
    public form: FormGroup;
    private modelService: ModelService;
    private calendarService: CalendarService;
    private subjectUserService: SubjectUserService;
    private colorpaletteService: ColorpaletteService;
    private validationService: ValidationService;
    private eventHandler: EventHandler;
    public calendarevent: Model;
    public formBox: FormBoxEmitter;
    public formModel: any;
    @Input('lmnDate')
    public prefillDate: Date;
    @Input('lmnCalendarevent')
    public editEvent: Model;
    public eventstart: string;
    public eventuntil: string;
    public highPriority: boolean;
    public repeat: any[];
    public subjects: any[];
    private garbage: Garbage;
    private validationConrols: any;
    public errors: string[];

    constructor(modelService: ModelService, calendarService: CalendarService, subjectUserService: SubjectUserService, colorpaletteService: ColorpaletteService, formBuilder: FormBuilder, validationService: ValidationService, eventHandler: EventHandler, formBoxService: FormBoxService, garbageService: GarbageService) {
        this.modelService = modelService;
        this.calendarService = calendarService;
        this.subjectUserService = subjectUserService;
        this.colorpaletteService = colorpaletteService;
        this.formBuilder = formBuilder;
        this.validationService = validationService;
        this.eventHandler = eventHandler;
        this.closeEmit = new EventEmitter<any>();
        this.submitEmit = new EventEmitter<any>();
        this.repeat = [
            {
                name: 'každý týždeň',
                value: 'ew'
            },
            {
                name: 'jednorázová udalosť',
                value: 'sd'
            }
        ];
        this.subjects = this.subjectUserService.all();
        this.formBox = formBoxService.createEmitter();
        this.garbage = garbageService.create();
        this.prefillDate = null;
        this.validationConrols = {
            'name': 'Názov',
            'location': 'Miesto',
            'daterange': false,
            'daterange.eventstart': 'Začiatok',
            'daterange.eventend': 'Koniec'
        };
        this.init();
    }

    private init(editEvent: Model = null): void {
        if (editEvent == null) {
            editEvent = this.modelService.create('calendarevent', {
                public_id: false,
                name: "",
                location: "",
                description: "",
                priority: 3,
                allday: false,
                eventstart: this.prefillDate,
                eventuntil: this.prefillDate,
                calendareventrules: {
                    rule: 'ew'
                },
                calendareventsubject: {
                    subject_id: 0
                },
            });
        }
        
        this.calendarevent = this.modelService.create('calendarevent');
        this.calendarevent.fill(editEvent.export());
        this.setEventuntilDate(this.calendarevent.get('until'));
        this.setEventstartDate(this.calendarevent.get('eventstart'));
        this.highPriority = this.calendarevent.get('priority') == 5;
        if (this.form) {
            this.form.reset();
            this.form.setValue({
                name: this.calendarevent.get('name'),
                location: this.calendarevent.get('location'),
                daterange: {
                    eventstart: this.calendarevent.get('eventstart'),
                    eventend: this.calendarevent.get('until')
                }
            });
        }
        this.formBox.reset();
    }

    private onFormEvent(data: any): void {
        if (!this.form) { 
            return; 
        }

        this.errors = this.validationService.validate(this.form, this.validationConrols);
    }

    private onLoad(subjectusers: any[]): void {
        this.subjects = subjectusers;
    }

    private startEdti(): void {

    }

    ngOnInit(): void {
        let builder: FormBuilder = this.formBuilder;
        this.form = builder.group({
            'name': ["", Validators.compose([
                Validators.required,
                Validators.maxLength(255)
            ])],
            'location': ["", Validators.compose([
                Validators.maxLength(255)
            ])],
            'daterange': builder.group({
                'eventstart': [this.prefillDate, Validators.compose([
                    Validators.required
                ])],
                'eventend': [this.prefillDate, Validators.compose([
                    Validators.required
                ])]
            }, {validator: this.validationService.rule('daterange', {since: 'eventstart', until: 'eventend'})})
        });

        this.garbage.subscription(
            this.form.statusChanges.subscribe(this.onFormEvent.bind(this))
        );

        this.garbage.event(
            this.eventHandler.on('subjectuser.load', this.onLoad.bind(this))
        );

        this.garbage.event(
            this.formBox.on('submit', this.create.bind(this))
        );

        this.subjects = this.subjectUserService.all();
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    ngOnChanges(): void {
        if (!this.form) {
            return;
        }
        this.init(this.editEvent);
    }

    close(event: any): void {
        this.closeEmit.emit(event);
    }

    getSubjectColorClass(): string {
        return this.colorpaletteService.getSubjectPaletteClass(this.calendarevent.get('calendareventsubject.subject_id'));
    }

    setEventstartDate(date: Date = null): void {
        if (date == null) {
            this.eventstart = "";
            this.calendarevent.set('eventstart', date);
            return;
        }
        let start: Date = this.calendarevent.get('eventstart');
        if (!start) {
            start = new Date();
        }

        start.setFullYear(date.getFullYear());
        start.setMonth(date.getMonth());
        start.setDate(date.getDate());
        this.calendarevent.set('eventstart', start);
        this.eventstart = start.getDate() + "." + (start.getMonth() + 1) + "." + start.getFullYear();
    }

    setEventstartInputValue(value: any): void {
        value = value.replace(" ", "");
        this.eventstart = value;
        let momentDate: any = moment(value, "D.M.YYYY");
        if (momentDate.isValid()) {
            this.calendarevent.set('eventstart', momentDate.toDate());
        }
        else {
            this.calendarevent.set('eventstart', null);
        }
    }

    setEventuntilDate(date: Date = null): void {
        if (date == null) {
            this.eventuntil = "";
            this.calendarevent.set('eventuntil', date);
            return;
        }
        let end: Date = this.calendarevent.get('eventuntil');
        if (!end) {
            end = new Date();
        }

        end.setFullYear(date.getFullYear());
        end.setMonth(date.getMonth());
        end.setDate(date.getDate());
        this.calendarevent.set('eventuntil', end);
        this.eventuntil = end.getDate() + "." + (end.getMonth() + 1) + "." + end.getFullYear();
    }

    setEventuntilInputValue(value: any): void {
        value = value.replace(" ", "");
        this.eventuntil = value;
        let momentDate: any = moment(value, "D.M.YYYY");
        if (momentDate.isValid()) {
            this.calendarevent.set('eventuntil', momentDate.toDate());
        }
        else {
            this.calendarevent.set('eventuntil', null);
        }
    }

    create(readyCallback: any): void {
        this.form.updateValueAndValidity();
        if (this.form.invalid) {
            this.errors = this.validationService.validateAll(this.form, this.validationConrols);
            readyCallback();
            return;
        }

        this.calendarevent.set('eventstart', this.form.get('daterange.eventstart').value);
        this.calendarevent.set('eventend', null);
        this.calendarevent.set('until', this.form.get('daterange.eventend').value);
        this.calendarevent.set('name', this.form.get('name').value);
        this.calendarevent.set('location', this.form.get('location').value);

        let startDate: Date = this.calendarevent.get('eventstart');
        let endDate: Date = this.calendarevent.get('until');
        let duration = Math.round((endDate.getTime() - startDate.getTime()) / 1000);
        this.calendarevent.set('duration', duration);

        if (this.calendarevent.get('calendareventsubject.subject_id') == 0) {
            this.calendarevent.set('calendareventsettings.private', true);
        }
        if (this.calendarevent.get('calendareventsettings.private') === true) {
            this.calendarevent.set('share', false);
        } else {
            this.calendarevent.set('share', true);
        }
        if (this.highPriority) {
            this.calendarevent.set('priority', 5);
        }
        else {
            this.calendarevent.set('priority', 3);
        }

        if (this.calendarevent.get('public_id')) {
            this.calendarService.update(this.calendarevent)
                .subscribe(this.afterSubmit.bind(this), readyCallback);
        } else {
            this.calendarService.create(this.calendarevent)
                .subscribe(this.afterSubmit.bind(this), readyCallback);
        }
    }

    private afterSubmit(data: any): void {
        this.init(this.editEvent);
        this.submitEmit.emit(data);
    }
}
