import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Model } from 'core/index';
import { SubjectUserService } from 'subject/index';

import { CalendarService } from '../service/calendar.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-upcomming-event',
    templateUrl: 'upcomming-event.component.html',
    styleUrls: ['calendar.component.css', 'upcomming-event.component.css']
})

export class UpcommingEventComponent implements OnInit {

    @Input('lmnEvent')
    public event: any;
    public calendarevent: any;
    private calendarService: CalendarService;

    constructor(calendarService: CalendarService, private subjectUserService: SubjectUserService, private router: Router) {
        this.calendarService = calendarService;
    }

    ngOnInit(): void {

    }

    ngOnChanges(): void {
        this.calendarevent = this.calendarService.get(this.event.event_id);
    }

    hasPriority(eventId: number): boolean {
        return this.calendarService.hasPriority(eventId);
    }

    getColorClass(eventId: number): string {
        return this.calendarService.getColorClass(eventId);
    }

    goToEvent(): void {
        if (!this.calendarevent.get('calendareventsubject.subject_id')) {
            return;
        }
        
        let subject: Model = this.subjectUserService.get(this.calendarevent.get('calendareventsubject.subject_id'));
        if (subject == null) {
            return;
        }
        
        this.router.navigate(['subject', subject.get('subjectprototype.public_id'), 'events', this.calendarevent.get('id')]);
    }
}