import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { fadeIfAnimation } from 'animation/index';
import { UserCalendarService } from 'calendarevent/index';

import { CalendarService } from '../service/calendar.service';
import { Calendar } from '../object/calendar.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-calendar-week-cell',
    templateUrl: 'calendar-week-cell.component.html',
    styleUrls: ['calendar.component.css', 'calendar-week-cell.component.css'],
    animations: [ fadeIfAnimation ]
})
export class CalendarWeekCellComponent implements OnInit, OnDestroy {

    @Input('lmnEvents')
    public events: any[];
    @Output('lmnClick')
    public clickEmitter: EventEmitter<any>;
    @Output('lmnEventClick')
    public clickEventEmitter: EventEmitter<any>;



    private calendarService: CalendarService;
    private userCalendarService: UserCalendarService;

    constructor(calendarService: CalendarService, userCalendarService: UserCalendarService) {
        this.calendarService = calendarService;
        this.userCalendarService = userCalendarService;
        this.clickEmitter = new EventEmitter<any>();
        this.clickEventEmitter = new EventEmitter<any>();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    onClick(event: any): void {
        this.clickEmitter.emit({});
    }
}
