import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { CalendarService } from '../service/calendar.service';
import { Calendar } from '../object/calendar.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-calendar-month-cell',
    templateUrl: 'calendar-month-cell.component.html',
    styleUrls: ['calendar.component.css', 'calendar-month-cell.component.css']
})
export class CalendarMonthCellComponent implements OnInit, OnDestroy {

    @Input('lmnDate')
    private date: Date;
    @Input('lmnCalendar')
    private calendar: Calendar;
    @Output('lmnClick')
    private clickEvent: EventEmitter<any>;

    private calendarService: CalendarService;

    constructor(calendarService: CalendarService) {
        this.calendarService = calendarService;
        this.clickEvent = new EventEmitter<any>();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    onClick(event: any): void {
        this.clickEvent.emit(this.date);
    }

    getDay(): number {
        return this.date.getDate();
    }

    isDisabled(): boolean {
        return (this.date.getMonth() != this.calendar.getPage().date().getMonth());
    }

    isCurrent(): boolean {
        let today: Date = new Date();
        return (this.date.getDate() == today.getDate() && this.date.getMonth() == today.getMonth() && this.date.getFullYear() == today.getFullYear());
    }

    hasPriority(): boolean {
        return this.calendarService.hasPriorityByDate(this.date);
    }

    isSelected(): boolean {
        let selected: Date = this.calendar.getSelected();
        return (this.date.getDate() == selected.getDate() && this.date.getMonth() == selected.getMonth() && this.date.getFullYear() == selected.getFullYear());
    }

    hasEvent(index: number): boolean {
        let eventKey: string = this.calendarService.getDateKey(this.date);
        let events: any = this.calendar.getEvents();
        return ((events[eventKey]) && (events[eventKey].get('events').length > index));
    }

    getColorClass(index: number): string {
        if (!this.hasEvent(index)) {
            return "";
        }
        let eventKey: string = this.calendarService.getDateKey(this.date);
        let events: any = this.calendar.getEvents();
        let eventsInDay: any[] = events[eventKey].get('events');
        let eventId: number = eventsInDay[index].event_id;

        return this.calendarService.getColorClass(eventId);
    }
}
