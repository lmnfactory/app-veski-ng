import { LmnDate } from 'core/index';

import { CalendarService } from '../service/calendar.service';
import { CalendarView } from './calendar-view.interface';
import { Calendar } from './calendar.object';

export class MonthView implements CalendarView {

    private calendar: Calendar;
    private months: string[];
    private calendarGrid: any[];
    private calendarService: CalendarService;

    constructor(calendar: Calendar, calendarService: CalendarService) {
        this.calendar = calendar;
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
        this.calendarGrid = [];
        this.calendarService = calendarService;
        this.doCalendarGrid();
    }

    private doCalendarGrid(): void {
        let firstMonth: LmnDate = this.calendar.getPage().clone();
        firstMonth.date().setDate(1);

        let lastMonth: LmnDate = firstMonth.clone();
        lastMonth.addMonth(1);
        lastMonth.removeDay(1);

        let gridDate: LmnDate = firstMonth.clone();

        let firstDay: number = firstMonth.getDay();

        let total: number = firstDay + lastMonth.date().getDate() + 6 - lastMonth.getDay();

        gridDate.removeDay(firstDay);
        this.calendarGrid = [];
        let i: number = 0;
        let j: number = 0;
        for (i = 0; i < 6; i++) {
            this.calendarGrid.push([]);
            for (j = 0; j < 7; j++) {
                this.calendarGrid[i].push(new Date(gridDate.date().getTime()));
                gridDate.addDay(1);
            }
        }
    }

    private onChange(): void {
        this.calendarService.request({since: this.getSinceDate(), until: this.getUntilDate()});
        this.doCalendarGrid();
    }

    getCellDate(cell: number): Date {
        let i: number = Math.floor(cell / 7);
        let j: number = cell % 7;
        return this.calendarGrid[i][j];
    }

    activate(): void {
        this.onChange();
    }

    getTitle(): string {
        let date: Date = this.calendar.getPage().date();
        return this.months[date.getMonth()] + " " + date.getFullYear();
    }

    getSinceDate(): Date {
        let firstMonth: LmnDate = this.calendar.getPage().clone();
        firstMonth.date().setDate(1);
        firstMonth.date().setHours(0);
        firstMonth.date().setMinutes(0);
        firstMonth.date().setSeconds(0);
        return firstMonth.date();
    }

    getUntilDate(): Date {
        let lastMonth: LmnDate = this.calendar.getPage().clone();
        lastMonth.date().setDate(1);
        lastMonth.addMonth(1);
        lastMonth.removeDay(1);
        lastMonth.date().setHours(23);
        lastMonth.date().setMinutes(59);
        lastMonth.date().setSeconds(59);
        return lastMonth.date();
    }

    next(): void {
        this.calendar.getPage().addMonth(1);
        this.onChange();
    }

    previous(): void {
        this.calendar.getPage().addMonth(-1);
        this.onChange();
    }

    select(date: Date): void {
        if (date.getMonth() == this.calendar.getPage().date().getMonth()) {
            return;
        }

        if (date.getTime() > this.calendar.getPage().date().getTime()) {
            this.next();
        }
        else {
            this.previous();
        }
    }

    hasCurrent(): boolean {
        let today: Date = new Date();
        let since = this.getSinceDate();
        let until = this.getUntilDate();
        return (since.getTime() <= today.getTime() && until.getTime() >= today.getTime());
    }
}
