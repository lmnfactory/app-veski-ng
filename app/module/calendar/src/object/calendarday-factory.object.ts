import { ModelService, ModelFactory, Model } from 'core/index';

import { CalendardayModel } from './calendarday.model';

export class CalendardayFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new CalendardayModel(this.modelService);
        model.fill(data);
        return model;
    }
}
