import { ModelService, BasicModel, Model } from 'core/index';

export class CalendardayModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getEvents(data: any): any {
        if (data == undefined || data == null) {
            return {};
        }

        return this._data.events;
    }

    export(): any {
        return {
            
        };
    }
}
