import { ModelService, ModelFactory, Model } from 'core/index';

import { CalendareventuserModel } from './calendareventuser.model';

export class CalendareventuserFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new CalendareventuserModel(this.modelService);
        model.fill(data);
        return model;
    }
}
