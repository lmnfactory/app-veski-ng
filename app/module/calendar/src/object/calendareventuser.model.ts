import { ModelService, BasicModel, Model } from 'core/index';

export class CalendareventuserModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    export(): any {
        return {
            user_id: this.get('user_id'),
            calendarevent_id: this.get('calendarevent_id')
        };
    }
}
