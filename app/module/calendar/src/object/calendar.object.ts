import { LmnDate } from 'core/index';

export class Calendar {

    private selected: Date;
    private current: Date;
    private page: LmnDate;
    private events: any;

    constructor(selected?: Date) {
        this.events = {};
        this.current = new Date();
        if (!selected) {
            this.selected = new Date(this.current.getTime());
        }
        else {
            this.selected = selected;
        }
        this.page = new LmnDate(new Date(this.selected.getTime()));
    }

    getSelected(): Date {
        return this.selected;
    }

    setSelected(date: Date): void {
        this.selected = date;
    }

    getPage(): LmnDate {
        return this.page;
    }

    setPage(page: LmnDate): void {
        this.page = page;
    }

    setEvents(events: any): void {
        this.events = events;
    }

    getEvents(): any {
        return this.events;
    }
}
