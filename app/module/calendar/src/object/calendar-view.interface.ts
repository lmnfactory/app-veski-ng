export interface CalendarView {
    getTitle(): string;
    getCellDate(cell: number): Date;
    getSinceDate(): Date;
    getUntilDate(): Date;
    next(): void;
    previous(): void;
    activate(): void;
    select(date: Date): void;
    hasCurrent(): boolean;
}
