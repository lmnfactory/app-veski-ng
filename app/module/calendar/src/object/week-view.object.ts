import { LmnDate } from 'core/index';

import { CalendarService } from '../service/calendar.service';
import { CalendarView } from './calendar-view.interface';
import { Calendar } from './calendar.object';

export class WeekView implements CalendarView {

    private calendar: Calendar;
    private calendarService: CalendarService;
    private months: any[];
    private calendarGrid: any[];

    constructor(calendar: Calendar, calendarService: CalendarService) {
        this.calendar = calendar;
        this.calendarService = calendarService;
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
        this.calendarGrid = [];
    }

    private doCalendarGrid(): void {
        let firstDay: LmnDate = this.calendar.getPage().clone();
        firstDay.removeDay(firstDay.getDay());

        this.calendarGrid = [];
        let i: number = 0;
        for (i = 0; i < 7; i++) {
            this.calendarGrid.push(new Date(firstDay.date().getTime()));
            firstDay.addDay(1);
        }
    }

    private onChange(): void {
        this.calendarService.request({since: this.getSinceDate(), until: this.getUntilDate()});
        this.doCalendarGrid();
    }

    activate(): void {
        this.onChange();
    }

    getCellDate(cell: number): Date {
        return this.calendarGrid[cell];
    }

    getTitle(): string {
        let date: Date = this.calendar.getPage().date();
        let startDate: LmnDate = new LmnDate(new Date(date.getTime()));
        let endDate: LmnDate = new LmnDate(new Date(date.getTime()));

        let fromMonday: number = date.getDay();
        if (fromMonday == 0) {
            fromMonday = 7;
        }

        startDate.removeDay(fromMonday - 1);
        endDate.addDay(7 - fromMonday);

        let month: string = this.months[startDate.date().getMonth()];
        if (startDate.date().getMonth() != endDate.date().getMonth()) {
            month += "/" + this.months[endDate.date().getMonth()]
        }

        return month + " " + startDate.date().getDate() + ". - " + endDate.date().getDate() + ".";
    }

    getSinceDate(): Date {
        let firstDay: LmnDate = this.calendar.getPage().clone();
        firstDay.removeDay(firstDay.getDay());
        firstDay.date().setHours(0);
        firstDay.date().setMinutes(0);
        firstDay.date().setSeconds(0);
        return firstDay.date();
    }

    getUntilDate(): Date {
        let lastDay: LmnDate = this.calendar.getPage().clone();
        lastDay.addDay(6 - lastDay.getDay());
        lastDay.date().setHours(23);
        lastDay.date().setMinutes(59);
        lastDay.date().setSeconds(59);
        return lastDay.date();
    }

    next(): void {
        this.calendar.getPage().addWeek(1);
        this.onChange();
    }

    previous(): void {
        this.calendar.getPage().removeWeek(1);
        this.onChange();
    }

    select(date: Date): void {

    }

    hasCurrent(): boolean {
        let today: Date = new Date();
        let since = this.getSinceDate();
        let until = this.getUntilDate();
        return (since.getTime() <= today.getTime() && until.getTime() >= today.getTime());
    }
}
