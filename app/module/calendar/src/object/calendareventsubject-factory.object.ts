import { ModelService, ModelFactory, Model } from 'core/index';

import { CalendareventsubjectModel } from './calendareventsubject.model';

export class CalendareventsubjectFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new CalendareventsubjectModel(this.modelService);
        model.fill(data);
        return model;
    }
}
