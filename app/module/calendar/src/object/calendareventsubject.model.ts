import { ModelService, BasicModel, Model } from 'core/index';

export class CalendareventsubjectModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    export(): any {
        return {
            subject_id: this.get('subject_id'),
            calendarevent_id: this.get('calendarevent_id')
        };
    }
}
