import { AbstractControl } from '@angular/forms';

import { ValidationRule } from 'core/index';

export class DateRangeValidation implements ValidationRule {

    private config: any;

    constructor(config: any) {
        this.config = config;
    }

    eval(formGroup: AbstractControl): any {
        if (!this.config.since || !this.config.until) {
            //TODO: log error
            return null;
        }
        if (!formGroup.get(this.config.since) || !formGroup.get(this.config.until)) {
            //TODO: log error
            return null;
        }

        if (!formGroup.get(this.config.since).value || !formGroup.get(this.config.until).value) {
            return null;
        }

        let sinceDate: Date = formGroup.get(this.config.since).value;
        let untilDate: Date = formGroup.get(this.config.until).value;
        if (sinceDate.getTime() <= untilDate.getTime()) {
            return null;
        }

        formGroup.get(this.config.until).setErrors({'daterange': true});
        return {
            daterange: true
        };
    }
}