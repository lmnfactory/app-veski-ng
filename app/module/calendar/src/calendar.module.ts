import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MyDatePickerModule } from 'mydatepicker';

import { CoreModule, ModelService, ValidationService } from 'core/index';
import { AccountModule } from 'account/index';
import { FormUIModule } from 'form-ui/index';
import { NotificationModule, NotificationService, NotificationFactory } from 'notification/index';
import { StateModule, StateService } from 'state/index';
import { ColorModule } from 'lmn-color/index';
import { CalendareventModule } from 'calendarevent/index';

import { CalendareventsubjectFactory } from './object/calendareventsubject-factory.object';
import { CalendareventuserFactory } from './object/calendareventuser-factory.object';
import { CalendardayFactory } from './object/calendarday-factory.object';
import { CalendarComponent } from './component/calendar.component';
import { MobileCalendarComponent } from './component/mobile-calendar.component';
import { EventCreateFormComponent } from './component/event-create-form.component';
import { CalendarMonthCellComponent } from './component/calendar-month-cell.component';
import { CalendarWeekCellComponent } from './component/calendar-week-cell.component';
import { CalendarWeekEventComponent } from './component/calendar-week-event.component';
import { UpcommingEventComponent } from './component/upcomming-event.component';
import { CalendarService } from './service/calendar.service';
import { NotificationExecHandlerService } from './service/notification-exec-handler.service';

import { DateRangeValidation } from './object/date-range-validation.oject';

@NgModule({
    imports: [ CommonModule, FormsModule, ReactiveFormsModule, FlexLayoutModule, CoreModule, ColorModule, FormUIModule, MyDatePickerModule, CalendareventModule ],
    declarations: [ CalendarComponent, CalendarMonthCellComponent, EventCreateFormComponent, CalendarWeekCellComponent, CalendarWeekEventComponent, MobileCalendarComponent, UpcommingEventComponent ],
    exports: [ CalendarComponent, CalendarMonthCellComponent, MobileCalendarComponent ],
    providers: [ CalendarService, NotificationExecHandlerService ]
})
export class CalendarModule {

    constructor(modelService: ModelService, stateService: StateService, validationService: ValidationService, notificationFactory: NotificationFactory, notificationExecHandlerService: NotificationExecHandlerService) {
        modelService.addFactory('calendarday', new CalendardayFactory(modelService));
        modelService.addFactory('calendareventsubject', new CalendareventsubjectFactory(modelService));
        modelService.addFactory('calendareventuser', new CalendareventuserFactory(modelService));

        validationService.addRule('daterange', DateRangeValidation);
        validationService.addMessage('daterange', 'Ako chceš skôr skončiť ako začať?');

        notificationFactory.addAfterCreate('calendarevent.create', notificationExecHandlerService.onCalendareventCreate.bind(notificationExecHandlerService));
    }
}
