import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from 'core/index';
import { HeaderService } from './service/header.service';

@NgModule({
    imports: [ CommonModule, CoreModule ],
    exports: [],
    declarations: [],
    providers: [ HeaderService ],
})
export class HeaderModule { }
