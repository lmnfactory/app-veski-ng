import { Injectable } from '@angular/core';

import { EventHandler, Exec } from 'core/index';

import { HeaderTab } from '../object/header-tab.object';

@Injectable()
export class HeaderService {

    private eventHandler: EventHandler;
    private _data: any;

    constructor(eventHandler: EventHandler) {
        this.eventHandler = eventHandler;
        this._data = {};
    }

    private notify(key: string): void {
        this.eventHandler.fire('header.change', key);
    }

    set(key: string, value: any): void {
        this._data[key] = value;
        this.notify(key);
    }

    get(key: string): any {
        return this._data[key];
    }

    createTab(name: string, exec: Exec, selected: boolean = false): HeaderTab {
        return new HeaderTab(name, exec, selected);
    }
}