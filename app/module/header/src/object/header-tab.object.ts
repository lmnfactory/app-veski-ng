import { Exec } from 'core/index';

export class HeaderTab {

    private name: string;
    private exec: Exec;
    private selected: boolean;

    constructor(name: string, exec: Exec, selected: boolean = false) {
        this.name = name;
        this.exec = exec;
        this.selected = selected;
    }

    execute(): void {
        if (!this.exec) {
            return;
        }
        this.exec.execute();
    }

    getName(): string {
        return this.name;
    }

    isSelected(): boolean {
        return this.selected;
    }

    select(): void {
        this.selected = true;
    }

    deselect(): void {
        this.selected = false;
    }
}