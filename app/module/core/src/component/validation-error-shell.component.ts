import { Component, Input,  OnInit, OnDestroy } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'lmn-validation-error-shell',
    templateUrl: 'validation-error-shell.component.html'
})

export class ValidationErrorShellComponent implements OnInit {

    @Input('lmnControl')
    public control: AbstractControl;

    constructor() { }

    ngOnInit() { }

    ngOnDestroy(): void {}

    isInvalid(): boolean {
        return (this.control.touched && this.control.invalid);
    }
}