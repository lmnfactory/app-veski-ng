import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-no-content-text',
    templateUrl: 'no-content-text.component.html',
    styleUrls: ['no-content-text.component.css']
})

export class NoContentTextComponent implements OnInit {

    @Input('lmnSmall')
    private _small: boolean;

    constructor() { }

    ngOnInit() { }

    isSmall(): boolean {
        return this._small;
    }
}