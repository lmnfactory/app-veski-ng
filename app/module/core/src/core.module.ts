import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { FlexLayoutModule } from "@angular/flex-layout";

import { HttpCommunicationModule, ConnectionService } from 'http-communication/index';

import { NoContentTextComponent } from './component/no-content-text.component';
import { ValidationErrorClassDirective } from './directive/validation-error-class.directive';
import { ValidationErrorShellComponent } from './component/validation-error-shell.component';
import { Observer } from './object/observer.interface';
import { Observable } from './object/observable.interface';
import { AbstractObservable } from './object/observable.abstract';

import { CacheService } from './service/cache.service';
import { ModelService } from './service/model.service';
import { EventHandler } from './service/event-handler.service';
import { InitLoadingService } from './service/init-loading.service';
import { ValidationService } from './service/validation.service';
import { InfiniteScrollService } from './service/infinite-scroll.service';
import { GarbageService } from './service/garbage.service';

@NgModule({
    imports:      [ CommonModule, FormsModule, RouterModule, FlexLayoutModule, HttpCommunicationModule ],
    providers: [ CacheService, ModelService, EventHandler, InitLoadingService, ValidationService, InfiniteScrollService, GarbageService ],
    declarations: [ ValidationErrorClassDirective, NoContentTextComponent, ValidationErrorShellComponent ],
    exports: [ ValidationErrorClassDirective, NoContentTextComponent, ValidationErrorShellComponent ]
})
export class CoreModule {
    constructor(validationService: ValidationService, eventHandler: EventHandler, connectionService: ConnectionService) {
        validationService.addMessage('required', '%LABEL% je povinný údaj.');
        validationService.addMessage('maxlength', 'Fúha, nepočítali sme s tým, že niekto sa tak rozpíše v obyčajom formulári. Prosím skráť dĺžku textu v označených poliach');
        validationService.addMessage('minlength', 'Tvoje navrhované heslo je zlé. Teda nič proti nemu, ale musí mať aspoň 6 znakov');

        connectionService.subscribe((state: boolean) => {
            if (state) {
                eventHandler.fire('connection.online', state);
            } else {
                eventHandler.fire('connection.offline', state);
            }
        });
    }
}
