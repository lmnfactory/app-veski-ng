import { Directive, Input, Renderer2, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Directive({ selector: '[lmnValidationError]' })
export class ValidationErrorClassDirective implements OnInit {

    @Input('lmnValidationError')
    private control: AbstractControl;
    private renderer: Renderer2;
    private element: ElementRef
    private sub: any;

    constructor(renderer: Renderer2, element: ElementRef) { 
        this.renderer = renderer;
        this.element = element;
        this.sub = null;
    }

    ngOnInit(): void {
        this.sub = this.control.statusChanges
            .subscribe((data: any) => {
                if (this.control.touched && this.control.invalid) {
                    this.renderer.addClass(this.element.nativeElement, 'validation-error');
                }
                else {
                    this.renderer.removeClass(this.element.nativeElement, 'validation-error');
                }
            });
    }

    ngOnDestroy(): void {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }
}