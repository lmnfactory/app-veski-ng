import { Model } from './model.interface';

import *  as moment from 'moment/moment';

export abstract class BasicModel implements Model {

    protected _data: any;
    protected _value: any;
    private getterPrefix: string;
    private setterPrefix: string;

    constructor() {
        this.getterPrefix = '_get';
        this.setterPrefix = '_set';
        this._value = {};
    }

    protected shouldCascade(key: string): boolean {
        return (key.indexOf('.') > -1);
    }

    protected cascadeObject(key: string, object: any): any {
        let split: string[];
        split = key.split(".");

        let i: number = 0;
        for (i = 0; i < split.length; i++) {
            if (object[split[i]] === undefined) {
                return undefined;
            }
            object = object[split[i]];
        }
        return object;
    }

    protected cascadeGet(key: string): any {
        let split: string[];
        split = key.split(".");
        let index: string[] = split.splice(0, 1);
        let object: any = this.get(index[0]);
        if (object === undefined || object === null) {
            return null;
        }
        if (object.get !== undefined && typeof object.get === "function") {
            return object.get(split.join("."));
        }
        return this.cascadeObject(split.join("."), object);
    }

    protected cascadeSet(key: string, value: any): any {
        let split: string[];
        split = key.split(".");
        let index: string[] = split.splice(0, 1);
        let object: any = this.get(index[0]);
        if (object === undefined || object === null) {
            return null;
        }
        return object.set(split.join("."), value);
    }

    private toPascalCase(str: string): string {
        let i: number = 0;
        let result: string = "";
        let prev: string = "";
        for (i = 0; i < str.length; i++) {
            if (i == 0 || prev == "_") {
                result += str.charAt(i).toUpperCase();
            }
            else if (str.charAt(i) != "_") {
                result += str.charAt(i);
            }
            prev = str.charAt(i);
        }
        return result
    }

    abstract export(): any;

    fill(data: any): void {
        if (data == null || data == undefined) {
            data = {};
        }
        this._data = data;
        this._value = {};

        // let key: string = "";
        // for (key in data) {
        //     if (data.hasOwnProperty(key)){
        //         this.sync(key);
        //     }
        // }
    }

    getter(value: any): any {
        return value;
    }

    getGetter(key: string): string {
        let pascalCase: string = this.toPascalCase(key);
        let functionName = this.getterPrefix + pascalCase;
        if (typeof this[functionName] === "function") {
            return functionName;
        }
        return 'getter';
    }

    get(key: string): any {
        if (this.shouldCascade(key)) {
            return this.cascadeGet(key);
        }
        if (this._value[key] === undefined) {
            this.sync(key);
            if (this._value[key] === undefined) {
                //throw new Error("Unknown model value in dot notation key chain '" + key + "'");
            }
        }
        if (this._value[key] === undefined) {
            return null;
        }
        return this._value[key];
    }

    sync(key: string): void {
        let getterFunctionName = this.getGetter(key);
        this._value[key] = this[getterFunctionName](this._data[key]);
        //this[key] = this[getterFunctionName](this._data[key]);
    }

    setter(value: any): any {
        return value;
    }

    getSetter(key: string): string {
        let pascalCase: string = this.toPascalCase(key);
        let functionName = this.setterPrefix + pascalCase;
        if (typeof this[functionName] === "function") {
            return functionName;
        }
        return 'setter';
    }

    set(key: string, value: any) {
        if (this.shouldCascade(key)) {
            return this.cascadeSet(key, value);
        }
        let setterFunctionName = this.getSetter(key);
        this._data[key] = this[setterFunctionName](value);
        this.sync(key);
    }

    data(): any {
        return this._data;
    }

    value(): any {
        return this._value;
    }

    _getCreatedAt(data: any): any {
        return moment(data).toDate();
    }
}
