export class LmnDate {

    private dateValue: Date;

    constructor(date: Date) {
        this.dateValue = date;
    }

    addSeconds(seconds: number): void {
        this.dateValue.setTime(this.dateValue.getTime()  + seconds * 1000);
    }

    addMinute(minutes: number): void {
        this.dateValue.setTime(this.dateValue.getTime()  + minutes * 60 * 1000);
    }

    addHour(hours: number): void {
        this.dateValue.setTime(this.dateValue.getTime()  + hours * 60 * 60 * 1000);
    }

    addDay(days: number): void {
        this.dateValue.setTime(this.dateValue.getTime()  + days * 24 * 60 * 60 * 1000);
    }

    removeDay(days: number): void {
        this.addDay(-days);
    }

    addWeek(weeks: number): void {
        this.addDay(weeks * 7);
    }

    removeWeek(weeks: number): void {
        this.addWeek(-weeks);
    }

    addMonth(months: number): void {
        let month = this.dateValue.getMonth() + months;
        let years = Math.floor(month / 12);
        let day = this.dateValue.getDate();
        month = month - years * 12;

        this.dateValue.setFullYear(this.dateValue.getFullYear() + years);
        this.dateValue.setMonth(month);

        if (this.dateValue.getMonth() != month) {
            this.dateValue.setDate(day - this.dateValue.getDate());
            this.dateValue.setMonth(month);
        }
    }

    addYear(years: number): void {

    }

    date(): Date {
        return this.dateValue;
    }

    /**
     * Wrong when there is a time ... (posun casu)
     * @method diffInDays
     * @param  {LmnDate}  date [description]
     * @deprecated
     * @return {number}        [description]
     */
    diffInDays(date: LmnDate): number {
        return Math.floor(((date.date().getTime() - this.date().getTime()) / (24 * 60 * 60 * 1000)));
    }

    getDay(): number {
        if (this.date().getDay() == 0) {
            return 6;
        }
        return (this.date().getDay() - 1);
    }

    clone(): LmnDate {
        return new LmnDate(new Date(this.dateValue.getTime()));
    }

    isToday(): boolean {
        let today: LmnDate = new LmnDate(new Date());
        return (today.date().getDate() == this.date().getDate() && today.date().getMonth() == this.date().getMonth() && today.date().getFullYear() == this.date().getFullYear());
    }

    isTomorrow(): boolean {
        let tomorrow: LmnDate = new LmnDate(new Date());
        tomorrow.addDay(1);
        return (tomorrow.date().getDate() == this.date().getDate() && tomorrow.date().getMonth() == this.date().getMonth() && tomorrow.date().getFullYear() == this.date().getFullYear());
    }
}
