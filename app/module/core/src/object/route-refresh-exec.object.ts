import { Router } from '@angular/router';

import { Exec } from 'core/index';

export class RouteRefreshExec implements Exec {

    private url: any;
    private router: Router;
    private refresh: any;

    constructor(navigateUrl: any, router: Router, refresh: any) {
        this.url = navigateUrl;
        this.router = router;
        this.refresh = refresh;
    }

    execute(): void {
        let urlTree: any = this.router.createUrlTree(this.url);
        if (this.router.isActive(urlTree, true)) {
            this.refresh();
        }
        else {
            this.router.navigate(this.url);
        }
    }
}
