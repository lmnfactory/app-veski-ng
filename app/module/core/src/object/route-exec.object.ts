import { Router } from '@angular/router';

import { Exec } from 'core/index';

export class RouteExec implements Exec {

    private url: string;
    private data: string;
    private router: Router;

    constructor(url: string, data: any, router: Router) {
        this.url = url;
        this.data = data;
        this.router = router;
    }

    execute(): void {
        this.router.navigate([this.url, this.data]);
    }
}
