export class LmnInfiniteScroll {

    private loading: boolean;
    private _canLoad: boolean;
    private count: number;

    constructor() {
        this.reset();
    }

    private canLoad(): boolean {
        return this._canLoad;
    }

    private isLoading(): boolean {
        return this.loading;
    }

    reset(): void {
        this.loading = false;
        this._canLoad = true;
        this.count = 0;
    }

    resolve(canLoad: boolean): void {
        this.loading = false;
        this._canLoad = canLoad;
    }

    resolveCount(count: number): void {
        this.resolve(this.count < count);
        this.count = count;
    }

    load(callback: any): void {
        if (!this.canLoad() || this.isLoading()) {
            return;
        }

        this.loading = true;
        callback(this.resolve.bind(this));
    }
}