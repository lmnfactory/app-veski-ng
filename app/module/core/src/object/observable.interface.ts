import { Observer } from './observer.interface';

export interface Observable {
    subscribe(observer: Observer, key?: String): void;
    unsubscribe(observer: Observer): void;
    notifyAll(): void;
}
