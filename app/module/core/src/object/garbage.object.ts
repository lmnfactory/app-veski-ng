export class Garbage {

    private events: any[];
    private subscriptions: any[];

    constructor() {
        this.events = [];
        this.subscriptions = [];
    }

    event(e: any): void {
        this.events.push(e)
    }

    subscription(s: any): void {
        this.subscriptions.push(s);
    }

    destroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
        for (let i: number = 0; i < this.subscriptions.length; i++) {
            this.subscriptions[i].unsubscribe();
        }
    }
}