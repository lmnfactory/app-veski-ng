export interface Model {

    get(key: string): any;
    set(key:string, value: any): void;
    fill(data: any): void;
    export(): any;
    data(): void;
    value(): void;
}
