import { Exec } from './exec.interface';

export class TabExec implements Exec {

    private tabset: any;
    private tabId: string;

    constructor(tabset: any, tabId: string) {
        this.tabset = tabset;
        this.tabId = tabId;
    }

    execute(): void {
        this.tabset.select(this.tabId);
    }
}
