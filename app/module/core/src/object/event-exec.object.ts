import { Router } from '@angular/router';

import { Exec } from './exec.interface';
import { EventHandler } from '../service/event-handler.service';

export class EventExec implements Exec {

    private event: string;
    private eventData: any;
    private eventHandler: EventHandler;

    constructor(eventHandler: EventHandler, event: string, eventData: any = {}) {
        this.event = event;
        this.eventData = eventData;
        this.eventHandler = eventHandler;
    }

    execute(): void {
        this.eventHandler.fire(this.event, this.eventData);
    }
}
