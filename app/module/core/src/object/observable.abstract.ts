import { Observable } from './observable.interface';
import { Observer } from './observer.interface';
import { ObserverMap } from './observer-map.object';

export abstract class AbstractObservable implements Observable {

    private map: Map<Observer, Observer>;

    constructor() {
        this.map = new Map();
    }

    subscribe(observer: Observer, key?: String): void {
        this.map.set(observer, new ObserverMap(observer, key));
    }

    unsubscribe(observer: Observer): void {
        this.map.delete(observer);
    }

    notifyAll(): void {
        this.map.forEach((mapped: Observer, origin: Observer) => {
            mapped.update();
        });
    }
}
