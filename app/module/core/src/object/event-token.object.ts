import { EventListener } from './event-listener.object';

export class EventToken {

    private listener: EventListener;

    constructor() {
        this.listener = new EventListener();
    }

    fire(data: any = {}): void {
        this.listener.fire(data);
    }

    subscribe(callback: any): void {
        return this.listener.on(callback);
    }

    unsubscribe(callback: any): void {
        this.listener.off(callback);
    }
}
