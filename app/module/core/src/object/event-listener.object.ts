export class EventListener {

    private callbacks: any[];

    constructor() {
        this.callbacks = [];
    }

    fire(data: any): void {
        for (let callback of this.callbacks) {
            callback(data);
        }
    }

    on(callback: any): any {
        this.callbacks.push(callback);

        return function() {
            this.off(callback);
        }.bind(this);
    }

    off(callback: any): void {
        let index = this.callbacks.indexOf(callback);
        if (index > -1) {
            this.callbacks.splice(index, 1);
        }
    }
}
