import { Model } from './model.interface';

export interface ModelFactory {
    create(data: any): Model;
}
