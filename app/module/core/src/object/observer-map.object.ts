import { Observer } from './observer.interface';

export class ObserverMap implements Observer {

    private key: String;
    private observer: Observer;

    constructor(observer: Observer, key?: String) {
        this.observer = observer;
        this.key = key;
    }

    update(key?: String) {
        this.observer.update(this.key);
    }
}
