export interface Observer {
    update(key?: String): void;
}
