import { AbstractControl } from '@angular/forms';

export interface ValidationRule {
    eval(control: AbstractControl): any;
}