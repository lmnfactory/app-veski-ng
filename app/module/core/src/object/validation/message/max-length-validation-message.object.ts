import { AbstractControl } from '@angular/forms';

import { ValidationMessage } from './validation-message.interface'

export class MaxLengthValidationMessage implements ValidationMessage {

    createMessage(label: string, control: AbstractControl, message: string): string {
        if (!control.errors || !control.errors.maxlength || !control.errors.maxlength.requiredLength) {
            return message;
        }
        message = message.replace("%0%", control.errors.maxlength.requiredLength);
        return message;
    }
}