import { AbstractControl } from '@angular/forms';

export interface ValidationMessage {
    createMessage(label: string, control: AbstractControl,  message: string): string;
}