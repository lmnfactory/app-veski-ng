import { Exec } from './exec.interface';

export class EmptyExec implements Exec {

    constructor() {

    }

    execute(): void {

    }
}
