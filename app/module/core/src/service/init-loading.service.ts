import { Injectable } from '@angular/core';

import { EventHandler } from './event-handler.service';

@Injectable()
export class InitLoadingService {

    private queue: any;
    private count: number;
    private eventHandler: EventHandler;

    constructor(eventHandler: EventHandler) {
        this.queue = {};
        this.count = 0;
        this.eventHandler = eventHandler;
    }

    start(key: string): void {
        this.queue[key] = true;
        if (this.count == 0) {
            this.eventHandler.fire('loading.start', {});
        }
        this.count++;
    }

    end(key: string): void {
        if (this.queue[key] === undefined) {
            return;
        }
        delete this.queue[key];
        this.count--;
        if (this.count == 0) {
            this.eventHandler.fire('loading.end', {});
        }
        else if (this.count < 0) {
            this.count = 0;
        }
    }

    fail(key: string): void {
        if (this.queue[key] === undefined) {
            return;
        }
        delete this.queue[key];
        this.count--;
        this.eventHandler.fire('loading.end', {failed: key});
        if (this.count < 0) {
            this.count = 0;
        }
    }

    isLoading(): boolean {
        return (this.count > 0);
    }
}
