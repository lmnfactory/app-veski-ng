import { Injectable } from '@angular/core';
import { FormGroup, AbstractControl, FormControl } from '@angular/forms';

import { ValidationRule } from '../object/validation/rule/validation-rule.interface';
import { ValidationMessage } from '../object/validation/message/validation-message.interface';
import { MaxLengthValidationMessage } from '../object/validation/message/max-length-validation-message.object';

@Injectable()
export class ValidationService {

    private messages: any;
    private messageRules: any;
    private _rules: any;

    constructor() {
        this.messages = {};
        this._rules = {};
        this.messageRules = {
            maxlength: new MaxLengthValidationMessage(),
        };
    }

    private getMessage(error: string): string {
        if (!this.messages[error]) {
            return error;
        }
        return this.messages[error];
    }

    private hasMessage(error: string): string {
        return this.messages.hasOwnProperty(error);
    }

    markAllAsTouched(form: FormGroup): void {
        Object.keys(form.controls).forEach(field => {
            const control = form.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.markAllAsTouched(control);
            }
        });
    }

    addMessage(validator: string, message: string): void {
        this.messages[validator] = message;
    }

    private createMessage(error: string, label: string, control: AbstractControl, message: string): string {
        message = message.replace("%LABEL%", label);
        if (this.messageRules[error]) {
            message = this.messageRules[error].createMessage(label, control, message);
        }
        return message;
    }

    validate(form: FormGroup, controls: any): any {
        let finalErrors: string[] = [];
        for (var control in controls) {
            if (!controls.hasOwnProperty(control) || !form.get(control)) {
                continue;
            }
            let label: any = controls[control];
            let formControl = form.get(control);
            
            if (!formControl.touched || !formControl.errors) {
                continue;
            }
            for (var error in formControl.errors) {
                if (!formControl.errors.hasOwnProperty(error)) {
                    continue;
                }
                let message: string
                if (this.hasMessage(error)) {
                    message = this.getMessage(error);
                } else {
                    message = formControl.errors[error];
                }
                if (label !== false) {
                    finalErrors.push(this.createMessage(error, label, formControl, message));
                }
            }
        }

        return finalErrors;
    }

    validateAll(form: FormGroup, controls: any): any {
        this.markAllAsTouched(form);
        return this.validate(form, controls);
    }

    addRule(key: string, ruleClass: any): void {
        this._rules[key] = ruleClass;
    }

    rule(key: string, config: any = {}): any {
        if (this._rules[key]) {
            let rule: ValidationRule = new this._rules[key](config);
            return rule.eval.bind(rule);
        }
        console.log("Validation rule '" + key + "' does not exists");
        return function(): any{
            return null;   
        };
    }

    serverError(form: FormGroup, response: any): void {
        let data: any = response.data;
        const fields = Object.keys(data || {});
        fields.forEach((field) => {
            const control = form.get(field);
            const error = data[field];
            control.setErrors({server: error.message});
        });
    }
}