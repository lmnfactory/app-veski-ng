import { Injectable } from '@angular/core';

import { Model } from '../object/model.interface';
import { ModelFactory } from '../object/model-factory.interface';

@Injectable()
export class ModelService {

    private factoriers: any;

    constructor() {
        this.factoriers = {};
    }

    addFactory(key: string, factory: ModelFactory): void {
        this.factoriers[key] = factory;
    }

    create(key: string, data: any = {}): Model {
        return this.factoriers[key].create(data);
    }
}
