import { Injectable } from '@angular/core';

import { LmnInfiniteScroll } from '../object/infinite-scroll.object';

@Injectable()
export class InfiniteScrollService {

    constructor() { }

    create(): LmnInfiniteScroll {
        return new LmnInfiniteScroll();
    }
}