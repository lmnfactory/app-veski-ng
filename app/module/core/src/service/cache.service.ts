import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable as HttpObservable } from 'rxjs/Observable';
import { LmnHttp } from 'http-communication/index';

import { AbstractObservable } from '../object/observable.abstract';

@Injectable()
export class CacheService extends AbstractObservable {

    private cacheData: any;
    private http: LmnHttp;

    constructor(http: LmnHttp) {
        super();
        this.http = http;
        this.cacheData = {};
    }

    all(): any {
        return this.cacheData;
    }

    get(key: string): any[] {
        if (!this.cacheData[key]) {
            return [];
        }
        return this.cacheData[key];
    }

    refresh(): void {
        this.http.post('/app/cache', {})
            .subscribe((data: any) => {
                this.cacheData = data.data;
                this.notifyAll();
            });
    }
}
