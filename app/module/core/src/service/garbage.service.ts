import { Injectable } from '@angular/core';

import { Garbage } from '../object/garbage.object';

@Injectable()
export class GarbageService {

    constructor() {

    }

    create(): Garbage {
        return new Garbage();
    }
}