import { Observer } from '../object/observer.interface';
import { CacheService } from '../service/cache.service';

export abstract class AbstractCacheable implements Observer {

    protected cacheService: CacheService;

    constructor(cacheService: CacheService) {
        this.cacheService = cacheService;
        this.cacheService.subscribe(this, 'cache');
    }

    update(key?: string) {
        if (key == 'cache') {
            this.onCacheChange();
        }
        else {
            this.onUpdate(key);
        }
    }

    onUpdate(key?: string): void {

    }

    abstract onCacheChange(): void;
}
