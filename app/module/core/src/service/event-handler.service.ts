import { Injectable } from '@angular/core';

import { EventListener } from '../object/event-listener.object';

@Injectable()
export class EventHandler {

    private listeners: Map<string, EventListener>;

    constructor() {
        this.listeners = new Map();
    }

    addListener(key: string): void {
        this.listeners.set(key, new EventListener());
    }

    fire(key: string, data: any = {}): void {
        if (!this.listeners.has(key)) {
            this.addListener(key);
        }

        this.listeners.get(key).fire(data);
    }

    on(key: string, callback: any): any {
        if (!this.listeners.has(key)) {
            this.addListener(key);
        }

        return this.listeners.get(key).on(callback);
    }

    off(key: string, callback: any): void {
        if (!this.listeners.has(key)) {
            return ;
        }

        this.listeners.get(key).off(callback);
    }
}
