export { UniversityFactory } from './object/university-factory.object';
export { UniversityService } from './service/university.service';
export { UniversitypersonService } from './service/universityperson.service';
export { UniversityModel } from './object/university.model';

export { UniversityModule } from './university.module';
