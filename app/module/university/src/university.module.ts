import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CoreModule, ModelService } from 'core/index';

import { UniversityFactory } from './object/university-factory.object';
import { UniversitypersonFactory } from './object/universityperson-factory.object';
import { UniversityService } from './service/university.service';
import { UniversitypersonService } from './service/universityperson.service';

@NgModule({
    imports: [ CommonModule, CoreModule ],
    declarations: [ ],
    providers: [ UniversityService, UniversitypersonService ],
    exports: [ ]
})
export class UniversityModule {

    constructor(modelService: ModelService) {
        modelService.addFactory('university', new UniversityFactory(modelService));
        modelService.addFactory('universityperson', new UniversitypersonFactory(modelService));
    }
}
