import { Injectable } from '@angular/core';

import { AbstractCacheable, CacheService, Model, ModelService } from 'core/index';

@Injectable()
export class UniversityService extends AbstractCacheable {

    private modelService: ModelService;
    private universities: Model[];

    constructor(cacheService: CacheService, modelService: ModelService) {
        super(cacheService);
        this.modelService = modelService;
        this.universities = [];
        this.onCacheChange();
    }

    onCacheChange(): void {
        let rawUniversity: any[] = this.cacheService.get('university');

        this.universities = [];
        for (let university of rawUniversity) {
            this.universities.push(this.modelService.create('university', university));
        }
    }

    get(id: number): Model {
        for (let i: number = 0; i < this.universities.length; i++) {
            if (id != this.universities[i].get('id')) {
                continue;
            }
            return this.universities[i];
        }

        return null;
    }
}
