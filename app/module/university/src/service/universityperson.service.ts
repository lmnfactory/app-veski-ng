import { Injectable } from '@angular/core';

import { AbstractCacheable, CacheService, Model, ModelService } from 'core/index';

@Injectable()
export class UniversitypersonService extends AbstractCacheable {

    private modelService: ModelService;
    private persons: any;

    constructor(cacheService: CacheService, modelService: ModelService) {
        super(cacheService);
        this.modelService = modelService;
        this.persons = {};
        this.onCacheChange();
    }

    get(key: string): number {
        if (this.persons[key]) {
            return this.persons[key];
        }
        return 0;
    }

    onCacheChange(): void {
        this.persons = this.cacheService.get('universitypersontype');
    }
}
