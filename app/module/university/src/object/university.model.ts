import { ModelService, BasicModel, Model } from 'core/index';

export class UniversityModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    export(): any {
        return {};
    }
}
