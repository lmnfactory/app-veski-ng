import { ModelService, ModelFactory, Model } from 'core/index';

import { UniversitypersonModel } from './universityperson.model';

export class UniversitypersonFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new UniversitypersonModel(this.modelService);
        model.fill(data);
        return model;
    }
}
