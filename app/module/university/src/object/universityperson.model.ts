import { ModelService, BasicModel, Model } from 'core/index';

export class UniversitypersonModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getPerson(data: any): Model {
        return this.modelService.create('person', data);
    }

    export(): any {
        return {};
    }
}
