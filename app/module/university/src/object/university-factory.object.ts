import { ModelService, ModelFactory, Model } from 'core/index';

import { UniversityModel } from './university.model';

export class UniversityFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new UniversityModel(this.modelService);
        model.fill(data);
        return model;
    }
}
