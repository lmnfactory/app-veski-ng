import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule, ModelService } from 'core/index';

import { ColorpaletteCacheService } from './service/colorpalette-cacheable.service';
import { ColorpaletteService } from './service/colorpalette.service';
import { ColorpaletteFactory } from './object/colorpalette-factory.object';
import { ColorpaletteModel } from './object/colorpalette.model';

@NgModule({
    imports: [ CommonModule, CoreModule ],
    declarations: [],
    exports: [],
    providers: [ ColorpaletteCacheService, ColorpaletteService ]
})
export class ColorModule {

    constructor(modelService: ModelService) {
        modelService.addFactory('colorpalette', new ColorpaletteFactory(modelService));
    }
}
