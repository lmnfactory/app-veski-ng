import { ModelService, ModelFactory, Model } from 'core/index';

import { ColorpaletteModel } from './colorpalette.model';

export class ColorpaletteFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new ColorpaletteModel(this.modelService);
        model.fill(data);
        return model;
    }
}
