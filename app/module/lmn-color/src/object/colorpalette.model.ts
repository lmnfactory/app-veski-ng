import { ModelService, BasicModel, Model } from 'core/index';

export class ColorpaletteModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    export(): any {
        return {};
    }
}
