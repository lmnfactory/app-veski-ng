export { ColorpaletteCacheService } from './service/colorpalette-cacheable.service';
export { ColorpaletteService } from './service/colorpalette.service';
export { ColorpaletteFactory } from './object/colorpalette-factory.object';
export { ColorpaletteModel } from './object/colorpalette.model';

export { ColorModule } from './color.module';
