import { Injectable } from '@angular/core';

import { Model } from 'core/index';

import { ColorpaletteCacheService } from './colorpalette-cacheable.service';

@Injectable()
export class ColorpaletteService {

    private classPrefix: string;
    private colorpaletteCacheService: ColorpaletteCacheService;
    private subjectColors: any;

    constructor(colorpaletteCacheService: ColorpaletteCacheService) {
        this.classPrefix = "palette-";
        this.subjectColors = {};
        this.colorpaletteCacheService = colorpaletteCacheService;
    }

    addSubject(subejctId: number, colorId: number): void {
        this.subjectColors[subejctId] = colorId;
    }

    getPaletteClass(id: number): string {
        let color: Model = this.colorpaletteCacheService.get(id);
        if (color == null) {
            return this.classPrefix + "black";
        }

        return (this.classPrefix + color.get('name'));
    }

    getSubjectPaletteClass(subjectId: number): string {
        if (this.subjectColors[subjectId] === undefined) {
            return this.getPaletteClass(0);
        }
        return this.getPaletteClass(this.subjectColors[subjectId]);
    }
}
