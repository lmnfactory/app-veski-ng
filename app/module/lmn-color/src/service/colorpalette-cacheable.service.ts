import { Injectable } from '@angular/core';

import { CacheService, AbstractCacheable, Model, ModelService } from 'core/index';

import { ColorpaletteModel } from '../object/colorpalette.model';

@Injectable()
export class ColorpaletteCacheService extends AbstractCacheable {

    private colors: Model[];

    private modelService: ModelService;

    constructor(cacheService: CacheService, modelService: ModelService) {
        super(cacheService);
        this.modelService = modelService;
        this.colors = [];
        this.onCacheChange();
    }

    all(): Model[] {
        return this.colors;
    }

    get(id: number): Model {
        let i: number = 0;
        for (i = 0; i < this.colors.length; i++) {
            if (this.colors[i].get('id') == id) {
                return this.colors[i];
            }
        }

        return null;
    }

    onCacheChange(): void {
        let rawColors = this.cacheService.get('colorpalette')

        this.colors = [];
        for (let color of rawColors) {
            this.colors.push(this.modelService.create('colorpalette', color));
        }
    }
}
