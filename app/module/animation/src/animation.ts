export { fadeIfAnimation } from './animation/fade-if.animation';
export { fadeHiddenAnimation } from './animation/fade-hidden.animation';

export { AnimationModule } from './animation.module';
