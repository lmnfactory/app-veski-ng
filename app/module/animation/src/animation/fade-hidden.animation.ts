import { trigger, state, style, animate, transition } from '@angular/animations';

export const fadeHiddenAnimation = trigger('fadeHiddenState', [
    state('1', style({
        opacity: 1
    })),
    state('0', style({
        opacity: 0
    })),
    transition('1 => 0', animate('0ms ease')),
    transition('* => 1', animate('480ms ease'))
]);
