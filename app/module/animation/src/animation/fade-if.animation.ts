import { trigger, state, style, animate, transition } from '@angular/animations';

export const fadeIfAnimation = trigger('fadeIfState', [
    state('*', style({
        opacity: 1
    })),
    state('void', style({
        opacity: 0
    })),
    transition('* => void', animate('0ms ease')),
    transition('void => *', animate('480ms ease'))
]);
