import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { HttpInterceptor } from './service/http-interceptor.service';
import { LmnHttp } from './service/lmn-http.service';
import { FileUploadService } from './service/file-upload.service';
import { ConnectionService } from './service/connection.service';

@NgModule({
    imports: [ CommonModule, HttpModule ],
    declarations: [ ],
    exports: [ ],
    providers: [ HttpInterceptor, LmnHttp, FileUploadService, ConnectionService ],
})
export class HttpCommunicationModule {

}
