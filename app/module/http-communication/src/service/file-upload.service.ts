import { Injectable } from '@angular/core';

import { UploadProgress } from '../object/upload-progress.object';
import { LmnHttpRequest } from '../object/lmn-http-request.object';
import { HttpInterceptor } from './http-interceptor.service';

@Injectable()
export class FileUploadService {

    private interceptor: HttpInterceptor;

    constructor(interceptor: HttpInterceptor) {
        this.interceptor = interceptor;
    }

    upload(file: File): UploadProgress {
        let formData: FormData = new FormData();
        let request: LmnHttpRequest = new LmnHttpRequest('', {});
        let xhr: XMLHttpRequest = new XMLHttpRequest();
        let headers: any = {};

        let upload: UploadProgress = new UploadProgress(xhr)

        formData.append("file", file, file.name);

        xhr.open('POST', 'upload', true);
        this.interceptor.request(request);
        headers = request.getHeaders();
        for (let key in headers) {
            if (headers.hasOwnProperty(key)) {
                xhr.setRequestHeader(key, headers[key]);
            }
        }
        xhr.send(formData);

        return upload;
    }
}