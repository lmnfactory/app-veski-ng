import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/publish';
import 'rxjs/add/operator/map';

import { LmnHttpRequest } from '../object/lmn-http-request.object';
import { HttpInterceptor } from './http-interceptor.service';

@Injectable()
export class LmnHttp {

    private interceptor: HttpInterceptor;
    private http: Http;

    constructor(http: Http, interceptor: HttpInterceptor) {
        this.interceptor = interceptor;
        this.http = http;
    }

    private mapResponse(response: Response): any {
        let body = response.json();
        return body || {};
    }

    post(url: string, data: any, headers: any = {}): Observable<Response> {
        let request: LmnHttpRequest = new LmnHttpRequest(url, data);

        this.interceptor.request(request);
        for (let key in headers) {
            if (headers.hasOwnProperty(key)) {
                request.addHeader(key, headers[key]);
            }
        }

        let finalHeaders = new Headers(request.getHeaders());
        let options: RequestOptions = new RequestOptions({headers: finalHeaders});

        let observable: Observable<Response> = this.http.post(request.getUrl(), request.getBody(), options)
            .map(this.mapResponse)
            .catch((err: any, c: Observable<any>) => {return Observable.throw(err);})
            .share();

        observable.subscribe(data => {
            this.interceptor.response(data);
        }, (err: any) => {return err;});

        return observable;
    }
}
