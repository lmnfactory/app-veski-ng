import { Injectable } from '@angular/core';

@Injectable()
export class ConnectionService {

    private connectionState: boolean;
    private callbacks: any[];

    constructor() {
        this.connectionState = navigator.onLine;
        this.callbacks = [];

        window.addEventListener('online', this.online.bind(this));
        window.addEventListener('offline', this.offline.bind(this));
    }

    private change(): void {
        for (let i: number = 0; i < this.callbacks.length; i++) {
            this.callbacks[i](this.connectionState);
        }
    }

    subscribe(callback: any): any {
        this.callbacks.push(callback);
        
        return function() {
            this.unsubscribe(callback);
        }.bind(this);
    }

    unsubscribe(callback: any): void {
        let index = this.callbacks.indexOf(callback);
        if (index > -1) {
            this.callbacks.splice(index, 1);
        }
    }

    online(): void {
        console.log("online");
        if (this.connectionState) {
            return;
        }
        this.connectionState = true;
        this.change();
    }

    offline(): void {
        console.log("offline");
        if (!this.connectionState) {
            return;
        }
        this.connectionState = false;
        this.change();
    }
}