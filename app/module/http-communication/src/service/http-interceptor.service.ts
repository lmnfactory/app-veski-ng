import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { LmnHttpRequest } from '../object/lmn-http-request.object';
import { RequestInterceptor } from '../object/request-interceptor.interface';
import { ResponseInterceptor } from '../object/response-interceptor.interface';

@Injectable()
export class HttpInterceptor {

    private requestInterceptors: RequestInterceptor[];
    private responseInterceptors: ResponseInterceptor[];

    constructor() {
        this.requestInterceptors = [];
        this.responseInterceptors = [];
    }

    addRequest(interceptor: RequestInterceptor): void {
        this.requestInterceptors.push(interceptor);
    }

    addResponse(interceptor: ResponseInterceptor): void {
        this.responseInterceptors.push(interceptor);
    }

    request(request: LmnHttpRequest): void {
        let i: number = 0;
        for (i = 0; i < this.requestInterceptors.length; i++) {
            this.requestInterceptors[i].execute(request);
        }
    }

    response(data: any): void {
        let i: number = 0;
        for (i = 0; i < this.responseInterceptors.length; i++) {
            this.responseInterceptors[i].execute(data);
        }
    }
}
