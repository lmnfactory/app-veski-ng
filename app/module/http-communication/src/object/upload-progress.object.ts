import { Observable } from 'rxjs/Rx';

export class UploadProgress {

    private _progress: Observable<any>;
    private _request: Observable<any>;
    private _done: boolean;
    private value: any;

    constructor(xhr: any) {
        this._done = false;
        this._request = Observable.create((observer: any) => {
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        this._done= true;
                        this.value = JSON.parse(xhr.response);
                        observer.next(this.value);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            };
        });
        this._progress = Observable.create((observer: any) => {
            xhr.upload.onprogress = (event: any) => {
                let p: number = Math.round(event.loaded / event.total * 100);
                
                observer.next(p);
            };
        });

        this._request.subscribe((data: any) => {});
        this._progress.subscribe((data: any) => {});
    }

    isDone(): boolean {
        return this._done;
    }

    getId(): any {
        return this.value;
    }

    subscribe(done: any, error: any = (e: any) => {return e;}): UploadProgress {
        this._request.subscribe(done, error);
        return this;
    }

    progress(callback: any): UploadProgress {
        this._progress.subscribe(callback);
        return this;
    }
}