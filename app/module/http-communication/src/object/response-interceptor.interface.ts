export interface ResponseInterceptor {
    execute(data: any): void;
}
