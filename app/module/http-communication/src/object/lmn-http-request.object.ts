export class LmnHttpRequest {

    private url: string;
    private body: any;
    private headers: any;

    constructor(url: string, body: any, headers: any = {}) {
        this.url = url;
        this.body = body;
        this.headers = headers;
    }

    getUrl(): string {
        return this.url;
    }

    getBody(): any {
        return this.body;
    }

    getHeaders(): any {
        return this.headers;
    }

    setHeaders(headers: any) {
        this.headers = headers;
    }

    addHeader(key: string, data: any): void {
        this.headers[key] = data;
    }
}
