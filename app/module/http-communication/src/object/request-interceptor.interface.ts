import { LmnHttpRequest } from './lmn-http-request.object';

export interface RequestInterceptor {
    execute(request: LmnHttpRequest): void;
}
