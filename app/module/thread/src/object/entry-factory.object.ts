import { ModelService, ModelFactory, Model } from 'core/index';
import { TextFormaterService } from 'text/index';

import { EntryModel } from './entry.model';

export class EntryFactory implements ModelFactory {

    private modelService: ModelService;
    private textFormaterService: TextFormaterService;

    constructor(modelService: ModelService, textFormaterService: TextFormaterService) {
        this.modelService = modelService;
        this.textFormaterService = textFormaterService;
    }

    create(data: any): Model {
        let model: Model = new EntryModel(this.modelService, this.textFormaterService);
        model.fill(data);
        return model;
    }
}
