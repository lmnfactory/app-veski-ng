import { ModelService, BasicModel, Model } from 'core/index';

export class EntryvoteModel extends BasicModel {

    constructor() {
        super();
    }

    export(): any {
        return {
            user_id: this.get('user_id'),
            entry_id: this.get('entry_id'),
            value: this.get('value'),
        };
    }
}
