import { StateFactory, StateMachine, EventStateMachine, StateNode } from 'state/index';

export class ThreadServiceStateFactory implements StateFactory {

    create(): StateMachine {
        let states: StateNode[] = [];

        states.push(new StateNode('stable'));
        states.push(new StateNode('loading'));
        states.push(new StateNode('changed'));
        states[0].linkTo(states[1]);
        states[0].linkTo(states[2]);
        states[1].linkTo(states[0]);
        states[1].linkTo(states[2]);
        states[2].linkTo(states[0]);
        states[2].linkTo(states[1]);

        return new EventStateMachine(states);
    }
}
