import { Injectable } from '@angular/core';

import { Model } from 'core/index';
import { MenuService } from 'menu/index';
import { BrowserNotificationService } from 'browser-notification/index';

import { ThreadDetailService } from '../service/thread-detail.service';

@Injectable()
export class ThreadNotificationController {

    private threadDetailService: ThreadDetailService;
    private menuService: MenuService;
    private browserNotificationService: BrowserNotificationService;

    constructor(threadDetailService: ThreadDetailService, menuService: MenuService, browserNotificationService: BrowserNotificationService) {
        this.threadDetailService = threadDetailService;
        this.menuService = menuService;
        this.browserNotificationService = browserNotificationService;
    }

    onVote(notificationBody: any): void {
        this.threadDetailService.request(notificationBody.thread_id);
    }

    onDetail(notificationBody: any): void {
        this.threadDetailService.request(notificationBody.thread_id);
    }

    onCreate(notificationBody: any): void {
        this.menuService.incNotification(notificationBody.subject_id);
        this.browserNotificationService.build()
            .title("Nový thread")
            .body("Nový thread pre [" + notificationBody.subject_id + "] predmet")
            .show();
    }
}
