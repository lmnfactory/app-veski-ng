import { ModelService, ModelFactory, Model } from 'core/index';

import { ThreadModel } from './thread.model';

export class ThreadFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new ThreadModel(this.modelService);
        model.fill(data);
        return model;
    }
}
