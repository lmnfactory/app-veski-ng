import { ModelService, BasicModel, Model } from 'core/index';
import { TextFormaterService } from 'text/index';

export class EntryModel extends BasicModel {

    private modelService: ModelService;
    private textFormaterService: TextFormaterService;

    constructor(modelService: ModelService, textFormaterService: TextFormaterService) {
        super();
        this.modelService = modelService;
        this.textFormaterService = textFormaterService;
    }

    _getVote(data: any = {}): Model {
        return this.modelService.create('entryvote', data);
    }

    _getUser(data: any = {}): Model {
        return this.modelService.create('user', data);
    }

    _getFiles(data: any): any {
        let files: Model[] = [];

        if (data == undefined || data == null) {
            return files;
        }

        let i: number = 0;
        for (i = 0; i < data.length; i++) {
            files.push(this.modelService.create('file', data[i]))
        }
        return files;
    }

    export(): any {
        return {
            text: this.get('text'),
            thread_id: this.get('thread_id'),
            files: this._data.files
        };
    }
}
