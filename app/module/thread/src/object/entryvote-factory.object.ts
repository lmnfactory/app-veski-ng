import { ModelService, ModelFactory, Model } from 'core/index';

import { EntryvoteModel } from './entryvote.model';

export class EntryvoteFactory implements ModelFactory {

    constructor() {

    }

    create(data: any): Model {
        let model: Model = new EntryvoteModel();
        model.fill(data);
        return model;
    }
}
