import { ModelService, BasicModel, Model } from 'core/index';

export class ThreadModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getFirstEntry(data: any): Model {
        return this.modelService.create('entry', data);
    }

    _getEntries(data: any): any {
        let entries: Model[] = [];

        if (data == undefined || data == null) {
            return entries;
        }

        let i: number = 0;
        for (i = 0; i < data.length; i++) {
            entries.push(this.modelService.create('entry', data[i]))
        }
        return entries;
    }

    export(): any {
        return {
            title: this.get('title'),
            subject_id: this.get('subject_id'),
            subject_pid: this.get('subject_pid'),
            first_entry: this.get('first_entry').export()
        };
    }
}
