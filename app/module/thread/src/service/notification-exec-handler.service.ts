import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Model, RouteRefreshExec } from 'core/index';

import { ThreadService } from '../service/thread.service';

@Injectable()
export class NotificationExecHandlerService {

    private router: Router;
    private threadService: ThreadService;

    constructor(router: Router, threadService: ThreadService) {
        this.router = router;
        this.threadService = threadService;
    }

    onEntryCreate(notification: Model): void {
        notification.set('exec', new RouteRefreshExec(['/thread', notification.get('options.threadPid'), 'e', notification.get('source_id')], this.router, () => {
                this.threadService.requestOne({public_id: notification.get('options.threadPid')});
            })
        );
    }
}