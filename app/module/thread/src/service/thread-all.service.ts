import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ModelService, AbstractObservable, Model, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { StateMachine, StateService } from 'state/index';

import { ThreadModel } from '../object/thread.model';

@Injectable()
export class ThreadAllService extends AbstractObservable {

    private http: LmnHttp;
    private modelService: ModelService;
    private threads: Model[];
    private eventHandler: EventHandler;

    constructor(http: LmnHttp, modelService: ModelService, eventHandler: EventHandler) {
        super();
        this.http = http;
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.threads = [];
    }

    notifyAll(): void {
        super.notifyAll();
        this.eventHandler.fire('threadall.load', this.all());
    }

    all(): Model[] {
        return this.threads;
    }

    add(thread: Model): void {
        this.threads.splice(0, 0, thread);
        this.notifyAll();
    }

    get(id: number): Model {
        let i: number = 0;
        for (i = 0; i < this.threads.length; i++) {
            if (id == this.threads[i].get('id')) {
                return this.threads[i];
            }
        }

        return null;
    }

    refresh(thread: Model): void {
        let change: boolean = false;
        let i: number = 0;
        for (i = 0; i < this.threads.length; i++) {
            if (this.threads[i].get('id') == thread.get('id')) {
                this.threads[i] = thread;
                change = true;
            }
        }

        if (change) {
            this.notifyAll();
        }
    }

    private mapRequest(data: any = {}): Observable<Model[]> {
        return this.http.post('api/thread/list_all', data)
            .map((data: any) => {
                let list = data.data;
                let i: number = 0;
                let threads: Model[] = [];
                for (i = list.length - 1; i >= 0; i--) {
                    threads.push(this.modelService.create('thread', list[i]));
                }
                return threads.reverse();
            });
    }

    request(data: any = {}): void {
        this.mapRequest(data)
            .subscribe((threads: Model[]) => {
                this.threads = threads;
                this.notifyAll();
            });
    }

    requestMore(data: any = {}): void {
        this.mapRequest(data)
            .subscribe((threads: Model[]) => {
                for (let i: number = 0; i < threads.length; i++) {
                    this.threads.push(threads[i]);
                }
                this.notifyAll();
            });
    }
}
