import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ModelService, AbstractObservable, Model, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { StateMachine, StateService } from 'state/index';

import { ThreadModel } from '../object/thread.model';

@Injectable()
export class ThreadService extends AbstractObservable {

    private http: LmnHttp;
    private modelService: ModelService;
    private threads: Model[];
    private allThreads: any;
    private stateMachine: StateMachine;
    private eventHandler: EventHandler;
    private stateService: StateService;

    constructor(http: LmnHttp, modelService: ModelService, stateService: StateService, eventHandler: EventHandler) {
        super();
        this.http = http;
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.stateService = stateService;
        this.threads = [];
        this.allThreads = {};
    }

    private getInstance(id: number, data: any): Model {
        let thread: Model = this.get(id);
        if (thread !== null && thread !== undefined) {
            thread.fill(data);
        } else {
            thread = this.modelService.create('thread', data);
            this.addToAll(thread);
        }
        return thread;
    }

    getStateMachine(): StateMachine {
        if (!this.stateMachine) {
            this.stateMachine = this.stateService.provide('threadListState');
        }
        return this.stateMachine;
    }

    notifyAll(): void {
        super.notifyAll();
        this.eventHandler.fire('thread.load', this.all());
    }

    addToAll(thread: Model): void {
        this.allThreads[thread.get('id')] = thread;
    }

    all(): Model[] {
        return this.threads;
    }

    add(thread: Model): void {
        this.addToAll(thread);
        this.threads.splice(0, 0, thread);
        this.notifyAll();
    }

    get(id: number): Model {
        let i: number = 0;
        if (this.allThreads[id]) {
            return this.allThreads[id];
        }

        return null;
    }

    refresh(thread: Model): void {
        let change: boolean = false;
        let i: number = 0;
        if (this.allThreads[thread.get('id')]) {
            this.allThreads[thread.get('id')].fill(thread.data());
            change = true;
        }

        if (change) {
            this.getStateMachine().change('change');
            this.notifyAll();
        }
    }

    requestNew(): void {

    }

    requestOne(data: any): Observable<Model> {
        let observer: any = this.http.post('api/thread/detail', data)
            .map((data: any) => {
                return this.getInstance(data.data.id, data.data);
            });

        return observer;
    }

    private mapRequest(data: any = {}): Observable<Model[]> {
        return this.http.post('api/thread/list', data)
            .map((data: any) => {
                let list = data.data;
                let i: number = 0;
                let threads: Model[] = [];
                for (i = list.length - 1; i >= 0; i--) {
                    threads.push(this.getInstance(list[i].id, list[i]));
                }
                return threads.reverse();
            });
    }

    request(data: any): Observable<Model[]> {
        this.getStateMachine().change('loading');
        let observer: Observable<Model[]> = this.mapRequest(data);
        observer.subscribe((threads: Model[]) => {
            this.threads = threads;
            if (threads.length > 0) {
                this.getStateMachine().change('changed');
            }
            else {
                this.getStateMachine().change('stable');
            }
            this.notifyAll();
        });

        return observer;
    }

    requestOld(data: any): Observable<Model[]> {
        this.getStateMachine().change('loading');
        let observer: Observable<Model[]> = this.mapRequest(data);
        observer.subscribe((threads: Model[]) => {
            let i: number = 0;
            for (i = threads.length - 1; i >= 0; i--) {
                this.threads.push(threads[i]);
            }
            this.notifyAll();
        });

        return observer;
    }

    requestSubject(data: any): Observable<Model> {
        let observer: Observable<Model> = Observable.create((observer: any) => {
            this.http.post('api/thread/subject', data)
                .subscribe((data: any) => {
                    let subject: Model = this.modelService.create('subject', data.data);
                    observer.next(subject);
                    observer.complete();
                }, (err: any) => {
                    observer.error(err);
                });
        });

        return observer
    }

    requestForTag(data: any): Observable<Model[]> {
        let observer: Observable<Model[]> = this.http.post('api/thread/tag/list', data)
            .map((data: any) => {
                let threads: Model[] = [];
                let list: any[] = data.data;
                for (let i: number = 0; i < list.length; i++) {
                    threads.push(this.modelService.create('thread', list[i]));
                }
                return threads;
            });

        return observer;
    }

    hasChanged(): boolean {
        return this.getStateMachine().getCurrent().is("changed");
    }
}
