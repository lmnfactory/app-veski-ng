import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ModelService, Model } from 'core/index';
import { LmnHttp } from 'http-communication/index';

import { ThreadService } from '../service/thread.service';

@Injectable()
export class EntryService {

    private http: LmnHttp;
    private modelService: ModelService;
    private threadService: ThreadService;

    constructor(http: LmnHttp, modelService: ModelService, threadService: ThreadService) {
        this.http = http;
        this.modelService = modelService;
        this.threadService = threadService;
    }

    create(entry: Model): Observable<Response> {
        let observer = this.http.post('api/entry/create', entry.export());

        observer.subscribe((data: any) => {
            let thread: Model = this.threadService.get(data.option.threadId);
            if (thread == null) {
                return ;
            }

            let entriesData = data.data;
            thread.set('entries', entriesData);
        });

        return observer;
    }

    vote(entryvote: Model): Observable<Response> {
        let observer = this.http.post('api/entry/vote', entryvote.export());

        observer.subscribe((data: any) => {
            let entryData: any = data.data;
            let thread: Model = this.threadService.get(entryData.thread_id);
            if (thread == null) {
                return ;
            }

            if (thread.get('first_entry.id') == entryData.id) {
                thread.set('first_entry', entryData);
                return;
            }

            let entries: Model[] = thread.get('entries');
            let i: number = 0;
            for(i = 0; i < entries.length; i++) {
                if (entryData.id == entries[i].get('id')) {
                    //thread.set('entries.' + i, entryData);
                    entries[i].fill(entryData);
                }
            }
        });

        return observer;
    }
}
