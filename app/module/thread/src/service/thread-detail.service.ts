import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ModelService, Model, AbstractObservable } from 'core/index';
import { LmnHttp } from 'http-communication/index';

import { ThreadModel } from '../object/thread.model';
import { ThreadService } from '../service/thread.service';

@Injectable()
export class ThreadDetailService extends AbstractObservable {

    private http: LmnHttp;
    private threadService: ThreadService;
    private modelService: ModelService;
    private threads: ThreadModel[];

    constructor(http: LmnHttp, threadService: ThreadService, modelService: ModelService) {
        super();
        this.http = http;
        this.threadService = threadService;
        this.modelService = modelService;
        this.threads = [];
    }

    create(thread: Model): Observable<Response> {
        let observer = this.http.post('api/thread/create', thread.export());

        observer.subscribe((data: any) => {
            let model: Model = this.modelService.create('thread', data.data);
            this.threadService.add(model);
        });

        return observer;
    }

    vote(entryvote: Model): Observable<Response> {
        let observer = this.http.post('api/thread/vote', entryvote.export());

        observer.subscribe((data: any) => {
            let model: Model = this.modelService.create('thread', data.data);
            this.threadService.refresh(model);
        });

        return observer;
    }

    request(threadId: number): Observable<Response> {
        let observer = this.http.post('api/thread/detail', {thread_id: threadId});

        observer.subscribe((data: any) => {
            let model: Model = this.modelService.create('thread', data.data);
            this.threadService.refresh(model);
        });

        return observer;
    }
}
