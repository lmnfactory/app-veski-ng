import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { ModelService, Model } from 'core/index';
import { TextFormaterService } from 'text/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { ThreadDetailService } from '../service/thread-detail.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-view',
    templateUrl: 'view.component.html',
    styleUrls: ['view.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})
export class ViewComponent implements OnInit, OnDestroy {

    @Input('lmnThread')
    public thread: Model;
    @Input('lmnHighlightEntry')
    public highlightEntryId: number;
    @Input('lmnShowAll')
    public _showAllEntry: boolean;

    private threadDetailService: ThreadDetailService;
    private textFormaterService: TextFormaterService;
    private modelServie: ModelService;

    constructor(threadDetailService: ThreadDetailService, modelService: ModelService, textFormaterService: TextFormaterService) {
        this.threadDetailService = threadDetailService;
        this.textFormaterService = textFormaterService;
        this.modelServie = modelService;
        this._showAllEntry = false;
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    vote(value: number): void {
        let model: Model = this.modelServie.create('entryvote', {
            entry_id: this.thread.get('first_entry.id'),
            value: value
        });
        this.threadDetailService.vote(model);
    }

    toggleAllEntry(): void {
        this._showAllEntry = !this._showAllEntry;
    }

    showAllEntry(): void {
        this._showAllEntry = true;
    }

    hasHiddenEntries(): boolean {
        return (!this._showAllEntry && this.thread.get('entries').length > 3)
    }

    getEntries(): Model[] {
        if (this._showAllEntry) {
            return this.thread.get('entries');
        }

        let entrieSize: number = this.thread.get('entries').length;
        if (entrieSize <= 3) {
            return this.thread.get('entries');
        }

        return this.thread.get('entries').slice(entrieSize - 3);
    }

    trackEntryList(index: number, entry: Model): any {
        return entry.get('id');
    }

    getTextComponent(): any[] {
        let text: string = this.thread.get('first_entry.text');
        let components: any[] = [this.textFormaterService.makePlain(text)];
        components = this.textFormaterService.detectLinks(components);
        components = this.textFormaterService.detectHashtags(components, this.thread.get('tags'));
        return components;
    }
}
