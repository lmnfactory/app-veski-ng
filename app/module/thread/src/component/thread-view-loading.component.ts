import { Component, Input, OnInit, OnDestroy } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-view-loading',
    templateUrl: 'thread-view-loading.component.html',
    styleUrls: ['thread-view-loading.component.css']
})
export class ThreadViewLoadingComponent implements OnInit, OnDestroy {

    constructor() {

    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }
}
