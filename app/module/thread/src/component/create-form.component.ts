import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { ModelService, Model, EventHandler, ValidationService, Garbage, GarbageService } from 'core/index';
import { TextEnrichService, FormBoxService, FormBoxEmitter, AutocompleteService } from 'form-ui/index';
import { FileUploadService } from 'http-communication/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { ThreadModel } from '../object/thread.model';
import { ThreadDetailService } from '../service/thread-detail.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-create',
    templateUrl: 'create-form.component.html',
    styleUrls: ['create-form.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})
export class CreateFormComponent implements OnInit, OnDestroy {

    @Input('lmnSubjectPid')
    private subjectPid: string;
    @ViewChild('textInput')
    private textInputElement: any;

    private formBuilder: FormBuilder;
    public form: FormGroup;
    private modelService: ModelService;
    private threadDetailService: ThreadDetailService;
    private textEnrichService: TextEnrichService;
    private autocompleteService: AutocompleteService;
    private eventHandler: EventHandler;
    private fileUploadService: FileUploadService;
    private validationService: ValidationService;
    public formBox: FormBoxEmitter;
    public thread: Model;
    public tags: any[];
    public attachments: any[];
    private garbage: Garbage;
    private validationConrols: any;
    public errors: string[];

    constructor(threadDetailService: ThreadDetailService, modelService: ModelService, formBoxService: FormBoxService, textEnrichService: TextEnrichService, autocompleteService: AutocompleteService, eventHandler: EventHandler, fileUploadService: FileUploadService, formBuilder: FormBuilder, validationService: ValidationService, garbageService: GarbageService) {
        this.modelService = modelService;
        this.threadDetailService = threadDetailService;
        this.textEnrichService = textEnrichService;
        this.autocompleteService = autocompleteService;
        this.eventHandler = eventHandler;
        this.fileUploadService = fileUploadService;
        this.validationService = validationService;
        this.formBuilder = formBuilder;
        this.formBox = formBoxService.createEmitter();
        this.garbage = garbageService.create();
        this.validationConrols = {
            'title': 'Nadpis',
            'text': 'Text',
            'files': 'Súbory'
        };
        this.init();
    }

    private init(): void {
        this.thread = this.modelService.create('thread');
        this.attachments = [];
        this.tags = [];
        this.errors = [];
        this.formBox.reset();
        if (this.form) {
            this.form.reset();
        }
    }

    private onFormEvent(data: any): void {
        if (!this.form) { 
            return; 
        }
        this.errors = this.validationService.validate(this.form, this.validationConrols);
    }

    ngOnInit(): void {
        let self: CreateFormComponent = this;
        let builder: FormBuilder = this.formBuilder;
        this.form = builder.group({
            'title': [this.thread.get('title'), Validators.compose([
                Validators.required,
                Validators.maxLength(255)
            ])],
            'text': ["", Validators.required],
            'files': [[], self.validationService.rule('fileupload')]
        });
        this.form.statusChanges.subscribe(this.onFormEvent.bind(this));

        this.garbage.event(
            this.formBox.on('submit', this.create.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('action.thread.create', () => {this.textInputElement.nativeElement.focus();})
        );
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    isActive(): boolean {
        return (this.formBox.hasFocus() || this.thread.get('title') || this.thread.get('first_entry.text') || this.attachments.length > 0);
    }

    textChange(value: string): void {
        this.thread.set('first_entry.text', value);
        this.tags = this.textEnrichService.extract(value, '#');
    }

    attachDropFiles(files: any): void {
        for (let i: number = 0; i < files.acceptedFiles.length; i++) {
            let file: any = files.acceptedFiles[i];
            this.attachments.push({
                file: file.acceptedFile,
                upload: this.fileUploadService.upload(file.acceptedFile)
            });
        }
    }

    attachFiles(files: any): void {
        for (let i: number = 0; i < files.length; i++) {
            let file: any = files[i];
            this.attachments.push({
                file: file,
                upload: this.fileUploadService.upload(file)
            });
        }
    }

    cancel(event: any): void {
        this.formBox.blur();
        this.init();
    }

    dettache(index: number): void {
        this.attachments.splice(index, 1);
    }

    create(readyCallback: any): void {
        this.form.get('files').setValue(this.attachments);
        this.form.updateValueAndValidity();
        if (this.form.invalid) {
            this.errors = this.validationService.validateAll(this.form, this.validationConrols);
            readyCallback();
            return;
        }

        let files: any[] = [];
        for (let i: number = 0; i < this.attachments.length; i++) {
            let att: any = this.attachments[i];
            if (!att.upload.isDone()) {
                continue;
            }
            files.push(att.upload.getId());
        }

        this.thread.set('first_entry.files', files);
        this.thread.set('subject_pid', this.subjectPid);
        this.threadDetailService.create(this.thread)
            .subscribe((data: any) => {
                this.init();
            });
    }
}
