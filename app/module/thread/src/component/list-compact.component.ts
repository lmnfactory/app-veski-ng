import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Model } from 'core/index';
import { fadeIfAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-list-compact',
    templateUrl: 'list-compact.component.html',
    styleUrls: ['list-compact.component.css'],
    animations: [ fadeIfAnimation ]
})

export class ListCompactComponent implements OnInit {

    @Input('lmnThreads')
    public threads: Model[];
    @Output('lmnLoadMore')
    public loadMoreEmitter: EventEmitter<any>;

    constructor() {
        this.loadMoreEmitter = new EventEmitter();
    }

    ngOnInit() { }

    listTrack(index: number, thread: Model): any{
        return thread.get('id');
    }

    onScroll(): any {
        this.loadMoreEmitter.emit({});
    }
}