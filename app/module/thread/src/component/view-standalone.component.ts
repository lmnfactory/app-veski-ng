import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Model, EventHandler } from 'core/index';
import { AuthenticationService, OnSignin } from 'account/index';
import { HeaderService } from 'header/index';
import { fadeIfAnimation } from 'animation/index';

import { ThreadService } from '../service/thread.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-view-standalone',
    templateUrl: 'view-standalone.component.html',
    styleUrls: ['view-standalone.component.css'],
    animations: [fadeIfAnimation]
})

export class ViewStandaloneComponent extends OnSignin implements OnInit {

    public publicId: string;
    public highlightEntry: number;
    public threads: Model[];
    private headerService: HeaderService;
    private threadService: ThreadService;
    private route: ActivatedRoute;
    private events: any[];

    constructor(eventHandler: EventHandler, authService: AuthenticationService, headerService: HeaderService, threadService: ThreadService, route: ActivatedRoute) {
        super(eventHandler, authService);
        this.headerService = headerService;
        this.threadService = threadService;
        this.route = route;
        this.threads = [];
        this.events = [];
        this.highlightEntry = 0;
    }

    private load(): void {
        if (!this.publicId || !this.isSignIn()) {
            return;
        }
        this.threadService.requestOne({public_id: this.publicId});
    }

    onLoad(threads: Model[]): void {
        this.threads = threads;
        if (this.threads.length == 1) {
            this.headerService.set('title', this.threads[0].get('title'))
        } else {
            this.headerService.set('title', 'Thready')
        }
    }

    onSignin(): void {
        this.load();
    }

    ngOnInit() {
        super.ngOnInit();
        this.route.params.subscribe((params: any) => {
            this.publicId = params.publicId;
            if (params.entryId) {
                this.highlightEntry = params.entryId;
            } else {
                this.highlightEntry = 0;
            }
            this.load();
        });
        this.headerService.set('tabs', []);
        this.events.push(
            this.eventHandler.on('thread.load', this.onLoad.bind(this))
        );
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }
}