import { Component, OnInit, Input } from '@angular/core';

import { Model } from 'core/index';
import { ColorpaletteService } from 'lmn-color/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-view-compact',
    templateUrl: 'view-compact.component.html',
    styleUrls: ['view-compact.component.css']
})

export class ViewCompactComponent implements OnInit {

    @Input('lmnThread')
    public thread: Model;
    public highlightEntry: number;

    private colorService: ColorpaletteService;

    constructor(colorService: ColorpaletteService) {
        this.colorService = colorService;
    }

    ngOnInit() {

    }

    getColorpalette(): string {
        return this.colorService.getSubjectPaletteClass(this.thread.get('subject_id'));
    }
}