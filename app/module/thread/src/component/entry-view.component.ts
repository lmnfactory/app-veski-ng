import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { ModelService, Model } from 'core/index';
import { TextFormaterService } from 'text/index';

import { EntryService } from '../service/entry.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-entry-view',
    templateUrl: 'entry-view.component.html',
    styleUrls: ['view.component.css', 'entry-view.component.css']
})
export class EntryViewComponent implements OnInit, OnDestroy {

    @Input('lmnEntry')
    public entry: Model;
    @Input('lmnHighlight')
    public highlightEntry: boolean;

    private entryService: EntryService;
    private modelServie: ModelService;
    private textFormaterService: TextFormaterService;

    constructor(entryService: EntryService, modelService: ModelService, textFormaterService: TextFormaterService) {
        this.entryService = entryService;
        this.modelServie = modelService;
        this.textFormaterService = textFormaterService;
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    vote(value: number): void {
        let model: Model = this.modelServie.create('entryvote', {
            entry_id: this.entry.get('id'),
            value: value
        });
        this.entryService.vote(model);
    }

    getTextComponent(): any[] {
        let text: string = this.entry.get('text');
        let components: any[] = [this.textFormaterService.makePlain(text)];
        components = this.textFormaterService.detectLinks(components);
        return components;
    }
}
