import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { Model, EventToken, EventHandler } from 'core/index';
import { fadeIfAnimation } from 'animation/index';

import { ThreadService } from '../service/thread.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-list',
    templateUrl: 'list.component.html',
    styleUrls: ['list.component.css'],
    animations: [ fadeIfAnimation ]
})
export class ListComponent implements OnInit, OnDestroy {

    @Input('lmnList')
    public threads: Model[];
    @Output('lmnLoadMore')
    public loadMoreEmitter: EventEmitter<any>;

    private threadService: ThreadService;
    private eventHandler: EventHandler;
    

    constructor(threadService: ThreadService, eventHandler: EventHandler) {
        this.threadService = threadService;
        this.eventHandler = eventHandler;
        this.threads = [];
        this.loadMoreEmitter = new EventEmitter();
    }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {
        
    }
    
    onScroll(): any {
        this.loadMoreEmitter.emit({});
    }

    listTrack(index: number, thread: Model): any{
        return thread.get('id');
    }
}
