import { Component, Input, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';

import { Model } from 'core/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-thread-vote',
    templateUrl: 'vote.component.html',
    styleUrls: ['vote.component.css']
})
export class VoteComponent implements OnInit, OnDestroy {

    @Input('lmnEnable')
    private enabled: boolean;
    @Input('lmnValue')
    public votevalue: number;
    @Input('lmnVote')
    private vote: Model;
    @Input('lmnLayout')
    public layout: string;
    @Output('lmnUpvote')
    private upvoteEvent: EventEmitter<any>;
    @Output('lmnDownvote')
    private downvoteEvent: EventEmitter<any>;

    constructor() {
        this.upvoteEvent = new EventEmitter();
        this.downvoteEvent = new EventEmitter();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    private hasVote(): boolean {
        return (this.vote != undefined && this.vote != null);
    }

    getLayout(): string {
        if (this.layout) {
            return this.layout;
        }
        return "column";
    }

    isUpvoted(): boolean {
        return (this.hasVote() && this.vote.get('value') == 1);
    }

    isDownvoted(): boolean {
        return (this.hasVote() && this.vote.get('value') == -1);
    }

    upvote($event: any): void {
        this.upvoteEvent.emit($event);
    }

    downvote($event: any): void {
        this.downvoteEvent.emit($event);
    }
}
