import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { ModelService, Model, ValidationService, Garbage, GarbageService } from 'core/index';
import { FormBoxService, FormBoxEmitter } from 'form-ui/index';
import { FileUploadService } from 'http-communication/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { EntryModel } from '../object/entry.model';
import { EntryService } from '../service/entry.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-entry-create',
    templateUrl: 'entry-create-form.component.html',
    styleUrls: ['entry-create-form.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})
export class EntryCreateFormComponent implements OnInit, OnDestroy {

    @Input('lmnThreadId')
    private threadId: number;
    private modelService: ModelService;
    private entryService: EntryService;
    private fileUploadService: FileUploadService;
    private validationService: ValidationService;
    private formBuilder: FormBuilder;
    public form: FormGroup;
    public formBox: FormBoxEmitter;
    public entry: Model;
    public attachments: any[];
    private garbage: Garbage;
    private validationConrols: any;
    public errors: string[];

    constructor(entryService: EntryService, modelService: ModelService, formBoxService: FormBoxService, fileUploadService: FileUploadService, validationService: ValidationService, formBuilder: FormBuilder, garbageService: GarbageService) {
        this.modelService = modelService;
        this.entryService = entryService;
        this.fileUploadService = fileUploadService;
        this.validationService = validationService;
        this.formBuilder = formBuilder;
        this.formBox = formBoxService.createEmitter();
        this.garbage = garbageService.create();
        this.validationConrols = {
            'text': 'Text',
            'files': 'Súbory'
        };
        this.init();
    }

    private init(): void {
        this.entry = this.modelService.create('entry');
        this.attachments = [];
        this.errors = [];
        this.formBox.reset();
        if (this.form) {
            this.form.reset();
        }
    }

    private onFormEvent(): void {
        if (!this.form) { 
            return; 
        }
        this.errors = this.validationService.validate(this.form, this.validationConrols);
    }

    ngOnInit(): void {
        let self: EntryCreateFormComponent = this;
        let builder: FormBuilder = this.formBuilder;
        this.form = builder.group({
            'text': [null, Validators.required],
            'files': [[], self.validationService.rule('fileupload')]
        });
        this.garbage.subscription(
            this.form.statusChanges.subscribe(this.onFormEvent.bind(this))
        );
        this.garbage.event(
            this.formBox.on('submit', this.create.bind(this))
        );
        this.init();
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    isActive(): boolean {
        return (this.formBox.hasFocus() || this.entry.get('text') || this.attachments.length > 0);
    }

    attachDropFiles(files: any): void {
        for (let i: number = 0; i < files.acceptedFiles.length; i++) {
            let file: any = files.acceptedFiles[i];
            this.attachments.push({
                file: file.acceptedFile,
                upload: this.fileUploadService.upload(file.acceptedFile)
            });
        }
    }

    attachFiles(files: any): void {
        for (let i: number = 0; i < files.length; i++) {
            let file: any = files[i];
            this.attachments.push({
                file: file,
                upload: this.fileUploadService.upload(file)
            });
        }
    }

    dettache(index: number): void {
        this.attachments.splice(index, 1);
    }

    cancel(event: any): void {
        this.formBox.blur();
        this.init();
    }

    create(readyCallback: any): void {
        this.form.get('files').setValue(this.attachments);
        this.form.updateValueAndValidity();

        if (this.form.invalid) {
            this.errors = this.validationService.validateAll(this.form, this.validationConrols);
            readyCallback();
            return;
        }

        let files: any[] = [];
        for (let i: number = 0; i < this.attachments.length; i++) {
            let att: any = this.attachments[i];
            if (!att.upload.isDone()) {
                continue;
            }
            files.push(att.upload.getId());
        }

        this.entry.set('files', files);
        this.entry.set('thread_id', this.threadId);
        this.entryService.create(this.entry)
            .subscribe((data: any) => {
                this.init();
            });
    }
}
