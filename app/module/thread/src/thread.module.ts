import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";
import { InfiniteScrollModule } from 'angular2-infinite-scroll/angular2-infinite-scroll';

import { HttpCommunicationModule } from 'http-communication/index';
import { CoreModule, ModelService } from 'core/index';
import { AccountModule } from 'account/index';
import { FormUIModule } from 'form-ui/index';
import { TagModule } from 'tag/index';
import { NotificationModule, NotificationService, NotificationFactory } from 'notification/index';
import { StateModule, StateService } from 'state/index';
import { FileModule } from 'file/index';
import { TextModule, TextFormaterService } from 'text/index';

import { ViewComponent } from './component/view.component';
import { ViewStandaloneComponent } from './component/view-standalone.component';
import { ThreadViewLoadingComponent } from './component/thread-view-loading.component';
import { ViewCompactComponent } from './component/view-compact.component';
import { ListComponent } from './component/list.component';
import { ListCompactComponent } from './component/list-compact.component';
import { VoteComponent } from './component/vote.component';
import { CreateFormComponent } from './component/create-form.component';
import { EntryViewComponent } from './component/entry-view.component';
import { EntryCreateFormComponent } from './component/entry-create-form.component';
import { ThreadFactory } from './object/thread-factory.object';
import { EntryFactory } from './object/entry-factory.object';
import { EntryvoteFactory } from './object/entryvote-factory.object';
import { ThreadService } from './service/thread.service';
import { ThreadAllService } from './service/thread-all.service';
import { ThreadDetailService } from './service/thread-detail.service';
import { EntryService } from './service/entry.service';
import { ThreadNotificationController } from './object/thread-notification-controller.object';
import { NotificationExecHandlerService } from './service/notification-exec-handler.service';
import { ThreadServiceStateFactory } from './object/thread-service-state-factory.object';

@NgModule({
    imports: [ CommonModule, FormsModule, ReactiveFormsModule, RouterModule, FlexLayoutModule, HttpCommunicationModule, FileModule, FormUIModule, CoreModule, TextModule, AccountModule, StateModule, TagModule, NotificationModule, InfiniteScrollModule ],
    declarations: [ CreateFormComponent, ViewComponent, ListComponent, VoteComponent, EntryCreateFormComponent, EntryViewComponent, ThreadViewLoadingComponent, ListCompactComponent, ViewCompactComponent, ViewStandaloneComponent ],
    providers: [ ThreadService, ThreadDetailService, ThreadNotificationController, EntryService, ThreadAllService, NotificationExecHandlerService ],
    exports: [ CreateFormComponent, ViewComponent, ListComponent, VoteComponent, EntryCreateFormComponent, EntryViewComponent, ThreadViewLoadingComponent, ListCompactComponent, ViewStandaloneComponent ]
})
export class ThreadModule {

    constructor(modelService: ModelService, stateService: StateService, notificationFactory: NotificationFactory, notificationExecHandlerService: NotificationExecHandlerService, textFormaterService: TextFormaterService) {
        modelService.addFactory('thread', new ThreadFactory(modelService));
        modelService.addFactory('entry', new EntryFactory(modelService, textFormaterService));
        modelService.addFactory('entryvote', new EntryvoteFactory());

        stateService.addFactory('threadListState', new ThreadServiceStateFactory());

        notificationFactory.addAfterCreate('entry.create', notificationExecHandlerService.onEntryCreate.bind(notificationExecHandlerService));
    }
}
