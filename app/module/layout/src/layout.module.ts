import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from "@angular/flex-layout";

import { BodySideLayoutComponent } from './component/body-side-layout.component';
import { AppLayoutComponent } from './component/app-layout.component';
import { MarkTopComponent } from './component/mark-top.component';
import { MoreDropdownComponent } from './component/more-dropdown.component';

@NgModule({
  imports:      [ CommonModule, FlexLayoutModule ],
  exports: [ AppLayoutComponent, BodySideLayoutComponent, MarkTopComponent, MoreDropdownComponent ],
  declarations: [ BodySideLayoutComponent, AppLayoutComponent, MarkTopComponent, MoreDropdownComponent ],
  providers: [  ]
})
export class LayoutModule { }
