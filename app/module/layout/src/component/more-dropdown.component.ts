import { Component, OnInit, Input, HostListener } from '@angular/core';

import { fadeHiddenAnimation, fadeIfAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-more-dropdown',
    templateUrl: 'more-dropdown.component.html',
    styleUrls: ['more-dropdown.component.css'],
    animations: [ fadeHiddenAnimation, fadeIfAnimation ]
})

export class MoreDropdownComponent implements OnInit {

    @Input('lmnDropdownLeft')
    public isDropdownLeft: boolean = false;
    @Input('lmnIcon')
    public icon: string = "ti-more-alt";
    private visible: boolean;
    private clickBuffer: number;

    constructor() {
        this.visible = false;
        this.clickBuffer = 0;
    }

    ngOnInit() { }

    toggle(): void {
        this.clickBuffer++;
        this.visible = !this.visible;
    }

    isVisible(): boolean {
        return this.visible;
    }

    getIcon(): string {
        return this.icon;
    }

    @HostListener('mousedown', ['$event'])
    onmousedown(event: any): void {
        
    }

    @HostListener('document:click', ['$event'])
    onOutsideClick(event: any): void {
        if (this.clickBuffer == 0) {
            this.visible = false;
        } else {
            this.clickBuffer--;
        }
        
    }
}