import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-app-layout',
    templateUrl: 'app-layout.html',
    styleUrls: ['app-layout.component.css']
})
export class AppLayoutComponent {

    @Input('lmnSideAlign')
    public sideAlign: string;

    constructor() {
        this.sideAlign = "start";
    }

    getSideAlign(): string {
        return this.sideAlign;
    }
}
