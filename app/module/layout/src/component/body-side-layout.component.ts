import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-body-side-layout',
    templateUrl: 'body-side-layout.html',
    styleUrls: ['body-side-layout.component.css']
})
export class BodySideLayoutComponent {

    @Input('lmnSideAlign')
    public sideAlign: string;

    constructor() {
        this.sideAlign = "start";
    }

    getSideAlign(): string {
        return this.sideAlign;
    }
}
