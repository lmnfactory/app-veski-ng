import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { Model } from 'core/index';
import { ColorpaletteService } from 'lmn-color/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-mark-top',
    templateUrl: 'mark-top.component.html',
    styleUrls: ['mark-top.component.css']
})
export class MarkTopComponent implements OnInit, OnDestroy {

    @Input('lmnSubject')
    private subject: Model;
    private colorpaletteService: ColorpaletteService;

    constructor(colorpaletteService: ColorpaletteService) {
        this.colorpaletteService = colorpaletteService;
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    getSubjectColor(): string {
        if (this.subject == null) {
            return "";
        }

        return this.colorpaletteService.getSubjectPaletteClass(this.subject.get('id'));
    }
}
