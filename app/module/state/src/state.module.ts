import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StateService } from './service/state.service';

@NgModule({
    imports: [ CommonModule ],
    declarations: [],
    exports: [],
    providers: [ StateService ]
})
export class StateModule {

}
