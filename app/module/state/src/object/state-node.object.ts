export class StateNode {

    private name: string;
    private links: StateNode[];

    constructor(name: string) {
        this.name = name;
        this.links = [];
    }

    linkTo(state: StateNode): void {
        this.links.push(state);
    }

    hasLinkTo(state: StateNode): boolean {
        for (let s of this.links) {
            if (state.is(s.getName())) {
                return true;
            }
        }

        return false;
    }

    is(stateName: string): boolean {
        return (this.name == stateName);
    }

    getName(): string {
        return this.name;
    }
}
