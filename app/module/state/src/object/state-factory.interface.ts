import { StateMachine } from 'state/index';

export interface StateFactory {
    create(): StateMachine;
}
