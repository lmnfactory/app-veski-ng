import { StateMachine } from './state-machine.interface';
import { StateNode } from './state-node.object';

export class EventStateMachine implements StateMachine {

    private states: StateNode[];
    private current: StateNode;

    constructor(states: StateNode[]) {
        this.states = states;
        this.current = states[0];
    }

    private getState(state: string): StateNode {
        for (let s of this.states) {
            if (s.is(state)) {
                return s;
            }
        }
        return null;
    }

    private hasState(state: string) {
        return (this.getState(state) != null);
    }

    private hasLink(from: StateNode, to: StateNode) {
        return from.hasLinkTo(to);
    }

    private setCurrent(state: StateNode): void {
        this.current = state;
    }

    change(key: string): boolean {
        let changeState = this.getState(key);

        if (changeState == null) {
            return false;
        }
        if (!this.hasLink(this.current, changeState)) {
            return false;
        }

        this.setCurrent(changeState);
        return true;
    }

    getCurrent(): StateNode {
        return this.current;
    }
}
