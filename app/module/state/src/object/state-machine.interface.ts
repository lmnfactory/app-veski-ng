import { StateNode } from './state-node.object';

export interface StateMachine {
    change(state: string): boolean;
    getCurrent(): StateNode;
}
