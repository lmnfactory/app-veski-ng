import { Injectable } from '@angular/core';

import { StateFactory } from '../object/state-factory.interface';
import { StateMachine } from '../object/state-machine.interface';

@Injectable()
export class StateService {

    private factories: any;

    constructor() {
        this.factories = {};
    }

    addFactory(key: string, factory: StateFactory): void {
        this.factories[key] = factory
    }

    provide(key: string): StateMachine {
        return this.factories[key].create();
    }
}
