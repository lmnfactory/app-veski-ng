import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { FlexLayoutModule } from "@angular/flex-layout";

import { CoreModule, ModelService } from 'core/index';
import { FormUIModule } from 'form-ui/index';
import { StateModule, StateService } from 'state/index';
import { FacultyModule } from 'faculty/index';

import { SignoutComponent } from './component/signout.component';
import { UserLabelComponent } from './component/user-label.component';
import { UserImageComponent } from './component/user-image.component';
import { UserLabelLoadingComponent } from './component/user-label-loading.component';
import { ProfileComponent } from './component/profile.component';
import { ProfileEditComponent } from './component/profile-edit.component';
import { PasswordEditComponent } from './component/password-edit.component';

import { AuthenticationService } from './service/authentication.service';
import { LoadingService } from './service/loading.service';
import { UserService } from './service/user.service';
import { JWTRequestInterceptor } from './object/jwt-request-interceptor.object';

import { AuthStateFactory } from './object/auth-state-factory.object';
import { UserFactory } from './object/user-factory.object';
import { UsersettingsFactory } from './object/usersettings-factory.object';

@NgModule({
  imports:      [ CommonModule, FormsModule, ReactiveFormsModule, RouterModule, FlexLayoutModule, CoreModule, FormUIModule, StateModule, FacultyModule ],
  declarations: [ UserLabelComponent, UserLabelLoadingComponent, SignoutComponent, ProfileComponent, ProfileEditComponent, PasswordEditComponent, UserImageComponent],
  exports: [ UserLabelComponent, UserLabelLoadingComponent, SignoutComponent, ProfileComponent, PasswordEditComponent, UserImageComponent ],
  providers: [ AuthenticationService, LoadingService, UserService ]
})
export class AccountModule {

    constructor(modelService: ModelService, stateService: StateService) {
        stateService.addFactory('auth', new AuthStateFactory());

        modelService.addFactory('user', new UserFactory(modelService));
        modelService.addFactory('usersettings', new UsersettingsFactory(modelService));
    }
}
