declare var Cookies: any;

import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
//import { CookieService } from 'angular2-cookie/services/cookies.service';
//import { CookieOptionsArgs } from 'angular2-cookie/services/cookie-options-args.model';
//import { CookieOptions } from 'angular2-cookie/core';

import { AbstractObservable, EventHandler, InitLoadingService, LmnDate } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { StateService, StateMachine } from 'state/index';

@Injectable()
export class AuthenticationService extends AbstractObservable {

    private http: LmnHttp;
    private eventHandler: EventHandler
    private router: Router;
    private refreshToken: string;
    private jwtToken: string;
    private stateMachine: StateMachine;
    private loadingService: InitLoadingService;
    private refreshTimeout: any;
    private cookieNames: any;

    constructor(http: LmnHttp, router: Router, stateService: StateService, eventHandler: EventHandler, loadingService: InitLoadingService) {
        super();
        this.http = http;
        this.router = router;
        this.eventHandler = eventHandler;
        this.loadingService = loadingService;
        this.refreshToken = "";
        this.jwtToken = "";
        this.stateMachine = stateService.provide('auth');
        this.refreshTimeout = null;
        this.cookieNames = {
            rm: 'RMAuth',
            jwtRefresh: 'JWTRefresh'
        };
    }

    private getRMHeader(): string {
        let authCookie = Cookies.get(this.cookieNames.rm);
        if (!authCookie) {
            return "";
        }
        let rmAuth: any = JSON.parse(authCookie);
        if (!rmAuth || !rmAuth.series || !rmAuth.token) {
            return "";
        }
        
        return rmAuth.series + "." + rmAuth.token;
    }

    private gotoSignin(): void {
        window.location.href = "signin";
    }

    startRefreshTimeout(timeout: number): void {
        this.stopRefreshTimeout();

        if (timeout > 0) {
            this.refreshTimeout = setTimeout(this.refresh.bind(this), timeout);
        }
    }

    stopRefreshTimeout(): void {
        if (this.refreshTimeout != null) {
            clearTimeout(this.refreshTimeout);
        }
    }

    getJwtToken(): string {
        return this.jwtToken;
    }

    setJwtAuth(jwt: string): void {
        let state: string = '';
        if (this.jwtToken == "" && jwt != "") {
            state = 'signin';
        }
        else if (this.jwtToken != "" && jwt == "") {
            state = 'signout'
        }
        this.jwtToken = jwt;
        if (this.stateMachine.change(state)) {
            if (this.isSignin()) {
                this.eventHandler.fire('auth.signin', this.jwtToken);
            }
            else {
                this.eventHandler.fire('auth.signout', {});
            }
            this.notifyAll();
        }
    }

    setJwtRefresh(jwt: string): void {
        this.refreshToken = jwt;
        let options: any = {};
        // Now it is set that the cookie expires on session
        Cookies.set(this.cookieNames.jwtRefresh, this.refreshToken, options)
    }

    setRMAuth(rmToken: any): void {
        let options: any = {};
        let date: LmnDate = new LmnDate(new Date());
        date.addDay(30);
        options.expires = date.date();
        Cookies.set('RMAuth', rmToken, options)
    }

    autosignin(): void {
        if (Cookies.get(this.cookieNames.jwtRefresh)) {
            let refreshToken: any = Cookies.get(this.cookieNames.jwtRefresh);
            this.refreshToken = refreshToken;
            this.refresh()
                .subscribe((success: any) => {
                    this.eventHandler.fire('signin',  {});
                }, (err: any) => {
                    this.gotoSignin();
                });
            Cookies.remove(this.cookieNames.jwtRefresh);
        } else {
            this.stateMachine.change('loading');            
            this.http.post('auth/autosignin', {}, {RMAuth: this.getRMHeader()})
                .subscribe((success: any) => {
                    let option: any = success.option;
                    //this.loadingService.end('signin');
                    this.eventHandler.fire('signin',  {});
                    this.startRefreshTimeout((option.expires_in - 60) * 1000);
                }, (err: any) => {
                    Cookies.remove(this.cookieNames.rm);
                    this.gotoSignin();
                });
        }
    }

    signin(data: any): void {
        this.stateMachine.change('loading');
        this.http.post('auth/signin', data)
            .subscribe((success: any) => {
                let option: any = success.option;
                this.eventHandler.fire('signin',  {});
                this.startRefreshTimeout((option.expires_in - 60) * 1000);
            }, (err: any) => {
                console.log("Signin error");
                console.log(err);
            })
    }

    signout(): void {
        this.http.post('auth/signout', {}, {RMAuth: this.getRMHeader()})
            .subscribe((success: any) => {
                this.setJwtAuth("");
                Cookies.remove(this.cookieNames.jwtRefresh);
                Cookies.remove(this.cookieNames.rm);
                this.stopRefreshTimeout();
                this.gotoSignin();
            }, (err: any) => {
                console.log("Signout error");
                console.log(err);
            })
    }

    refresh(): any {
        if (!this.refreshToken || this.refreshToken == "" || this.refreshToken == null) {
            return;
        }
        let observable = this.http.post('auth/refresh', {}, {jwtRefresh: this.refreshToken});
        observable.subscribe((success: any) => {
                let option: any = success.option;
                this.startRefreshTimeout((option.expires_in - 60) * 1000);
            }, (err: any) => {
                console.log("Refresh error");
                console.log(err);
            });

        return observable;
    }

    isSignin(): boolean {
        return (this.jwtToken != "");
    }

    getState(): string {
        return this.stateMachine.getCurrent().getName();
    }
}
