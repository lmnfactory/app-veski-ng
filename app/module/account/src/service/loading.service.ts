import { Injectable } from '@angular/core';

@Injectable()
export class LoadingService {

    private queue: any;
    private count: number;

    constructor() {
        this.queue = {};
        this.count = 0;
    }

    start(key: string): void {
        this.queue[key] = true;
        this.count++;
    }

    stop(key: string): void {
        if (this.queue[key] === undefined) {
            return;
        }
        delete this.queue[key];
        this.count--;
        if (this.count < 0) {
            this.count = 0;
        }
    }

    isLoading(): boolean {
        return (this.count > 0);
    }
}
