import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { EventHandler, Model, ModelService } from 'core/index';
import { LmnHttp } from 'http-communication/index';

@Injectable()
export class UserService {

    private eventHandler: EventHandler;
    private http: LmnHttp;
    private modelService: ModelService;

    private user: Model;

    constructor(http: LmnHttp, eventHandler: EventHandler, modelService: ModelService) {
        this.http = http;
        this.eventHandler = eventHandler;
        this.modelService = modelService;
        this.user = null;
        this.eventHandler.on('signin', this.request.bind(this));
    }

    private notifyAll(): void {
        this.eventHandler.fire('user.load', this.user);
    }

    get(): Model {
        return this.user;
    }

    request(data: any = {}): Observable<Response> {
        let observer: Observable<Response> = this.http.post('api/user/profile', data);
        observer.subscribe((data: any) => {
            this.user = this.modelService.create('user', data.data);
            this.notifyAll();
        });

        return observer;
    }

    update(data: any): Observable<Response> {
        let observer: Observable<Response> = this.http.post('api/user/profile/update', data);
        observer.subscribe((data: any) => {
            this.user = this.modelService.create('user', data.data);
            this.notifyAll();
        }, (err: any) => {return err;});

        return observer;
    }

    changePassword(data: any): Observable<Response> {
        let observer: Observable<Response> = this.http.post('user/change_password', data);
        observer.subscribe((data: any) => {}, (err: any) => {return err;});

        return observer;
    }
}