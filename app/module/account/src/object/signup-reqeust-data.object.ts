export class SignupRequestData {

    private auth: any;
    private method: string;
    private rememberMe: boolean;

    constructor(data: any) {
        this.auth = (data.auth) ? data.auth : {};
        this.method = (data.method) ? data.method : 'lmn-auth';
        this.rememberMe = (data.rememberMe) ? data.rememberMe : false;
    }

    getData(): any {
        return {
            auth: this.auth,
            method: this.method,
            rememberMe: this.rememberMe
        };
    }
}
