import { ResponseInterceptor } from 'http-communication/index';

import { AuthenticationService } from '../service/authentication.service';

export class JWTResponseInterceptor implements ResponseInterceptor {

    private authService: AuthenticationService;

    constructor(authService: AuthenticationService){
        this.authService = authService
    }

    execute(data: any): void {
        if (data.header) {
            if (data.header.JWTAuth) {
                this.authService.setJwtAuth(data.header.JWTAuth);
            }
            if (data.header.JWTRefresh) {
                this.authService.setJwtRefresh(data.header.JWTRefresh);
            }
            if (data.header.RMAuth) {
                this.authService.setRMAuth(data.header.RMAuth);
            }
        }
    }
}
