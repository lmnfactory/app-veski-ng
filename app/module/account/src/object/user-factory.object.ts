import { ModelService, ModelFactory, Model } from 'core/index';

import { UserModel } from './user.model';

export class UserFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new UserModel(this.modelService);
        model.fill(data);
        return model;
    }
}
