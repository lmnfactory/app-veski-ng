import { StateFactory, StateMachine, EventStateMachine, StateNode } from 'state/index';

export class AuthStateFactory implements StateFactory {

    create(): StateMachine {
        let states: StateNode[] = [];

        states.push(new StateNode('signout'));
        states.push(new StateNode('signin'));
        states.push(new StateNode('loading'));
        states[0].linkTo(states[1]);
        states[0].linkTo(states[2]);
        states[2].linkTo(states[1]);
        states[1].linkTo(states[0]);

        return new EventStateMachine(states);
    }
}
