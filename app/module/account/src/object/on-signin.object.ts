import { EventHandler } from 'core/index';

import { AuthenticationService } from '../service/authentication.service';

export abstract class OnSignin {

    protected authService: AuthenticationService;
    protected eventHandler: EventHandler;
    private signinEvent: any;

    constructor(eventHandler: EventHandler, authService: AuthenticationService) {
        this.eventHandler = eventHandler;
        this.authService = authService;
    }

    ngOnInit(): void {
        if (this.isSignIn()) {
            this.onSignin();
        }
        this.signinEvent = this.eventHandler.on('signin', this.onSignin.bind(this));
    }

    ngOnDestroy(): void {
        this.signinEvent();
    }

    isSignIn(): boolean {
        return this.authService.isSignin();
    }

    abstract onSignin(): void;
}
