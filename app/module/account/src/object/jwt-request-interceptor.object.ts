import { RequestInterceptor, LmnHttpRequest } from 'http-communication/index';
import { AuthenticationService } from '../service/authentication.service';

export class JWTRequestInterceptor implements RequestInterceptor {

    private authService: AuthenticationService;

    constructor(authService: AuthenticationService) {
        this.authService = authService;
    }

    execute(request: LmnHttpRequest): void {
        if (this.authService.isSignin()) {
            request.addHeader('JWTAuth', this.authService.getJwtToken());
        }
    }
}
