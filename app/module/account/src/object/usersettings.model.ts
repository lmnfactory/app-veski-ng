import { ModelService, BasicModel, Model } from 'core/index';

export class UsersettingsModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getAvatarFile(data: any): Model {
        return this.modelService.create('file', data);
    }

    _getFaculty(data: any): Model {
        return this.modelService.create('faculty', data);
    }

    // TODO: create model for degree
    //_getDegree(data: any): Model {
        //return this.modelService.create('degree', data);
    //}

    export(): any {
        return {
            faculty_id: this.get('faculty_id'),
            degree_id: this.get('degree_id'),
            studyyear: this.get('studyyear'),
            gender: this.get('gender'),
            birthday: this.get('birthday'),
            avatar: this.get('avatar')
        };
    }
}
