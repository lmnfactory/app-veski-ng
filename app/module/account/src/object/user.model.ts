import { ModelService, BasicModel, Model } from 'core/index';

export class UserModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    getInitials(): string {
        return (this.get('name').charAt(0) + this.get('surname').charAt(0));
    }

    _getSettings(data: any): Model {
        return this.modelService.create('usersettings', data);
    }

    export(): any {
        return {
            fullname: this.get('fullname'),
            name: this.get('name'),
            surname: this.get('surname'),
            avatar: this.get('avatar'),
            settings: this.get('settings').export()
        };
    }
}
