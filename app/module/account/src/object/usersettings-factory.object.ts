import { ModelService, ModelFactory, Model } from 'core/index';

import { UsersettingsModel } from './usersettings.model';

export class UsersettingsFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new UsersettingsModel(this.modelService);
        model.fill(data);
        return model;
    }
}
