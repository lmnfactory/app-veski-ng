import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-user-label-loading',
    templateUrl: 'user-label-loading.component.html',
    styleUrls: ['user-label-loading.component.css']
})
export class UserLabelLoadingComponent {

}
