import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { AuthenticationService } from '../service/authentication.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-signout',
    templateUrl: 'signout.component.html',
    styleUrls: ['signout.component.css']
})
export class SignoutComponent implements OnInit, OnDestroy {

    private authService: AuthenticationService;

    constructor(authService: AuthenticationService) {
        this.authService = authService;
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    signout(event: any): void {
        event.preventDefault();

        this.authService.signout();
    }
}
