import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';
import { FormBoxService, FormBoxEmitter } from 'form-ui/index';
import { ValidationService, Garbage, GarbageService } from 'core/index';

import { UserService } from '../service/user.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-password-edit',
    templateUrl: 'password-edit.component.html',
    styleUrls: ['password-edit.component.css'],
    animations: [ fadeHiddenAnimation, fadeIfAnimation ]
})

export class PasswordEditComponent implements OnInit, OnDestroy {

    @Output('lmnClose')
    public closeEmitter: EventEmitter<any>;

    private userService: UserService;
    public formBox: FormBoxEmitter;
    private formBuilder: FormBuilder;
    public form: FormGroup;
    private validationService: ValidationService;
    public errors: string[];
    private validationConrols: any;
    private garbage: Garbage;

    constructor(userService: UserService, formBoxService: FormBoxService, formBuilder: FormBuilder, validationService: ValidationService, garbageService: GarbageService) {
        this.userService = userService;
        this.formBuilder = formBuilder;
        this.validationService = validationService;
        this.closeEmitter = new EventEmitter();
        this.formBox = formBoxService.createEmitter();
        this.errors = [];
        this.validationConrols = {
            password: 'Heslo',
            newPassword: 'Nové heslo'
        };
        this.garbage = garbageService.create();
    }

    private onFormEvent(data: any): void {
        if (!this.form) { 
            return; 
        }

        this.errors = this.validationService.validate(this.form, this.validationConrols);
    }

    ngOnInit(): void {
        let builder: FormBuilder = this.formBuilder;
        this.form = builder.group({
            'password': ["", Validators.required],
            'newPassword': ["", Validators.compose([
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(40)
                ])]
        });

        this.garbage.subscription(
            this.form.statusChanges.subscribe(this.onFormEvent.bind(this))
        );

        this.garbage.event(
            this.formBox.on('submit', this.update.bind(this))
        );
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    onClose(): void {
        this.closeEmitter.emit({});
    }

    update(readyCallback: any): void {
        this.validationService.markAllAsTouched(this.form);
        this.form.updateValueAndValidity();
        if (this.form.invalid) {
            readyCallback();
            return;
        }

        let data: any = {
            password: this.form.get('password').value,
            new_password: this.form.get('newPassword').value
        };
        this.userService.changePassword(data)
            .subscribe((data: any) => {
                this.onClose();
                readyCallback();
            }, (err: Response) => {
                if (err.status == 422) {
                    let body: any = err.json();
                    this.validationService.serverError(this.form, body);
                    readyCallback();
                }
            });
    }
}