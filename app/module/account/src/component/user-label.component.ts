import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { Model } from 'core/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-user-label',
    templateUrl: 'user-label.component.html',
    styleUrls: ['user-label.component.css']
})
export class UserLabelComponent implements OnInit, OnDestroy {

    @Input('lmnUser')
    public user: any;

    constructor() {

    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    getAvatar(): string {
        if (!this.user || !this.hasAvatar()) {
            return "";
        }
        return "file/" + this.user.get('settings.avatar_file.public_id');
    }

    hasAvatar(): boolean {
        return (this.user && this.user.get('settings.avatar_file.public_id') && this.user.get('settings.avatar_file.public_id') != null);
    }

    getInitials(): string {
        if (!this.user) {
            return "XZ"
        }
        return this.user.getInitials();
    }
}
