import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { Model, ValidationService, Garbage, GarbageService } from 'core/index';
import { FormBoxService, FormBoxEmitter } from 'form-ui/index';
import { FileUploadService } from 'http-communication/index';
import { DegreeService } from 'degree/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { UserService } from '../service/user.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-profile-edit',
    templateUrl: 'profile-edit.component.html',
    styleUrls: ['profile-edit.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})

export class ProfileEditComponent implements OnInit {

    @Input('lmnUser')
    public user: Model;
    @Output('lmnClose')
    private closeEmitter: EventEmitter<any>;

    private userService: UserService;
    private fileUploadService: FileUploadService;
    private degreeService: DegreeService;
    private validationService: ValidationService;
    private formBuilder: FormBuilder;
    public form: FormGroup;
    public name: string;
    public surname: string;
    public faculty: any;
    public degreeId: string;
    public formBox: FormBoxEmitter;
    public avatarFile: any;
    public degrees: Model[];
    public studyyear: number;
    private validationConrols: any;
    public errors: string[];
    private garbage: Garbage;

    constructor(formBoxService: FormBoxService, userService: UserService, fileUploadService: FileUploadService, degreeService: DegreeService, validationService: ValidationService, formBuilder: FormBuilder, garbageService: GarbageService) {
        this.userService = userService;
        this.fileUploadService = fileUploadService;
        this.degreeService = degreeService;
        this.validationService = validationService;
        this.formBuilder = formBuilder;
        this.closeEmitter = new EventEmitter();
        this.formBox = formBoxService.createEmitter();
        this.errors = [];
        this.validationConrols = {
            'name': 'Meno',
            'surname': 'Priezvisko',
            'avatar': 'Avatar'
        };
        this.garbage = garbageService.create();
    }

     private onFormEvent(data: any): void {
        if (!this.form) { 
            return; 
        }
        this.errors = this.validationService.validate(this.form, this.validationConrols);
    }

    ngOnInit(): void {
        this.degrees = this.degreeService.all();
        this.form = this.formBuilder.group({
            'name': ["", Validators.compose([
                Validators.required,
                Validators.maxLength(128)
            ])],
            'surname': ["", Validators.compose([
                Validators.required,
                Validators.maxLength(128)
            ])],
            'faculty': [],
            'degreeId': [],
            'studyyear': [],
            'avatar': [null, this.validationService.rule('fileupload')]
        });
        // TODO: memory leak
        this.garbage.subscription(
            this.form.statusChanges.subscribe(this.onFormEvent.bind(this))
        );

        this.garbage.event(
            this.formBox.on('submit', this.update.bind(this))
        );
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    ngOnChanges(): void {
        this.name = this.user.get('name');
        this.surname = this.user.get('surname');
        this.faculty = this.user.get('settings.faculty');
        this.degreeId = this.user.get('settings.degree_id');
        this.studyyear = this.user.get('settings.studyyear');
    }

    onClose(): void {
        this.closeEmitter.emit({});
    }

    attachDropFiles(files: any): void {
        let acceptedFiles: any[] = [];
        for (let i: number = 0; i < files.acceptedFiles.length; i++) {
            acceptedFiles.push(files.acceptedFiles[i].acceptedFile);
        }

        this.attachFiles(acceptedFiles);
    }

    attachFiles(files: any): void {
        if (files.length == 0) {
            return;
        }
        let lastFile: any = files[files.length - 1];
        this.avatarFile = {
            file: lastFile,
            upload: this.fileUploadService.upload(lastFile)
        };
    }

    dettache(): void {
        this.avatarFile = false;
    }

    update(readyCallback: any): void {
        this.form.get('avatar').setValue(this.avatarFile);
        this.form.updateValueAndValidity();
        if (this.form.invalid) {
            this.errors = this.validationService.validateAll(this.form, this.validationConrols);
            readyCallback();
            return;
        }
        if (this.avatarFile) {
            if (!this.avatarFile.upload.isDone()) {
                readyCallback();
                return;
            }
            this.user.set('settings.avatar', this.avatarFile.upload.getId());
        }
        this.user.set('name', this.name);
        this.user.set('surname', this.surname);
        this.user.set('fullname', this.name + " " + this.surname);
        let facultyId: number = null;
        if (this.faculty) {
            facultyId = this.faculty.get('id');
        }
        this.user.set('settings.faculty_id', facultyId);
        this.user.set('settings.degree_id', this.degreeId);
        this.user.set('settings.studyyear', this.studyyear);
        this.userService.update(this.user.export())
            .subscribe((data: any) => {
                this.onClose();
                readyCallback();
            }, (err: any) => {
                readyCallback();
            });
    }
}