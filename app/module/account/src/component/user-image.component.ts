import { Component, OnInit, OnDestroy, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-user-image',
    templateUrl: 'user-image.component.html',
    styleUrls: ['user-image.component.css']
})

export class UserImageComponent implements OnInit {

    @Input('lmnUser')
    public user: any;

    constructor() { }

    ngOnInit() { }

    ngOnDestroy(): void {}

    getAvatar(): string {
        if (!this.user || !this.hasAvatar()) {
            return "";
        }
        return "file/" + this.user.get('settings.avatar_file.public_id');
    }

    hasAvatar(): boolean {
        return (this.user && this.user.get('settings.avatar_file.public_id') && this.user.get('settings.avatar_file.public_id') != null);
    }

    getInitials(): string {
        if (!this.user) {
            return "XZ"
        }
        return this.user.getInitials();
    }
}