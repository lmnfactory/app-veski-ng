import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { EventHandler } from 'core/index';
import { fadeHiddenAnimation } from 'animation/index';

import { UserService } from '../service/user.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.css'],
    animations: [ fadeHiddenAnimation ]
})

export class ProfileComponent implements OnInit {

    @Input('lmnUser')
    private userId: number;

    private userService: UserService;
    private eventHandler: EventHandler;
    public user: any;
    private events: any[];
    private formVisibility: string;

    constructor(userService: UserService, eventHandler: EventHandler) {
        this.userService = userService;
        this.eventHandler = eventHandler;
        this.events = [];
        this.user = null;
        this.formVisibility = "";
    }

    private onLoad(): void {
        this.user = this.userService.get();
    }

    ngOnInit() {
        this.userService.request({});
        this.events.push(
            this.eventHandler.on('user.load', this.onLoad.bind(this))
        );
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    getAvatar(): string {
        if (!this.user) {
            return "";
        }
        return "file/" + this.user.get('settings.avatar_file.public_id');
    }

    hasAvatar(): boolean {
        return (this.user && this.user.get('settings.avatar_file.public_id') && this.user.get('settings.avatar_file.public_id') != null);
    }

    show(name: string): void {
        this.formVisibility = name;
    }

    hide(): void {
        this.formVisibility = "";
    }

    isVisible(name: string = ""): boolean {
        return (this.formVisibility == name);
    }
}