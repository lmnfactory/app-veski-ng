import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FormatTextContainerDirective } from './directive/format-text-container.directive';

import { FormatTextComponent } from './component/format-text.component';
import { FormatPlainComponent } from './component/format-plain.component';
import { FormatTagComponent } from './component/format-tag.component';
import { FormatLinkComponent } from './component/format-link.component';

import { TextFormaterService } from './service/text-formater.service';
import { HashtagTextService } from './service/hashtag-text.service';

@NgModule({
    imports: [ CommonModule, RouterModule ],
    exports: [ FormatTextComponent, FormatPlainComponent, FormatLinkComponent, FormatTagComponent ],
    declarations: [ FormatTextComponent, FormatPlainComponent, FormatTagComponent, FormatLinkComponent, FormatTextContainerDirective ],
    providers: [TextFormaterService, HashtagTextService],
    entryComponents: [ FormatPlainComponent, FormatTagComponent, FormatLinkComponent ]
})
export class TextModule { }
