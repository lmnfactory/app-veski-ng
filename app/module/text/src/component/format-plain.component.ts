import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-format-plain',
    templateUrl: 'format-plain.component.html'
})

export class FormatPlainComponent implements OnInit {

    @Input('text')
    public text: string;
    
    constructor() { }

    ngOnInit() { }
}