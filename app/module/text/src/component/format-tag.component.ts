import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-format-tag',
    templateUrl: 'format-tag.component.html'
})

export class FormatTagComponent implements OnInit {

    @Input('text')
    public text: string;
    @Input('link')
    public link: string;
    
    constructor() { }

    ngOnInit() { }
}