import { Component, OnInit, AfterViewInit, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef } from '@angular/core';

import { FormatPlainComponent } from './format-plain.component';
import { FormatTagComponent } from './format-tag.component';
import { FormatTextContainerDirective } from '../directive/format-text-container.directive';

@Component({
    moduleId: module.id,
    selector: 'lmn-format-text',
    templateUrl: 'format-text.component.html',
    styleUrls: ['format-text.component.css']
})

export class FormatTextComponent implements OnInit, AfterViewInit {

    @Input('lmnComponents')
    public components: any;
    @ViewChild(FormatTextContainerDirective)
    public containerDirective: FormatTextContainerDirective;

    private container: ViewContainerRef;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {

    }

    ngAfterViewInit() {
        this.container = this.containerDirective.viewContainerRef;
        this.container.clear();

        for (let i: number = 0; i < this.components.length; i++) {
            let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.components[i].getType());
            let componentRef: ComponentRef<any> = this.container.createComponent(componentFactory);
            let inputs: any = this.components[i].getInputs();
            for (let key in inputs) {
                componentRef.instance[key] = inputs[key];
            }
        }
    }

    ngOnInit() { }
}