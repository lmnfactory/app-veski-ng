import { Injectable } from '@angular/core';

@Injectable()
export class HashtagTextService {

    constructor() { }

    extract(text: string): string[] {
        let wordend: string = ' \t\n\r\v.';
        let startTag: boolean = false;
        let tag: string = "";
        let tags: any[] = [];
        let i: number = 0;
        if (!text) {
            return tags;
        }
        for (i = 0; i < text.length; i++) {
            if (wordend.indexOf(text[i]) > -1 && startTag) {
                tags.push(tag);
                tag = "";
                startTag = false;
            }

            if (startTag) {
                tag += text[i];
            }

            if (text[i] == "#") {
                startTag = true;
            }
        }

        if (tag != "") {
            tags.push(tag);
            tag = "";
        }

        return tags;
    }
}