import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import * as _ from "lodash";

import { HashtagTextService } from './hashtag-text.service';
import { TextFormat } from '../object/text-format.object';
import { FormatPlainComponent } from '../component/format-plain.component';
import { FormatTagComponent } from '../component/format-tag.component';
import { FormatLinkComponent } from '../component/format-link.component';

@Injectable()
export class TextFormaterService {

    private hashtagTextService: HashtagTextService;
    private router: Router;

    constructor(hashtagTextService: HashtagTextService, router: Router, @Inject(APP_BASE_HREF) private appBaseHref: any) {
        this.hashtagTextService = hashtagTextService;
        this.router = router;
    }

    private getIndices(text: string, searchText: string): number[] {
        let indices: number[] = [];
        for(let pos: number = text.indexOf(searchText); pos !== -1; pos = text.indexOf(searchText, pos + 1)) {
            indices.push(pos);
        }
        return indices;
    }

    private addMarks(text: string, startPosition: number, endPosition: number, startMark: string, endMark: string): string {
        return text.substring(0, startPosition) + startMark + text.substring(startPosition, endPosition) + endMark + text.substring(endPosition);
    }

    private splitComponent(component: any, startPosition: number, endPosition: number): any[] {
        return [component];
    }

    escapeHtml(text: string): string {
        return _.escape(text);
    }

    detectLinks(components: any[]): any[]  {
        let newComponents: any[] = [];
        var exp = /(\bhttps?:\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        for (let i: number = 0; i < components.length; i++) {
            if (!components[i].isPlain()) {
                newComponents.push(components[i]);
                continue;
            }
            let matches: any = components[i].getText().match(exp);
            if (!matches) {
                newComponents.push(components[i]);
            } else {
                let text: string = components[i].getText();
                for (let j: number = 0; j < matches.length; j++) {
                    let startPosition = text.indexOf(matches[j]);
                    if (startPosition == -1) {
                        continue;
                    }
                    if (startPosition > 0) {
                        newComponents.push(new TextFormat(FormatPlainComponent, {text: text.substr(0, startPosition)}, true));
                    }

                    let link: string = text.substr(startPosition, matches[j].length);
                    newComponents.push(new TextFormat(FormatLinkComponent, {text: link, link: link}));
                    text = text.substr(startPosition + matches[j].length);
                }
                if (text.length > 0) {
                    newComponents.push(new TextFormat(FormatPlainComponent, {text: text}, true));
                }
            }
        }
        return newComponents;
    }

    detectHashtags(components: any[], tags: any[]): any[] {
        let newComponents: any[] = [];
        for (let i: number = 0; i < components.length; i++) {
            if (!components[i].isPlain()) {
                newComponents.push(components[i]);
                continue;
            }
            
            let text: string = components[i].getText();
            let pos: number = text.indexOf("#");
            while (pos > -1) {
                let tag: any = null;
                for (let j: number = 0; j < tags.length; j++) {
                    if (tags[j].value != text.substr(pos + 1, tags[j].value.length)) {
                        continue;
                    }
                    tag = tags[j];
                    if (pos > 0) {
                        newComponents.push(new TextFormat(FormatPlainComponent, {text: text.substr(0, pos)}, true));
                    }
                    let url: string = this.router.createUrlTree(['tag', tag.slug]).toString();
                    newComponents.push(new TextFormat(FormatTagComponent, {text: "#" + tag.value, link: url}));
                    text = text.substr(pos + tag.value.length + 1);
                    break;
                }
                if (tag == null) {
                    text = text.substr(pos + 1);
                }
                pos = text.indexOf("#");
            }
            if (text.length > 0) {
                newComponents.push(new TextFormat(FormatPlainComponent, {text: text}, true));
            }
        }
        return newComponents
    }

    detectUsers(text: string): string {
        return text;
    }

    format(text: string): string {
        text = text.replace(/{\[#\]/g, '<span class="highlight-word">');
        text = text.replace(/\[#\]}/g, '</span>');
        return text;
    }

    makePlain(text: string): any {
        return new TextFormat(FormatPlainComponent, {text: text}, true);
    }
}