import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[lmn-format-text-container]',
})
export class FormatTextContainerDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}