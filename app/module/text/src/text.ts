export { FormatTextComponent } from './component/format-text.component';
export { FormatPlainComponent } from './component/format-plain.component';
export { FormatTagComponent } from './component/format-tag.component';
export { FormatLinkComponent } from './component/format-link.component';

export { TextFormaterService } from './service/text-formater.service';
export { HashtagTextService } from './service/hashtag-text.service';

export { TextModule } from './text.module';