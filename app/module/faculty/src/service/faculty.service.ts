import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { EventHandler, Model, ModelService } from 'core/index';
import { LmnHttp } from 'http-communication/index';

@Injectable()
export class FacultyService {

    private eventHandler: EventHandler;
    private http: LmnHttp;
    private modelService: ModelService;

    constructor(http: LmnHttp, eventHandler: EventHandler, modelService: ModelService) {
        this.http = http;
        this.eventHandler = eventHandler;
        this.modelService = modelService;
    }

    private notifyAll(): void {
        
    }

    model(data: any): Model {
        return this.modelService.create('faculty', data);
    }

    search(search: any): Observable<Model[]> {
        let observer: Observable<Model[]> = this.http.post('api/faculty/search', {search: search})
            .map((data: any) => {
                let items: any[] = [];
                for (let i: number = 0; i < data.data.length; i++) {
                    items.push(this.model(data.data[i]));
                }
                return items;
            });

        return observer;
    }
}