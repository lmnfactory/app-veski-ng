import { Injectable } from '@angular/core';

import { CacheService, AbstractCacheable, Model, ModelService } from 'core/index';

import { FacultyModel } from '../object/faculty.model';

@Injectable()
export class FacultyCacheService extends AbstractCacheable {

    private faculties: Model[];

    private modelService: ModelService;

    constructor(cacheService: CacheService, modelService: ModelService) {
        super(cacheService);
        this.modelService = modelService;
        this.faculties = [];
        this.onCacheChange();
    }

    onCacheChange(): void {
        let rawFaculties = this.cacheService.get('faculty')

        this.faculties = [];
        for (let faculty of rawFaculties) {
            this.faculties.push(this.modelService.create('faculty', faculty));
        }
    }
}
