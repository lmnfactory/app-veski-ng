import { ModelService, BasicModel, Model } from 'core/index';

import { UniversityService } from 'university/index';

export class FacultyModel extends BasicModel {

    private modelService: ModelService;
    private universityService: UniversityService;

    constructor(modelService: ModelService, universityService: UniversityService) {
        super();
        this.modelService = modelService;
        this.universityService = universityService;
    }

    _getUniversity(data: any): Model {
        let universityId: number = this.get('university_id');
        return this.universityService.get(universityId);
    }

    export(): any {
        return {};
    }
}
