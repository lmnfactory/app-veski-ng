import { ModelService, ModelFactory, Model } from 'core/index';
import { UniversityService } from 'university/index';

import { FacultyModel } from './faculty.model';

export class FacultyFactory implements ModelFactory {

    private modelService: ModelService;
    private universityService: UniversityService;

    constructor(modelService: ModelService, universityService: UniversityService) {
        this.modelService = modelService;
        this.universityService = universityService;
    }

    create(data: any): Model {
        let model: Model = new FacultyModel(this.modelService, this.universityService);
        model.fill(data);
        return model;
    }
}
