export { FacultyCacheService } from './service/faculty-cacheable.service';
export { FacultyService } from './service/faculty.service';

export { FacultyFactory } from './object/faculty-factory.object';
export { FacultyModel } from './object/faculty.model';

export { FacultyInputComponent } from './component/faculty-input.component';

export { FacultyModule } from './faculty.module';
