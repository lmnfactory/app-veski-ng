import { Component, Input, Output, OnInit, OnDestroy, EventEmitter, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Model, CacheService } from 'core/index';
import { AutocompleteService, Autocomplete, FormService } from 'form-ui/index';

import { FacultyService } from '../service/faculty.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-faculty-input',
    templateUrl: 'faculty-input.component.html',
    styleUrls: ['faculty-input.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FacultyInputComponent),
            multi: true
        }
    ]
})

export class FacultyInputComponent implements OnInit, OnDestroy, ControlValueAccessor {

    @Input('lmnPlaceholder')
    public placeholder: string;
    @Input('lmnId')
    public id: string;
    @Output('lmnFocus')
    private focusEmitter: EventEmitter<any>;
    @Output('lmnBlur')
    private blurEmitter: EventEmitter<any>;

    private facultyService: FacultyService;
    private autocompleteService: AutocompleteService;
    private formService: FormService;
    private cacheService: CacheService;
    private _value: any;
    private valueChange: any;
    public inputValue: string;
    public autocomplete: Autocomplete;
    private defaultItems: any[];

    constructor(facultyService: FacultyService, autocompleteService: AutocompleteService, formService: FormService, cacheService: CacheService) {
        this.facultyService = facultyService;
        this.autocompleteService = autocompleteService;
        this.formService = formService;
        this.cacheService = cacheService;
        this.focusEmitter = new EventEmitter();
        this.blurEmitter = new EventEmitter();
        this.autocomplete = autocompleteService.create([]);
        this.id = this.formService.generateId();
        this.defaultItems = [];
    }

    private getDetaulItems(): Model[] {
        if (this.defaultItems.length == 0) {
            let cache: any[] = this.cacheService.get('facultyAutocomplete');
            for (let i: number = 0; i < cache.length; i++) {
                this.defaultItems.push(this.facultyService.model(cache[i]));
            }
        }
        return this.defaultItems;
    }

    private search(): void {
        if (!this.inputValue || this.inputValue == null || this.inputValue == "") {
            this.autocomplete.setItems(this.getDetaulItems());
            this.autocomplete.setSelected(0);
            this.autocomplete.show();
        } else {
            this.facultyService.search(this.inputValue)
                .subscribe((faculties: Model[]) => {
                    if (!this.autocomplete.isVisible()) {
                        return;
                    }
                    this.autocomplete.setItems(faculties);
                    this.autocomplete.setSelected(0);
                    this.autocomplete.show();
                });
        }         
    }

    private hasInputValue(): boolean {
        return (this.inputValue != null && this.inputValue != "" && this.inputValue !== undefined);
    }

    ngOnInit(): void {}

    ngOnDestroy(): void {}

    set(value: string): void {
        this.inputValue = value;
        this.autocompleteService.timeout(() => {
            this.search();
        });
        
        this.setValue(null);
    }

    setValue(value: any): void {
        this._value = value;
        if (this._value) {
            this.inputValue = this._value.get('name');
            this.autocomplete.hide();
        }
        if (this.valueChange) {
            this.valueChange(this._value);
        }
    }

    getValue(value: any): void {
        this._value = value;
        if (this.valueChange) {
            this.valueChange(this._value);
        }
    }

    focus(event: any): void {
        this.search();
        this.focusEmitter.emit(event);
    }

    blur(event: any): void {
        if (this._value == null && this.hasInputValue()) {
            let items: Model[] = this.autocomplete.getItems();
            let inputValueLower: string = this.inputValue.toLowerCase();
            for (let i: number = 0; i < items.length; i++) {
                if (inputValueLower == items[i].get('name').toLowerCase() || this.inputValue == items[i].get('code').toLowerCase()) {
                    this.setValue(items[i]);
                    break;
                }
            }
        }
        this.autocomplete.hide();
        this.blurEmitter.emit(event);
    }

    writeValue(obj: any) : void {
        this.setValue(obj);
    }

    registerOnChange(fn: any) : void {
        this.valueChange = fn;
    }

    registerOnTouched(fn: any) : void { }
}