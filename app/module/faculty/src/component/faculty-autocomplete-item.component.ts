import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-faculty-autocomplete-item',
    templateUrl: 'faculty-autocomplete-item.component.html',
    styleUrls: ['faculty-autocomplete-item.component.css']
})

export class FacultyAutocompleteItemComponent implements OnInit {

    @Input('lmnItem')
    public item: any;
    @Input('lmnSelected')
    public selected: boolean;

    constructor() { }

    ngOnInit() { }
}