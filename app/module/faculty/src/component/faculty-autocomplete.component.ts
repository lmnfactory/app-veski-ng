import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { Autocomplete } from 'form-ui/index';
import { fadeHiddenAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-faculty-autocomplete',
    templateUrl: 'faculty-autocomplete.component.html',
    styleUrls: ['faculty-autocomplete.component.css'],
    animations: [ fadeHiddenAnimation ]
})

export class FacultyAutocompleteComponent implements OnInit {

    @Input('lmnAutocomplete')
    public autocomplete: Autocomplete;
    @Output('lmnSelect')
    public selectEmitter: EventEmitter<any>;

    constructor() {
        this.selectEmitter = new EventEmitter();
    }

    ngOnInit() { }

    ngOnDestroy(): void {}

    onSelect(item: any): void {
        this.selectEmitter.emit(item);
    }
}