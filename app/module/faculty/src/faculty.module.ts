import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { CoreModule, ModelService } from 'core/index';
import { FormUIModule } from 'form-ui/index';
import { UniversityModule, UniversityService } from 'university/index';

import { FacultyInputComponent } from './component/faculty-input.component';
import { FacultyAutocompleteItemComponent } from './component/faculty-autocomplete-item.component';
import { FacultyAutocompleteComponent } from './component/faculty-autocomplete.component';

import { FacultyCacheService } from './service/faculty-cacheable.service';
import { FacultyService } from './service/faculty.service';
import { FacultyFactory } from './object/faculty-factory.object';
import { FacultyModel } from './object/faculty.model';

@NgModule({
    imports:      [ CommonModule, FlexLayoutModule, FormsModule, FormUIModule, CoreModule, UniversityModule ],
    declarations: [ FacultyInputComponent, FacultyAutocompleteComponent, FacultyAutocompleteItemComponent ],
    exports: [ FacultyInputComponent ],
    providers: [ FacultyCacheService, FacultyService ]
})
export class FacultyModule {

    constructor(modelService: ModelService, universityService: UniversityService) {
        modelService.addFactory('faculty', new FacultyFactory(modelService, universityService));
    }
}
