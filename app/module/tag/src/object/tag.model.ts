import { Router } from '@angular/router';

import { ModelService, BasicModel, Model, RouteExec } from 'core/index';

export class TagModel extends BasicModel {

    private modelService: ModelService;
    private router: Router;

    constructor(modelService: ModelService, router: Router) {
        super();
        this.modelService = modelService;
        this.router = router;
    }

    _getSearchModel(data: any): Model {
        return this.modelService.create('search', {
            main: this.get('value'),
            side: this.get('slug'),
            exec: new RouteExec('/tag/' + this.get('slug'), {}, this.router)
        });
    }
 
    export(): any {
        return {
            value: this.get('value')
        };
    }
}
