import { Router } from '@angular/router';

import { ModelService, ModelFactory, Model } from 'core/index';

import { TagModel } from './tag.model';

export class TagFactory implements ModelFactory {

    private modelService: ModelService;
    private router: Router;

    constructor(modelService: ModelService, router: Router) {
        this.modelService = modelService;
        this.router = router;
    }

    create(data: any): Model {
        let model: Model = new TagModel(this.modelService, this.router);
        model.fill(data);
        return model;
    }
}
