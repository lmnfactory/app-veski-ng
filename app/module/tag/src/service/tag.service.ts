import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { LmnHttp } from 'http-communication/index';

@Injectable()
export class TagService {

    private http: LmnHttp;

    constructor(http: LmnHttp) {
        this.http = http;
    }

    search(value: string): Observable<Response> {
        return this.http.post('api/tag/search', {search: value});
    }

    recommend(value: string): Observable<Response> {
        return this.http.post('api/thread/tag/recommend', {});
    }
}
