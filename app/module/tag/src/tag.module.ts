import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';

import { CoreModule, ModelService } from 'core/index';

import { TagComponent } from './component/tag.component';
import { InlineTagsComponent } from './component/inline-tags.component';
import { TagService } from './service/tag.service';
import { TagFactory } from './object/tag-factory.object';

@NgModule({
    imports: [ CommonModule, RouterModule ],
    declarations: [ TagComponent, InlineTagsComponent ],
    providers: [ TagService ],
    exports: [ TagComponent, InlineTagsComponent ]
})
export class TagModule {

    constructor(modelService: ModelService, router: Router) {
        modelService.addFactory('tag', new TagFactory(modelService, router));
    }
}
