export { InlineTagsComponent } from './component/inline-tags.component';
export { TagComponent } from './component/tag.component';
export { TagService } from './service/tag.service';

export { TagModule } from './tag.module';
