import { Component, Input, OnInit, OnDestroy, OnChanges } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-inline-tags',
    templateUrl: 'inline-tags.component.html',
    styleUrls: ['inline-tags.component.css']
})
export class InlineTagsComponent implements OnInit, OnDestroy, OnChanges {

    @Input('lmnTags')
    public tags: any[];
    @Input('lmnLimit')
    public limit: number;

    public showTags: any[];

    constructor() {
        this.showTags = []
    }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {

    }

    ngOnChanges(): void {
        if (this.tags == null || !this.tags) {
            this.showTags = [];
            return;
        }
        if (this.limit && this.limit < this.tags.length) {
            this.showTags = this.tags.slice(0, this.limit);

        } else {
            this.showTags = this.tags;
        }
    }
}
