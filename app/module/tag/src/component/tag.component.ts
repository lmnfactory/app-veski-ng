import { Component, Input, OnInit, OnDestroy } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-tag',
    templateUrl: 'tag.component.html',
    styleUrls: ['tag.component.css']
})
export class TagComponent implements OnInit, OnDestroy {

    @Input('lmnTag')
    public tag: any;

    constructor() {

    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }
}
