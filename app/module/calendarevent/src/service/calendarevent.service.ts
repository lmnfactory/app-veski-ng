import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ModelService, Model, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';

import { CalendareventModel } from '../object/calendarevent.model';

@Injectable()
export class CalendareventService {

    private http: LmnHttp;
    private modelService: ModelService;
    private eventHandler: EventHandler;
    private calendarevents: Model[];
    private events: any[];

    constructor(http: LmnHttp, modelService: ModelService, eventHandler: EventHandler) {
        this.http = http;
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.calendarevents = [];
        this.events = [];
    }

    private notifyAll(): void {
        this.eventHandler.fire('calendarevent.load', this.calendarevents);
    }

    all(): Model[] {
        return this.calendarevents;
    }

    add(calendarevent: Model): void {
        //calendarevent.set('state', 'highlight');
        this.calendarevents.splice(0, 0, calendarevent);
        this.notifyAll();
    }

    get(id: number): Model {
        let i: number = 0;
        for (i = 0; i < this.calendarevents.length; i++) {
            if (id == this.calendarevents[i].get('id')) {
                return this.calendarevents[i];
            }
        }

        return null;
    }

    refresh(calendarevent: Model): void {
        let change: boolean = false;
        let i: number = 0;
        for (i = 0; i < this.calendarevents.length; i++) {
            if (this.calendarevents[i].get('id') == calendarevent.get('id')) {
                this.calendarevents[i] = calendarevent;
                change = true;
            }
        }

        if (change) {
            this.notifyAll();
        }
        else {
            this.add(calendarevent);
        }
    }

    request(data: any): void {
        this.http.post('api/sharedcalendar/subject_list', data)
            .subscribe((data: any) => {
                this.calendarevents = [];
                let list = data.data;
                let i: number = 0;
                for (i = list.length - 1; i >= 0; i--) {
                    let calendareventModel: Model = this.modelService.create('calendarevent', list[i]);
                    this.calendarevents.push(calendareventModel);
                }
                this.notifyAll();
            });
    }
}
