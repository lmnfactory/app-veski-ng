import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ModelService, AbstractObservable, Model, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { StateMachine, StateService } from 'state/index';

import { CalendareventModel } from 'calendarevent/index';

@Injectable()
export class UserCalendarService {

    private http: LmnHttp;
    private modelService: ModelService;
    private eventHandler: EventHandler;
    private calendardays: any;
    private calendarevents: any;

    constructor(http: LmnHttp, modelService: ModelService, eventHandler: EventHandler) {
        this.http = http;
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.calendardays = {};
        this.calendarevents = {};
    }

    private notifyAll() {
        this.eventHandler.fire('calendareventuser.load', this.calendardays);
    }

    all(): any {
        return this.calendardays;
    }

    allEvents(): Model[] {
        return this.calendarevents;
    }

    requestDigest(data: any): any {            
        let days: any = {};
        let list = data.data;
        let options = data.option;
        for (var key in list) {
            if (list.hasOwnProperty(key)) {
                let calendareventModel: Model = this.modelService.create('calendarday', {
                    events: list[key],
                    day: key
                });
                days[key] = calendareventModel;
            }
        }

        let events: any[] = [];
        for (let op of options) {
            events.push(this.modelService.create('calendarevent', op));
        }

        return {
            days: days,
            events: events
        };
    }

    request(data: any): void {
        this.http.post('api/sharedcalendar/user_calendar', data)
            .subscribe((data: any) => {
                let value: any = this.requestDigest(data);
                this.calendardays = value.days;
                for (let i: number = 0; i < value.events.length; i++) {
                    let id: string = value.events[i].get('id');
                    this.calendarevents[id] = value.events[i];
                }
                
                this.notifyAll();
            });
    }

    join(data: any): Observable<Response> {
        let observer: Observable<Response> = this.http.post('api/sharedcalendar/join', data);
        
        observer.subscribe((data: any) => {
            this.eventHandler.fire('calendareventuser.change', {});
        });

        return observer;
    }

    leave(data: any): Observable<Response> {
        let observer: Observable<Response> = this.http.post('api/sharedcalendar/leave', data);
            
        observer.subscribe((data: any) => {
            this.eventHandler.fire('calendareventuser.change', {});
        });

        return observer;
    }
}
