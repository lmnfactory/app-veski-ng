import { ModelService, BasicModel, Model } from 'core/index';

export class CalendareventruleModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    // _setRule(data: any): any {
    //     this.set('rule_base', data);
    //     return data;
    // }

    // _getRuleBase(data: any): string {
    //     if (!data) {
    //         data = this.get('rule');
    //     }

    //     let exp: string[] = data.split(":");

    //     return exp[0];
    // }

    export(): any {
        return {
            rule: this.get('rule')
        };
    }
}
