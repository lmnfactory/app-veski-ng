import { ModelService, ModelFactory, Model } from 'core/index';

import { CalendareventruleModel } from './calendareventrule.model';

export class CalendareventruleFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new CalendareventruleModel(this.modelService);
        model.fill(data);
        return model;
    }
}
