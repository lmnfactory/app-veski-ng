import { ModelService, ModelFactory, Model } from 'core/index';

import { CalendareventModel } from './calendarevent.model';

export class CalendareventFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new CalendareventModel(this.modelService);
        model.fill(data);
        return model;
    }
}
