import { ModelService, BasicModel, Model } from 'core/index';

import *  as moment from 'moment/moment';

export class CalendareventModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getEventstart(data: any): Date {
        return moment(data).toDate();
    }

    _setUntil(data: any): any {
        return data;
    }

    _getUntil(data: any): any {
        if (!this._data.until) {
            let start: Date = this.get('eventstart');
            return new Date(start.getTime() + this.get('duration') * 1000);
        }
        return new Date(data);
    }

    _getCalendareventrules(data: any): any {
        if (data.length > 0) {
            data = data[0];
        }
        return this.modelService.create('calendareventrule', data);
    }

    _getCalendareventsubject(data: any): any {
        return this.modelService.create('calendareventsubject', data);
    }

    _getCalendareventuser(data: any): any {
        return this.modelService.create('calendareventuser', data);
    }

    printTime(eventstart: Date = null): string {
        if (eventstart == null) {
            eventstart = this.get('eventstart');
        }
        if (this.get('allday')) {
            return "celý deň";
        }
        return moment(eventstart).format("HH:mm");
    }

    printDatetime(eventstart: Date = null): string {
        if (eventstart == null) {
            eventstart = this.get('eventstart');
        }
        if (this.get('allday')) {
            return moment(eventstart).format("MMM DD, Y") + " celý deň";
        }
        return moment(eventstart).format("MMM DD, Y HH:mm");
    }

    export(): any {
        let eventend: string = null;
        if (this.get('eventend')) {
            let eventend: string = moment(this.get('eventend')).format("YYYY-MM-DD HH:mm:ss");
        }
        let eventstart: string = moment(this.get('eventstart')).format("YYYY-MM-DD HH:mm:ss");
        return {
            public_id: this.get('public_id'),
            name: this.get('name'),
            description: this.get('description'),
            location: this.get('location'),
            eventstart: eventstart,
            eventend: eventend,
            duration: this.get('duration'),
            priority: this.get('priority'),
            allday: this.get('allday'),
            share: this.get('share'),
            subject_id: this.get('subject_id'),
            calendareventsubject: this.get('calendareventsubject').export(),
            calendareventrules: this.get('calendareventrules').export()
        };
    }
}
