import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { Model, EventHandler, EventToken, Garbage, GarbageService } from 'core/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { CalendareventService } from '../service/calendarevent.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-calendarevent-list',
    templateUrl: 'calendarevent-list.component.html',
    styleUrls: ['calendarevent-list.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})

export class CalendareventListComponent implements OnInit {

    @Input('lmnPublicId')
    private subjectPid: string;
    @Input('lmnListen')
    public eventToken: EventToken;
    @Input('lmnHighlight')
    public highlightId: number;

    public calendarevents: Model[];
    private calendareventService: CalendareventService;
    private eventHandler: EventHandler;
    private garbage: Garbage;
    private loading: boolean;

    constructor(calendareventService: CalendareventService, eventHandler: EventHandler, garbageService: GarbageService) {
        this.calendareventService = calendareventService;
        this.eventHandler = eventHandler;
        this.calendarevents = [];
        this.garbage = garbageService.create();
        this.loading = false;
    }

    private onLoad(data: any): void {
        this.calendarevents = this.calendareventService.all();
        this.loading = false;
    }

    private load(): void {
        if (!this.subjectPid) {
            return;
        }
        this.loading = true;
        this.calendareventService.request({subject_pid: this.subjectPid});
    }

    private onCalendareventChange(calendarevent: Model): void {
        // TODO: load only if calendarevent is for this subject
        this.load();
    }

    ngOnInit() {
        this.garbage.event(
            this.eventToken.subscribe(this.onChange.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('calendarevent.load', this.onLoad.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('calendareventuser.change', this.load.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('calendarevent.create', this.onCalendareventChange.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('calendarevent.update', this.onCalendareventChange.bind(this))
        );
        this.calendarevents = this.calendareventService.all();
        this.load();
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    onChange(event: any): void {
        this.subjectPid = event.subjectPid;
        this.load();
    }

    gotoCreateeventForm(): void {
        this.eventHandler.fire('action.calendarevent.create');
    }

    shouldShowNoContentText(): boolean {
        return (!this.calendarevents.length && !this.loading)
    }
}