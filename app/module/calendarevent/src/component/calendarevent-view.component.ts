import { Component, OnInit, Input } from '@angular/core';

import { Model, EventHandler } from 'core/index';

import { UserCalendarService } from '../service/user-calendar.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-calendarevent-view',
    templateUrl: 'calendarevent-view.component.html',
    styleUrls: ['calendarevent-view.component.css']
})

export class CalendareventViewComponent implements OnInit {

    @Input('lmnCalendarevent')
    public calendarevent: Model;

    private userCalendarService: UserCalendarService;
    private eventHandler: EventHandler;
    public inProgress: boolean;

    constructor(userCalendarService: UserCalendarService, eventHandler: EventHandler) {
        this.userCalendarService = userCalendarService;
        this.eventHandler = eventHandler;
        this.inProgress = false;
    }

    ngOnInit() { }

    hasPriority(): boolean {
        return (this.calendarevent.get('priority') == 5);
    }

    join(calendareventId: number): void {
        this.inProgress = true;
        this.userCalendarService.join({calendarevent_id: calendareventId});
    }

    leave(calendareventId: number): void {
        this.inProgress = true;
        this.userCalendarService.leave({calendarevent_id: calendareventId});
    }

    edit(): void {
        this.eventHandler.fire('action.calendarevent.edit', this.calendarevent);
    }
}