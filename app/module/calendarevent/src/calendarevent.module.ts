import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";

import { CoreModule, ModelService } from 'core/index';
import { FormUIModule } from 'form-ui/index';
import { LayoutModule } from 'layout/index';

import { CalendareventFactory } from './object/calendarevent-factory.object';
import { CalendareventruleFactory } from './object/calendareventrule-factory.object';
import { CalendareventService } from './service/calendarevent.service';
import { UserCalendarService } from './service/user-calendar.service';
import { CalendareventListComponent } from './component/calendarevent-list.component';
import { CalendareventViewComponent } from './component/calendarevent-view.component';

@NgModule({
    imports: [ CommonModule, FormsModule, FlexLayoutModule, CoreModule, LayoutModule, FormUIModule ],
    declarations: [ CalendareventListComponent, CalendareventViewComponent ],
    exports: [ CalendareventListComponent ],
    providers: [ CalendareventService, UserCalendarService ]
})
export class CalendareventModule {

    constructor(modelService: ModelService) {
        modelService.addFactory('calendarevent', new CalendareventFactory(modelService));
        modelService.addFactory('calendareventrule', new CalendareventruleFactory(modelService));
    }
}
