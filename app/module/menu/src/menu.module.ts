import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from "@angular/flex-layout";

import { AnimationModule } from 'animation/index';
import { AccountModule } from 'account/index';

import { MenuService } from './service/menu.service';
import { MenuItemComponent } from './component/menu-item.component';
import { MenuComponent } from './component/menu.component';
import { MobileMenuComponent } from './component/mobile-menu.component';
import { MenuItemLoadingComponent } from './component/menu-item-loading.component';

@NgModule({
  imports:      [ CommonModule, FormsModule, RouterModule, FlexLayoutModule, AnimationModule, AccountModule ],
  declarations: [ MenuComponent, MenuItemComponent, MenuItemLoadingComponent, MobileMenuComponent ],
  exports: [ MenuComponent, MenuItemComponent, MenuItemLoadingComponent, MobileMenuComponent ],
  providers: [ MenuService ]
})
export class MenuModule { }
