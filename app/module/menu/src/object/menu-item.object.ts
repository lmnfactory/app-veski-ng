import { Exec } from 'core/index';

export class MenuItem {

    public title: String;
    public description: String;
    public palette: String;
    private clickHandler: Exec;
    private notificationCount: number;
    private _active: boolean;

    constructor(title: String, description: String, notificationCount: number, palette: String, clickHandler: Exec) {
        this.title = title;
        this.description = description;
        this.palette = palette;
        this.clickHandler = clickHandler;
        this.notificationCount = notificationCount;
        this._active = false;
    }

    getPalette(): String {
        return this.palette;
    }

    setPalette(palette: String): void {
        this.palette = palette;
    }

    getTitle(): String {
        return this.title
    }

    getDescription(): String {
        return this.description;
    }

    onClick(): void {
        this.clickHandler.execute();
    }

    getNotificationCount() {
        return this.notificationCount;
    }

    isActive(): boolean {
        return this._active;
    }

    active(): void {
        this._active = true;
    }

    idle(): void {
        this._active = false;
    }

    clearNotificationCount() {
        this.notificationCount = 0;
    }

    incNotification(n: number = 1) {
        this.notificationCount += n;
    }
}
