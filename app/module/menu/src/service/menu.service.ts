import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, Observer, RouteExec, Exec } from 'core/index';

import { MenuItem } from '../object/menu-item.object';

@Injectable()
export class MenuService implements Observable {

    private items: Map<String, MenuItem>;
    private subscribers: Array<Observer>;
    private activeItem: string;

    constructor(router: Router) {
        this.items = new Map();
        this.subscribers = new Array();
    }

    create(title: String, description: String, notificationCount: number, palette: String, clickHandler: Exec): MenuItem {
        return new MenuItem(title, description, notificationCount, palette, clickHandler);
    }

    add(key: String, item: MenuItem): void {
        this.items.set(key, item);
        if (key == this.activeItem) {
            item.active();
        }
        this.notifyAll();
    }

    remove(key: String) {
        this.items.delete(key);
        this.notifyAll();
    }

    incNotification(key: string, n: number = 1): void {
        let item: MenuItem = this.items.get(key);
        if (item == null) {
            return ;
        }

        item.incNotification(n);
        //this.notifyAll();
    }

    clearNotification(key: string): void {

    }

    getAll(): Map<String, MenuItem> {
        return this.items;
    }

    activateByKey(key: string): void {
        this.activeItem = key;
        this.activate(this.items.get(key));
    }

    activate(item: MenuItem): void {
        this.items.forEach((i: MenuItem) => {
            i.idle();
        });
        if (item) {
            item.active();
        } else {
            this.activeItem = "";
        }
    }

    subscribe(observer: Observer): void {
        this.subscribers.push(observer);
    }

    unsubscribe(observer: Observer): void {
        let index = this.subscribers.indexOf(observer);
        if (index > -1) {
            this.subscribers.splice(index, 1);
        }
    }

    notifyAll(): void {
        this.subscribers.forEach(item => {
            item.update();
        });
    }
}
