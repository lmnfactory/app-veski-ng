import { Component, OnInit, OnDestroy, Input, HostListener } from '@angular/core';

import { Observer, EmptyExec } from 'core/index';
import { fadeIfAnimation } from 'animation/index';

import { MenuService } from '../service/menu.service';
import { MenuItem } from '../object/menu-item.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-menu',
    templateUrl: 'menu.component.html',
    styleUrls: ['menu.component.css'],
    animations: [ fadeIfAnimation ]
})
export class MenuComponent implements OnInit, OnDestroy, Observer {

    public items: Map<String, MenuItem>;
    public dummyItems: MenuItem[];
    public keys: String[];
    private menuService: MenuService;
    private loading: boolean;
    @Input('lmnScrollFix')
    public scorllFix: boolean;

    constructor(menuService: MenuService) {
        this.menuService = menuService;
    }

    ngOnInit(): void {
        this.menuService.subscribe(this);
        this.initItems();
        this.dummyItems = [];
        this.dummyItems.push(new MenuItem('', "", 0, 'palette-loading', new EmptyExec()));
        this.dummyItems.push(new MenuItem('', "", 0, 'palette-loading', new EmptyExec()));
        this.dummyItems.push(new MenuItem('', "", 0, 'palette-loading', new EmptyExec()));
    }

    ngOnDestroy(): void {
        this.menuService.unsubscribe(this);
    }

    private initItems(): void {
        this.items = this.menuService.getAll();
        this.keys = [];
        this.items.forEach((value: MenuItem, key: String) => {
            this.keys.push(key);
        });
    }

    ngOnChanges(args: any) {

    }

    update(): void {
        this.initItems();
    }

    onClick(item: MenuItem): void {
        //this.menuService.activate(item);
    }

    isLoading(): boolean {
        // TODO: get notified about loading.
        return false;
    }
}
