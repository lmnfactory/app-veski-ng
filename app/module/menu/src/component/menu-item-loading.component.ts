import { Component, Input } from '@angular/core';

import { MenuItem } from '../object/menu-item.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-menu-item-loading',
    templateUrl: 'menu-item.component.html',
    styleUrls: ['menu-item.component.css']
})
export class MenuItemLoadingComponent {

    @Input()
    item: MenuItem;

    constructor() {

    }

    onClick(): void {

    }

    getNotificationCount(): string {
        return (this.item.getNotificationCount() + "");
    }
}
