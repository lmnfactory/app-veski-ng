import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { Observer, EmptyExec, EventHandler } from 'core/index';
import { HeaderService } from 'header/index';

import { MenuService } from '../service/menu.service';
import { MenuItem } from '../object/menu-item.object';
import { fadeHiddenAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-mobile-menu',
    templateUrl: 'mobile-menu.component.html',
    styleUrls: ['mobile-menu.component.css'],
    animations: [ fadeHiddenAnimation ]
})
export class MobileMenuComponent implements OnInit, OnDestroy, Observer {

    @Input('lmnShow')
    public show: boolean;
    @Output('lmnClose')
    private closeEmitter: EventEmitter<any>;
    public items: Map<String, MenuItem>;
    public keys: String[];
    private menuService: MenuService;
    private headerService: HeaderService;
    private eventHandler: EventHandler;
    public tabs: any[];
    private events: any[];

    constructor(menuService: MenuService, headerService: HeaderService, eventHandler: EventHandler) {
        this.menuService = menuService;
        this.headerService = headerService;
        this.eventHandler = eventHandler;
        this.closeEmitter = new EventEmitter();
        this.tabs = [];
        this.events = [];
    }

    private loadTabs(): void {
        this.tabs = this.headerService.get('tabs');
        if (!this.tabs) {
            this.tabs = [];
        }
    }

    ngOnInit(): void {
        this.menuService.subscribe(this);
        this.events.push(
            this.eventHandler.on('header.change', this.loadTabs.bind(this))
        )
        this.initItems();
    }

    ngOnDestroy(): void {
        this.menuService.unsubscribe(this);
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    private initItems(): void {
        this.items = this.menuService.getAll();
        this.keys = [];
        this.items.forEach((value: MenuItem, key: String) => {
            this.keys.push(key);
        });
    }

    ngOnChanges(args: any) {

    }

    update(): void {
        this.initItems();
    }

    hide(): void {
        this.show = false;
        this.closeEmitter.emit({});
    }

    onTabClick(tab: any): void {
        for (let i: number = 0; i < this.tabs.length; i++) {
            this.tabs[i].deselect();
        }
        if (!tab) {
            return;
        }
        tab.execute();
        tab.select();
    }
}
