import { Component, Input, Output, EventEmitter } from '@angular/core';

import { MenuItem } from '../object/menu-item.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-menu-item',
    templateUrl: 'menu-item.component.html',
    styleUrls: ['menu-item.component.css']
})
export class MenuItemComponent {

    @Input()
    public item: MenuItem;
    @Output('lmnClick')
    public clickEmitter: EventEmitter<any>;

    constructor() {
        this.clickEmitter = new EventEmitter();
    }

    onClick(): void {
        this.item.onClick();
        this.item.clearNotificationCount();
        this.clickEmitter.emit(this.item);
    }

    getNotificationCount(): string {
        if (this.item.getNotificationCount() > 9) {
            return "9+";
        }
        return (this.item.getNotificationCount() + "");
    }
}
