import { ModelService, ModelFactory, Model } from 'core/index';

import { DegreeModel } from './degree.model';

export class DegreeFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new DegreeModel(this.modelService);
        model.fill(data);
        return model;
    }
}
