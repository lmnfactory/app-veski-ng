import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule, ModelService } from 'core/index';

import { DegreeFactory } from './object/degree-factory.object';
import { DegreeService } from './service/degree.service';

@NgModule({
    imports: [ CommonModule, CoreModule ],
    exports: [],
    declarations: [],
    providers: [ DegreeService ],
})
export class DegreeModule {

    constructor(modelService: ModelService) {
        modelService.addFactory('degree', new DegreeFactory(modelService));
    }
}
