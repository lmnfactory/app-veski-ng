import { Injectable } from '@angular/core';

import { AbstractCacheable, CacheService, Model, ModelService } from 'core/index';

@Injectable()
export class DegreeService extends AbstractCacheable {

    private modelService: ModelService;
    private degrees: Model[];

    constructor(cacheService: CacheService, modelService: ModelService) {
        super(cacheService);
        this.modelService = modelService;
        this.degrees = [];
        this.onCacheChange();
    }

    all(): Model[] {
        return this.degrees;
    }

    onCacheChange(): void {
        let rawDegrees: any[] = this.cacheService.get('degree');

        this.degrees = [];
        for (let degree of rawDegrees) {
            this.degrees.push(this.modelService.create('degree', degree));
        }
    }
}
