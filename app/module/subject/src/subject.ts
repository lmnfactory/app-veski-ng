export { SubjectModel } from './object/subject.model';
export { SubjectFactory } from './object/subject-factory.object';
export { SubjectsettingsModel } from './object/subject-settings.model';
export { SubjectsettingsFactory } from './object/subject-settings-factory.object';
export { SubjectuserModel } from './object/subject-user.model';
export { SubjectuserFactory } from './object/subject-user-factory.object';
export { SubjectusersettingsModel } from './object/subject-user-settings.model';
export { SubjectusersettingsFactory } from './object/subject-user-settings-factory.object';
export { SubjectprototypeModel } from './object/subjectprototype.model';
export { SubjectprototypeFactory } from './object/subjectprototype-factory.object';
export { SubjectService } from './service/subject.service';
export { SubjectSettingsService } from './service/subject-settings.service';
export { SubjectUserService } from './service/subject-user.service';
export { SubjectRouterService } from './service/subject-router.service';

export { SubjectViewComponent } from './component/view.component';
export { JoinComponent } from './component/join.component';
export { CreateComponent } from './component/create.component';
export { AboutComponent } from './component/about.component';
export { AboutEditComponent } from './component/about-edit.component';
export { SearchItemComponent } from './component/search-item.component';
export { SubjectItemComponent } from './component/subject-item.component';
export { ThreadsComponent } from './component/threads.component';

export { SubjectModule } from './subject.module';
