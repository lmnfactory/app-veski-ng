import { Component, OnInit, Input } from '@angular/core';

import { ModelService, Model, CacheService } from 'core/index';
import { fadeHiddenAnimation } from 'animation/index';
import { AutocompleteService } from 'form-ui/index';

import { SubjectService } from '../service/subject.service';
import { SubjectUserService } from '../service/subject-user.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-subject-join',
    templateUrl: 'join.component.html',
    styleUrls: ['join.component.css'],
    animations: [ fadeHiddenAnimation ]
})

export class JoinComponent implements OnInit {

    @Input('lmnSearch')
    public searchValue: string;
    private focusValue: boolean;
    private visible: boolean;
    public items: any[];
    public subjects: any[];
    public selected: number;
    public showCreateForm: boolean;
    private defaultItems: any[];

    private subjectService: SubjectService;
    private subjectUserService: SubjectUserService;
    private modelService: ModelService;
    private autocompleteService: AutocompleteService;
    private cacheService: CacheService;

    constructor(subjectService: SubjectService, subjectUserService: SubjectUserService, modelService: ModelService, autocompleteService: AutocompleteService, cacheService: CacheService) { 
        this.subjectService = subjectService;
        this.subjectUserService = subjectUserService;
        this.modelService = modelService;
        this.autocompleteService = autocompleteService;
        this.cacheService = cacheService;
        this.setItems([]);
        this.selected = 0;
        this.visible = false;
        this.showCreateForm = false;
        this.defaultItems = [];
    }

    private setItems(items: any[]): void {
        this.subjects = items;
        this.items = [];
        for (let i: number = 0; i < this.subjects.length; i++) {
            this.items.push(this.subjects[i]);
        }
        this.items.push(null);
    }

    private getDetaulItems(): Model[] {
        if (this.defaultItems.length == 0) {
            let cache: any[] = this.cacheService.get('subjectAutocomplete');
            for (let i: number = 0; i < cache.length; i++) {
                this.defaultItems.push(this.subjectService.model(cache[i]));
            }
        }
        return this.defaultItems;
    }

    ngOnInit(): void { }

    focus(): void {
        this.search();
        this.focusValue = true;
        this.visible = true;
    }

    blur(): void {
        this.focusValue = false;
        this.visible = false;
    }

    hasFocus(): boolean {
        return this.focusValue;
    }

    isAutocompleteHidden(): boolean {
        return !this.visible;
    }

    hideCreateForm(): void {
        this.showCreateForm = false;
    }

    set(searchValue: any): void {
        this.searchValue = searchValue;

        this.autocompleteService.timeout(() => {
            this.visible = true;
            this.search();
        });
    }

    setSelected(selected: number): void {
        if (!this.isAutocompleteHidden()){
            this.selected = selected;
        }
        this.visible = true;
    }

    onSelect(subject: any): void {
        if (this.isAutocompleteHidden()) {
            return;
        }

        // create new subject
        if (subject == null) {
            this.showCreateForm = true;
        }
        // select subject
        else {
            let all: Model[] = this.subjectUserService.all();
            for (let i: number = 0; i < all.length; i++) {
                if (all[i].get('subjectprototype.id') == subject.get('subjectprototype.id')) {
                    return;
                }
            }
            this.subjectUserService.join({subject_id: subject.get('id')})
            .subscribe((data: any) => {
                this.searchValue = "";
            });
        }
        this.visible = false;
    }

    onClose(event: any): void {
        this.visible = false;
    }

    search(): void {
        if (!this.searchValue || this.searchValue == null || this.searchValue == "") {
            this.setItems(this.getDetaulItems());
            this.selected = 0;
        } else {
            this.subjectService.search({search: this.searchValue})
            .subscribe((data: any) => {
                this.selected = 0;
                let items: any[] = [];

                for (let i: number = 0; i < data.data.length; i++) {
                    items.push(this.modelService.create('subject', data.data[i]));
                }

                this.setItems(items);
            });
        }
    }
}