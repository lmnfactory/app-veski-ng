import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-about-points',
    templateUrl: 'about-points.component.html',
    styleUrls: ['about-points.component.css']
})

export class AboutPointsComponent implements OnInit {

    @Input('lmnMax')
    public max: number;
    @Input('lmnValue')
    public value: number;

    constructor() { }

    ngOnInit() { }

    private getValue(): number {
        if (!this.value) {
            return 0;
        }
        return this.value;
    }

    private getMax(): number {
        if (!this.max) {
            return 0;
        }
        return this.max;
    }

    getFirstValue(): number {
        return this.getValue();
    }

    getSecondValue(): number {
        return this.getMax() - this.getFirstValue();
    }

    getFirstValuePercentage(): number {
        if (this.getMax() == 0) {
            return 0;
        }
        return Math.round(this.getValue() / this.getMax() * 10000) / 100;
    }

    getSecondValuePercentage(): number {
        return Math.round((100 - this.getFirstValuePercentage()) * 100) / 100;
    }

    getPinPosition(): string {
        return this.getFirstValuePercentage() + "%";
    }
}