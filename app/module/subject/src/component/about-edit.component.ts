import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter } from '@angular/core';

import { Model, ModelService, Garbage, GarbageService } from 'core/index';
import { FormBoxService, FormBoxEmitter } from 'form-ui/index';
import { UniversitypersonService } from 'university/index';

import { SubjectSettingsService } from '../service/subject-settings.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-subject-about-edit',
    templateUrl: 'about-edit.component.html',
    styleUrls: ['about.component.css', 'about-edit.component.css']
})

export class AboutEditComponent implements OnInit {

    @Input('lmnAbout')
    public origAbout: Model;
    @Output('lmnClose')
    private closeEmitter: EventEmitter<any>;

    private modelService: ModelService;
    private subjectSettingsService: SubjectSettingsService;
    private universitypersonService: UniversitypersonService;
    public formBox: FormBoxEmitter;
    public professor: string;
    public lecturers: any[];
    public about: Model;
    private garbage: Garbage;

    constructor(formBoxService: FormBoxService, modelService: ModelService, subjectSettingsService: SubjectSettingsService, universitypersonService: UniversitypersonService, garbageService: GarbageService) {
        this.modelService = modelService;
        this.subjectSettingsService = subjectSettingsService;
        this.universitypersonService = universitypersonService;
        this.formBox = formBoxService.createEmitter();
        this.closeEmitter = new EventEmitter();
        this.about = this.modelService.create('subjectsettings', {});
        this.lecturers = [""];
        this.garbage = garbageService.create();
    }

    private init(): void { 
        this.about.fill(this.origAbout.export());
        let persons: any[] = this.about.get('lecturers');
        this.lecturers = [];
        for (let i: number = 0; i < persons.length; i++) {
            this.lecturers.push(persons[i].get('person.name'));
        }
        this.lecturers.push("");

        persons = this.about.get('professors');
        if (persons && persons.length > 0) {
            this.professor = persons[0].get('person.name');
        }
    }

    ngOnInit() {
        this.garbage.event(
            this.formBox.on('submit', this.update.bind(this))
        );
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    ngOnChanges(): void {
        this.init();
    }

    onClose(): void {
        this.closeEmitter.emit({});
    }

    update(readyCallback: any): void {
        let persons: any[] = [];

        if (this.professor && this.professor != "") {
            persons.push({
                universitypersontype_id: this.universitypersonService.get('professor'),
                name: this.professor
            });
        }

        for (let i: number = 0; i < this.lecturers.length; i++) {
            if (!this.lecturers[i] || this.lecturers[i] == "") {
                continue;
            }
            persons.push({
                universitypersontype_id: this.universitypersonService.get('lecturer'),
                name: this.lecturers[i]
            });
        }

        this.about.set('persons', persons);
        this.subjectSettingsService.update(this.about.export())
            .subscribe((data: any) => {
                readyCallback();
                this.onClose();
            });
    }
}