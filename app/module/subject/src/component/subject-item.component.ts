import { Component, OnInit, Input } from '@angular/core';
import { ColorpaletteService } from 'lmn-color/index';

import { SubjectUserService } from '../service/subject-user.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-subject-item',
    templateUrl: 'subject-item.component.html',
    styleUrls: ['subject-item.component.css']
})

export class SubjectItemComponent implements OnInit {

    @Input('lmnSubject')
    public subject: any;

    private subjectUserService: SubjectUserService;
    private colorpaletteService: ColorpaletteService;
    public isLeaving: boolean;

    constructor(subjectUserService: SubjectUserService, colorpaletteService: ColorpaletteService) { 
        this.subjectUserService = subjectUserService;
        this.colorpaletteService = colorpaletteService;
        this.isLeaving = false;
    }

    ngOnInit() { }

    leave(subjectId: number): void {
        this.isLeaving = true;
        this.subjectUserService.leave({subject_id: subjectId})
            .subscribe(() => {
                this.isLeaving = false;
            });
    }

    getColorClass(): string {
        return this.colorpaletteService.getSubjectPaletteClass(this.subject.get('id'));
    }
}