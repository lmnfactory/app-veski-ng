import { Component, OnInit, OnDestroy, OnChanges, Input } from '@angular/core';

import { Model, ModelService, EventHandler, EventToken } from 'core/index';
import { fadeHiddenAnimation } from 'animation/index';

import { SubjectSettingsService } from '../service/subject-settings.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-subject-about',
    templateUrl: 'about.component.html',
    styleUrls: ['about.component.css'],
    animations: [ fadeHiddenAnimation ]
})

export class AboutComponent implements OnInit {

    @Input('lmnPublicId')
    private subjectPid: string;
    @Input('lmnListen')
    private refreshToken: EventToken;

    private subjectSettingsService: SubjectSettingsService;
    private eventHandler: EventHandler;
    private modelService: ModelService;
    private events: any[];
    public settings: Model;
    private formVisibility: boolean;

    constructor(subjectSettingsService: SubjectSettingsService, eventHandler: EventHandler, modelService: ModelService) {
        this.subjectSettingsService = subjectSettingsService;
        this.eventHandler = eventHandler;
        this.modelService = modelService;
        this.events = [];
        this.settings = this.modelService.create('subjectsettings', {});
        this.formVisibility = false;
    }

    private onLoad(settings: Model): void {
        this.settings = settings;
    }

    private load(): void {
        if (!this.subjectPid) {
            return;
        }
        this.subjectSettingsService.request({subject_pid: this.subjectPid});
    }

    ngOnInit() { 
        this.events.push(
            this.eventHandler.on('subjectsettings.load', this.onLoad.bind(this))
        );
        this.events.push(
            this.refreshToken.subscribe(this.load.bind(this))
        );
        this.load();
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    showForm(): void {
        this.formVisibility = true;
    }

    hideForm(): void {
        this.formVisibility = false;
    }

    isFormVisible(): boolean {
        return this.formVisibility;
    }

    getAbsoluteLink(url: string): string {
        if (!/^(f|ht)tps?:\/\//i.test(url)) {
            url = "http://" + url;
        }
        return url;
    }
}