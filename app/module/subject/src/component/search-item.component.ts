import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-subject-search-item',
    templateUrl: 'search-item.component.html',
    styleUrls: ['search-item.component.css']
})

export class SearchItemComponent implements OnInit {

    @Input('lmnSelected')
    public selected: boolean;
    @Input('lmnItem')
    public item: any;

    constructor() { }

    ngOnInit() { }

    isSelected(): boolean {
        return this.selected;
    }
}