import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Model, Observer, EventHandler, EventToken, TabExec } from 'core/index';
import { AuthenticationService, OnSignin } from 'account/index';
import { MenuService } from 'menu/index';
import { ThreadService } from 'thread/index';
import { fadeIfAnimation } from 'animation/index';
import { BrowserNotificationService } from 'browser-notification/index';
import { HeaderService } from 'header/index';

import { SubjectUserService } from '../service/subject-user.service';
import { SubjectService } from '../service/subject.service';
import { SubjectRouterService } from '../service/subject-router.service';

@Component({
    moduleId: module.id,
    selector: 'subject-view',
    templateUrl: 'view.component.html',
    styleUrls: ['view.component.css'],
    animations: [ fadeIfAnimation ]
})
export class SubjectViewComponent extends OnSignin implements OnInit, OnDestroy, Observer {

    private menuService: MenuService;
    private threadService: ThreadService;
    private subjectUserService: SubjectUserService;
    private subjectService: SubjectService;
    private subjectRouterService: SubjectRouterService;
    private browserNotificationService: BrowserNotificationService;
    private headerService: HeaderService;
    private title: Title;
    @ViewChild('tabset')
    public tabset: any;

    public publicId: string;
    public subject: Model;
    private routeSubscription: any;
    public threadRefresh: EventToken;
    public settingsRefresh: EventToken;
    private activatedRoute: ActivatedRoute;
    public refreshTab: any;

    private events: any[];
    private tabsIdMapping: any;
    public selectedTab: string;
    public eventHighlight: number;
    public tabParams: any;

    constructor(menuService: MenuService, threadService: ThreadService, subjectUserService: SubjectUserService, subjectService: SubjectService, subjectRouterService: SubjectRouterService, eventHandler: EventHandler, browserNotificationService: BrowserNotificationService, authService: AuthenticationService, headerService: HeaderService, activatedRoute: ActivatedRoute, title: Title) {
        super(eventHandler, authService);
        this.menuService = menuService;
        this.threadService = threadService;
        this.subjectUserService = subjectUserService;
        this.subjectService = subjectService;
        this.browserNotificationService = browserNotificationService;
        this.headerService = headerService;
        this.subjectRouterService = subjectRouterService;
        this.activatedRoute = activatedRoute;
        this.title = title;
        this.subject = null;
        this.publicId = "";
        this.events = [];
        this.refreshTab = {
            'main_wall': new EventToken(),
            'main_about': new EventToken(),
            'main_events': new EventToken()
        };
        this.tabsIdMapping = {
            wall: 'main_wall',
            about: 'main_about',
            events: 'main_events'
        };
        this.selectedTab = this.tabsIdMapping.wall;
        this.eventHighlight = 0;
        this.tabParams = {};

        this.events.push(
            this.eventHandler.on('router.subject.change', this.onRouteChange.bind(this))
        );
    }

    private onRouteChange(pid: string): void {
        this.publicId = pid;
        if (this.isSignIn()) {
            this.subjectService.request({subject_pid: this.publicId});
        }

        this.tabParams = this.subjectRouterService.getParams();
        if (this.tabParams.tab && this.tabsIdMapping[this.tabParams.tab]) {
            this.selectedTab = this.tabsIdMapping[this.tabParams.tab];
        }
        this.eventHighlight = this.tabParams.eventId;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.subjectService.subscribe(this, 'subject');

        this.headerService.set('tabs', [
            this.headerService.createTab('NÁSTENKA', new TabExec(this.tabset, 'main_wall'), true),
            this.headerService.createTab('POPIS', new TabExec(this.tabset, 'main_about')),
            this.headerService.createTab('UDALOSTI', new TabExec(this.tabset, 'main_events'))
        ]);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.subjectService.unsubscribe(this);
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    onSignin(): void {
        this.activatedRoute.params.subscribe((params: any) => {
            this.subjectRouterService.routeChange(params);
        });
    }

    update(key: string): void {
        if (key == 'subject') {
            this.subject = this.subjectService.get();
            this.publicId = this.subject.get('subjectprototype.public_id');
            this.headerService.set('title', this.subject.get('subjectprototype.code') + " | " + this.subject.get('subjectprototype.name'));
            this.headerService.set('subjectId', this.subject.get('id'));
            this.title.setTitle(this.subject.get('subjectprototype.code') + " | " + this.subject.get('subjectprototype.name'));
            if (this.refreshTab[this.tabset.activeId]) {
                this.refreshTab[this.tabset.activeId].fire({subjectPid: this.publicId});
            }
        }
    }
}
