import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { EventToken, EventHandler, Model, InfiniteScrollService, LmnInfiniteScroll, Garbage, GarbageService } from 'core/index';
import { AuthenticationService, OnSignin } from 'account/index';
import { ThreadService} from 'thread/index';
import { fadeIfAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-threads',
    templateUrl: 'threads.component.html',
    styleUrls: ['threads.component.css'],
    animations: [ fadeIfAnimation ]
})

export class ThreadsComponent extends OnSignin implements OnInit, OnDestroy {

    @Input('lmnSubjectPid')
    public subjectPid: string;
    @Input('lmnListen')
    public refreshToken: EventToken;
    @Input('lmnOptions')
    public options: any;

    private threadService: ThreadService;
    private infiniteScroll: LmnInfiniteScroll;
    public threads: Model[];
    public priorityThread: Model;
    public loading: boolean;
    private garbage: Garbage;

    constructor(eventHandler: EventHandler, authService: AuthenticationService, threadService: ThreadService, infiniteScrollService: InfiniteScrollService, garbageService: GarbageService) {
        super(eventHandler, authService);
        this.threadService = threadService;
        this.infiniteScroll = infiniteScrollService.create();
        this.garbage = garbageService.create();
        this.threads = [];
        this.priorityThread = null;
        this.options = {};
    }

    private removePriorityFromList(): void {
        if (!this.priorityThread) {
            return;
        }
        let removeIndex = -1;
        for (let i: number = 0; i < this.threads.length; i++) {
            if (this.priorityThread.get('id') != this.threads[i].get('id')) {
                continue;
            }
            removeIndex = i;
            break;
        }
        if (removeIndex == -1) {
            return;
        }
        this.threads.splice(removeIndex, 1);
    }

    private load(): void {
        if (!this.subjectPid) {
            return;
        }
        this.threadService.request({subject_pid: this.subjectPid});
        this.infiniteScroll.reset();
        this.loading = true;

        if (this.options.threadPid) {
            this.garbage.subscription(
                this.threadService.requestOne({public_id: this.options.threadPid})
                    .subscribe((thread: Model) => {
                        this.priorityThread = thread;
                        this.removePriorityFromList();
                    })
            );
        } else {
            this.priorityThread = null;
        }
    }

    private loadMoreThreads(resolve: any): void {
        this.threadService.requestOld({
            subject_pid: this.subjectPid,
            until: this.threads[this.threads.length - 1].get('created_at')
        });
    }

    private onLoad(): void {
        this.loading = false;
        this.threads = this.threadService.all();
        this.infiniteScroll.resolveCount(this.threads.length);
        this.removePriorityFromList();
    }

    onSignin(): void {
        this.load();
    }

    ngOnInit() {
        super.ngOnInit();
        this.garbage.event(
            this.refreshToken.subscribe(this.onChange.bind(this))
        );
        this.garbage.event(
            this.eventHandler.on('thread.load', this.onLoad.bind(this))
        );
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.garbage.destroy();
    }

    onChange(event: any): void {
        this.subjectPid = event.subjectPid;
        this.load();
    }

    hasStandaloneThread(): boolean {
        return (this.priorityThread != null);
    }

    getStandaloneThread(): Model {
        return this.priorityThread;
    }

    gotoCreateThread(): void {
        this.eventHandler.fire('action.thread.create');
    }

    onLoadMore(): void {
        this.infiniteScroll.load(this.loadMoreThreads.bind(this));
    }

    shouldShowNoContentText(): boolean {
        return (!this.threads.length && !this.loading && !this.hasStandaloneThread());
    }
}