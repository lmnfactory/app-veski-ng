import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { ModelService, Model, ValidationService, Garbage, GarbageService } from 'core/index';
import { UserService } from 'account/index';
import { TextEnrichService, FormBoxService, FormBoxEmitter } from 'form-ui/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { SubjectService } from '../service/subject.service';
import { SubjectUserService } from '../service/subject-user.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-subject-create',
    templateUrl: 'create.component.html',
    styleUrls: ['create.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})

export class CreateComponent implements OnInit {

    @Input('lmnSubjectName')
    public name: string;
    @Output('lmnSubjectNameChange')
    public nameEmitter: EventEmitter<string>;
    @Output('lmnCancel')
    public cancelEmitter: EventEmitter<any>;

    private formBuilder: FormBuilder;
    public form: FormGroup;
    private modelService: ModelService;
    private subjectService: SubjectService;
    private subjectUserService: SubjectUserService;
    private validationService: ValidationService;
    private userService: UserService;
    public formBox: FormBoxEmitter;
    public subjectprototype: Model;
    public faculty: any;
    private validationConrols: any;
    public errors: string[];
    private garbage: Garbage;

    constructor(modelService: ModelService, formBoxService: FormBoxService, subjectService: SubjectService, subjectUserService: SubjectUserService, formBuilder: FormBuilder, validationService: ValidationService, garbageService: GarbageService, userService: UserService) {
        this.modelService = modelService;
        this.subjectService = subjectService;
        this.subjectUserService = subjectUserService;  
        this.validationService = validationService;
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.formBox = formBoxService.createEmitter();
        this.nameEmitter = new EventEmitter();
        this.cancelEmitter = new EventEmitter();
        this.validationConrols = {
            'name': 'Názov',
            'code': 'Skratka',
            'faculty': 'Fakulta'
        };
        this.garbage = garbageService.create();
        this.init();
    }

    private init(): void {
        this.subjectprototype = this.modelService.create('subjectprototype');
        this.errors = [];
        this.formBox.reset();
        if (this.form) {
            this.form.reset();
        }
    }

    private onFormEvent(data: any): void {
        if (!this.form) { 
            return; 
        }
        this.errors = this.validationService.validate(this.form, this.validationConrols);
    }

    ngOnInit() {
        let user: Model = this.userService.get();
        let defaultFaculty: Model = null;
        if (user) {
            defaultFaculty = user.get('settings.faculty');
        }
        let self: CreateComponent = this;
        let builder: FormBuilder = this.formBuilder;
        this.form = builder.group({
            'name': ["", Validators.compose([
                Validators.required,
                Validators.maxLength(255)
            ])],
            'code': ["", Validators.compose([
                Validators.required,
                Validators.maxLength(6)
            ])],
            'faculty': [defaultFaculty, Validators.required]
        });
        this.form.statusChanges.subscribe(this.onFormEvent.bind(this));
        this.garbage.event(
            this.formBox.on('submit', this.create.bind(this))
        );
    }

    ngOnDestroy(): void {
        this.garbage.destroy();
    }

    isActive(): boolean {
        return this.formBox.hasFocus();
    }

    setName(name: string): void {
        this.name = name;
        this.nameEmitter.emit(this.name);
    }

    cancelForm($event: any): void {
        this.cancelEmitter.emit({});
    }

    create(readyCallback: any): void {
        this.form.updateValueAndValidity();
        if (this.form.invalid) {
            this.errors = this.validationService.validateAll(this.form, this.validationConrols);
            readyCallback();
            return;
        }

        this.subjectprototype.set('name', this.name);
        this.subjectprototype.set('faculty_id', this.faculty.get('id'));
        this.garbage.subscription(
            this.subjectService.create(this.subjectprototype.export())
                .subscribe((data: any) => {
                    this.subjectUserService.join({subject_id: data.data.id})
                        .subscribe((joinData: any) => {
                            this.setName("");
                            this.cancelForm({});
                            this.init();
                        });
                }, (err: any) => {
                    readyCallback();
                })
        );
    }
}