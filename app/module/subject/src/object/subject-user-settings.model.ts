import { ModelService, BasicModel, Model } from 'core/index';

export class SubjectusersettingsModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    export(): any {
        return {
            colorpalette_id: this.get('colorpalette_id')
        };
    }
}
