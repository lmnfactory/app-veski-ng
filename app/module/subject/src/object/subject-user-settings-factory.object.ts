import { ModelService, ModelFactory, Model } from 'core/index';

import { SubjectusersettingsModel } from './subject-user-settings.model';

export class SubjectusersettingsFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new SubjectusersettingsModel(this.modelService);
        model.fill(data);
        return model;
    }
}
