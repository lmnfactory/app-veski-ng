import { ModelService, ModelFactory, Model, EventHandler } from 'core/index';

import { SubjectModel } from './subject.model';

export class SubjectFactory implements ModelFactory {

    private modelService: ModelService;
    private eventHandler: EventHandler;

    constructor(modelService: ModelService, eventHandler: EventHandler) {
        this.modelService = modelService;
        this.eventHandler = eventHandler;
    }

    create(data: any): Model {
        let model: Model = new SubjectModel(this.modelService, this.eventHandler);
        model.fill(data);
        return model;
    }
}
