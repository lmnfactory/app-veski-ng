import { ModelService, BasicModel, Model, EventExec, EventHandler } from 'core/index';

export class SubjectModel extends BasicModel {

    private modelService: ModelService;
    private eventHandler: EventHandler;

    constructor(modelService: ModelService, eventHandler: EventHandler) {
        super();
        this.modelService = modelService;
        this.eventHandler = eventHandler;
    }

    _getSubjectprototype(data: any): Model {
        return this.modelService.create('subjectprototype', data);
    }

    _getUsersettings(data: any): Model {
        return this.modelService.create('subjectuser', data);
    }

    _getSearchModel(data: any): Model {
        return this.modelService.create('search', {
            main: this.get('subjectprototype.code'),
            side: this.get('subjectprototype.name'),
            exec: new EventExec(this.eventHandler, 'menu.subject', {subject_pid: this.get('subjectprototype.public_id')})
        });
    }

    export(): any {
        return {
            subjectprototype_id: this.get('subjectprototype_id')
        };
    }
}
