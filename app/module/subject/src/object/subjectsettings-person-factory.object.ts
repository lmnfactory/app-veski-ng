import { ModelService, ModelFactory, Model } from 'core/index';

import { SubjectsettingsPersonModel } from './subjectsettings-person.model';

export class SubjectsettingsPersonFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new SubjectsettingsPersonModel(this.modelService);
        model.fill(data);
        return model;
    }
}
