import { ModelService, ModelFactory, Model } from 'core/index';

import { SubjectprototypeModel } from './subjectprototype.model';

export class SubjectprototypeFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new SubjectprototypeModel(this.modelService);
        model.fill(data);
        return model;
    }
}
