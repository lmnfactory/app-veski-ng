import { ModelService, BasicModel, Model } from 'core/index';

export class SubjectsettingsPersonModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getPerson(data: any): Model {
        return this.modelService.create('universityperson', data);
    }

    export(): any {
        return {
            
        };
    }
}
