import { ModelService, BasicModel, Model } from 'core/index';

export class SubjectprototypeModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getFaculty(data: any): Model {
        return this.modelService.create('faculty', data);
    }

    export(): any {
        return {
            name: this.get('name'),
            code: this.get('code'),
            faculty_id: this.get('faculty_id')
        };
    }
}
