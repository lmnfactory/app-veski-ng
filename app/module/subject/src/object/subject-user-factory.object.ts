import { ModelService, ModelFactory, Model } from 'core/index';

import { SubjectuserModel } from './subject-user.model';

export class SubjectuserFactory implements ModelFactory {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        this.modelService = modelService;
    }

    create(data: any): Model {
        let model: Model = new SubjectuserModel(this.modelService);
        model.fill(data);
        return model;
    }
}
