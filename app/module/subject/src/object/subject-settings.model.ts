import { ModelService, BasicModel, Model } from 'core/index';

import { UniversitypersonService } from 'university/index';

export class SubjectsettingsModel extends BasicModel {

    private modelService: ModelService;
    private universitypersonService: UniversitypersonService;

    constructor(modelService: ModelService, universitypersonService: UniversitypersonService) {
        super();
        this.modelService = modelService;
        this.universitypersonService = universitypersonService;
    }

    private getPersonsOfType(persontypeId: number): Model[] {
        let persons: Model[] = this.get('universitypersons');
        let personsOfType: Model[] = [];
        for (let i: number = 0; i < persons.length; i++) {
            if (persons[i].get('universitypersontype_id') != persontypeId) {
                continue;
            }
            personsOfType.push(persons[i]);
        }
        return personsOfType;
    }

    _getPointslimit(data: any): number {
        if (!data) {
            return 0;
        }
        return data;
    }

    _getPointsdelimeter(data: any): number {
        if (!data) {
            return 0;
        }
        return data;
    }

    _getUniversitypersons(data: any): Model[] {
        let persons: Model[] = [];
        if (!data) {
            return persons;
        }
        for (let i: number = 0; i < data.length; i++) {
            persons.push(this.modelService.create('subjectsettings_person', data[i]));
        }
        return persons;
    }

    _getProfessors(data: any): Model[] {
        let profesorTypeId: number = this.universitypersonService.get('professor');
        return this.getPersonsOfType(profesorTypeId);
    }

    _getLecturers(data: any): Model[] {
        let lecturerTypeId: number = this.universitypersonService.get('lecturer');
        return this.getPersonsOfType(lecturerTypeId);
    }

    _getSubjectprototype(data: any): Model {
        return this.modelService.create('subjectprototype', data);
    }

    export(): any {
        return {
            id: this.get('id'),
            description: this.get('description'),
            webpage: this.get('webpage'),
            pointslimit: this.get('pointslimit'),
            pointsdelimeter: this.get('pointsdelimeter'),
            universitypersons: this._data.universitypersons,
            subject_id: this.get('subject_id'),
            persons: this.get('persons')
        };
    }
}
