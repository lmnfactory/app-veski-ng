import { ModelService, BasicModel, Model } from 'core/index';

export class SubjectuserModel extends BasicModel {

    private modelService: ModelService;

    constructor(modelService: ModelService) {
        super();
        this.modelService = modelService;
    }

    _getSubject(data: any): Model {
        return this.modelService.create('subject', data);
    }

    _getSettings(data: any): Model {
        return this.modelService.create('subjectusersettings', data);
    }

    export(): any {
        return {

        };
    }
}
