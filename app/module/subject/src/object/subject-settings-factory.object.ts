import { ModelService, ModelFactory, Model } from 'core/index';
import { UniversitypersonService } from 'university/index';

import { SubjectsettingsModel } from './subject-settings.model';

export class SubjectsettingsFactory implements ModelFactory {

    private modelService: ModelService;
    private universitypersonService: UniversitypersonService;

    constructor(modelService: ModelService, universitypersonService: UniversitypersonService) {
        this.modelService = modelService;
        this.universitypersonService = universitypersonService;
    }

    create(data: any): Model {
        let model: Model = new SubjectsettingsModel(this.modelService, this.universitypersonService);
        model.fill(data);
        return model;
    }
}
