import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { ModelService, Model, RouteExec, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';

@Injectable()
export class SubjectSettingsService {

    private http: LmnHttp;
    private modelService: ModelService;
    private eventHandler: EventHandler;
    private subjectSettings: Model;

    constructor(http: LmnHttp, modelService: ModelService, eventHandler: EventHandler) {
        this.http = http;
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.subjectSettings = null;
    }

    private notifyAll(): void {
        this.eventHandler.fire('subjectsettings.load', this.subjectSettings);
    }

    get(): Model {
        return this.subjectSettings;
    }

    refresh(subject: Model): void {
        this.subjectSettings = subject;
    }

    requestNew(): void {

    }

    request(data: any): void {
        this.http.post('api/subject/settings', data)
            .subscribe((data: any) => {
                // TODO: update subject service
                this.subjectSettings = this.modelService.create('subjectsettings', data.data);
                this.notifyAll();
            });
    }

    requestOld(): void {

    }

    update(data: any): Observable<Response> {
        let observer: Observable<Response> = this.http.post('api/subject/settings/update', data);
        
        observer.subscribe((data: any) => {
            this.subjectSettings = this.modelService.create('subjectsettings', data.data);
            this.notifyAll();
        });

        return observer;
    }
}
