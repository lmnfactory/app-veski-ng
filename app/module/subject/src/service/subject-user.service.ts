import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { ModelService, Model, EventExec, RouteRefreshExec, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { MenuService, MenuItem } from 'menu/index';
import { ColorpaletteService } from 'lmn-color/index';

import { SubjectService } from './subject.service';

@Injectable()
export class SubjectUserService {

    private http: LmnHttp;
    private modelService: ModelService;
    private userSubjects: Model[];
    private menuService: MenuService;
    private eventHandler: EventHandler;
    private colorpaletteService: ColorpaletteService;
    private subjectService: SubjectService;
    private router: Router;

    constructor(http: LmnHttp, modelService: ModelService, menuService: MenuService, eventHandler: EventHandler, colorpaletteService: ColorpaletteService, router: Router, subjectService: SubjectService) {
        this.http = http;
        this.modelService = modelService;
        this.menuService = menuService;
        this.eventHandler = eventHandler;
        this.colorpaletteService = colorpaletteService;
        this.subjectService = subjectService;
        this.router = router;
        this.userSubjects = [];
    }

    afterLoad(data: any): void {
        let i: number = 0;
        for (i = 0; i < this.userSubjects.length; i++) {
            this.menuService.remove(this.userSubjects[i].get('id'));
        }
        this.userSubjects = [];
        let list = data.data;
        
        for (i = list.length - 1; i >= 0; i--) {
            let model: Model = this.modelService.create('subject', list[i]);
            let menuItem: MenuItem = this.menuService.create(
                model.get('subjectprototype.code'),
                model.get('subjectprototype.name'),
                model.get('unread'),
                this.colorpaletteService.getPaletteClass(model.get('usersettings.settings.colorpalette_id')),
                new RouteRefreshExec(['/subject', model.get('subjectprototype.public_id')], this.router, () => {
                    this.subjectService.request({subject_pid: model.get('subjectprototype.public_id')});
                })
            );

            this.colorpaletteService.addSubject(model.get('id'), model.get('usersettings.settings.colorpalette_id'));
            this.userSubjects.push(model);
            this.menuService.add(model.get('id'), menuItem);
        }
        this.eventHandler.fire('subjectuser.load', this.userSubjects);
    }

    all(): Model[] {
        return this.userSubjects;
    }

    add(subject: Model): void {
        this.userSubjects.splice(0, 0, subject);
    }

    get(id: number): Model {
        let i: number = 0;
        for (i = 0; i < this.userSubjects.length; i++) {
            if (id == this.userSubjects[i].get('id')) {
                return this.userSubjects[i];
            }
        }

        return null;
    }

    refresh(subject: Model): void {
        let change: boolean = false;
        let i: number = 0;
        for (i = 0; i < this.userSubjects.length; i++) {
            if (this.userSubjects[i].get('id') == subject.get('id')) {
                this.userSubjects[i] = subject;
                change = true;
            }
        }
    }

    requestNew(): void {

    }

    request(data: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/user/subjects', data);

        observable.subscribe((data: any) => {
            this.afterLoad(data);
        });

        return observable;
    }

    join(data: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/subject/join', data);

        observable.subscribe((data: any) => {
            this.afterLoad(data);
        });

        return observable;
    }

    leave(data: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/subject/leave', data);

        observable.subscribe((data: any) => {
            this.afterLoad(data);
        });

        return observable;
    }
}
