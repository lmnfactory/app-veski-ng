import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { Model, EventHandler } from 'core/index';

import { SubjectService } from './subject.service';

@Injectable()
export class SubjectRouterService {

    private router: Router;
    private activatedRoute: ActivatedRoute;
    private eventHandler: EventHandler;
    private subjectService: SubjectService;
    private pid: string;
    private params: any;
    private routes: any[];

    constructor(router: Router, eventHandler: EventHandler, subjectService: SubjectService) {
        this.router = router;
        this.eventHandler = eventHandler;
        this.subjectService = subjectService;
        this.pid = "";
        this.params = {};
        this.routes = [];
        this.routeSubscribe();
    }

    private routeSubscribe(): void {
        
    }

    private notify(): void {
        this.eventHandler.fire('router.subject.change', this.pid);
    }

    routeChange(params: any): void {
        this.params = params;
        if (params.publicId) {
            this.setPublicId(params.publicId);
        } else {
            for (let i: number = 0; i < this.routes.length; i++) {
                this.routes[i](this);
            }
        }
    }

    route(callback: any): void {
        this.routes.push(callback);
    }

    setPublicId(pid: string): void {
        this.pid = pid;
        this.notify();
    }

    getPublicId(): string {
        return this.pid;
    }

    getParams(): any {
        return this.params;
    }
}