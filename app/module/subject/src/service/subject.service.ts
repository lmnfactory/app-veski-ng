import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { ModelService, Model, RouteExec, AbstractObservable, EventHandler } from 'core/index';
import { LmnHttp } from 'http-communication/index';
import { MenuService } from 'menu/index';

@Injectable()
export class SubjectService extends AbstractObservable {

    private http: LmnHttp;
    private modelService: ModelService;
    private subject: Model;
    private eventHandler: EventHandler;
    private router: Router;
    private menuService: MenuService;
    private currentSubjectPid: string;

    constructor (http: LmnHttp, modelService: ModelService, eventHandler: EventHandler, router: Router, menuService: MenuService) {
        super();
        this.http = http;
        this.modelService = modelService;
        this.eventHandler = eventHandler;
        this.router = router;
        this.menuService = menuService;
        this.subject = null;
        this.currentSubjectPid = "";
        this.eventHandler.on('menu.subject', this.onMenuSubject.bind(this))
    }

    model(data: any): Model {
        return this.modelService.create('subject', data);
    }

    get(): Model {
        return this.subject;
    }

    refresh(subject: Model): void {
        this.subject = subject;
    }

    requestNew(): void {

    }

    request(data: any): void {
        this.http.post('api/subject/detail', data)
            .subscribe((data: any) => {
                this.subject = this.model(data.data);
                this.menuService.activateByKey(this.subject.get('id'));
                this.notifyAll();
            });
    }

    requestPrototype(data: any): Observable<Model> {
        let observer: any = this.http.post('api/subject/prototype', data)
            .map((data: any) => {
                return this.modelService.create('subject', data.data);
            });

        return observer;
    }

    create(data: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/subject/create', data);

        observable.subscribe((data: any) => {
            this.subject = this.modelService.create('subject', data.data);
            this.notifyAll();
        });

        return observable;
    }

    search(data: any): Observable<Response> {
        let observable: Observable<Response> = this.http.post('api/subject/search', data);
           
        observable.subscribe((data: any) => {});
        return observable
    }

    onMenuSubject(data: any): void {
        this.request(data);
        this.router.navigateByUrl('subject/' + data.subject_pid);
    }
}
