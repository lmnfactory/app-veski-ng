import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';

import { CoreModule, ModelService, EventHandler } from 'core/index';
import { AccountModule } from 'account/index';
import { MenuModule } from 'menu/index';
import { LayoutModule } from 'layout/index';
import { ThreadModule } from 'thread/index';
import { BrowserNotificationModule } from 'browser-notification/index';
import { ColorModule } from 'lmn-color/index';
import { SearchService } from 'search/index';
import { FormUIModule } from 'form-ui/index';
import { CalendareventModule } from 'calendarevent/index';
import { HeaderModule } from 'header/index';
import { FacultyModule } from 'faculty/index';
import { UniversityModule, UniversitypersonService } from 'university/index';

import { SubjectModel } from './object/subject.model';
import { SubjectFactory } from './object/subject-factory.object';
import { SubjectsettingsFactory } from './object/subject-settings-factory.object';
import { SubjectuserModel } from './object/subject-user.model';
import { SubjectsettingsPersonFactory } from './object/subjectsettings-person-factory.object';
import { SubjectuserFactory } from './object/subject-user-factory.object';
import { SubjectusersettingsModel } from './object/subject-user-settings.model';
import { SubjectusersettingsFactory } from './object/subject-user-settings-factory.object';
import { SubjectprototypeModel } from './object/subjectprototype.model';
import { SubjectprototypeFactory } from './object/subjectprototype-factory.object';
import { SubjectUserService } from './service/subject-user.service';
import { SubjectService } from './service/subject.service';
import { SubjectSettingsService } from './service/subject-settings.service';
import { SubjectRouterService } from './service/subject-router.service';

import { SubjectViewComponent } from './component/view.component';
import { JoinComponent } from './component/join.component';
import { CreateComponent } from './component/create.component';
import { AboutComponent } from './component/about.component';
import { AboutEditComponent } from './component/about-edit.component';
import { AboutPointsComponent } from './component/about-points.component';
import { SearchItemComponent } from './component/search-item.component';
import { SubjectItemComponent } from './component/subject-item.component';
import { ThreadsComponent } from './component/threads.component';

@NgModule({
  imports: [ CommonModule, FormsModule, ReactiveFormsModule, RouterModule, NgbModule, CoreModule, MenuModule, LayoutModule, FlexLayoutModule, UniversityModule, FacultyModule, ThreadModule, BrowserNotificationModule, ColorModule, AccountModule, FormUIModule, CalendareventModule, HeaderModule ],
  exports: [ SubjectViewComponent, JoinComponent, SubjectItemComponent ],
  declarations: [ SubjectViewComponent, JoinComponent, CreateComponent, SearchItemComponent, SubjectItemComponent, AboutComponent, AboutPointsComponent, AboutEditComponent, ThreadsComponent ],
  providers: [ SubjectService, SubjectUserService, SubjectSettingsService, SubjectRouterService ]
})
export class SubjectModule {

    constructor(modelService: ModelService, subejctService: SubjectService, eventHandler: EventHandler, searchService: SearchService, universitypersonService: UniversitypersonService) {
        modelService.addFactory('subject', new SubjectFactory(modelService, eventHandler));
        modelService.addFactory('subjectsettings', new SubjectsettingsFactory(modelService, universitypersonService));
        modelService.addFactory('subjectprototype', new SubjectprototypeFactory(modelService));
        modelService.addFactory('subjectuser', new SubjectuserFactory(modelService));
        modelService.addFactory('subjectusersettings', new SubjectusersettingsFactory(modelService));
        modelService.addFactory('subjectsettings_person', new SubjectsettingsPersonFactory(modelService));

        searchService.mapModel('subject', 'subject');
    }
}
