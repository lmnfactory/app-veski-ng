import { Exec } from 'core/index';

import { BrowserNotificationService } from '../service/browser-notification.service';
import { BrowserNotification } from './browser-notification.object';

export class BrowserNotificationBuilder {

    private browserNotificationService: BrowserNotificationService;
    private notification: BrowserNotification;

    constructor(browserNotificationService: BrowserNotificationService) {
        this.browserNotificationService = browserNotificationService;
        this.notification = new BrowserNotification();
        this.image("app/assets/image/logo-notext.png");
        this.sound("app/assets/sound/notification/default.mp3");
    }

    title(title: string): BrowserNotificationBuilder {
        this.notification.setTitle(title);
        return this;
    }

    body(body: string): BrowserNotificationBuilder {
        this.notification.setOption('body', body);
        return this;
    }

    image(image: string): BrowserNotificationBuilder {
        this.notification.setOption('icon', image);
        return this;
    }

    sound(sound: string): BrowserNotificationBuilder {
        this.notification.setOption('sound', sound);
        return this;
    }

    onExec(exec: Exec): BrowserNotificationBuilder {
        this.notification.setExec(exec);
        return this;
    }

    show(): void {
        this.browserNotificationService.show(this.notification);
    }
}
