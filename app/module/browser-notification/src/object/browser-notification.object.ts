import { Exec } from 'core/index';

export class BrowserNotification {

    private title: string;
    private options: any;
    private exec: Exec;

    constructor() {
        this.title = "";
        this.options = {};
        this.exec = null;
        this.setOption('onclick', this.onClick.bind(this));
    }

    getTitle(): string {
        return this.title;
    }

    getOptions(): any {
        return this.options;
    }

    setTitle(title: string): void {
        this.title = title;
    }

    setOption(key: string, value: any): void {
        this.options[key] = value;
    }

    setExec(exec: Exec): void {
        this.exec = exec;
    }

    onClick(): void {
        if (this.exec == null) {
            return;
        }

        this.exec.execute();
    }
}
