import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BrowserNotificationService } from './service/browser-notification.service';

@NgModule({
    imports: [ CommonModule ],
    declarations: [],
    exports: [],
    providers: [ BrowserNotificationService ]
})
export class BrowserNotificationModule {

}
