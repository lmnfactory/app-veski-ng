declare var Notification: any;
declare var Audio: any;

import { Injectable } from '@angular/core';

import { BrowserNotification } from '../object/browser-notification.object';
import { BrowserNotificationBuilder } from '../object/browser-notification-builder.object';

@Injectable()
export class BrowserNotificationService {

    private shouldRequest: boolean;
    private serviceWorker: ServiceWorkerRegistration;

    constructor() {
        this.shouldRequest = false;
        this.serviceWorker = null;

        if (navigator && navigator.serviceWorker) {
            navigator.serviceWorker.register('/sw.js')
                .then((sw: any) => {
                    this.serviceWorker = sw;
                });
        }
    }

    private onNotificationClick(event: any): void {
        console.log(event);
        let notification: any = event.notification;
        if (notification.exec) {
            notification.exec();
        }
    }

    hasPermission(): boolean {
        return (Notification.permission === "granted");
    }

    build(): BrowserNotificationBuilder {
        return new BrowserNotificationBuilder(this);
    }

    requestPermission(): Promise<any> {
        if (this.hasPermission()) {
            return null;
        }
        let self: BrowserNotificationService = this;
        return Notification.requestPermission()
            .then((permission: string) => {
                if (permission == "granted") {
                    self.build()
                        .title('Výborne')
                        .body("Teraz budeš vedieť o všetkom, čo sa na stránke stane :)")
                        .show();
                }
                return permission;
            });
    }

    show(notification: BrowserNotification): void {
        if (this.serviceWorker == null) {
            return;
        }
        if (!this.hasPermission()) {
            if (this.shouldRequest) {
                this.requestPermission();
            }
            return;
        }
        
        if (notification.getOptions().sound) {
            let player: any = new Audio();
            player.src = notification.getOptions().sound;
            player.load();
            player.play();
        }
        this.serviceWorker.showNotification(notification.getTitle(), notification.getOptions());
    }
}
