import { Directive, HostListener, Input, ElementRef, Renderer2 } from '@angular/core';

import { FormBoxEmitter } from '../object/form-box-emitter.object';

@Directive({ selector: '[lmnLinput]' })
export class LinputDirective {

    @Input('lmnLinput')
    public formBox: FormBoxEmitter;
    @Input('lmnName')
    public name: string;

    private elementRef: ElementRef
    private renderer: Renderer2;
    private events: any[];

    constructor(elementRef: ElementRef, renderer: Renderer2) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.events = [];
        this.renderer.addClass(this.elementRef.nativeElement, "lmn-linput");
    }

    private onChange(): void {
        if (this.formBox.hasFocus(this.name)) {
            this.renderer.addClass(this.elementRef.nativeElement, "lmn-focus");
        } else {
            this.renderer.removeClass(this.elementRef.nativeElement, "lmn-focus");
        }
    }

    ngOnInit(): void {
        this.events.push(
            this.formBox.on('blur', this.onChange.bind(this))
        );

        this.events.push(
            this.formBox.on('focus', this.onChange.bind(this))
        );
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }
}