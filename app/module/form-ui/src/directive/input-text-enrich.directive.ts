import { Directive, Input, Output, OnInit, OnDestroy, ElementRef, EventEmitter } from '@angular/core';

import { TextEnrichService } from '../service/text-enrich.service';

@Directive({
    selector: '[lmnInputTextEnrich]',
    host: {
        '(ngModelChange)': 'modelChange($event)'
    }
})
export class InputTextEnrichDirective {

    @Input('lmnInputTextEnrich')
    private model: any;
    private element: ElementRef;
    private whitespaces: string;
    @Output('lmnEnrich')
    private enrich: EventEmitter<any>;

    constructor(elementRef: ElementRef) {
        this.element = elementRef;
        this.whitespaces = ' \t\n\r\v';
        this.enrich = new EventEmitter();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    modelChange(event: any): void {
        let carotPosition = this.element.nativeElement.selectionStart;
        let word = this.extractWord(event, carotPosition);
        this.enrich.emit({
            model: event,
            word: word,
            carotPosition: carotPosition
        });
    }

    private extractWord(text: string, position: number) {
        let wordStart = position
        while (wordStart > 0) {
            let c = text.charAt(wordStart - 1);
            if (this.whitespaces.indexOf(c) > -1) {
                break;
            }
            if (wordStart == 0) {
                break;
            }
            wordStart--;
        }

        return text.substr(wordStart, position - wordStart);
    }
}
