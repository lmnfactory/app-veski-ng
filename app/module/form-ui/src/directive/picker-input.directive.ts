import { Directive, HostListener, Input } from '@angular/core';

import { PickerComponent } from '../component/picker.component';

@Directive({ selector: '[lmnPickerInput]' })
export class PickerInputDirective {

    @Input('lmnPickerInput')
    private picker: PickerComponent;

    constructor() { }

    @HostListener('focus', ['$event'])
    onFocus(event: any): void {
        this.picker.show();
    }

    @HostListener('blur', ['$event'])
    onBlur(event: any): void {
        this.picker.hide();
    }
}