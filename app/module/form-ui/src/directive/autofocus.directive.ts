import { Directive, OnInit, AfterViewInit, ElementRef, Input, Renderer } from '@angular/core';

@Directive({
  selector: '[lmnAutofocus]'
})
export class AutofocusDirective implements OnInit, AfterViewInit {

  @Input('lmnAutofocus')
  private shouldFocus: boolean;

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    if (this.shouldFocus) {
      this.elementRef.nativeElement.focus(); 
    }
  }
}
