import { Directive, Input, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';

import { Autocomplete } from '../object/autocomplete.object';

@Directive({
    selector: '[lmnAutocompleteSelector]',
    host: {
        '(keydown)': 'onKeydown($event)'
    }
})
export class AutocompleteSelectorDirective {

    @Input('lmnAutocompleteSelector')
    private items: any[];
    @Input('lmnSelected')
    private selected: number;
    @Input('lmnAutocomplete')
    public autocomplete: Autocomplete;
    @Output('lmnChange')
    private changeEmitter: EventEmitter<any>;
    @Output('lmnClose')
    private closeEmitter: EventEmitter<any>;
    @Output('lmnSelect')
    private selectEmitter: EventEmitter<any>;

    constructor() {
        this.changeEmitter = new EventEmitter();
        this.closeEmitter = new EventEmitter();
        this.selectEmitter = new EventEmitter();
    }

    private getSelected(): number {
        if (this.autocomplete) {
            return this.autocomplete.getSelected();
        }
        return this.selected;
    }

    private onChange(selected: number): void {
        if (this.autocomplete) {
            if (this.autocomplete.isVisible()){
                this.autocomplete.setSelected(selected);
            }
            this.autocomplete.show()
        }
        this.selected = selected;
        this.changeEmitter.emit(selected);
    }

    private next(): void {
        let s: number = this.getSelected();
        s++;
        if (this.items.length <= s) {
            s = 0;
        }

        this.onChange(s);
    }

    private previous(): void {
        let s: number = this.getSelected();
        s--;
        if (s < 0) {
            s = this.items.length - 1;
        }

        this.onChange(s);
    }

    ngOnInit(): void {}

    ngOnDestroy(): void {}

    onKeydown($event: any): void {
        let s: number = this.getSelected();
        switch ($event.keyCode) {
            // Arrow down
            case 40:
                this.next();
                event.preventDefault();
                break;
            // Arrow up
            case 38:
                this.previous();
                event.preventDefault();
                break;
            // Tab
            case 9:
                break;
            // Enter
            case 13:
                if (!$event.shiftKey && (!this.autocomplete || this.autocomplete.isVisible())) {
                    this.selectEmitter.emit(this.items[s]);
                    event.preventDefault();
                }
                break;
            // ESC
            case 27:
                this.closeEmitter.emit({});
                event.preventDefault();
                break;
        }
    }
}
