import { DOCUMENT } from '@angular/platform-browser';
import { ElementRef, HostListener, Directive, Inject } from '@angular/core';

@Directive({
    selector: 'textarea[autosize]'
})

export class Autosize {
  private document: any;
 @HostListener('input',['$event.target'])
  onInput(textArea: HTMLTextAreaElement): void {
    this.adjust();
  }
  constructor(public element: ElementRef, @Inject(DOCUMENT) document: any){
    this.document = document;
  }
  ngAfterContentChecked(): void {
    this.adjust();
  }
  adjust(): void{
    let scrollTop: number = window.scrollY;
    let scrollLeft: number = window.scrollX
    this.element.nativeElement.style.height = 'auto';
    this.element.nativeElement.style.height = this.element.nativeElement.scrollHeight + "px";
    window.scrollTo(scrollLeft, scrollTop);
  }
}
