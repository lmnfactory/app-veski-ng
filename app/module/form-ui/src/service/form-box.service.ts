import { Injectable } from '@angular/core';

import { FormBox } from '../object/form-box.object';
import { FormBoxObservable } from '../object/form-box-observable.object';
import { FormBoxEmitter } from '../object/form-box-emitter.object';

@Injectable()
export class FormBoxService {

    constructor() {

    }

    create(): FormBox {
        return new FormBox();
    }

    createObservable(): FormBoxObservable {
        return new FormBoxObservable();
    }

    createEmitter(): FormBoxEmitter {
        return new FormBoxEmitter();
    }
}
