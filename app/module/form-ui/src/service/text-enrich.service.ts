import { Injectable } from '@angular/core';
import { HashtagTextService } from 'text/index';
import { TagService } from 'tag/index';

import { AsyncPush } from 'core/index';

import { TextEnricher } from '../object/text-enricher.interface';
import { AtTextEnricher } from '../object/at-text-enricher.object';
import { HashtagTextEnricher } from '../object/hashtag-text-enricher.object';
import { ImageAutocompleteItem } from '../object/image-autocomplete-item.object';

@Injectable()
export class TextEnrichService {

    private methods: Map<string, TextEnricher>;
    private list: ImageAutocompleteItem[];
    private position: any;

    constructor(tagService: TagService, hashtagTextService: HashtagTextService) {
        this.list = [];
        this.position = {
            x: 0,
            y: 0
        };
        this.methods = new Map();
        //this.methods.set('@', new AtTextEnricher());
        this.methods.set('#', new HashtagTextEnricher(tagService, hashtagTextService));
    }

    addMethod(key: string, method: TextEnricher) {
        this.methods.set(key, method);
    }

    enrich(word: string, component: AsyncPush): boolean {
        let hasRun = false;

        this.methods.forEach((enricher: TextEnricher, key: string) => {
            if (enricher.shouldRun(word)) {
                enricher.run(word, component);
                hasRun = true;
            }
        });

        return hasRun;
    }

    extract(text: string, key: string): any[] {
        let method: TextEnricher = this.methods.get(key);
        if  (method == null) {
            return [];
        }

        return method.extract(text);
    }
}
