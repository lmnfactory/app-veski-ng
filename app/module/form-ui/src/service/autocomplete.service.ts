import { Injectable } from '@angular/core';

import { Autocomplete } from '../object/autocomplete.object';

@Injectable()
export class AutocompleteService {

    private delayTime: number;
    private _timeout: any;

    constructor() {
        this.delayTime = 100;
    }

    create(items: any[], actions: any[] = []): Autocomplete {
        return new Autocomplete(items, actions);
    }

    setDelayTime(time: number): void {
        this.delayTime = time;
    }

    timeout(callback: any): void {
        if (this._timeout) {
            clearTimeout(this._timeout);
        }
        this._timeout = setTimeout(callback, this.delayTime);
    }
}