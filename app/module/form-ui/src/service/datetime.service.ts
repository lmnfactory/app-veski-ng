import { Injectable } from '@angular/core';

@Injectable()
export class DatetimeService {

    private minutesOffset: number;

    constructor() {
        this.minutesOffset = 5;
    }

    setMinutesOffset(offset: number): void {
        this.minutesOffset = offset;
    }

    getMinutesOffset(): number {
        return this.minutesOffset;
    }
}