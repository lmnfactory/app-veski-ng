import { Injectable } from '@angular/core';

import { EventHandler } from 'core/index';

@Injectable()
export class FileDropService {

    private eventHandler: EventHandler;

    constructor(eventHandler: EventHandler) {
        this.eventHandler = eventHandler;
    }

    onFileOver(event: any): void {
        this.eventHandler.fire('file.over', event);
    }
}