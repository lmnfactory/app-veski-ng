import { AbstractControl } from '@angular/forms';

import { ValidationRule } from 'core/index';

export class FileuploadValidation implements ValidationRule {

    private config: any;

    constructor(config: any) {
        this.config = config;
    }

    private evalSingle(file: any): any {
        if (!file.upload.isDone()) {
            
            return {
                fileupload: true
            };
        }
        return false;
    }

    eval(formGroup: AbstractControl): any {
        let files: any = formGroup.value;
        if (!files) {
            return null;
        }
        if (files.length === undefined) {
            files = [files];
        } 
        for (let i: number = 0; i < files.length; i++) {
            let response: any = this.evalSingle(files[i]);
            if (response !== false) {
                formGroup.setErrors({'fileupload': true});
                return response;
            }
        }

        return null;
    }
}