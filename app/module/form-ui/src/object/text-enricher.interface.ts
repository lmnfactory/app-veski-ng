import { AsyncPush } from 'core/index';

import { ImageAutocompleteItem } from './image-autocomplete-item.object';

export interface TextEnricher {
    shouldRun(word: string): boolean;
    run(word: string, component: AsyncPush): void;
    extract(text: string): any[];
}
