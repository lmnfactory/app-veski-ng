export class ImageAutocompleteItem {

    private img: string;
    private label: string;
    private value: any;

    constructor(label: string, value: any, img: string = "") {
        this.label = label;
        this.value = value;
        this.img = img;
    }

    getLabel(): string {
        return this.label;
    }

    getImage(): string {
        return this.img;
    }

    getValue(): any {
        return this.value;
    }

    hasImage(): boolean {
        return (this.img != "");
    }

    toString(): string {
        return this.label;
    }
}
