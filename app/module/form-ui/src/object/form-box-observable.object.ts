import { AbstractObservable } from 'core/index';

export class FormBoxObservable extends AbstractObservable {

    private focused: string;
    private firstFocus: boolean;

    constructor() {
        super();
        this.focused = null;
        this.firstFocus = false;
    }

    focus(name: string): void {
        let notify: boolean = false;
        if (!this.hasFocus()) {
            notify = true;
        }
        this.firstFocus = true;
        this.focused = name;

        if (notify) {
            this.notifyAll();
        }
    }

    blur(name: string): void {
        let self: FormBoxObservable = this;
        setTimeout(() => {
            if (self.focused == name) {
                let notify: boolean = false;
                if (this.hasFocus()) {
                    notify = true;
                }
                self.focused = null;
                if (notify) {
                    this.notifyAll();
                }
            }
        }, 0);
    }

    wahFocused(): boolean {
        return this.firstFocus;
    }

    hasFocus(): boolean {
        return (this.focused != null);
    }
}
