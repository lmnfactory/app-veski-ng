import { AsyncPush } from 'core/index';
import { HashtagTextService } from 'text/index';
import { TagService } from 'tag/index';

import { TextEnricher } from './text-enricher.interface';
import { ImageAutocompleteItem } from './image-autocomplete-item.object';
import { HashtagAutocompleteItem } from './hashtag-autocomplete-item.object';

export class HashtagTextEnricher implements TextEnricher {

    private tagService: TagService;
    private hashtagTextService: HashtagTextService;
    private list: ImageAutocompleteItem[];

    constructor(tagService: TagService, hashtagTextService: HashtagTextService) {
        this.list = [];
        this.tagService = tagService;
        this.hashtagTextService = hashtagTextService;
    }

    private successRun(data: any, component: AsyncPush): void {
        let body: any = data.data;
        let i: number = 0;
        this.list = [];
        for (i = 0; i < body.length; i++) {
            this.list.push(new HashtagAutocompleteItem(body[i].value, {}));
        }
        component.pushData(this.list);
    }

    shouldRun(word: string): boolean {
        return (word.length > 0 && word.charAt(0) == '#');
    }

    run(word: string, component: AsyncPush): void {
        let noSignWord = word.substr(1);

        if (noSignWord.length > 0) {
            this.tagService.search(noSignWord)
                .subscribe((data: any) => {
                    this.successRun(data, component);
                });
        }
        else {
            this.tagService.recommend(noSignWord)
                .subscribe((data: any) => {
                    this.successRun(data, component);
                });
        }
    }

    extract(text: string): any[] {
        let tags: string[] = this.hashtagTextService.extract(text);
        let tagObjects: any[] = [];
        for (let i: number = 0; i < tags.length; i++) {
            tagObjects.push({value: tags[i]});
        }
        return tagObjects;
    }
}
