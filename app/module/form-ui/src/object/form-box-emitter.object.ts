import { EventListener } from 'core/index';

export class FormBoxEmitter {

    private focused: string;
    private firstFocus: boolean;
    private state: string;
    private listeners: any;

    constructor() {
        this.reset();
        this.listeners = {};
    }

    reset(): void {
        this.focused = null;
        this.firstFocus = false;
        this.setState();
    }

    focus(name: string): void {
        this.firstFocus = true;
        this.focused = name;

        this.fire('focus', this.focused);
    }

    blur(name: string = null): void {
        let self: FormBoxEmitter = this;
        setTimeout(() => {
            if (name == null || self.focused == name) {
                self.focused = null;
                this.fire('blur', self.focused);
            }
        }, 0);
    }

    wasFocused(): boolean {
        return this.firstFocus;
    }

    hasFocus(name: string = null): boolean {
        if (name == null) {
            return (this.focused != null);
        }
        return (this.focused == name);
    }

    private setState(state: string = "ready"): void {
        this.state = state;
    }

    private hasState(state: string): boolean {
        return this.state == state;
    }

    private fire(eventName: string, data: any = {}): void {
        if (!this.listeners[eventName]) {
            return;
        }
        this.listeners[eventName].fire(data);
    }

    on(eventName: string, callback: any) {
        if (!this.listeners[eventName]) {
            this.listeners[eventName] = new EventListener();
        }
        return this.listeners[eventName].on(callback);
    }

    ready(): void {
        this.setState("ready");
        this.fire('ready');
    }

    submit($event: any): void {
        $event.preventDefault();
        if (!this.isReady()) {
            return;
        }
        this.setState("submit");
        this.fire('submit', this.ready.bind(this));
    }

    isReady(): boolean {
        return this.hasState("ready");
    }
}
