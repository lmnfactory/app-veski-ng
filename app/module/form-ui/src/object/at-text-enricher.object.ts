import { AsyncPush } from 'core/index';

import { TextEnricher } from './text-enricher.interface';
import { ImageAutocompleteItem } from './image-autocomplete-item.object';
import { AtAutocompleteItem } from './at-autocomplete-item.object';

export class AtTextEnricher implements TextEnricher {

    private list: ImageAutocompleteItem[];

    constructor() {
        this.list = [];
        this.list.push(new AtAutocompleteItem('peter-gasparik', {}, 'app/assets/image/profile.jpg'));
        this.list.push(new AtAutocompleteItem('peter-aaaaa', {}, 'app/assets/image/profile.jpg'));
        this.list.push(new AtAutocompleteItem('karol-aaaaa', {}, 'app/assets/image/profile.jpg'));
    }

    shouldRun(word: string): boolean {
        return (word.length > 0 && word.charAt(0) == '@');
    }

    run(word: string, component: AsyncPush): void {
        let noSignWord = word.substr(1);

        let i = 0;
        let searchList = [];
        for (i = 0; i < this.list.length; i++) {
            if (this.list[i].getLabel().indexOf(noSignWord) > -1) {
                searchList.push(this.list[i]);
            }
        }

        component.pushData(this.list);
    }

    extract(text: string): any[] {
        return [];
    }
}
