export class FormBox {

    private focused: string;
    private firstFocus: boolean;

    constructor() {
        this.reset();
    }

    reset(): void {
        this.focused = null;
        this.firstFocus = false;
    }

    focus(name: string): void {
        this.firstFocus = true;
        this.focused = name;
    }

    blur(name: string): void {
        let self: FormBox = this;
        setTimeout(() => {
            if (self.focused == name) {
                self.focused = null;
            }
        }, 0);
    }

    wasFocused(): boolean {
        return this.firstFocus;
    }

    hasFocus(name: string = null): boolean {
        if (name == null) {
            return (this.focused != null);
        }
        return (this.focused == name);
    }
}
