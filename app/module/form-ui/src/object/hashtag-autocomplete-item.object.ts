import { ImageAutocompleteItem } from './image-autocomplete-item.object';

export class HashtagAutocompleteItem extends ImageAutocompleteItem {

    constructor(label: string, value: any, img: string = "") {
        super(label, value, img);
    }

    toString(): string {
        return '#' + this.getLabel();
    }
}
