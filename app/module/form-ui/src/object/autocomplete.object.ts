export class Autocomplete {

    private selected: number;
    private items: any[];
    private actions: any[];
    private visible: boolean;

    constructor(items: any[], actions: any[] = []) {
        this.setItems(items);
        this.setActions(actions);
        this.hide();
        this.setSelected(0);
    }

    getItems(): any[] {
        return this.items;
    }

    setItems(items: any[]): void {
        this.items = items;
    }

    getActions(): any[] {
        return this.actions;
    }

    setActions(actions: any[]): void {
        this.actions = actions;
    }

    getSelected(): number {
        return this.selected;
    }

    setSelected(selected: number): void {
        this.selected = selected;
    }

    show(): void {
        this.visible = true;
    }

    hide(): void {
        this.visible = false;
    }

    toggle(): void {
        this.visible = !this.visible;
    }

    isVisible(): boolean {
        return this.visible;
    }
}