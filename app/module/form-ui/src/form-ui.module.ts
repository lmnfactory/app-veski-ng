import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Autosize } from './directive/autosize.directive';
import { Ng2FileDropModule }  from 'ng2-file-drop';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from "@angular/flex-layout";

import { CoreModule, ValidationService } from 'core/index';
import { TextModule } from 'text/index';

import { FormBoxComponent } from './component/form-box.component';
import { TextEnrichAutocompleteComponent } from './component/text-enrich-autocomplete.component';
import { TextareaAutocompleteComponent } from './component/textarea-autocomplete.component';
import { InputSelectComponent } from './component/input-select.component';
import { LmnSwitchComponent } from './component/lmn-switch.component';
import { DatetimepickerComponent } from './component/datetimepicker.component';
import { DatepickerComponent } from './component/datepicker.component';
import { DatepickerMonthCellComponent } from './component/datepicker-month-cell.component';
import { TimepickerComponent } from './component/timepicker.component';
import { TimepickerHourComponent } from './component/timepicker-hour.component';
import { TimepickerHourCellComponent } from './component/timepicker-hour-cell.component';
import { TimepickerMinuteComponent } from './component/timepicker-minute.component';
import { TimepickerMinuteCellComponent } from './component/timepicker-minute-cell.component';
import { FileDropzoneComponent } from './component/file-dropzone.component';
import { FileInputComponent } from './component/file-input.component';
import { FileUploadComponent } from './component/file-upload.component';
import { FileDownloadComponent } from './component/file-download.component';
import { NumberInputComponent } from './component/number-input.component';
import { StringListInputComponent } from './component/string-list-input.component';
import { UiSwitchComponent } from './component/ui-switch.component';

import { InputTextEnrichDirective } from './directive/input-text-enrich.directive';
import { AutofocusDirective } from './directive/autofocus.directive';
import { PickerInputDirective } from './directive/picker-input.directive';
import { AutocompleteSelectorDirective } from './directive/autocomplete-selector.directive';
import { LinputDirective } from './directive/linput.directive';

import { TextEnrichService } from './service/text-enrich.service';
import { FormBoxService } from './service/form-box.service';
import { DatetimeService } from './service/datetime.service';
import { AutocompleteService } from './service/autocomplete.service';
import { FileDropService } from './service/file-drop.service';
import { FormService } from './service/form.service';

import { FileuploadValidation } from './object/fileupload-valiadtion.object';

@NgModule({
    imports: [ CommonModule, FormsModule, ReactiveFormsModule, FlexLayoutModule, NgbModule, Ng2FileDropModule, CoreModule, TextModule ],
    declarations: [ FormBoxComponent, InputTextEnrichDirective, AutofocusDirective, PickerInputDirective, TextEnrichAutocompleteComponent, TextareaAutocompleteComponent, Autosize, InputSelectComponent, LmnSwitchComponent, DatetimepickerComponent, DatepickerComponent, DatepickerMonthCellComponent, TimepickerComponent, TimepickerHourComponent, TimepickerHourCellComponent, TimepickerMinuteComponent, TimepickerMinuteCellComponent, AutocompleteSelectorDirective, FileDropzoneComponent, FileInputComponent, FileUploadComponent, FileDownloadComponent, NumberInputComponent, LinputDirective, StringListInputComponent, UiSwitchComponent ],
    providers: [ TextEnrichService, FormBoxService, DatetimeService, AutocompleteService, FileDropService, FormService ],
    exports: [ FormBoxComponent, InputTextEnrichDirective, AutofocusDirective, PickerInputDirective, TextEnrichAutocompleteComponent, TextareaAutocompleteComponent, Autosize, InputSelectComponent, LmnSwitchComponent, DatetimepickerComponent, DatepickerComponent, TimepickerComponent, AutocompleteSelectorDirective, FileDropzoneComponent, FileInputComponent, FileUploadComponent, FileDownloadComponent, NumberInputComponent, LinputDirective, StringListInputComponent, UiSwitchComponent ]
})
export class FormUIModule {
    constructor (validationService: ValidationService) {
        validationService.addRule('fileupload', FileuploadValidation);
        validationService.addMessage('fileupload', 'Počkaj chvíľku, tvoje súbory k nám ešte cestujú');
    }
}
