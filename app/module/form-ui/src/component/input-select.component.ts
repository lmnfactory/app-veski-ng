import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { fadeIfAnimation } from 'animation/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-input-select',
    templateUrl: 'input-select.component.html',
    styleUrls: ['input-select.component.css'],
    animations: [ fadeIfAnimation ]
})
export class InputSelectComponent implements OnInit, OnDestroy {

    @Input('lmnItems')
    public items: any[];

    constructor() {

    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    getText(): string {
        return "Test";
    }
}
