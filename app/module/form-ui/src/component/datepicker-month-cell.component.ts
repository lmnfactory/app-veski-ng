import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { LmnDate } from 'core/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-datepicker-month-cell',
    templateUrl: 'datepicker-month-cell.component.html',
    styleUrls: ['picker.css', 'datepicker-month-cell.component.css']
})
export class DatepickerMonthCellComponent implements OnInit, OnDestroy {

    @Input('lmnDate')
    private date: Date;
    @Input('lmnPage')
    private page: LmnDate;
    @Input('lmnSelected')
    private selected: Date;
    @Output('lmnClick')
    private clickEvent: EventEmitter<any>;

    constructor() {
        this.clickEvent = new EventEmitter<any>();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    onClick(event: any): void {
        this.clickEvent.emit(this.date);
    }

    getDay(): number {
        return this.date.getDate();
    }

    isDisabled(): boolean {
        return (this.date.getMonth() != this.page.date().getMonth());
    }

    isCurrent(): boolean {
        let today: Date = new Date();
        return (this.date.getDate() == today.getDate() && this.date.getMonth() == today.getMonth() && this.date.getFullYear() == today.getFullYear());
    }

    isSelected(): boolean {
        if (!this.selected) {
            return false;
        }

        return (this.date.getFullYear() == this.selected.getFullYear() && this.date.getMonth() == this.selected.getMonth() && this.date.getDate() == this.selected.getDate());
    }
}
