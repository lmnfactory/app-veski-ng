import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { TextEnrichService } from '../service/text-enrich.service';
import { ImageAutocompleteItem } from '../object/image-autocomplete-item.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-text-enrich-autocomplete',
    templateUrl: 'text-enrich-autocomplete.component.html',
    styleUrls: ['text-enrich-autocomplete.component.css']
})
export class TextEnrichAutocompleteComponent {

    @Input('lmnList')
    public list: ImageAutocompleteItem[];
    @Input('lmnVisible')
    public visible: boolean;
    @Output('lmnActiveItemChange')
    public activeEmmiter: EventEmitter<number>;
    @Output('lmnSelect')
    public select: EventEmitter<any>;

    private textEnrichService: TextEnrichService;
    private active: number;

    constructor(textEnrichService: TextEnrichService) {
        this.textEnrichService = textEnrichService;
        this.activeEmmiter = new EventEmitter();
        this.select = new EventEmitter();
    }

    @Input('lmnActiveItem')
    set setActive(active: number) {
        this.active = active;
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    onClick(): boolean {
        this.select.emit();
        return false;
    }

    activate(index: number): void {
        this.activeEmmiter.emit(index);
    }
}
