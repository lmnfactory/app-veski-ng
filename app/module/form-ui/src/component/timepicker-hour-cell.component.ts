import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { LmnDate } from 'core/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-timepicker-hour-cell',
    templateUrl: 'timepicker-hour-cell.component.html',
    styleUrls: ['picker.css', 'timepicker-hour-cell.component.css']
})
export class TimepickerHourCellComponent implements OnInit, OnDestroy {

    @Input('lmnHour')
    private hour: number;
    @Input('lmnSelected')
    private selected: Date;
    @Output('lmnClick')
    private clickEvent: EventEmitter<any>;

    constructor() {
        this.clickEvent = new EventEmitter<any>();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    onClick(event: any): void {
        this.clickEvent.emit(this.hour);
    }

    getHour(): number {
        return this.hour;
    }

    isCurrent(): boolean {
        let today: Date = new Date();
        return (this.hour == today.getHours());
    }

    isSelected(): boolean {
        if (!this.selected) {
            return false;
        }

        return (this.hour == this.selected.getHours());
    }
}
