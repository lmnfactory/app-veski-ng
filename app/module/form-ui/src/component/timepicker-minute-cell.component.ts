import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { LmnDate } from 'core/index';

import { DatetimeService } from '../service/datetime.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-timepicker-minute-cell',
    templateUrl: 'timepicker-minute-cell.component.html',
    styleUrls: ['picker.css', 'timepicker-minute-cell.component.css']
})
export class TimepickerMinuteCellComponent implements OnInit, OnDestroy {

    @Input('lmnMinute')
    private minute: number;
    @Input('lmnSelected')
    private selected: Date;
    @Output('lmnClick')
    private clickEvent: EventEmitter<any>;

    private datetimeService: DatetimeService;

    constructor(datetimeService: DatetimeService) {
        this.datetimeService = datetimeService;
        this.clickEvent = new EventEmitter<any>();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    onClick(event: any): void {
        this.clickEvent.emit(this.minute);
    }

    getMinute(): number {
        return this.minute;
    }

    isCurrent(): boolean {
        let today: Date = new Date();
        let diff = this.minute - today.getMinutes();
        return (diff <= 0 && diff > -this.datetimeService.getMinutesOffset());
    }

    isSelected(): boolean {
        if (!this.selected) {
            return false;
        }

        return (this.minute == this.selected.getMinutes());
    }
}
