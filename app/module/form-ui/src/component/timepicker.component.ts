import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter } from '@angular/core';

import { DatetimeService } from '../service/datetime.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-timepicker',
    templateUrl: 'timepicker.component.html',
    styleUrls: ['picker.css', 'timepicker.component.css']
})
export class TimepickerComponent implements OnInit, OnDestroy, OnChanges {

    @Output('lmnChange')
    private changeEmitter: EventEmitter<any>;
    @Input('lmnDate')
    public selected: Date;
    @Input('lmnName')
    public name: string;

    private datetimeService: DatetimeService;
    public hour: string;
    public minute: string;

    constructor(datetimeService: DatetimeService) {
        this.datetimeService = datetimeService;
        this.changeEmitter = new EventEmitter<any>();

        this.hour = "";
        this.minute = "";
        if (this.selected) {
            this.selected.setSeconds(0);
            this.selected.setMilliseconds(0);
        }
     }

     private init(): void {
        if (!this.selected) {
            this.selected = new Date();
        }
        this.selected.setSeconds(0);
        this.selected.setMilliseconds(0);
     }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {}

    ngOnChanges(): void {
        if (!this.selected) {
            this.hour = "";
            this.minute = "";
        }
        else {
            this.hour = this.selected.getHours().toString();

            let offset: number = this.datetimeService.getMinutesOffset();
            let sub: number = this.selected.getMinutes() % offset;
            this.selected.setMinutes(this.selected.getMinutes() - sub);
            this.minute = this.selected.getMinutes().toString();
        }
    }

    select(date: Date): void {
        this.changeEmitter.emit(date);
    }

    selectHour(hour: any): void {
        this.init();
        
        this.hour = hour;
        this.selected.setHours(hour);
        this.select(this.selected);
    }

    selectMinute(minute: any): void {
        this.init();

        this.minute = minute;
        this.selected.setMinutes(minute);
        this.select(this.selected);
    }

    setHour(hour: any): void {
        if (/^\d+$/.test(hour) && hour < 24 && hour >= 0) {
            this.hour = hour;
        }
        else {
            this.hour = "";
        }
    }

    setMinute(minute: any): void {
        if (/^\d+$/.test(minute) && minute < 60 && minute >= 0) {
            this.minute = minute;
        }
        else {
            this.minute = "";
        }
    }
}