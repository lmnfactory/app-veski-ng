export interface PickerComponent {
    isHidden(): boolean;
    show(): void;
    hide(): void;
}