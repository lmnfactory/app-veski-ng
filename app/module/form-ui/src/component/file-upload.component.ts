import { Component, OnInit, Input, Output, ChangeDetectorRef, EventEmitter } from '@angular/core';

import { UploadProgress } from 'http-communication/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-file-upload',
    templateUrl: 'file-upload.component.html',
    styleUrls: ['file.css', 'file-upload.component.css']
})

export class FileUploadComponent implements OnInit {

    @Input('lmnUpload')
    public upload: UploadProgress;
    @Input('lmnFile')
    public file: File;
    @Output('lmnClose')
    public emitClose: EventEmitter<any>;

    private progress: number;
    public done: boolean;
    public error: boolean;
    private _mouseover: boolean;
    private changeDetectorRef: ChangeDetectorRef;
    private dragCounter: number;

    constructor(changeDetectorRef: ChangeDetectorRef) {
        this.changeDetectorRef = changeDetectorRef;
        this.progress = 0;
        this.done = false;
        this.error = false;
        this._mouseover = false;
        this.emitClose = new EventEmitter();
        this.dragCounter = 0;
    }

    ngOnInit() {
        this.upload.progress((progress: number) => {
            this.progress = progress;
            this.changeDetectorRef.detectChanges();
        }).subscribe((data: any) => {
            this.done = true;
        }, (data: any) => {
            this.error = true;
        });
    }

    getProgressPercentage(): string {
        return this.progress + "%";
    }

    mouseover(event: any): void {
        if (this.dragCounter == 0) {
            this._mouseover = true;
        }
        this.dragCounter++;
    }

    mouseout(event: any): void {
        this.dragCounter--;
        if (this.dragCounter == 0) {
            this._mouseover = false;
        }
    }

    onClose(event: any): void {
        this.emitClose.emit(this.file);
    }

    showClose(): boolean {
        return this._mouseover;
    }
}