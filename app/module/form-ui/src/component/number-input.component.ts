import { Component, Input, Output, OnInit, OnDestroy, EventEmitter, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Observer } from 'core/index';

import { FormBoxObservable } from '../object/form-box-observable.object';
import { FormBoxService } from '../service/form-box.service';
import { FormService } from '../service/form.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-number-input',
    templateUrl: 'number-input.component.html',
    styleUrls: ['number-input.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => NumberInputComponent),
            multi: true
        }
    ]
})

export class NumberInputComponent implements OnInit, OnDestroy, ControlValueAccessor, Observer {

    @Input('lmnId')
    public id: string;
    @Output('lmnFocus')
    private focusEmitter: EventEmitter<any>;
    @Output('lmnBlur')
    private blurEmitter: EventEmitter<any>;

    private formBoxService: FormBoxService;
    private formService: FormService;
    private _value: number;
    private valueChange: any;
    public formBox: FormBoxObservable;

    constructor(formBoxService: FormBoxService, formService: FormService) {
        this.formBoxService = formBoxService;
        this.formService = formService;
        this.focusEmitter = new EventEmitter();
        this.blurEmitter = new EventEmitter();
        this.formBox = formBoxService.createObservable();
        this.id = this.formService.generateId();
    }

    ngOnInit(): void {
        this.formBox.subscribe(this, 'form');
    }

    ngOnDestroy(): void {}

    update(key: string): void {
        if (key == 'form') {
            if (this.formBox.hasFocus()) {
                this.onFocus({});
            }
            else {
                this.onBlur({});
            }
        }
    }

    setValue(val: any) {
        this._value = val;
        if (this.valueChange) {
            this.valueChange(this._value);
        }
    }

    getValue(): number {
        return this._value;
    }

    incrementValue(): void {
        this.setValue(this._value + 1);
    }

    decrementValue(): void {
        this.setValue(this._value - 1);
    }

    onFocus(event: any): void {
        this.focusEmitter.emit(event);
    }

    onBlur(event: any): void {
        this.blurEmitter.emit(event);
    }

    writeValue(number: any) : void {
        this.setValue(number);
    }

    registerOnChange(fn: any) : void {
        this.valueChange = fn;
    }

    registerOnTouched(fn: any) : void { }
}