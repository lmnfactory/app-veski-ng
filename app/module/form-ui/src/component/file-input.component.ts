import { Component, OnInit, Input, Output, OnDestroy, EventEmitter, forwardRef } from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'lmn-file-input',
    templateUrl: 'file-input.component.html',
    styleUrls: ['file-input.component.css']
})

export class FileInputComponent implements OnInit, OnDestroy, ControlValueAccessor {

    @Input('lmnText')
    private text: string;
    @Output('lmnFiles')
    private filesEmitter: EventEmitter<any>;
    @Output('lmnFocus')
    private focusEmitter: EventEmitter<any>;
    @Output('lmnBlur')
    private blurEmitter: EventEmitter<any>;
    private valueChange: any;

    constructor() {
        this.filesEmitter = new EventEmitter();
        this.focusEmitter = new EventEmitter();
        this.blurEmitter = new EventEmitter();
    }

    ngOnInit() {

    }

    ngOnDestroy(): void {

    }

    onFocus(event: any): void {
        this.focusEmitter.emit(event);
    }

    onBlur(event: any): void {
        this.blurEmitter.emit(event);
    }

    onSelect(event: any): void {
        if (this.valueChange) {
            this.valueChange(event);
        }
        this.filesEmitter.emit(event.srcElement.files);
    }

    getText(): string {
        if (!this.text) {
            return "Prilož súbory";
        }
        return this.text;
    }

    writeValue(obj: any) : void {
        this.onSelect(obj);
    }

    registerOnChange(fn: any) : void {
        this.valueChange = fn;
    }

    registerOnTouched(fn: any) : void { }
}