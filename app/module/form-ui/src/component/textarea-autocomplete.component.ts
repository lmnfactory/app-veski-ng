import { Component, Input, Output, OnInit, OnDestroy, EventEmitter, forwardRef, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { AsyncPush } from 'core/index';

import { TextEnrichService } from '../service/text-enrich.service';
import { AutocompleteService } from '../service/autocomplete.service';
import { ImageAutocompleteItem } from '../object/image-autocomplete-item.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-textarea-autocomplete',
    templateUrl: 'textarea-autocomplete.component.html',
    styleUrls: ['textarea-autocomplete.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TextareaAutocompleteComponent),
            multi: true
        }
    ]
})
export class TextareaAutocompleteComponent implements AsyncPush, OnInit, OnDestroy, ControlValueAccessor {

    @Input('lmnPlaceholder')
    public placeholder: string;
    @Input('lmnEnterSubmit')
    private submitOnEnter: any;
    @Output('lmnFocus')
    private focusCallback: EventEmitter<any>;
    @Output('lmnBlur')
    private blurCallback: EventEmitter<any>;
    @ViewChild('input')
    private input: any;

    private textEnrichService: TextEnrichService;
    private autocompleteService: AutocompleteService;
    public autocompleteList: ImageAutocompleteItem[];
    private autocompleteVisible: boolean;
    public activeItem: number;
    private word: string;
    private carotPosition: number;
    public _value: string;
    public _origValue: string;
    private valueChange: any;
    private valueTouched: any;
    private blured: boolean;

    constructor(textEnrichService: TextEnrichService, autocompleteService: AutocompleteService) {
        this.textEnrichService = textEnrichService;
        this.autocompleteService = autocompleteService;
        this.focusCallback = new EventEmitter();
        this.blurCallback = new EventEmitter();
        this._value = "";
        this.blured = false;
    }

    ngOnInit(): void {
        this.autocompleteList = [];
        this.activeItem = 0;
        this.autocompleteVisible = false;
        if (!this.submitOnEnter) {
            this.submitOnEnter = false;
        }
    }

    ngOnDestroy(): void {

    }

    onFocus($event: any): void {
        this.focusCallback.emit($event);
    }

    onBlur($event: any): void {
        this.autocompleteList = [];
        this.blured = true;
        this.onTouche();
        this.blurCallback.emit($event);
    }

    onTouche(): void {
        if (this.valueTouched && this.blured) {
            this.valueTouched();
        }
    }

    onChange($event: any): void {
        this.word = $event.word;
        this.carotPosition = $event.carotPosition;
        this.autocompleteService.timeout(() => {
            this.autocompleteVisible = this.textEnrichService.enrich(this.word, this);
        });
        this.setValue($event.model);
    }

    getValue(): string {
        return this._value;
    }

    setValue(val: any) {
        this._value = val;
        this.onValueChange(this._value);
    }

    onValueChange(event: any): void {
        if (this.valueChange) {
            this.valueChange(event);
        }
    }

    onKeydown($event: any): boolean {
        switch ($event.keyCode) {
            // Arrow down
            case 40:
                if (this.isAutocompleteVisible()) {
                    let result = this.next();
                    return false;
                }
                break;
            // Arrow up
            case 38:
                if (this.isAutocompleteVisible()) {
                    this.previous();
                    return false;
                }
                break;
            // Tab
            case 9:
                if (this.isAutocompleteVisible()) {
                    this.select();
                    return false;
                }
                break;
            // Enter
            case 13:
                if (this.isAutocompleteVisible()) {
                    this.select();
                    return false;
                }
                else if (this.submitOnEnter && !$event.shiftKey) {
                    this.submitOnEnter.ngSubmit.emit($event);
                }
                break;
            // ESC
            case 27:
                if (this.isAutocompleteVisible()) {
                    this.autocompleteList = [];
                    return false;
                }
                break;
        }
        return true;
    }

    focus(): void {
        this.input.nativeElement.focus();
    }

    pushData(data: any): void {
        this.autocompleteList = data;
        this.activeItem = 0;
    }

    isAutocompleteVisible(): boolean {
        return (this.autocompleteVisible && this.autocompleteList.length > 0);
    }

    select(): void {
        let start = this.getValue().substring(0, this.carotPosition - this.word.length);
        let end = this.getValue().substring(this.carotPosition);
        let value: string = start + this.autocompleteList[this.activeItem].toString() + " " + end;
        this.autocompleteList = [];
        this.setValue(value);
    }

    next(): void {
        if (this.activeItem < this.autocompleteList.length - 1) {
            this.activeItem++;
        }
        else {
            this.activeItem = 0;
        }
    }

    previous(): void {
        if (this.activeItem > 0) {
            this.activeItem--;
        }
        else {
            this.activeItem = this.autocompleteList.length - 1;
        }
    }

    writeValue(obj: any) : void {
        this._origValue = obj;
        this.setValue(obj);
    }

    registerOnChange(fn: any) : void {
        this.valueChange = fn;
    }

    registerOnTouched(fn: any) : void {
        this.valueTouched = fn;
    }
}
