import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, HostListener } from '@angular/core';

import { LmnDate } from 'core/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { PickerComponent } from './picker.component';

@Component({
    moduleId: module.id,
    selector: 'lmn-datepicker',
    templateUrl: 'datepicker.component.html',
    styleUrls: ['datepicker.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})
export class DatepickerComponent implements OnInit, OnDestroy, PickerComponent {

    @Output('lmnChange')
    private changeEmitter: EventEmitter<any>;
    @Input('lmnDate')
    public selected: Date;

    public page: LmnDate;
    private calendarGrid: any[];
    private months: string[];
    private hidden: boolean;

    constructor() {
        this.changeEmitter = new EventEmitter<any>();
        this.page = null;
        this.hidden = true;
        this.calendarGrid = [];
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
     }

    ngOnInit(): void {
        if (!this.page) {
            if (!this.selected) {
                this.page = new LmnDate(new Date());
            }
            else {
                this.page = new LmnDate(this.selected);
            }
        }
        this.doCalendarGrid();
     }

    ngOnDestroy(): void {}

    @HostListener('mousedown', ['$event'])
    onmousedown(event: any): void {
        event.preventDefault();
    }

    private doCalendarGrid(): void {
        let firstMonth: LmnDate = this.page.clone();
        firstMonth.date().setDate(1);

        let lastMonth: LmnDate = firstMonth.clone();
        lastMonth.addMonth(1);
        lastMonth.removeDay(1);

        let gridDate: LmnDate = firstMonth.clone();
        let firstDay: number = firstMonth.getDay();
        let total: number = firstDay + lastMonth.date().getDate() + 6 - lastMonth.getDay();

        gridDate.removeDay(firstDay);
        this.calendarGrid = [];
        let i: number = 0;
        let j: number = 0;
        for (i = 0; i < 6; i++) {
            this.calendarGrid.push([]);
            for (j = 0; j < 7; j++) {
                this.calendarGrid[i].push(new Date(gridDate.date().getTime()));
                gridDate.addDay(1);
            }
        }
    }

    getCellDate(cell: number): Date {
        let i: number = Math.floor(cell / 7);
        let j: number = cell % 7;
        return this.calendarGrid[i][j];
    }

    getTitle(): string {
        let date: Date = this.page.date();
        return this.months[date.getMonth()] + " " + date.getFullYear();
    }

    next(): void {
        this.page.addMonth(1);
        this.doCalendarGrid();
    }

    previous(): void {
        this.page.addMonth(-1);
        this.doCalendarGrid();
    }

    select(date: Date): void {
        this.changeEmitter.emit(date);
        if (date.getMonth() == this.page.date().getMonth()) {
            return;
        }

        if (date.getTime() > this.page.date().getTime()) {
            this.next();
        }
        else {
            this.previous();
        }
    }

    isHidden(): boolean {
        return this.hidden;
    }

    show(): void {
        this.hidden = false;
    }

    hide(): void {
        this.hidden = true;
    }

    rows(): any[] {
        return [0, 1, 2, 3, 4, 5];
    }

    columns(): any[] {
        return [0, 1, 2, 3, 4, 5, 6];
    }
}