import { Component, Input, Output, OnInit, OnDestroy, EventEmitter, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'lmn-switch',
    templateUrl: 'lmn-switch.component.html',
    styleUrls: ['lmn-switch.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => LmnSwitchComponent),
            multi: true
        }
    ]
})
export class LmnSwitchComponent implements OnInit, OnDestroy, ControlValueAccessor {

    @Input('lmnLabel')
    public label: string;

    private focusValue: boolean;
    public _value: boolean;
    private valueChange: any;

    constructor() {
        this._value = false;
    }

    ngOnInit(): void {
        this.focusValue = false;
    }

    ngOnDestroy(): void {

    }

    getValue() {
        return this._value;
    }

    setValue(val: any) {
        this._value = val;
        this.onChange(this._value);
    }

    onChange(event: any): void {
        if (this.valueChange) {
            this.valueChange(event);
        }
    }

    onKeypress(event: any): void {
        event.preventDefault();
        // On Enter
        if (event.charCode == 32) {
            this.setValue(!this._value);
        }
    }

    onLabelClick(evenet: any): void {
        this.setValue(!this._value);
    }

    onFocus(): void {
        this.focusValue = true;
    }

    onBlur(): void {
        this.focusValue = false;
    }

    writeValue(obj: any) : void {
        this.setValue(obj);
    }

    registerOnChange(fn: any) : void {
        this.valueChange = fn;
    }

    registerOnTouched(fn: any) : void { }

    hasFocus(): boolean {
        return this.focusValue;
    }
}
