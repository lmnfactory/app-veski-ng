import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-form-box',
    templateUrl: 'form-box.component.html',
    styleUrls: ['form-box.component.css']
})
export class FormBoxComponent {

    @Input('lmnCompact')
    private compact: boolean;

    constructor() {

    }

    isCompact(): boolean {
        return this.compact;
    }

    focus(name: string): void {

    }

    blur(name: string): void {

    }
}
