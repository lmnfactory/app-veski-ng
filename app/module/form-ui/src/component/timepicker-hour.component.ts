import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, HostListener } from '@angular/core';

import { LmnDate } from 'core/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { PickerComponent } from './picker.component';

@Component({
    moduleId: module.id,
    selector: 'lmn-timepicker-hour',
    templateUrl: 'timepicker-hour.component.html',
    styleUrls: ['picker.css', 'timepicker-hour.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})
export class TimepickerHourComponent implements OnInit, OnDestroy, PickerComponent {

    @Output('lmnChange')
    private changeEmitter: EventEmitter<any>;
    @Input('lmnDate')
    public selected: Date;

    private hidden: boolean;
    private _hoursList: number[];

    constructor() {
        this.changeEmitter = new EventEmitter<any>();
        this.hidden = true;

        this._hoursList = [];
        for (let i: number = 0; i < 24; i++) {
            this._hoursList.push(i);
        }
     }

    ngOnInit(): void {}

    ngOnDestroy(): void {}

    @HostListener('mousedown', ['$event'])
    onmousedown(event: any): void {
        event.preventDefault();
    }

    select(hour: any): void {
        this.changeEmitter.emit(hour);
    }

    isHidden(): boolean {
        return this.hidden;
    }

    show(): void {
        this.hidden = false;
    }

    hide(): void {
        this.hidden = true;
    }

    hoursList(): number[] {
        return this._hoursList;
    }
}