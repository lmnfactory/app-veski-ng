import { Component, OnInit, OnDestroy, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';

import { EventHandler } from 'core/index';

@Component({
    moduleId: module.id,
    selector: 'lmn-file-dropzone',
    templateUrl: 'file-dropzone.component.html',
    styleUrls: ['file-dropzone.component.css']
})

export class FileDropzoneComponent implements OnInit {

    @Output('lmnFiles')
    public filesEmitter: EventEmitter<any>;

    private fileInDoc: boolean;
    private focus: boolean;
    private eventHandler: EventHandler;
    private changeDetectorRef: ChangeDetectorRef;
    private events: any[];
    private fileOverDelay: any;

    constructor(eventHandler: EventHandler, changeDetectorRef: ChangeDetectorRef) {
        this.eventHandler = eventHandler;
        this.changeDetectorRef = changeDetectorRef;
        this.filesEmitter = new EventEmitter();
        this.events = [];
        this.fileInDoc = false;
        this.focus = false;
    }

    private onFileOver(value: any): void {
        this.fileInDoc = value;
    }

    ngOnInit(): void {
        this.events.push(
            this.eventHandler.on('file.over', this.onFileOver.bind(this))
        );
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    hasFileInDoc(): boolean{
        return this.fileInDoc;
    }

    hasFocus(): boolean {
        return this.focus;
    }

    mouseover(event: any): void {
        event.preventDefault();
        event.stopPropagation();
        this.focus = true;
    }

    mouseout(event: any): void {
        event.preventDefault();
        event.stopPropagation();
        this.focus = false;
    }

    onFileDrop(event: any): void {
        this.focus = false;
        this.filesEmitter.emit(event);
    }
}