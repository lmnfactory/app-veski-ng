import { Component, OnInit, OnDestroy, Input, Output, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import * as moment from 'moment/moment';

@Component({
    moduleId: module.id,
    selector: 'lmn-datetimepicker',
    templateUrl: 'datetimepicker.component.html',
    styleUrls: ['datetimepicker.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatetimepickerComponent),
            multi: true
        }
    ]
})
export class DatetimepickerComponent implements OnInit, OnDestroy {

    private _value: Date;
    private valueChange: any;
    private valueTouched: any;
    private date: Date;
    private time: Date;

    public dateValue: any;
    @Input('lmnPlaceholder')
    public placeholder: string;

    constructor() {
        this._value = null;
        this.date = null;
        this.time = null;
        this.dateValue = "";
    }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {}

    private refreshValue(): void {
        if (this.date && this.time) {
            this.date.setSeconds(this.time.getSeconds());
            this.date.setMinutes(this.time.getMinutes());
            this.date.setHours(this.time.getHours());
            this.setValue(this.date);
        }
        else {
            this.setValue(null);
        }
    }

    getValue() {
        return this._value;
    }

    setValue(val: any) {
        this._value = val;
        this.onChange(this._value);
    }

    setDate(date: Date): void {
        this.date = date;
        if (this.date) {
            this.dateValue = this.date.getDate() + "." + (this.date.getMonth() + 1) + "." + this.date.getFullYear();
        }
        else {
            this.dateValue = "";
        }
        this.refreshValue();
    }

    getDate(): Date {
        return this.date;
    }

    setTime(time: Date): void {
        this.time = time;
        this.refreshValue();
    }

    getTime(): Date {
        return this.time;
    }

    getStringValue(): string {
        return this.dateValue;
    }

    setStringValue(value: string): void {
        value = value.replace(" ", "");
        this.dateValue = value;
        let momentDate: any = moment(value, "D.M.YYYY");
        if (momentDate.isValid()) {
            this.setValue(momentDate.toDate());
        }
        else {
            this.setValue(null);
        }
    }

    onChange(event: any): void {
        if (this.valueChange) {
            this.valueChange(event);
        }
        if (this.valueTouched) {
            this.valueTouched(event);
        }
    }

    writeValue(obj: any) : void {
        this.setDate(obj);
        this.setTime(obj);
    }

    registerOnChange(fn: any) : void {
        this.valueChange = fn;
    }

    registerOnTouched(fn: any) : void {
        this.valueTouched = fn;
    }
}