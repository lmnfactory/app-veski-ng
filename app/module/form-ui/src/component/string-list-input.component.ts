import { Component, Input, Output, OnInit, OnDestroy, EventEmitter, forwardRef, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { FormBoxService } from '../service/form-box.service';
import { FormBoxEmitter } from '../object/form-box-emitter.object';

@Component({
    moduleId: module.id,
    selector: 'lmn-string-list-input',
    templateUrl: 'string-list-input.component.html',
    styleUrls: ['string-list-input.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => StringListInputComponent),
            multi: true
        }
    ]
})

export class StringListInputComponent implements OnInit, OnDestroy, ControlValueAccessor {

    @Input('lmnPlaceholder')
    public placeholder: string;
    @Output('lmnFocus')
    public focusEmitter: EventEmitter<any>;
    @Output('lmnBlur')
    public blurEmitter: EventEmitter<any>;

    private elementRef: ElementRef;
    public value: string[];
    public inputs: any[];
    private nextId: number;
    private valueChange: any;
    public formBox: FormBoxEmitter;
    private events: any[];

    constructor(elementRef: ElementRef, formBoxService: FormBoxService) {
        this.elementRef = elementRef;
        this.focusEmitter = new EventEmitter();
        this.blurEmitter = new EventEmitter();
        this.formBox = formBoxService.createEmitter();
        this.events = [];
    }

    private formBoxChange(): void {
        if (this.formBox.hasFocus()) {
            this.focusEmitter.emit({});
        } else {
            this.blurEmitter.emit();
        }
    }

    ngOnInit() {
        this.events.push(
            this.formBox.on('focus', this.formBoxChange.bind(this))
        );
        this.events.push(
            this.formBox.on('blur', this.formBoxChange.bind(this))
        );
    }

    ngOnDestroy(): void {
        for (let i: number = 0; i < this.events.length; i++) {
            this.events[i]();
        }
    }

    private onChange(): void {
        if (this.valueChange) {
            this.valueChange(this.value);
        }
    }

    private resetAutofocus(): void {
        for (let i: number = 0; i < this.inputs.length; i++) {
            this.inputs[i].focus = false;
        }
    }

    setItem(index: number, value: string): void {
        this.value[index] = value;
        this.onChange();
    }

    onKeydown($event: any, index: number): void {
        let elements: any;
        switch ($event.keyCode) {
            // ENTER
            case 13:
                event.preventDefault();
                this.value.splice(index + 1, 0, "");
                this.inputs.splice(index + 1, 0, {value: '', id: this.nextId++, focus: true});
                this.onChange();
            break;
            // BACKSPACE
            case 8:
                if (this.value.length > 1 && (!this.value[index] || this.value[index].length == 0)) {
                    event.preventDefault();
                    this.value.splice(index, 1);
                    this.inputs.splice(index, 1);
                    elements = this.elementRef.nativeElement.querySelectorAll('input');
                    if (index > 0) {
                        elements[index - 1].focus();
                    } else {
                        elements[index + 1].focus();
                    }
                    this.onChange();
                }
                break;
            // Arrow down
            case 40:
                if (index < this.inputs.length) {
                    event.preventDefault();
                    elements = this.elementRef.nativeElement.querySelectorAll('input');
                    elements[index + 1].focus();
                } 
                break;
            // Arrow up
            case 38:
                if (index > 0) {
                    event.preventDefault();
                    elements = this.elementRef.nativeElement.querySelectorAll('input');
                    elements[index - 1].focus();
                }
                break;
        }
    }

    hasFocus(item: any): boolean {
        return item.focus;
    }

    listTrack(index: number, item: any): any{
        return item.id;
    }

    writeValue(list: string[]) : void {
        if (list == null) {
            return;
        }
        this.nextId = 0;
        this.inputs = [];
        for (let i: number = 0; i < list.length; i++) {
            this.inputs.push({value: list[i], id: this.nextId++, focus: false});
        }
        this.value = list;
    }

    registerOnChange(fn: any) : void {
        this.valueChange = fn;
    }

    registerOnTouched(fn: any) : void { }
}