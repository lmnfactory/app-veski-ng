import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lmn-file-download',
    templateUrl: 'file-download.component.html',
    styleUrls: ['file.css', 'file-download.component.css']
})

export class FileDownloadComponent implements OnInit {

    @Input('lmnFile')
    public file: any;

    constructor() { }

    ngOnInit() { }

    onclick(): void {
        
    }
}