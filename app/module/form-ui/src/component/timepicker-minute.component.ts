import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, HostListener } from '@angular/core';

import { LmnDate } from 'core/index';
import { fadeIfAnimation, fadeHiddenAnimation } from 'animation/index';

import { PickerComponent } from './picker.component';
import { DatetimeService } from '../service/datetime.service';

@Component({
    moduleId: module.id,
    selector: 'lmn-timepicker-minute',
    templateUrl: 'timepicker-minute.component.html',
    styleUrls: ['picker.css', 'timepicker-minute.component.css'],
    animations: [ fadeIfAnimation, fadeHiddenAnimation ]
})
export class TimepickerMinuteComponent implements OnInit, OnDestroy, PickerComponent {

    @Output('lmnChange')
    private changeEmitter: EventEmitter<any>;
    @Input('lmnDate')
    public selected: Date;

    private datetimeService: DatetimeService;
    private hidden: boolean;
    private _minutesList: number[];
    public cellOffset: number;    

    constructor(datetimeService: DatetimeService) {
        this.datetimeService = datetimeService;
        this.changeEmitter = new EventEmitter<any>();
        this.hidden = true;
        this.cellOffset = this.datetimeService.getMinutesOffset();

        this._minutesList = [];
        for (let i: number = 0; i < 60; i += this.cellOffset) {
            this._minutesList.push(i);
        }
     }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {}

    @HostListener('mousedown', ['$event'])
    onmousedown(event: any): void {
        event.preventDefault();
    }

    select(hour: any): void {
        this.changeEmitter.emit(hour);
    }

    isHidden(): boolean {
        return this.hidden;
    }

    show(): void {
        this.hidden = false;
    }

    hide(): void {
        this.hidden = true;
    }

    minutesList(): number[] {
        return this._minutesList;
    }
}