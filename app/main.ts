import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

import { VeskiModule } from 'app-veski/index';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(VeskiModule)
    .catch(err => {
        console.log("Bootstrap error");
        console.log(err);
    });
