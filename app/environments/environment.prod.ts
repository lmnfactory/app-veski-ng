export const environment = {
  production: true,
  websocket: {
    ip: 'www.veski.sk',
    port: 8443,
    secure: true
  }
};
