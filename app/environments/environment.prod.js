"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: true,
    websocket: {
        ip: 'www.veski.sk',
        port: 8443,
        secure: true
    }
};
//# sourceMappingURL=environment.prod.js.map