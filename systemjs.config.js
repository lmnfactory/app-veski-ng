/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
        paths: {

          // paths serve as alias
          'npm:': 'node_modules/',
          'module:': 'app/module/'
        },
        // map tells the System loader where to look for things
        map: {
          // our app is within the app folder
          app: 'app',

          // angular bundles
          '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
          '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
          '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
          '@angular/platform-browser/animations': 'npm:@angular/platform-browser/bundles/platform-browser-animations.umd.js',
          '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
          '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
          '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
          '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
          '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
          '@angular/animations/browser': 'npm:@angular/animations/bundles/animations-browser.umd.js',
          '@angular/animations': 'npm:@angular/animations/bundles/animations.umd.js',

          '@angular/flex-layout': 'npm:@angular/flex-layout/bundles/flex-layout.umd.js',
          '@ng-bootstrap/ng-bootstrap': 'npm:@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js',
          'angular2-infinite-scroll': 'npm:angular2-infinite-scroll',
          'mydatepicker': 'npm:mydatepicker/bundles/mydatepicker.umd.min.js',
          'moment': 'npm:moment',
          'lodash': 'npm:lodash',
          'tslerp': 'npm:tslerp',
          'ng2-file-drop': 'npm:ng2-file-drop',

          'app-veski': 'module:app-veski',
          'core': 'module:core',
          'account': 'module:account',
          'menu': 'module:menu',
          'layout': 'module:layout',
          'form-ui': 'module:form-ui',
          'palette': 'module:pallete',
          'faculty': 'module:faculty',
          'university': 'module:university',
          'subject': 'module:subject',
          'thread': 'module:thread',
          'tag': 'module:tag',
          'http-communication': 'module:http-communication',
          'state': 'module:state',
          'websocket': 'module:websocket',
          'notification': 'module:notification',
          'animation': 'module:animation',
          'browser-notification': 'module:browser-notification',
          'calendar': 'module:calendar',
          'calendarevent': 'module:calendarevent',
          'lmn-color': 'module:lmn-color',
          'search': 'module:search',
          'file': 'module:file',
          'degree': 'module:degree',
          'header': 'module:header',
          'timeline': 'module:timeline',
          'text': 'module:text',

          // other libraries
          'rxjs':                      'npm:rxjs',
          'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            app: {
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
            },
            'angular2-infinite-scroll': {
                defaultExtension: 'js'
            },
            'tslerp': { 
                main: 'index.js', 
                defaultExtension: 'js'
            },
            'ng2-file-drop': { 
                main: 'index.js', 
                defaultExtension: 'js' 
            },
            'moment': {
                defaultExtension: 'js' 
            },
            'lodash': {
                main: 'index.js',
                defaultExtension: 'js' 
            }
        }
    });
})(this);
