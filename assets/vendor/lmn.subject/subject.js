"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SubjectForm = function (_React$Component) {
    _inherits(SubjectForm, _React$Component);

    function SubjectForm(props) {
        _classCallCheck(this, SubjectForm);

        var _this = _possibleConstructorReturn(this, (SubjectForm.__proto__ || Object.getPrototypeOf(SubjectForm)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService"),
            filter: lmn.provider.get('filterService')
        };

        var self = _this;

        _this.state = {
            facultyId: "",
            subjectName: "",
            subjectCode: ""
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.save = _this.save.bind(_this);
        _this.facultyIdChange = _this.facultyIdChange.bind(_this);
        _this.subjectNameChange = _this.subjectNameChange.bind(_this);
        _this.subjectCodeChange = _this.subjectCodeChange.bind(_this);
        return _this;
    }

    _createClass(SubjectForm, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "subjectCreate", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "facultyId", placeholder: "faculty id", value: this.state.facultyId, onChange: this.facultyIdChange }),
                React.createElement("input", { type: "text", name: "subjectName", placeholder: "name", value: this.state.subjectName, onChange: this.subjectNameChange }),
                React.createElement("input", { type: "text", name: "subjectCode", placeholder: "code", value: this.state.subjectCode, onChange: this.subjectCodeChange }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Create Subject"
                )
            );
        }
    }, {
        key: "facultyIdChange",
        value: function facultyIdChange(event) {
            this.setState({ facultyId: event.target.value });
        }
    }, {
        key: "subjectNameChange",
        value: function subjectNameChange(event) {
            this.setState({ subjectName: event.target.value });
        }
    }, {
        key: "subjectCodeChange",
        value: function subjectCodeChange(event) {
            this.setState({ subjectCode: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.save(this.state);
            event.preventDefault();
        }
    }, {
        key: "save",
        value: function save(state) {
            var self = this;

            var request = self.$s.request.create("api/subject/create", {
                faculty_id: state.facultyId,
                name: state.subjectName,
                code: state.subjectCode
            });

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {
                    resolve(user);
                } else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return SubjectForm;
}(React.Component);
//# sourceMappingURL=SubjectForm.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SubjectSearch = function (_React$Component) {
    _inherits(SubjectSearch, _React$Component);

    function SubjectSearch(props) {
        _classCallCheck(this, SubjectSearch);

        var _this = _possibleConstructorReturn(this, (SubjectSearch.__proto__ || Object.getPrototypeOf(SubjectSearch)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        var self = _this;

        _this.state = {
            subjectName: ""
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.search = _this.search.bind(_this);
        _this.subjectNameChange = _this.subjectNameChange.bind(_this);
        return _this;
    }

    _createClass(SubjectSearch, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "subjectName", value: this.state.subjectName, onChange: this.subjectNameChange }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Search Subject"
                )
            );
        }
    }, {
        key: "subjectNameChange",
        value: function subjectNameChange(event) {
            this.setState({ subjectName: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.search();
            event.preventDefault();
        }
    }, {
        key: "search",
        value: function search() {
            var self = this;

            var request = self.$s.request.create("api/subject/search", {
                search: this.state.subjectName
            });

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {
                    resolve(user);
                } else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return SubjectSearch;
}(React.Component);
//# sourceMappingURL=SubjectSearch.js.map
