"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserCalendareventForm = function (_React$Component) {
    _inherits(UserCalendareventForm, _React$Component);

    function UserCalendareventForm(props) {
        _classCallCheck(this, UserCalendareventForm);

        var _this = _possibleConstructorReturn(this, (UserCalendareventForm.__proto__ || Object.getPrototypeOf(UserCalendareventForm)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        var self = _this;

        _this.state = {
            name: "",
            location: "",
            description: "",
            eventstart: "",
            eventend: null,
            duration: 0,
            priority: "3",
            calendareventrules: {
                rule: "",
                exception: false
            },
            subject_id: 1,
            calendareventsettings: {
                private: false
            }
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.save = _this.save.bind(_this);
        _this.nameChange = _this.nameChange.bind(_this);
        _this.locationChange = _this.locationChange.bind(_this);
        _this.descriptionChange = _this.descriptionChange.bind(_this);
        _this.eventstartChange = _this.eventstartChange.bind(_this);
        _this.eventendChange = _this.eventendChange.bind(_this);
        _this.priorityChange = _this.priorityChange.bind(_this);
        _this.durationChange = _this.durationChange.bind(_this);
        _this.ruleChange = _this.ruleChange.bind(_this);
        _this.exceptionChange = _this.exceptionChange.bind(_this);
        _this.privateChange = _this.privateChange.bind(_this);
        return _this;
    }

    _createClass(UserCalendareventForm, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "subjectCreate", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "name", placeholder: "name", value: this.state.name, onChange: this.nameChange }),
                React.createElement("input", { type: "text", name: "location", placeholder: "location", value: this.state.location, onChange: this.locationChange }),
                React.createElement("textarea", { name: "description", placeholder: "description", value: this.state.description, onChange: this.descriptionChange }),
                React.createElement("input", { type: "text", name: "eventstart", placeholder: "eventstart", value: this.state.eventstart, onChange: this.eventstartChange }),
                React.createElement("input", { type: "text", name: "eventend", placeholder: "eventend", value: this.state.eventend, onChange: this.eventendChange }),
                React.createElement("input", { type: "text", name: "priority", placeholder: "priority", value: this.state.priority, onChange: this.priorityChange }),
                React.createElement("input", { type: "text", name: "hourstart", placeholder: "hourstart", value: this.state.duration, onChange: this.durationChange }),
                React.createElement("input", { type: "text", name: "rule", placeholder: "rule", value: this.state.calendareventrules.rule, onChange: this.ruleChange }),
                "ex",
                React.createElement("input", { type: "checkbox", name: "exception", checked: this.state.calendareventrules.exception, onChange: this.exceptionChange }),
                "pr",
                React.createElement("input", { type: "checkbox", name: "private", checked: this.state.calendareventsettings.private, onChange: this.privateChange }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Create Event"
                )
            );
        }
    }, {
        key: "nameChange",
        value: function nameChange(event) {
            this.setState({ name: event.target.value });
        }
    }, {
        key: "locationChange",
        value: function locationChange(event) {
            this.setState({ location: event.target.value });
        }
    }, {
        key: "descriptionChange",
        value: function descriptionChange(event) {
            this.setState({ description: event.target.value });
        }
    }, {
        key: "eventstartChange",
        value: function eventstartChange(event) {
            this.setState({ eventstart: event.target.value });
        }
    }, {
        key: "eventendChange",
        value: function eventendChange(event) {
            this.setState({ eventend: event.target.value });
        }
    }, {
        key: "priorityChange",
        value: function priorityChange(event) {
            this.setState({ priority: event.target.value });
        }
    }, {
        key: "durationChange",
        value: function durationChange(event) {
            this.setState({ duration: event.target.value });
        }
    }, {
        key: "ruleChange",
        value: function ruleChange(event) {
            this.setState({ calendareventrules: {
                    rule: event.target.value,
                    exception: this.state.calendareventrules.exception
                } });
        }
    }, {
        key: "exceptionChange",
        value: function exceptionChange(event) {
            this.setState({ calendareventrules: {
                    exception: event.target.checked,
                    rule: this.state.calendareventrules.rule
                } });
        }
    }, {
        key: "privateChange",
        value: function privateChange(event) {
            this.setState({ calendareventsettings: { private: event.target.checked } });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.save(this.state);
            event.preventDefault();
        }
    }, {
        key: "save",
        value: function save(state) {
            var self = this;

            var request = self.$s.request.create("api/calendar/create", state);

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {
                    resolve(user);
                } else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return UserCalendareventForm;
}(React.Component);
//# sourceMappingURL=UserCalendareventForm.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserCalendareventList = function (_React$Component) {
    _inherits(UserCalendareventList, _React$Component);

    function UserCalendareventList(props) {
        _classCallCheck(this, UserCalendareventList);

        var _this = _possibleConstructorReturn(this, (UserCalendareventList.__proto__ || Object.getPrototypeOf(UserCalendareventList)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService"),
            filter: lmn.provider.get('filterService')
        };

        var self = _this;

        _this.state = {
            user_id: ""
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.get = _this.get.bind(_this);
        _this.userIdChange = _this.userIdChange.bind(_this);
        _this.calendarstartChange = _this.calendarstartChange.bind(_this);
        _this.calendarendChange = _this.calendarendChange.bind(_this);
        return _this;
    }

    _createClass(UserCalendareventList, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "calendarGet", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "user_id", placeholder: "user id", value: this.state.user_id, onChange: this.userIdChange }),
                React.createElement("input", { type: "text", name: "calendarstart", placeholder: "start", value: this.state.since, onChange: this.calendarstartChange }),
                React.createElement("input", { type: "text", name: "calendarend", placeholder: "end", value: this.state.unil, onChange: this.calendarendChange }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Get calendar"
                )
            );
        }
    }, {
        key: "userIdChange",
        value: function userIdChange(event) {
            this.setState({ user_id: event.target.value });
        }
    }, {
        key: "calendarstartChange",
        value: function calendarstartChange(event) {
            this.setState({ since: event.target.value });
        }
    }, {
        key: "calendarendChange",
        value: function calendarendChange(event) {
            this.setState({ until: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.get(this.state);
            event.preventDefault();
        }
    }, {
        key: "get",
        value: function get(data) {
            var self = this;

            var request = self.$s.request.create("api/sharedcalendar/user_calendar", data);

            request.option({ filter: data });

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {
                    resolve(user);
                } else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return UserCalendareventList;
}(React.Component);
//# sourceMappingURL=UserCalendareventList.js.map
