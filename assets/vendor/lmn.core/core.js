"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LmnInitializationComponent = function (_React$Component) {
    _inherits(LmnInitializationComponent, _React$Component);

    function LmnInitializationComponent(props) {
        _classCallCheck(this, LmnInitializationComponent);

        var _this = _possibleConstructorReturn(this, (LmnInitializationComponent.__proto__ || Object.getPrototypeOf(LmnInitializationComponent)).call(this, props));

        _this.$s = {
            init: lmn.provider.get("initialization"),
            logger: lmn.provider.get("logger")
        };

        _this.$e = [];

        var e = _this.$s.init.onEnd(function () {
            this.setState({ isDone: this.$s.init.isDone() });
        }.bind(_this));

        _this.$e.push(e);
        return _this;
    }

    _createClass(LmnInitializationComponent, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            lmn.init();
            this.state = { isDone: this.$s.init.isDone() };
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            for (var i = 0; i < this.$e.length; i++) {
                this.$e[i]();
            }
        }
    }, {
        key: "render",
        value: function render() {
            if (this.state.isDone) {
                return React.createElement(
                    "div",
                    null,
                    this.props.children
                );
            } else {
                return React.createElement(
                    "div",
                    null,
                    "Loading..."
                );
            }
        }
    }]);

    return LmnInitializationComponent;
}(React.Component);
//# sourceMappingURL=LmnInitializationComponent.js.map

"use strict";

function ModelHandler() {}

ModelHandler.prototype.rollback = function (model) {};
//# sourceMappingURL=ModelHandler.js.map

"use strict";

function ModelHistory(model) {
    this.data = model.get();
}
//# sourceMappingURL=ModelHistory.js.map

"use strict";

function Filter(column, value, rule) {
    this.column = column;
    this.value = value;
    if (lmn.isUndefined(rule)) {
        rule = "eq";
    }
    this.rule = rule;
}

Filter.prototype.export = function () {
    return {
        column: this.column,
        rule: this.rule,
        value: this.value
    };
};
//# sourceMappingURL=Filter.js.map

"use strict";

function FilterHandler() {
    this.filters = {};
}

FilterHandler.prototype.add = function (column, value, rule) {
    var f = new Filter(column, value, rule);
    this.filters[column] = f;
};

FilterHandler.prototype.export = function () {
    var exp = {};
    for (var key in this.filters) {
        if (this.filters.hasOwnProperty(key)) {
            exp[key] = this.filters[key].export();
        }
    }
    return exp;
};
//# sourceMappingURL=FilterHandler.js.map

"use strict";

function LmnRequest() {
    this.$method = "post";
    this.$url = "";
    this.$headers = {};
};

LmnRequest.prototype.getset = function (index, value, get) {
    if (get) {
        return this[index];
    }

    var indexes = index.split(".");
    var pointer = this;
    for (var i = 0; i < indexes.length - 1; i++) {
        if (lmn.isUndefined(pointer[indexes[i]])) {
            pointer[indexes[i]] = {};
        }
        pointer = pointer[indexes[i]];
    }
    pointer[indexes[i]] = value;
    return this;
};

LmnRequest.prototype.method = function (method) {
    return this.getset("$method", method, arguments.length == 0);
};

LmnRequest.prototype.url = function (url) {
    return this.getset("$url", url, arguments.length == 0);
};

LmnRequest.prototype.data = function (data) {
    return this.getset("$data", data, arguments.length == 0);
};

LmnRequest.prototype.option = function (option) {
    return this.getset("$option", option, arguments.length == 0);
};

LmnRequest.prototype.headers = function (headers) {
    return this.getset("$headers", headers, arguments.length == 0);
};

LmnRequest.prototype.addHeader = function (index, headers) {
    return this.getset("$headers." + index, headers, arguments.length == 0);
};

LmnRequest.prototype.message = function () {
    return this.data();
};

LmnRequest.prototype.axios = function () {
    return {
        method: this.method(),
        url: this.url(),
        data: this.message(),
        headers: this.headers()
    };
};
//# sourceMappingURL=LmnRequest.js.map

"use strict";

function LmnResponse(request, serverResponse) {
    this.$request = request;
    this.$serverResponse = serverResponse;
};

LmnResponse.prototype.getRequest = function () {
    return this.$request;
};

LmnResponse.prototype.getServerResponse = function () {
    return this.$serverResponse;
};

LmnResponse.prototype.getMessage = function () {
    return this.getServerResponse().data;
};

LmnResponse.prototype.getData = function () {
    if (lmn.isUndefined(this.getMessage())) {
        return this.getMessage();
    }

    return this.getMessage().data;
};

LmnResponse.prototype.getOption = function () {
    if (lmn.isUndefined(this.getMessage())) {
        return this.getMessage();
    }

    return this.getMessage().option;
};

LmnResponse.prototype.getCode = function () {
    return this.getServerResponse().status;
};

LmnResponse.prototype.getStatus = function () {
    return this.getServerResponse().statusText;
};

LmnResponse.prototype.isOk = function () {
    return this.getCode() == 200;
};
//# sourceMappingURL=LmnResponse.js.map

"use strict";

function NotificationRecieveEvent(event, jsonData) {
    this.data = jsonData;
    this.event = event;
}

NotificationRecieveEvent.prototype.getData = function () {
    return this.data;
};

NotificationRecieveEvent.prototype.getEvent = function () {
    return this.event;
};
//# sourceMappingURL=NotificationRecieveEvent.js.map

"use strict";

lmn.provider.service("config", ConfigService);
lmn.provider.service("httpInterceptor", HttpInterceptorService);
lmn.provider.service("http", HttpService);
lmn.provider.service("initialization", InitService);
lmn.provider.service("modelService", RequestService);
lmn.provider.service("requestService", RequestService);
lmn.provider.service("response", ResponseService);
lmn.provider.service("websocketService", WebsocketService);
lmn.provider.service("notificationService", NotificationService);
lmn.provider.service("filterService", FilterService);
lmn.provider.service("cacheService", CacheService);

lmn.run(function () {
    var httpService = lmn.provider.get('http');
    var httpInterceptor = lmn.provider.get('httpInterceptor');
    var cacheService = lmn.provider.get('cacheService');

    httpService.setInterceptor(httpInterceptor);
    cacheService.load();
});
//# sourceMappingURL=provider.js.map

'use strict';

function CacheService(config) {
    this.cache = {};

    this.$s = {
        logger: lmn.provider.get('logger'),
        requestService: lmn.provider.get('requestService'),
        http: lmn.provider.get('http'),
        eventHandler: lmn.provider.get('eventHandler')
    };
}

CacheService.prototype.load = function () {
    var self = this;
    var request = this.$s.requestService.create('app/cache', {});

    this.$s.http.request(request).then(function (response) {
        if (response.isOk()) {
            self.cache = response.getData();
            self.$s.logger.debug({ cache: self.cache });
            self.$s.eventHandler.trigger('cache.load', self);
        }
    });
};

CacheService.prototype.get = function (key) {
    return this.cache[key];
};
//# sourceMappingURL=CacheService.js.map

"use strict";

function ConfigService(config) {}

ConfigService.prototype.select = function (values, def) {
    if (!lmn.isArray(values)) {
        values = [values];
    }

    for (var i = 0; i < values.length; i++) {
        if (!lmn.isUndefined(values[i])) {
            return values[i];
        }
    }

    return def;
};
//# sourceMappingURL=ConfigService.js.map

"use strict";

function FilterService(config) {}

FilterService.prototype.create = function (values, def) {
    return new FilterHandler();
};
//# sourceMappingURL=FilterService.js.map

"use strict";

function HttpInterceptorService(config) {}

HttpInterceptorService.prototype.response = function (success, fail) {
    var eject = axios.interceptors.response.use(success, fail);

    return function () {
        axios.interceptors.response.eject(eject);
    };
};

HttpInterceptorService.prototype.request = function (success, fail) {
    var eject = axios.interceptors.request.use(success, fail);

    return function () {
        axios.interceptors.request.eject(eject);
    };
};
//# sourceMappingURL=HttpInterceptorService.js.map

"use strict";

function HttpService(config) {

    this.interceptor = null;

    this.$s = {
        response: lmn.provider.get("response"),
        logger: lmn.provider.get("logger")
    };
}

HttpService.prototype.setInterceptor = function (interceptor) {
    this.interceptor = interceptor;
};

HttpService.prototype.getInterceptor = function () {
    return this.interceptor;
};

HttpService.prototype.request = function (request) {
    var self = this;

    return new Promise(function (resolve, reject) {
        axios(request.axios()).then(function (response) {
            var res = self.$s.response.create(request, response);
            resolve(res);
        }).catch(function (error) {
            reject(self.$s.response.create(request, error.response));
        });
    });
};
//# sourceMappingURL=HttpService.js.map

"use strict";

function InitService() {
    this.inits = {};

    this.$s = {
        gen: lmn.provider.get("generator").createIdGenerator(),
        eventHandler: lmn.provider.get("eventHandler")
    };
}

InitService.prototype.begin = function () {
    var id = this.$s.gen.nextId();
    var done = this.isDone();

    this.inits[id] = true;
    if (done) {
        this.triggerBegin();
    }

    return function () {
        this.end(id);
    }.bind(this);
};

InitService.prototype.end = function (id) {
    var done = this.isDone();

    delete this.inits[id];

    if (!done && this.isDone()) {
        this.triggerEnd();
    }
};

InitService.prototype.isDone = function () {
    for (var key in this.inits) {
        if (this.inits.hasOwnProperty(key) & this.inits[key] === true) {
            return false;
        }
    }
    return true;
};

InitService.prototype.triggerEnd = function () {
    this.$s.eventHandler.trigger("init.end", {});
};

InitService.prototype.triggerBegin = function () {
    this.$s.eventHandler.trigger("init.begin", {});
};

InitService.prototype.onEnd = function (f) {
    return this.$s.eventHandler.on("init.end", f);
};

InitService.prototype.onBegin = function (f) {
    return this.$s.eventHandler.on("init.begin", f);
};
//# sourceMappingURL=InitService.js.map

"use strict";

function ModelService(config) {
    this.models = {};
    this.addins = {};

    if (!lmn.isUndefined(config.models)) {
        this.models = models;
    }
    if (!lmn.isUndefined(config.addins)) {
        this.addins = config.addins;
    }
};

ModelService.prototype.getProperty = function (model) {
    var properties = {};
    for (var key in model) {
        if (model.hasOwnProperty(key)) {
            properties[key] = model[key];
        }
    }
    return properties;
};

ModelService.prototype.clone = function (model) {
    var modelData = this.getProperty(model);

    return this.create(model.$modelName, model, model.$modelOption);
};

ModelService.prototype.create = function (key, data, option) {
    if (lmn.isUndefined(this.models[key])) {
        return null;
    }
    var model = new this.models[key](data);

    return model;
};

ModelService.prototype.add = function (key, args) {};
//# sourceMappingURL=ModelService.js.map

'use strict';

function NotificationService(config) {
    this.$s = {
        logger: lmn.provider.get('logger'),
        config: lmn.provider.get('config'),
        eventHandler: lmn.provider.get('eventHandler'),
        websocketService: lmn.provider.get('websocketService')
    };

    config = this.$s.config.select([config], {});

    this.socketAddr = {};
    this.setSocketAddr(config.addr, config.protocol);
}

NotificationService.prototype.setSocketAddr = function (addr, protocol) {
    this.socketAddr.addr = addr;
    this.socketAddr.protocol = protocol;
};

NotificationService.prototype.start = function () {
    if (!lmn.hasValue(this.socketAddr.addr)) {
        return;
    }

    this.stop();
    this.socket = this.$s.websocketService.create(this.socketAddr.addr, this.socketAddr.protocol);

    var self = this;
    this.socket.onmessage = function (event) {
        self.$s.logger.debug({ message: event });
        var json = JSON.parse(event.data);
        self.$s.eventHandler.trigger('notification.recieve', new NotificationRecieveEvent(event, json));
    };
};

NotificationService.prototype.auth = function (authData) {
    if (!lmn.hasValue(this.socket)) {
        return;
    }

    this.socket.send(authData);
};

NotificationService.prototype.stop = function () {
    if (lmn.hasValue(this.socket) && !lmn.isUndefined(this.socket.close)) {
        this.socket.close();
    }
};
//# sourceMappingURL=NotificationService.js.map

"use strict";

function RequestService(config) {
    if (lmn.isUndefined(config)) {
        config = {};
    }

    this.$s = {
        config: lmn.provider.get("config"),
        logger: lmn.provider.get("logger")
    };

    this.defaultMethod = this.$s.config.select([config.REQUEST_METHOD], "post");
};

RequestService.prototype.create = function (url, data, option) {
    var request = new LmnRequest();
    request.method(this.defaultMethod).url(url).data(data).option(option);
    return request;
};
//# sourceMappingURL=RequestService.js.map

"use strict";

function ResponseService(config) {};

ResponseService.prototype.create = function (request, serverResopnse) {
    return new LmnResponse(request, serverResopnse);
};
//# sourceMappingURL=ResponseService.js.map

"use strict";

function WebsocketService(config) {}

WebsocketService.prototype.create = function (host, protocol) {
    return new WebSocket(host, protocol);
};
//# sourceMappingURL=WebsocketService.js.map
