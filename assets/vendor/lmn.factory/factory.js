/**
 * Lmn class. This class can be expended by adding new utilities to it.
 * @method Lmn constructor of Lmn class
 */
function Lmn(){
    this.runs = [];

    this.register = this.register.bind(this);
    this.run = this.run.bind(this);
    this.init = this.init.bind(this);
}

/**
 * This method extends utilities that lmn object provides. You cannot register key that already exists.
 * @method register
 * @param  {string} key key under witch it should be registered. This utility will be accessible by calling lmn.key
 * @param  {Object | function} val functionality that this utility provide. It can be single function or a object with multiple methods
 */
Lmn.prototype.register = function(key, val){
    if (this[key] !== undefined){
        throw new Error("Trying to register existing property '" + key + "'");
    }

    this[key] = val;
}

/**
 * Add method that should run at the beginning of the application
 * @method run
 * @param  {function} f function that will be called at the start of the application.
 */
Lmn.prototype.run = function(f){
    this.runs.push(f);
}

/**
 * Run all methods that were added by run method.
 * @method init
 */
Lmn.prototype.init = function(){
    for (var i = 0; i < this.runs.length; i++){
        this.runs[i]();
    }
}

//Creating lmn instance of Lmn class
var lmn = new Lmn();

/**
 * Method that checks if value is not undefined and is not null.
 * @method hasValue
 * @param  {any}  value value to be check
 * @return {Boolean}       true if value is not undefined and is not null
 */
var hasValue = function(value){
    return (!lmn.isUndefined(value) && !lmn.isNull(value));
};

lmn.register("hasValue", hasValue);

var isArray = function(value){
    return (Object.prototype.toString.call(value) === '[object Array]');
};

lmn.register("isArray", isArray);

/**
 * Check if value is function.
 * @method isFunction
 * @param  {any}   value value to be check
 * @return {Boolean}        true if value is function.
 */
var isFunction = function(value){
    return ((typeof value) == "function");
};

lmn.register("isFunction", isFunction);

/**
 * Check if value is null.
 * @method isNull
 * @param  {any}  value value to be check.
 * @return {Boolean}       true if value is null.
 */
var isNull = function(value){
    return value === null;
}

lmn.register("isNull", isNull);

var isObject = function(value){
    return ((typeof value) == 'object');
};

lmn.register("isObject", isObject);

/**
 * Check if value is type of string.
 * @method isString
 * @param  {any}  value value to check
 * @return {Boolean}       true if value is type of string
 */
var isString = function(value){
    return ((typeof value) == "string");
};

lmn.register("isString", isString);

/**
 * Check if value is undefined
 * @method isUndefined
 * @param  {any}    value value to check
 * @return {Boolean}         true if value is undefined
 */
var isUndefined = function(value){
    return value === undefined;
};

lmn.register("isUndefined", isUndefined);

/**
 * Provider class that enables you to register services and config objects. This is a constructor of Provider utility.
 * @method LmnProvider
 */
function LmnProvider(){
    this.values = {};
    this.services = {};
    this.circularInjection = [];
}

/**
 * Method that will validate key and add new item to values object.
 * @method add
 * @param  {string} key   name of service or object or value ...
 * @param  {any} value actual object or service or value ...
 */
LmnProvider.prototype.add = function(key, value){
    if (lmn.isUndefined(key)){
        throw new Error("Key to register cannot be undefined.");
    }
    if (lmn.isNull(key)){
        throw new Error("Key to register cannot be null.");
    }
    if (!lmn.isString(key)){
        throw new Error("Key to register has to be string.");
    }
    if (key == ""){
        throw new Error("Key to register cannot be empty.");
    }
    if (!lmn.isUndefined(this.values[key])){
        throw new Error("This property '" + key + "' is already registered.");
    }

    this.values[key] = value;
}

/**
 * Register new value in provider. Value is a object or function or anything that you want to be able retrieve.
 * @method value
 * @param  {string} key    name of the value
 * @param  {any} values object to store and be able to retrieve
 */
LmnProvider.prototype.value = function(key, values){
    this.add(key, values);
}

/**
 * Register service. Service is a class. It can be configured and new object, out of this class, is created when sombody needs this service.
 * @method service
 * @param  {string} key     name of service
 * @param  {[type]} service class that represents service
 */
LmnProvider.prototype.service = function(key, service){
    this.add(key, null);

    this.services[key] = service;
}

/**
 * Configure service. Only services can be configured until they are not instancieted. You should call this method at the beginning of the application run.
 * @method config
 * @param  {string} key    name of the service
 * @param  {[type]} config configuration object
 */
LmnProvider.prototype.config = function(key, config){
    if (lmn.isUndefined(this.services[key])){
        throw new Error("Service you want to config does not exist '" + key + "'");
    }

    if (lmn.isFunction(config)){
        config(this.services[key]);
    }
    else{
        this.services[key].$config = config;
    }
}

/**
 * Retrieve service or value by key.
 * @method get
 * @param  {string} key service or value name
 * @return {any}     throws error if there is nothing with the key
 */
LmnProvider.prototype.get = function(key){

    if (this.circularInjection.indexOf(key) > -1){
        throw new Error("Circular injection in '" + key + "': " + this.circularInjection.join(" -> "));
    }
    this.circularInjection.push(key);

    if (!lmn.hasValue(this.values[key])){
        if (lmn.isUndefined(this.services[key])){
            this.circularInjection.pop();
            throw new Error("Service you requested does not exist '" + key + "'");
        }

        this.values[key] = new this.services[key](this.services[key].$config);
    }

    this.circularInjection.pop();

    return this.values[key];
}

lmn.register("provider", new LmnProvider());

/**
 * Service that handles all custom events.
 * @method EventHandler
 */
function EventHandler(){
    this.listeners = {};
};

/**
 * Check if there is listener with name
 * @method hasListener
 * @param  {string}    name name of the listener
 * @return {Boolean}        true if there is listener with name
 */
EventHandler.prototype.hasListener = function(name){
    return lmn.hasValue(this.listeners[name]);
}

/**
 * Retrieve listener with name
 * @method getListener
 * @param  {string}    name name of the listener
 * @return {EventListener}
 */
EventHandler.prototype.getListener = function(name){
    return this.listeners[name];
}

/**
 * Trigger event with name (this name is simular to listener)
 * @method trigger
 * @param  {string} name  name of listener
 * @param  {LmnEvent} event custom event
 */
EventHandler.prototype.trigger = function(name, event){
    if (!this.hasListener(name)){
        return ;
    }

    this.getListener(name).trigger(event);
};

/**
 * Register listener for specific name
 * @method on
 * @param  {[type]} name name of the listener
 * @param  {[type]} f    method that should be called when event fires
 * @return {function}      function that will unbind this event listener.
 */
EventHandler.prototype.on = function(name, f){
    if (!this.hasListener(name)){
        this.listeners[name] = new EventListener();
    }

    this.getListener(name).on(f);
};

/**
 * Unregister listener for specific event
 * @method off
 * @param  {[type]} name name of the listener
 * @param  {[type]} id   identification number of the listener
 */
EventHandler.prototype.off = function(name, id){
    if (!this.hasListener(name)){
        return;
    }

    this.getListener(name).off(id);
}

/*
 * EVENT LISTENER
 */
/**
 * EventListener class. It holds all callbacks for specific listener.
 * @method EventListener
 */
function EventListener(){
    this.callbacks = {};

    this.$s = {
        gen: lmn.provider.get("generator").createIdGenerator()
    };
}

/**
 * Trigger event for all listener callbacks that this listener has.
 * @method trigger
 * @param  {LmnEvent} event event
 */
EventListener.prototype.trigger = function(event){
    for (var key in this.callbacks){
        if (this.callbacks.hasOwnProperty(key)){
            this.callbacks[key](event);
        }
    }
}

/**
 * Register callback for this listener.
 * @method on
 * @param  {function} f callback taht will be called when event is triggered.
 * @return {function}   function that will ungind this callback function from this listener
 */
EventListener.prototype.on = function(f){
    var id = this.$s.gen.nextId();
    this.callbacks[id] = f;

    return function(){
        this.off(id);
    }.bind(this);
}

/**
 * Unregister callback from this listener.
 * @method off
 * @param  {number} id callback identification number
 */
EventListener.prototype.off = function(id){
    delete this.callbacks[id];
}

/*
 * EVENT
 */

function LmnEvent(){

}

lmn.provider.service("eventHandler", EventHandler);

/**
 * Service Factory for all kind of generators.
 * @method Generator
 */
function Generator(){

}

/**
 * Create id generator, that will provide you incremental number id.
 * @method createIdGenerator
 * @return {IdGenerator}          new id generator instance
 */
Generator.prototype.createIdGenerator = function(){
    return new IdGenerator();
}

lmn.provider.service("generator", Generator);

/**
 * Id generator class. It produces incrementaly increasing number.
 * @method IdGenerator
 */
function IdGenerator(){
    this.id = 1;
}

/**
 * Retrieve new number that wasnt assign yet.
 * @method nextId
 * @return {number} new number
 */
IdGenerator.prototype.nextId = function(){
    id = this.id;
    this.id++;
    return id;
}

/**
 * Logger service. If you have need to use console.log, then use this logger instead.
 * @method LmnLogger
 * @param  {Object}  config configuration object
 */
function LmnLogger(config){

    if(!lmn.hasValue(config)){
        config = {DEBUG_MODE: false};
    }

    this.setDebugMode(config.DEBUG_MODE);
}

/**
 * Set if application is in debug mode. Debug mode enables debug method output.
 * @method setDebugMode
 * @param  {[type]}     mode [description]
 */
LmnLogger.prototype.setDebugMode = function(mode){
    this.debugMode = (mode === true);
}

LmnLogger.prototype.messageType = function(message){
    if (lmn.isObject(message)){
        return "%O";
    }
    return "%s";
}

/**
 * Information message
 * @method info
 * @param  {string} message console text message
 */
LmnLogger.prototype.info = function(message){
    //console.info(this.messageType(message), message);
    console.info(message);
}

/**
 * Warning message
 * @method warn
 * @param  {string} message warning message
 */
LmnLogger.prototype.warn = function(message){
    //console.warn("%c" + this.messageType(message), "color: rgb(198,116, 38)");
    console.warn(message);
}

LmnLogger.prototype.dep = function(message){
    //console.warn("%c" + this.messageType(message), "color: rgb(198,116, 38)");
    console.warn("%s", "DEPRECATED: ", message);
}

/**
 * Error message
 * @method error
 * @param  {string} message error message
 * @return {[type]}         [description]
 */
LmnLogger.prototype.error = function(message){
    //console.error("%c" + this.messageType(message), "color: rgb(212,38, 38)");
    console.error(message);
}

/**
 * Debug message. This message is not visible if debug mode is set to false
 * @method debug
 * @param  {string} message debug message
 */
LmnLogger.prototype.debug = function(message){
    if (this.debugMode){
        //console.log("%c" + this.messageType(message), "color: rgb(38, 116, 198)", message);
        console.log(message);
    }
}

lmn.provider.service("logger", LmnLogger);
