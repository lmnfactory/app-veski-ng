"use strict";

function AuthService(config) {

    this.jwtAuth = null;
    this.jwtRefresh = null;

    this.$s = {
        logger: lmn.provider.get("logger"),
        http: lmn.provider.get("http")
    };

    this.$s.http.getInterceptor().response(this.responseJWTAuth.bind(this));
    this.$s.http.getInterceptor().request(this.requestJWTAuth.bind(this));
}

AuthService.prototype.getJWTAuth = function () {
    return this.jwtAuth;
};

AuthService.prototype.getJWTRefresh = function () {
    return this.jwtRefresh;
};

AuthService.prototype.isSignedIn = function () {
    return this.jwtAuth != null;
};

AuthService.prototype.responseJWTAuth = function (response) {
    if (lmn.hasValue(response.data) && lmn.hasValue(response.data.header)) {
        var header = response.data.header;

        if (lmn.hasValue(header.JWTAuth)) {
            this.jwtAuth = header.JWTAuth;
            // TODO signin event
        }

        if (lmn.hasValue(header.JWTRefresh)) {
            this.jwtRefresh = header.JWTRefresh;
            // TODO signin event
        }

        if (header.JWTAuthClear === true) {
            this.jwtAuth = null;
            this.jwtRefresh = null;
            // TODO signout event
        }

        return response;
    }
};

AuthService.prototype.requestJWTAuth = function (config) {
    if (lmn.hasValue(this.jwtAuth)) {
        if (lmn.isUndefined(config.headers)) {
            config.headers = {};
        }

        config.headers.JWTAuth = this.jwtAuth;
    }

    return config;
};

lmn.provider.service("authService", AuthService);

lmn.run(function () {
    var authService = lmn.provider.get("authService");
});
//# sourceMappingURL=AuthService.js.map

"use strict";
//# sourceMappingURL=FacebookService.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FacebookSignIn = function (_React$Component) {
    _inherits(FacebookSignIn, _React$Component);

    function FacebookSignIn(props) {
        _classCallCheck(this, FacebookSignIn);

        var _this = _possibleConstructorReturn(this, (FacebookSignIn.__proto__ || Object.getPrototypeOf(FacebookSignIn)).call(this, props));

        _this.$s = {
            //google: lmn.provider.get("facebook"),
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = { isSignedin: false };

        //this.$s.google.auth2.isSignedIn.listen(this.updateSignState.bind(this))

        var self = _this;
        // FB.getLoginStatus(function(response) {
        //     self.$s.logger.debug({fbResponse: response});
        // });

        _this.signIn = _this.signIn.bind(_this);
        return _this;
    }

    _createClass(FacebookSignIn, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                React.createElement(
                    "button",
                    { onClick: this.signIn },
                    "Sign in [FB]"
                )
            );
        }
    }, {
        key: "updateSignState",
        value: function updateSignState(val) {
            this.setState({ isSignedIn: val });
        }
    }, {
        key: "serverSignIn",
        value: function serverSignIn(user) {
            var self = this;

            var request = self.$s.request.create("auth/signin", {
                auth: user,
                method: "facebook-oauth",
                rememberMe: true
            });

            return self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {
                    resolve(user);
                } else {
                    throw 'Authentication failed.';
                }
            });
        }
    }, {
        key: "signIn",
        value: function signIn() {
            var self = this;

            FB.login(function (response) {
                if (response.authResponse) {
                    self.serverSignIn(response.authResponse);
                }
            }, { scope: 'email' });
        }
    }]);

    return FacebookSignIn;
}(React.Component);
//# sourceMappingURL=FacebookSignin.js.map

"use strict";

function GoogleService(config) {
    this.gapi = gapi;
    this.auth2 = {};
    this.user = {};
    this.config = config;

    this.$s = {
        init: lmn.provider.get("initialization"),
        logger: lmn.provider.get("logger")
    };
}

GoogleService.prototype.init = function () {
    var self = this;
    var endInit = this.$s.init.begin();
    this.gapi.load('auth2', function () {
        var googleAuth = self.gapi.auth2.init({
            scope: self.config.SCOPE,
            client_id: self.config.CLIENT_ID,
            cookie_policy: self.config.COOKIE_POLICY
        });

        googleAuth.then(function () {
            self.auth2 = gapi.auth2.getAuthInstance();
            self.user = self.auth2.currentUser.get();

            self.$s.logger.debug({ inutUser: self.user, auth: self.auth2 });

            endInit();
        });
    });
};

lmn.provider.service("google", GoogleService);
lmn.run(function () {
    lmn.provider.get("google").init();
});
//# sourceMappingURL=GoogleService.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GoogleSignIn = function (_React$Component) {
    _inherits(GoogleSignIn, _React$Component);

    function GoogleSignIn(props) {
        _classCallCheck(this, GoogleSignIn);

        var _this = _possibleConstructorReturn(this, (GoogleSignIn.__proto__ || Object.getPrototypeOf(GoogleSignIn)).call(this, props));

        _this.$s = {
            google: lmn.provider.get("google"),
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        //this.state = {isSignedin: this.$s.google.auth2.isSignedIn.get()};

        //this.$s.google.auth2.isSignedIn.listen(this.updateSignState.bind(this))

        _this.signIn = _this.signIn.bind(_this);
        return _this;
    }

    _createClass(GoogleSignIn, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                React.createElement(
                    "button",
                    { onClick: this.signIn },
                    "Sign in [G+]"
                )
            );
        }
    }, {
        key: "updateSignState",
        value: function updateSignState(val) {
            this.$s.logger.debug({ signOut: val });
            this.setState({ isSignedIn: val });
        }
    }, {
        key: "serverSignIn",
        value: function serverSignIn(user) {
            var self = this;

            return new Promise(function (resolve) {
                var idToken = user.getAuthResponse().id_token;
                if (!idToken) {
                    throw 'Authentication failed.';
                }

                var request = self.$s.request.create("auth/signin", {
                    auth: user.getAuthResponse(),
                    method: "google-oauth2",
                    rememberMe: true
                });

                return self.$s.http.request(request).then(function (response) {
                    if (response.isOk()) {
                        resolve(user);
                    } else {
                        throw 'Authentication failed.';
                    }
                });
            });
        }
    }, {
        key: "signIn",
        value: function signIn() {
            var self = this;

            this.$s.google.auth2.signIn().then(this.serverSignIn.bind(this)).then(function (user) {
                self.$s.google.user = user;

                if (self.$s.google.user.hasGrantedScopes(self.$s.google.config.SCOPE)) {
                    self.$s.logger.debug("I have permission " + self.$s.google.config.SCOPE);
                } else {
                    self.$s.logger.debug("I DONT have permission " + self.$s.google.config.SCOPE);
                    self.$s.google.user.grant({
                        scope: self.$s.google.config.SCOPE
                    }).then(function () {
                        self.$s.logger.debug("I got permission " + self.$s.google.config.SCOPE);
                    });
                }
            });
        }
    }]);

    return GoogleSignIn;
}(React.Component);
//# sourceMappingURL=GoogleSignIn.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RefreshtokenForm = function (_React$Component) {
    _inherits(RefreshtokenForm, _React$Component);

    function RefreshtokenForm(props) {
        _classCallCheck(this, RefreshtokenForm);

        var _this = _possibleConstructorReturn(this, (RefreshtokenForm.__proto__ || Object.getPrototypeOf(RefreshtokenForm)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService"),
            authService: lmn.provider.get("authService")
        };

        _this.state = {};

        _this.refresh = _this.refresh.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        return _this;
    }

    _createClass(RefreshtokenForm, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "signupForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "submit", name: "refresh", value: "Refresh" })
            );
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            event.preventDefault();
            this.refresh();
        }
    }, {
        key: "refresh",
        value: function refresh() {
            var self = this;

            var request = self.$s.request.create("auth/refresh", {});
            request.headers({
                JWTRefresh: this.$s.authService.getJWTRefresh()
            });

            return self.$s.http.request(request).then(function (response) {

                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return RefreshtokenForm;
}(React.Component);
//# sourceMappingURL=RefreshtokenForm.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RememberMeToken = function (_React$Component) {
    _inherits(RememberMeToken, _React$Component);

    function RememberMeToken(props) {
        _classCallCheck(this, RememberMeToken);

        var _this = _possibleConstructorReturn(this, (RememberMeToken.__proto__ || Object.getPrototypeOf(RememberMeToken)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = {
            series: "",
            token: ""
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.seriesChange = _this.seriesChange.bind(_this);
        _this.tokenChange = _this.tokenChange.bind(_this);
        return _this;
    }

    _createClass(RememberMeToken, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "validateForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "series", value: this.state.series, onChange: this.seriesChange, placeholder: "series" }),
                React.createElement("input", { type: "text", name: "token", value: this.state.token, onChange: this.tokenChange, placeholder: "token" }),
                React.createElement("input", { type: "submit", name: "rmauth", value: "sign in [RMAuth]" })
            );
        }
    }, {
        key: "seriesChange",
        value: function seriesChange(event) {
            this.setState({ series: event.target.value });
        }
    }, {
        key: "tokenChange",
        value: function tokenChange(event) {
            this.setState({ token: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.send(this.state);
            event.preventDefault();
        }
    }, {
        key: "send",
        value: function send(data) {
            var self = this;

            var request = self.$s.request.create("auth/autosignin", {});
            request.headers({ RMAuth: data.series + "." + data.token });

            return self.$s.http.request(request).then(function (response) {

                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return RememberMeToken;
}(React.Component);
//# sourceMappingURL=RememberMeToken.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SigninForm = function (_React$Component) {
    _inherits(SigninForm, _React$Component);

    function SigninForm(props) {
        _classCallCheck(this, SigninForm);

        var _this = _possibleConstructorReturn(this, (SigninForm.__proto__ || Object.getPrototypeOf(SigninForm)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = {
            email: "",
            password: "",
            rememberMe: false
        };

        _this.signIn = _this.signIn.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.emailChange = _this.emailChange.bind(_this);
        _this.passwordChange = _this.passwordChange.bind(_this);
        _this.rememberMeChange = _this.rememberMeChange.bind(_this);
        return _this;
    }

    _createClass(SigninForm, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "signupForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "email", name: "email", value: this.state.email, onChange: this.emailChange, placeholder: "email" }),
                React.createElement("input", { type: "password", name: "password", value: this.state.password, onChange: this.passwordChange, placeholder: "password" }),
                React.createElement("input", { type: "checkbox", name: "rememberMe", checked: this.state.rememberMe,
                    onChange: this.rememberMeChange }),
                React.createElement("input", { type: "submit", name: "signin", value: "Sign in" })
            );
        }
    }, {
        key: "emailChange",
        value: function emailChange(event) {
            this.setState({ email: event.target.value });
        }
    }, {
        key: "passwordChange",
        value: function passwordChange(event) {
            this.setState({ password: event.target.value });
        }
    }, {
        key: "rememberMeChange",
        value: function rememberMeChange(event) {
            this.setState({ rememberMe: event.target.checked });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.signIn(this.state);
            event.preventDefault();
        }
    }, {
        key: "signIn",
        value: function signIn(user) {
            var self = this;

            var request = self.$s.request.create("auth/signin", {
                auth: user,
                method: "lmn-auth",
                rememberMe: user.rememberMe
            });

            return self.$s.http.request(request).then(function (response) {

                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return SigninForm;
}(React.Component);
//# sourceMappingURL=SigninForm.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SignoutForm = function (_React$Component) {
    _inherits(SignoutForm, _React$Component);

    function SignoutForm(props) {
        _classCallCheck(this, SignoutForm);

        var _this = _possibleConstructorReturn(this, (SignoutForm.__proto__ || Object.getPrototypeOf(SignoutForm)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.signOut = _this.signOut.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        return _this;
    }

    _createClass(SignoutForm, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "signupForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "submit", name: "signout", value: "Sign out" })
            );
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.signOut();
            event.preventDefault();
        }
    }, {
        key: "signOut",
        value: function signOut() {
            var self = this;

            var request = self.$s.request.create("auth/signout", {});

            return self.$s.http.request(request).then(function (response) {

                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return SignoutForm;
}(React.Component);
//# sourceMappingURL=SignoutForm.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SignupForm = function (_React$Component) {
    _inherits(SignupForm, _React$Component);

    function SignupForm(props) {
        _classCallCheck(this, SignupForm);

        var _this = _possibleConstructorReturn(this, (SignupForm.__proto__ || Object.getPrototypeOf(SignupForm)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = {
            email: "",
            name: "",
            surname: "",
            password: ""
        };

        //this.signUp = this.signUp.bind(this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.nameChange = _this.nameChange.bind(_this);
        _this.surnameChange = _this.surnameChange.bind(_this);
        _this.emailChange = _this.emailChange.bind(_this);
        _this.passwordChange = _this.passwordChange.bind(_this);
        return _this;
    }

    _createClass(SignupForm, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "signupForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "name", value: this.state.name, onChange: this.nameChange, placeholder: "Name" }),
                React.createElement("input", { type: "text", name: "surname", value: this.state.surname, onChange: this.surnameChange, placeholder: "Surname" }),
                React.createElement("input", { type: "email", name: "email", value: this.state.email, onChange: this.emailChange, placeholder: "Email" }),
                React.createElement("input", { type: "password", name: "password", value: this.state.password, onChange: this.passwordChange, placeholder: "Password" }),
                React.createElement("input", { type: "submit", name: "submit", value: "Sign up" })
            );
        }
    }, {
        key: "nameChange",
        value: function nameChange(event) {
            this.setState({ name: event.target.value });
        }
    }, {
        key: "surnameChange",
        value: function surnameChange(event) {
            this.setState({ surname: event.target.value });
        }
    }, {
        key: "emailChange",
        value: function emailChange(event) {
            this.setState({ email: event.target.value });
        }
    }, {
        key: "passwordChange",
        value: function passwordChange(event) {
            this.setState({ password: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.signUp(this.state);
            event.preventDefault();
        }
    }, {
        key: "signUp",
        value: function signUp(user) {
            var self = this;

            var request = self.$s.request.create("auth/signup", user);

            return self.$s.http.request(request).then(function (response) {

                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return SignupForm;
}(React.Component);
//# sourceMappingURL=SignupForm.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ValidateForm = function (_React$Component) {
    _inherits(ValidateForm, _React$Component);

    function ValidateForm(props) {
        _classCallCheck(this, ValidateForm);

        var _this = _possibleConstructorReturn(this, (ValidateForm.__proto__ || Object.getPrototypeOf(ValidateForm)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = {
            email: "",
            token: ""
        };

        _this.validate = _this.validate.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.emailChange = _this.emailChange.bind(_this);
        _this.tokenChange = _this.tokenChange.bind(_this);
        return _this;
    }

    _createClass(ValidateForm, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "validateForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "email", name: "email", value: this.state.email, onChange: this.emailChange, placeholder: "email" }),
                React.createElement("input", { type: "text", name: "token", value: this.state.token, onChange: this.tokenChange, placeholder: "token" }),
                React.createElement("input", { type: "submit", name: "validate", value: "Validate" })
            );
        }
    }, {
        key: "emailChange",
        value: function emailChange(event) {
            this.setState({ email: event.target.value });
        }
    }, {
        key: "tokenChange",
        value: function tokenChange(event) {
            this.setState({ token: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.validate(this.state);
            event.preventDefault();
        }
    }, {
        key: "validate",
        value: function validate(data) {
            var self = this;

            var request = self.$s.request.create("auth/validate", { email: data.email, token: data.token });

            return self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return ValidateForm;
}(React.Component);
//# sourceMappingURL=ValidateForm.js.map

"use strict";

function User() {}

User.prototype.set = function () {};

User.prototype.rollback = function () {};
//# sourceMappingURL=User.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AuthAction = function (_React$Component) {
    _inherits(AuthAction, _React$Component);

    function AuthAction(props) {
        _classCallCheck(this, AuthAction);

        var _this = _possibleConstructorReturn(this, (AuthAction.__proto__ || Object.getPrototypeOf(AuthAction)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.authAction = _this.authAction.bind(_this);
        _this.unauthAction = _this.unauthAction.bind(_this);
        return _this;
    }

    _createClass(AuthAction, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "signupForm" },
                React.createElement("input", { type: "button", name: "signout", value: "Auth action", onClick: this.authAction }),
                React.createElement("input", { type: "button", name: "signout", value: "Unauth action", onClick: this.unauthAction })
            );
        }
    }, {
        key: "authAction",
        value: function authAction() {
            var self = this;

            var request = self.$s.request.create("test/auth/authAction", {});

            return self.$s.http.request(request).then(function (response) {

                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }, {
        key: "unauthAction",
        value: function unauthAction() {
            var self = this;

            var request = self.$s.request.create("test/auth/unauthAction", {});

            return self.$s.http.request(request).then(function (response) {

                if (response.isOk()) {} else {
                    throw 'Authentication failed.';
                }
            });
        }
    }]);

    return AuthAction;
}(React.Component);
//# sourceMappingURL=AuthAction.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UpdateProfile = function (_React$Component) {
    _inherits(UpdateProfile, _React$Component);

    function UpdateProfile(props) {
        _classCallCheck(this, UpdateProfile);

        var _this = _possibleConstructorReturn(this, (UpdateProfile.__proto__ || Object.getPrototypeOf(UpdateProfile)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = {
            account: "",
            data: {
                usersettings: {
                    faculty_id: null,
                    degree_id: null,
                    studyyear: null,
                    gender: null,
                    birthday: null
                }
            }
        };

        _this.updateProfile = _this.updateProfile.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.accountChange = _this.accountChange.bind(_this);
        _this.facultyIdChange = _this.facultyIdChange.bind(_this);
        _this.degreeIdChange = _this.degreeIdChange.bind(_this);
        _this.studyyearChange = _this.studyyearChange.bind(_this);
        _this.genderChange = _this.genderChange.bind(_this);
        _this.birthdayChange = _this.birthdayChange.bind(_this);
        return _this;
    }

    _createClass(UpdateProfile, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "profileForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "account", placeholder: "account", onChange: this.accountChange, value: this.state.account }),
                React.createElement("input", { type: "text", name: "faculty_id", placeholder: "faculty id", onChange: this.facultyIdChange, value: this.state.data.usersettings.faculty_id }),
                React.createElement("input", { type: "text", name: "degree_id", placeholder: "degree id", onChange: this.degreeIdChange, value: this.state.data.usersettings.degree_id }),
                React.createElement("input", { type: "text", name: "studyyear", placeholder: "studyyear", onChange: this.studyyearChange, value: this.state.data.usersettings.studyyear }),
                React.createElement("input", { type: "text", name: "gender", placeholder: "gender", onChange: this.genderChange, value: this.state.data.usersettings.gender }),
                React.createElement("input", { type: "text", name: "birthday", placeholder: "birthday", onChange: this.birthdayChange, value: this.state.data.usersettings.birthday }),
                React.createElement("input", { type: "submit", name: "profile", value: "Update Profile" })
            );
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            event.preventDefault();
            this.updateProfile(this.state);
        }
    }, {
        key: "accountChange",
        value: function accountChange(event) {
            this.setState({ account: event.target.value });
        }
    }, {
        key: "facultyIdChange",
        value: function facultyIdChange(event) {
            var state = this.state;
            state.data.usersettings.faculty_id = event.target.value;
            this.setState(state);
        }
    }, {
        key: "degreeIdChange",
        value: function degreeIdChange(event) {
            var state = this.state;
            state.data.usersettings.degree_id = event.target.value;
            this.setState(state);
        }
    }, {
        key: "studyyearChange",
        value: function studyyearChange(event) {
            var state = this.state;
            state.data.usersettings.studyyear = event.target.value;
            this.setState(state);
        }
    }, {
        key: "genderChange",
        value: function genderChange(event) {
            var state = this.state;
            state.data.usersettings.gender = event.target.value;
            this.setState(state);
        }
    }, {
        key: "birthdayChange",
        value: function birthdayChange(event) {
            var state = this.state;
            state.data.usersettings.birthday = event.target.value;
            this.setState(state);
        }
    }, {
        key: "updateProfile",
        value: function updateProfile(state) {
            var self = this;

            var request = self.$s.request.create("user/update", state);

            return self.$s.http.request(request).then(function (response) {});
        }
    }]);

    return UpdateProfile;
}(React.Component);
//# sourceMappingURL=UpdateProfile.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserProfile = function (_React$Component) {
    _inherits(UserProfile, _React$Component);

    function UserProfile(props) {
        _classCallCheck(this, UserProfile);

        var _this = _possibleConstructorReturn(this, (UserProfile.__proto__ || Object.getPrototypeOf(UserProfile)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = {
            publicId: ""
        };

        _this.profile = _this.profile.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.accountChange = _this.accountChange.bind(_this);
        return _this;
    }

    _createClass(UserProfile, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "profileForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "account", onChange: this.accountChange, value: this.state.account }),
                React.createElement("input", { type: "submit", name: "profile", value: "Profile" })
            );
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.profile(this.state);
            event.preventDefault();
        }
    }, {
        key: "accountChange",
        value: function accountChange(event) {
            this.setState({ account: event.target.value });
        }
    }, {
        key: "profile",
        value: function profile(state) {
            var self = this;

            var request = self.$s.request.create("user/privateProfile", state);

            return self.$s.http.request(request).then(function (response) {});
        }
    }]);

    return UserProfile;
}(React.Component);
//# sourceMappingURL=UserProfile.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserPublicProfile = function (_React$Component) {
    _inherits(UserPublicProfile, _React$Component);

    function UserPublicProfile(props) {
        _classCallCheck(this, UserPublicProfile);

        var _this = _possibleConstructorReturn(this, (UserPublicProfile.__proto__ || Object.getPrototypeOf(UserPublicProfile)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        _this.state = {
            publicId: ""
        };

        _this.profile = _this.profile.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.accountChange = _this.accountChange.bind(_this);
        return _this;
    }

    _createClass(UserPublicProfile, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { noValidate: true, name: "profileForm", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "account", onChange: this.accountChange, value: this.state.account }),
                React.createElement("input", { type: "submit", name: "profile", value: "Public Profile" })
            );
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.profile(this.state);
            event.preventDefault();
        }
    }, {
        key: "accountChange",
        value: function accountChange(event) {
            this.setState({ account: event.target.value });
        }
    }, {
        key: "profile",
        value: function profile(state) {
            var self = this;

            var request = self.$s.request.create("user/publicProfile", state);

            return self.$s.http.request(request).then(function (response) {});
        }
    }]);

    return UserPublicProfile;
}(React.Component);
//# sourceMappingURL=UserPublicProfile.js.map
