'use strict';

lmn.google = {
    gapi: gapi,
    auth2: {},
    user: {}
};

lmn.google.gapi.load('auth2', function () {
    lmn.google.gapi.auth2.init();
    lmn.google.auth2 = gapi.auth2.getAuthInstance();
});
//# sourceMappingURL=_first.js.map
