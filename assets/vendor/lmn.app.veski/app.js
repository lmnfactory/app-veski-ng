"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var WebsocketConnection = function (_React$Component) {
    _inherits(WebsocketConnection, _React$Component);

    function WebsocketConnection(props) {
        _classCallCheck(this, WebsocketConnection);

        var _this = _possibleConstructorReturn(this, (WebsocketConnection.__proto__ || Object.getPrototypeOf(WebsocketConnection)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService"),
            eventHandler: lmn.provider.get('eventHandler'),
            notificationService: lmn.provider.get('notificationService'),
            authService: lmn.provider.get('authService')
        };

        _this.events = [];

        _this.connect = _this.connect.bind(_this);
        _this.notify = _this.notify.bind(_this);
        return _this;
    }

    _createClass(WebsocketConnection, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                React.createElement("input", { type: "button", name: "connect", value: "Connect websocket", onClick: this.connect }),
                React.createElement("input", { type: "button", name: "notify", value: "Notify all", onClick: this.notify })
            );
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            for (var i = 0; i < this.events.length; i++) {
                this.events[i]();
            }
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {
            var self = this;

            var e = this.$s.eventHandler.on("notification.recieve", function (notificationEvent) {
                self.$s.logger.debug({ notification: notificationEvent.getData() });
            });
            this.events.push(e);
        }
    }, {
        key: "connect",
        value: function connect() {
            if (this.$s.authService.isSignedIn()) {
                this.$s.notificationService.auth(this.$s.authService.getJWTAuth());
            }
            // else {
            //     var e = this.$e.eventHandler.on("auth.signin", function(user) {
            //
            //     })
            // }
        }
    }, {
        key: "notify",
        value: function notify() {
            var self = this;

            var request = self.$s.request.create("notification", {});

            return self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {} else {}
            });
        }
    }]);

    return WebsocketConnection;
}(React.Component);
//# sourceMappingURL=WebsocketConnection.js.map

'use strict';

lmn.provider.value("APP", {
    DOMAIN: 'faceschool.local',
    WEBSOCKET: 'ws://127.0.0.1:5510'
});
//# sourceMappingURL=APP.js.map

"use strict";

lmn.provider.value("ENVIROMENT", {
    DEBUG_MODE: true
});
//# sourceMappingURL=ENVIROMENT.js.map

"use strict";

lmn.provider.value("GOOGLE_CONFIG", {
    CLIENT_ID: "961742008840-edm8q7f4pchb80p1q8466ffjq8sg98rk.apps.googleusercontent.com",
    PROJECT_ID: "faceschool-149618",
    COOKIE_POLICY: "single_host_origin",
    SCOPE: "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile openid profile email",
    AUTH_URI: "https://accounts.google.com/o/oauth2/auth",
    TOKEN_URI: "https://accounts.google.com/o/oauth2/token",
    AUTH_PROVIDER_X509_CER_URL: "https://www.googleapis.com/oauth2/v1/certs",
    CLIENT_SECRET: "DRWHIU08azJ6HAqF4Mfzhehm"
});
//# sourceMappingURL=GOOGLE_CONFIG.js.map

"use strict";

lmn.provider.config("logger", lmn.provider.get("ENVIROMENT"));
lmn.provider.config("google", lmn.provider.get("GOOGLE_CONFIG"));

lmn.run(function () {
    var notificationService = lmn.provider.get('notificationService');
    var websocketService = lmn.provider.get('websocketService');
    var appConfig = lmn.provider.get('APP');

    notificationService.setSocketAddr(appConfig.WEBSOCKET);
    notificationService.start();
});
//# sourceMappingURL=provider.js.map

"use strict";

ReactDOM.render(React.createElement(
    LmnInitializationComponent,
    null,
    React.createElement(GoogleSignIn, { googleService: lmn.provider.get("google") }),
    React.createElement(FacebookSignIn, null),
    React.createElement(SignupForm, null),
    React.createElement(SigninForm, null),
    React.createElement(ValidateForm, null),
    React.createElement(RememberMeToken, null),
    React.createElement(SignoutForm, null),
    React.createElement(RefreshtokenForm, null),
    React.createElement(AuthAction, null),
    React.createElement(WebsocketConnection, null),
    React.createElement("hr", null),
    React.createElement(UserProfile, null),
    React.createElement(UserPublicProfile, null),
    React.createElement(UpdateProfile, null),
    React.createElement("hr", null),
    React.createElement(SubjectSearch, null),
    React.createElement(SubjectForm, null),
    React.createElement("hr", null),
    React.createElement(ThreadList, null),
    React.createElement(ThreadDetail, null),
    React.createElement(ThreadCreate, null),
    React.createElement(EntryCreate, null),
    React.createElement("hr", null),
    React.createElement(UserCalendareventForm, null),
    React.createElement(UserCalendareventList, null)
), document.getElementById('root'));
//# sourceMappingURL=Root.js.map
