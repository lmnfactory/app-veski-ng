"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EntryCreate = function (_React$Component) {
    _inherits(EntryCreate, _React$Component);

    function EntryCreate(props) {
        _classCallCheck(this, EntryCreate);

        var _this = _possibleConstructorReturn(this, (EntryCreate.__proto__ || Object.getPrototypeOf(EntryCreate)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService"),
            filter: lmn.provider.get('filterService')
        };

        var self = _this;

        _this.state = {
            thread_id: "",
            text: ""
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.save = _this.save.bind(_this);
        _this.threadIdChange = _this.threadIdChange.bind(_this);
        _this.textChange = _this.textChange.bind(_this);
        return _this;
    }

    _createClass(EntryCreate, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "threadCreate", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "threadId", value: this.state.thread_id, onChange: this.threadIdChange, placeholder: "thread ID" }),
                React.createElement("input", { type: "text", name: "text", value: this.state.text, onChange: this.textChange, placeholder: "zadaj text" }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Response to thraed"
                )
            );
        }
    }, {
        key: "threadIdChange",
        value: function threadIdChange(event) {
            this.setState({ thread_id: event.target.value });
        }
    }, {
        key: "textChange",
        value: function textChange(event) {
            this.setState({ text: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.save(this.state);
            event.preventDefault();
        }
    }, {
        key: "save",
        value: function save(state) {
            var self = this;

            var request = self.$s.request.create("api/entry/create", state);

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {}
            });
        }
    }]);

    return EntryCreate;
}(React.Component);
//# sourceMappingURL=EntryCreate.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ThreadCreate = function (_React$Component) {
    _inherits(ThreadCreate, _React$Component);

    function ThreadCreate(props) {
        _classCallCheck(this, ThreadCreate);

        var _this = _possibleConstructorReturn(this, (ThreadCreate.__proto__ || Object.getPrototypeOf(ThreadCreate)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService"),
            filter: lmn.provider.get('filterService')
        };

        var self = _this;

        _this.state = {
            subject_id: "",
            title: "",
            entry: {
                text: ""
            },
            tags: []
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.save = _this.save.bind(_this);
        _this.subjectIdChange = _this.subjectIdChange.bind(_this);
        _this.titleChange = _this.titleChange.bind(_this);
        _this.textChange = _this.textChange.bind(_this);
        return _this;
    }

    _createClass(ThreadCreate, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "threadCreate", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "threadId", value: this.state.subject_id, onChange: this.subjectIdChange, placeholder: "subject ID" }),
                React.createElement("input", { type: "text", name: "title", value: this.state.title, onChange: this.titleChange, placeholder: "Bez nazvu" }),
                React.createElement("input", { type: "text", name: "text", value: this.state.entry.text, onChange: this.textChange, placeholder: "zadaj text" }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Thread create"
                )
            );
        }
    }, {
        key: "subjectIdChange",
        value: function subjectIdChange(event) {
            this.setState({ subject_id: event.target.value });
        }
    }, {
        key: "titleChange",
        value: function titleChange(event) {
            this.setState({ title: event.target.value });
        }
    }, {
        key: "textChange",
        value: function textChange(event) {
            this.setState({ entry: { text: event.target.value } });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.save(this.state);
            event.preventDefault();
        }
    }, {
        key: "save",
        value: function save(state) {
            var self = this;

            var request = self.$s.request.create("api/thread/create", state);

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {}
            });
        }
    }]);

    return ThreadCreate;
}(React.Component);
//# sourceMappingURL=ThreadCreate.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ThreadDetail = function (_React$Component) {
    _inherits(ThreadDetail, _React$Component);

    function ThreadDetail(props) {
        _classCallCheck(this, ThreadDetail);

        var _this = _possibleConstructorReturn(this, (ThreadDetail.__proto__ || Object.getPrototypeOf(ThreadDetail)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService"),
            filter: lmn.provider.get('filterService')
        };

        var self = _this;

        _this.state = {
            threadId: ""
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.getDetail = _this.getDetail.bind(_this);
        _this.threadIdChange = _this.threadIdChange.bind(_this);
        return _this;
    }

    _createClass(ThreadDetail, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "threadDetail", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "threadId", value: this.state.threadId, onChange: this.threadIdChange, placeholder: "thread ID" }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Thread detail"
                )
            );
        }
    }, {
        key: "threadIdChange",
        value: function threadIdChange(event) {
            this.setState({ threadId: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.getDetail();
            event.preventDefault();
        }
    }, {
        key: "getDetail",
        value: function getDetail() {
            var self = this;

            var request = self.$s.request.create("api/thread/detail", this.state);

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {}
            });
        }
    }]);

    return ThreadDetail;
}(React.Component);
//# sourceMappingURL=ThreadDetail.js.map

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ThreadList = function (_React$Component) {
    _inherits(ThreadList, _React$Component);

    function ThreadList(props) {
        _classCallCheck(this, ThreadList);

        var _this = _possibleConstructorReturn(this, (ThreadList.__proto__ || Object.getPrototypeOf(ThreadList)).call(this, props));

        _this.$s = {
            logger: lmn.provider.get("logger"),
            http: lmn.provider.get("http"),
            request: lmn.provider.get("requestService")
        };

        var self = _this;

        _this.state = {
            subjectId: ""
        };

        _this.handleSubmit = _this.handleSubmit.bind(_this);
        _this.getList = _this.getList.bind(_this);
        _this.subjectIdChange = _this.subjectIdChange.bind(_this);
        return _this;
    }

    _createClass(ThreadList, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "form",
                { name: "", onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", name: "subjectId", value: this.state.subjectId, onChange: this.subjectIdChange, placeholder: "subejct ID" }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Thread list"
                )
            );
        }
    }, {
        key: "subjectIdChange",
        value: function subjectIdChange(event) {
            this.setState({ subjectId: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            this.getList();
            event.preventDefault();
        }
    }, {
        key: "getList",
        value: function getList() {
            var self = this;

            var request = self.$s.request.create("api/thread/list", this.state);

            self.$s.http.request(request).then(function (response) {
                if (response.isOk()) {}
            });
        }
    }]);

    return ThreadList;
}(React.Component);
//# sourceMappingURL=ThreadList.js.map
