var signinForm;

function signinSuccess(response) {
    var header = response.header;
    var option = response.option;

    var rm = header.RMAuth;
    if (!rm) {
        rm = null;
    }

    Cookies.set('JWTRefresh', header.JWTRefresh);
    Cookies.set('RMAuth', rm, {expires: 30});

    window.location.replace('/apka');
}

function signin(event) {
    event.preventDefault();
    signinForm.submit();
    
    if (signinForm.hasErrors()) {
        return false;
    }

    var rememberMe = $("[name='rememberme']").is(":checked");

    var requestData = {
        auth: signinForm.getValues(),
        method: 'lmn-auth',
        rememberMe: rememberMe
    };

    $.ajax({
        type: "POST",
        url: "auth/signin",
        data: JSON.stringify(requestData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: signinSuccess,
        error: function(errMsg) {
            var response = errMsg.responseJSON;
            var data = response.data;
            for (var key in data) {
                if (!data.hasOwnProperty(key)) {
                    continue;
                }

                signinForm.serverError(key, data[key].message);
            }
            signinForm.render();
        }
    });

    return false;
}

$(document).ready(function() {
    signinForm = new Form(
        ['email', 'password'],
        {
            email: ['required', 'email'],
            password: ['required']
        });
});
