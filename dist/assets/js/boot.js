app.validation = new Validation();

app.validation.addRule('required', function(data, value, args = []) {
    return (value != undefined && value != null && value != "");
});
app.validation.addRule('checked', function(data, value, args = []) {
    console.log("CHECKED");
    console.log(value);
    return (value != undefined && value != null && value != "" && value !== false);
});
app.validation.addRule('maxLength', function(data, value, args = []) {
    if (args.length == 0) {
        console.log("Max length validation rule needs one argument");
        return false;
    }
    return (value.length <= args[0]);
});
app.validation.addRule('minLength', function(data, value, args = []) {
    if (args.length == 0) {
        console.log("Min length validation rule needs one argument");
        return false;
    }
    return (value.length >= args[0]);
});
app.validation.addRule('email', function(data, value, args = []) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value);
});