var Form = function(inputs, rules) {
    this.inputs = inputs;
    this.values = {};
    this.rules = rules;
    this.errors = false;
    this.submited = false;
    this.refresh();

    for (var i = 0; i < this.inputs.length; i++) {
        var key = this.inputs[i];
        $('[name="' + key + '"]').on('input', this.validate.bind(this));
    }
}

Form.prototype.getValues = function() {
    return this.values;
}

Form.prototype.hasErrors = function() {
    return (this.errors !== false);
}

Form.prototype.refresh = function() {
    this.values = {};
    for (var i = 0; i < this.inputs.length; i++) {
        var key = this.inputs[i];
        var inputElement = $("[name='" + key + "']");
        if (inputElement.attr('type') == 'checkbox') {
            this.values[key] = inputElement.is(':checked');
        } else {
            this.values[key] = inputElement.val();
        }
    }
}

Form.prototype.submit = function() {
    this.submited = true;
    this.validate();
}

Form.prototype.validate = function() {
    this.refresh();
    this.errors = false;
    if (this.submited) {
        this.errors = app.validation.validate(this.values, this.rules);
    }

    this.render();
}

Form.prototype.serverError = function(key, error) {
    this.addError(key, "server");
    $('[validation="' + key + '"] [val-message="' + this.errors[key] + '"]').text(error);
}

Form.prototype.addError = function(key, error) {
    if (this.errors === false) {
        this.errors = {};
    }
    this.errors[key] = error;
}

Form.prototype.resetErrors = function() {
    for (var i = 0; i < this.inputs.length; i++) {
        var key = this.inputs[i];

        $('[validation="' + key + '"]').removeClass('validation-error');
        $('[validation="' + key + '"] .show').removeClass('show');
    }
}

Form.prototype.render = function() {
    this.resetErrors();

    for (var key in this.errors) {
        
        if (!this.errors.hasOwnProperty(key)) {
            continue;
        }
        
        $('[validation="' + key + '"]').addClass('validation-error');
        $('[validation="' + key + '"] [val-message="' + this.errors[key] + '"]').addClass('show');
    }
}